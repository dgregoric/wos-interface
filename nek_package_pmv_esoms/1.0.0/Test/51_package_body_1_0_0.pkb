CREATE OR REPLACE PACKAGE BODY NEK_PACKAGE_PMV_ESOMS AS
/******************************************************************************
   NAME:       NEK_PACKAGE_PMV_ESOMS
   PURPOSE:  Interfacing with Primavera utility

   1.0.0: 14.8.2013:M. Clemente: Initial version per doc 1.13 specification
******************************************************************************/

PROCEDURE START_PRIMAVERA_SYNC IS
  /********************************************************************************
  /   START_PRIMAVERA_SYNC - Starts the primavera time windows sync.
  /********************************************************************************/           
      PV_TOS_ NEK_PROJ_NAME_TYPE;
      PV_TIME_WINDOWS_ NEK_PROJ_NAME_TYPE;
      TO_PROJ_ID_ VARCHAR2(50);
      TO_NAME_ VARCHAR2(50);
  BEGIN       
       PV_TOS_ :=  RETURN_PROJECT_TOS;
       IF (PV_TOS_.LAST > 0) THEN
            -- CLEAN OLD VALUES 
           FOR i IN PV_TOS_.FIRST..PV_TOS_.LAST
           LOOP
            TO_NAME_ := PV_TOS_(i);
            TO_PROJ_ID_ := RETURN_TO_PROJ_ID(TO_NAME_);
            IF (LENGTH(TO_PROJ_ID_) > 0) THEN
                CLEAN_VALUES(TO_PROJ_ID_);
            END IF;
           END LOOP;
       
           -- INSERT NEW VALUES
           FOR i IN PV_TOS_.FIRST..PV_TOS_.LAST
           LOOP
            TO_NAME_ := PV_TOS_(i);
            TO_PROJ_ID_ := RETURN_TO_PROJ_ID(TO_NAME_);
            IF (LENGTH(TO_PROJ_ID_) > 0) THEN
                PV_TIME_WINDOWS_ := RETURN_TO_WINDOWS(TO_NAME_);
                ADD_WINDOWS_TO_PROJECT(PV_TIME_WINDOWS_, TO_PROJ_ID_);
            END IF;
           END LOOP;
       END IF;
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'START_PRIMAVERA_SYNC');
  END START_PRIMAVERA_SYNC;

PROCEDURE CLEAN_VALUES(TO_PROJ_ID_ IN VARCHAR2) IS
  /********************************************************************************
  /   CLEAN_VALUES - Clears values for PV attribute on certain project
  /********************************************************************************/ 
    ATTR_NUM_ NUMBER;      
  BEGIN
    ATTR_NUM_ := GET_ATTRIBUTE_NUMBER(TO_PROJ_ID_);
    
    DELETE FROM VALID_SECTION_ATTRIBUTES 
    WHERE TAGOUT_TYPE_ID = TO_PROJ_ID_ 
    AND ATTRIBUTE_NUMBER = ATTR_NUM_;
      
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'CLEAN_VALUES');
  END CLEAN_VALUES;  

PROCEDURE ADD_WINDOWS_TO_PROJECT(WINDOWS_ IN NEK_PROJ_NAME_TYPE, TO_PROJ_ID_ IN VARCHAR2) IS
  /********************************************************************************
  /   ADD_WINDOWS_TO_PROJECT - Adds PV Windows to Project
  /********************************************************************************/ 
    ATTR_NUM_ NUMBER;
    WINDOW_ VARCHAR2(40);               
  BEGIN
    ATTR_NUM_ := GET_ATTRIBUTE_NUMBER(TO_PROJ_ID_);
    
    IF (WINDOWS_.LAST > 0) THEN
        FOR i IN WINDOWS_.FIRST..WINDOWS_.LAST
        LOOP
            WINDOW_ := WINDOWS_(i);
            INSERT INTO VALID_SECTION_ATTRIBUTES 
                (TAGOUT_TYPE_ID, ATTRIBUTE_NUMBER, VALID_ATTRIBUTE_VALUE) VALUES 
                (TO_PROJ_ID_, ATTR_NUM_, WINDOW_);
        END LOOP;    
    END IF;
      
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'ADD_WINDOWS_TO_PROJECT');
  END ADD_WINDOWS_TO_PROJECT;  
  
FUNCTION GET_ATTRIBUTE_NUMBER(TO_PROJ_ID_ IN VARCHAR2) RETURN NUMBER IS
  /********************************************************************************
  /   GET_ATTRIBUTE_NUMBER - Returns the number of the attribute on the project
  /********************************************************************************/           
  CURSOR getAttributeNum IS 
    SELECT ATTRIBUTE_NUMBER 
    FROM 
        SECTION_ATTRIBUTES_ALLOWED 
    WHERE 
        TAGOUT_TYPE_ID = TO_PROJ_ID_
        AND ATTRIBUTE_DESCRIPTION = PMV_ATTRIBUTE_NAME;
   
   PREV_ATTR_ID_ NUMBER;     
   ATTR_ID_ NUMBER;
  BEGIN
    OPEN getAttributeNum;
    FETCH getAttributeNum INTO ATTR_ID_;
   
    IF getAttributeNum%NOTFOUND THEN
        SELECT MAX(ATTRIBUTE_NUMBER) INTO PREV_ATTR_ID_
        FROM SECTION_ATTRIBUTES_ALLOWED
        WHERE TAGOUT_TYPE_ID = TO_PROJ_ID_;
        
        ATTR_ID_ := PREV_ATTR_ID_ + 1;
        
        INSERT INTO SECTION_ATTRIBUTES_ALLOWED (
            TAGOUT_TYPE_ID, ATTRIBUTE_NUMBER, ATTRIBUTE_DESCRIPTION, 
            ATTRIBUTE_IS_VALIDATED, ATTRIBUTE_IS_REQUIRED, ATTRIBUTE_TYPE, ATTRIBUTE_ACTIVE, ATTRIBUTE_SORT_ORDER, 
            COPY_ATTRIBUTE) VALUES (
            TO_PROJ_ID_, ATTR_ID_, PMV_ATTRIBUTE_NAME, 1, 0, 'S', 1, ATTR_ID_, 
            1);
         
        RETURN ATTR_ID_; 
    ELSE
        RETURN ATTR_ID_;
    END IF;
   
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'GET_ATTRIBUTE_NUMBER');
      RETURN -1;
  END GET_ATTRIBUTE_NUMBER;    
  
FUNCTION RETURN_TO_WINDOWS(TO_NAME_ IN VARCHAR2) RETURN NEK_PROJ_NAME_TYPE IS
  /********************************************************************************
  /   RETURN_TO_WINDOWS - Returns the PV windows for a TO
  /********************************************************************************/           
      TO_WINDOWS_ NEK_PROJ_NAME_TYPE;
      
  BEGIN
        SELECT 
               --SUBSTR (task_code, 3, 4) || '     [ ' || TO_CHAR (target_start_date, 'DD.MM.YYYY HH24:MI:SS') || ' - ' || TO_CHAR (target_end_date, 'DD.MM.YYYY HH24:MI:SS') || ' (' || task_id || ') ]' bulk collect into TO_WINDOWS_
               SUBSTR (task_code, 3, 4) bulk collect into TO_WINDOWS_       
        FROM XXEA_PRIMAVERA_IMPORT 
        WHERE     
            LENGTH (proj_short_name) = 4
            AND SUBSTR (task_code, 3, 4) NOT IN
                (SELECT LOOKUP_CODE
                FROM FND_LOOKUP_VALUES_VL
                WHERE LOOKUP_TYPE = 'XXEA_SYS_WIN_PROCESS_EXCL_LKP') 
            AND (task_code LIKE '%W' OR task_code LIKE '%Y' OR task_code LIKE '%U')
            AND SUBSTR (proj_short_name, 1, 2) IN
                (SELECT LOOKUP_CODE
                FROM FND_LOOKUP_VALUES_VL
                WHERE LOOKUP_TYPE = 'XXEA_CYCLE_TYPE_LKP') 
            AND target_end_date > SYSDATE
            AND '1-' || proj_short_name =  TO_NAME_ 
        ORDER BY SUBSTR (task_code, 3, 4) ASC;
                      
    RETURN TO_WINDOWS_;
    
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'RETURN_TO_WINDOWS');
      RETURN NULL;
  END RETURN_TO_WINDOWS;  

FUNCTION RETURN_TO_PROJ_ID(TO_ IN VARCHAR2) RETURN VARCHAR2 IS
  /********************************************************************************
  /   RETURN_TO_PROJ_ID - Returns the project ID for a TO
  /********************************************************************************/                 
  PROJ_ID_ VARCHAR2(50);
  
  CURSOR getProjId IS 
    SELECT DISTINCT (TAGOUT_TYPE_ID)
    FROM ACT_TAGOUT_SECTIONS
    WHERE
        ACT_TAGOUT_NUMBER = TO_
        AND ARCHIVED != 1;
  
  BEGIN
    OPEN getProjId;
    FETCH getProjId INTO PROJ_ID_;
    IF getProjId%NOTFOUND THEN
        PROJ_ID_ := '';
    END IF;
                          
    RETURN PROJ_ID_;
    
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'RETURN_TO_PROJ_ID');
      RETURN NULL;
  END RETURN_TO_PROJ_ID;

FUNCTION RETURN_PROJECT_TOS RETURN NEK_PROJ_NAME_TYPE IS
  /********************************************************************************
  /   RETURN_PROJECT_IDS - Returns the project IDs for all PV windows
  /********************************************************************************/           
      PROJ_NAMES NEK_PROJ_NAME_TYPE;
      
  BEGIN
    SELECT DISTINCT('1-' || proj_short_name) bulk collect into PROJ_NAMES
    FROM XXEA_PRIMAVERA_IMPORT 
    WHERE     
        LENGTH (proj_short_name) = 4
        AND SUBSTR (task_code, 3, 4) NOT IN
            (SELECT LOOKUP_CODE
            FROM FND_LOOKUP_VALUES_VL
            WHERE LOOKUP_TYPE = 'XXEA_SYS_WIN_PROCESS_EXCL_LKP')
        AND (task_code LIKE '%W' OR task_code LIKE '%Y' OR task_code LIKE '%U')
        AND SUBSTR (proj_short_name, 1, 2) IN
            (SELECT LOOKUP_CODE
            FROM FND_LOOKUP_VALUES_VL
            WHERE LOOKUP_TYPE = 'XXEA_CYCLE_TYPE_LKP')
        AND target_end_date > SYSDATE;
                      
    RETURN PROJ_NAMES;
    
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'RETURN_PROJECT_TOS');
      RETURN NULL;
  END RETURN_PROJECT_TOS;
      
  PROCEDURE NEK_LOG (VALUE_ IN VARCHAR2, LOC_ IN VARCHAR2)
   IS
      /********************************************************************************
      \ NEK_LOG - function logs inproper interface functionality. All exceptions are written through log function.
      ********************************************************************************/
   PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      INSERT INTO NEK_LOG_MAIN (LOG_DATETIME,
                                LOG_TYPE,
                                LOG_OWNER,
                                LOG_LOCATION,
                                LOG_VALUE,
                                LOG_DESCRIPTION)
           VALUES (SYSTIMESTAMP,
                   'ERR',
                   'PMV',
                   LOC_,
                   TRIM (SUBSTR (VALUE_, 1, 4000)),
                   TRIM (SUBSTR (LOC_, 1, 1000)));
      NULL;
      COMMIT;                                         
   END NEK_LOG;                
END NEK_PACKAGE_PMV_ESOMS;
/