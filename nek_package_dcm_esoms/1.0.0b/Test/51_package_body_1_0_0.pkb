CREATE OR REPLACE PACKAGE BODY NEK_PACKAGE_DCM_ESOMS AS
/******************************************************************************
   NAME:       NEK_PACKAGE_DCM_ESOMS
   PURPOSE:    Interfacing with DCM system

   1.0.0: 16.8.2013:M. Clemente: Initial version per doc 3. specification
******************************************************************************/

  PROCEDURE UPDATE_EQUIP_DOCS(EQUIP_ID_ IN VARCHAR2) 
  IS
      /********************************************************************************
      \ UPDATE_EQUIP_DOCS - Start point for equipment update
      ********************************************************************************/
  BEGIN

    DELETE_EQUIP_CURRENT_VALUES(EQUIP_ID_);
    INSERT_EQUIP_NEW_VALUES(EQUIP_ID_);
      
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'UPDATE_EQUIP_DOCS');
  END UPDATE_EQUIP_DOCS; 

  PROCEDURE DELETE_EQUIP_CURRENT_VALUES(EQUIP_ID_ IN VARCHAR2) 
  IS
      /********************************************************************************
      \ DELETE_EQUIP_CURRENT_VALUES - Deletes current values from equipment
      ********************************************************************************/
      CURSOR getOldAttachLabels IS 
        SELECT DESCRIPTION
        FROM NEK_DCM_EQUIP_PROPERTIES
        WHERE EQUIP_ID = EQUIP_ID_ AND DESC_TYPE = 'ATT';
        
      CURSOR getOldPrintLabels IS 
        SELECT DESCRIPTION
        FROM NEK_DCM_EQUIP_PROPERTIES
        WHERE EQUIP_ID = EQUIP_ID_ AND DESC_TYPE = 'PRN';     
        
      DOC_DESC_ VARCHAR2(2000);
      
  BEGIN
     OPEN getOldAttachLabels;
        LOOP
          FETCH getOldAttachLabels INTO DOC_DESC_;
             IF getOldAttachLabels%NOTFOUND
             THEN
                EXIT;
             ELSE
                DELETE FROM EQUIP_DOCUMENTS WHERE DOCUMENT_DESCRIPTION = DOC_DESC_
                    AND EQUIP_OPERATOR_ID = EQUIP_ID_;
             END IF;
        END LOOP;
     CLOSE getOldAttachLabels;
     
     OPEN getOldPrintLabels;
        LOOP
          FETCH getOldPrintLabels INTO DOC_DESC_;
             IF getOldPrintLabels%NOTFOUND
             THEN
                EXIT;
             ELSE
                DELETE FROM EQUIP_PRINTS WHERE PRINT_NUMBER = DOC_DESC_
                    AND EQUIP_OPERATOR_ID = EQUIP_ID_;
             END IF;
        END LOOP;
     CLOSE getOldPrintLabels;
     
     DELETE FROM NEK_DCM_EQUIP_PROPERTIES
        WHERE EQUIP_ID = EQUIP_ID_;
        
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'DELETE_EQUIP_CURRENT_VALUES');
  END DELETE_EQUIP_CURRENT_VALUES; 
  
    PROCEDURE INSERT_EQUIP_NEW_VALUES(EQUIP_ID_ IN VARCHAR2) 
  IS
      /********************************************************************************
      \ INSERT_EQUIP_NEW_VALUES - Inserts new values to equipment
      ********************************************************************************/
-- NEK MUST SUPPLY THE CONNECTION TO THE DATA!
--    CURSOR getEquipData IS 
--        SELECT 
--            OZNAKA_DOKUMENTA, 
--            STRAN, 
--            REVIZIJA, 
--            TITLE, 
--            LOKACIJA, 
--            LOG_NUMBER, 
--            TYPE, 
--            PCN, 
--            DOC_LINK, 
--            DOC_FILE, 
--            EQUIP_DOC_LINK        
--        FROM ???
--        WHERE EQUIP_ID = EQUIP_ID_;
  
      OZ_DOC_ VARCHAR2(30);
      STRAN_ VARCHAR2(10);
      REV_ VARCHAR2(10);
      TITLE_ VARCHAR2(150);
      LOKAC_ VARCHAR2(150);
      LOG_NUM_ VARCHAR2(50);
      TYPE_ VARCHAR2(10);
      PCN_ VARCHAR2(5);
      DOC_LINK_ VARCHAR2(4000);
      DOC_FILE_ VARCHAR2(4000);
      EQUIP_DOC_LINK_ VARCHAR2(4000);
      
      GEN_PRINT_ VARCHAR2(100);
      GEN_DOC_ VARCHAR2(250); 
  BEGIN
  
    -- ONCE THE CONNECTION IS ACTIVE - USE THIS
--        OPEN getEquipData;
--        LOOP
--          FETCH getEquipData INTO OZ_DOC_, STRAN_, REV_, TITLE_, LOKAC_, LOG_NUM_, TYPE_, PCN_, DOC_LINK_, DOC_FILE_, EQUIP_DOC_LINK;
--             IF getEquipData%NOTFOUND
--             THEN
--                EXIT;
--             ELSE
--                GEN_PRINT_ := TRIM(SUBSTR(OZ_DOC_ || ', sh. ' || STRAN_ || ', rev.' || REV_ || ', ' || LOKAC_, 1, 100));
--                INSERT_NEW_PRINT(EQUIP_ID_, GEN_PRINT_);
--                INSERT_NEW_ATTACH(EQUIP_ID_, GEN_PRINT_, DOC_LINK_);
--             END IF;
--        END LOOP;
--     CLOSE getEquipData;
--     INSERT_NEW_ATTACH(EQUIP_ID_, 'Documents for '  || EQUIP_ID_ , EQUIP_DOC_LINK_);  -- This one is used only once
         

    -- UNTIL THEN - TEST CODE
    OZ_DOC_ := 'D-302-121';
    STRAN_ := '1';
    REV_ := '06';
    TITLE_ := '';
    LOKAC_ := 'E-11';
    LOG_NUM_ := '35329';
    TYPE_ := '021001';
    PCN_ := '';
    DOC_LINK_ := 'http://mis.nek.si/pmis/DCM3002W.prikaz_dokumenta?P_DOCUMENT_NO=D-302-121=/=06';
    DOC_FILE_ := 'http://mis.nek.si/dokumenti/0210/01/d-302-121r06.pdf';
    EQUIP_DOC_LINK_ := 'http://mis.nek.si/pmis/mel3007w.Reference_Documents?P_NAPRAVA=25501';
    -- END TEST CODE
    GEN_PRINT_ := TRIM(SUBSTR(OZ_DOC_ || ', sh. ' || STRAN_ || ', rev.' || REV_ || ', ' || LOKAC_, 1, 100));
    
    INSERT_NEW_PRINT(EQUIP_ID_, GEN_PRINT_);
    INSERT_NEW_ATTACH(EQUIP_ID_, GEN_PRINT_, DOC_LINK_);
    
    INSERT_NEW_ATTACH(EQUIP_ID_, 'Documents for '  || EQUIP_ID_ , EQUIP_DOC_LINK_);   
      
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'INSERT_EQUIP_NEW_VALUES');
  END INSERT_EQUIP_NEW_VALUES; 
   
  PROCEDURE INSERT_NEW_PRINT(EQUIP_ID_ IN VARCHAR2, PRINT_ IN VARCHAR2) 
  IS
      /********************************************************************************
      \ INSERT_NEW_PRINT - inserts new print
      ********************************************************************************/
  BEGIN
        INSERT INTO EQUIP_PRINTS 
            (EQUIP_OPERATOR_ID, PRINT_NUMBER)
        VALUES
            (EQUIP_ID_, TRIM(SUBSTR(PRINT_, 1, 100)));
      
        INSERT INTO NEK_DCM_EQUIP_PROPERTIES 
            (EQUIP_ID, DESCRIPTION, DESC_TYPE)
        VALUES
            (EQUIP_ID_, TRIM(SUBSTR(PRINT_, 1, 100)), 'PRN');
  
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'INSERT_NEW_PRINT');
  END INSERT_NEW_PRINT; 
  
  PROCEDURE INSERT_NEW_ATTACH(EQUIP_ID_ IN VARCHAR2, ATTACH_LAB_ IN VARCHAR2, ATTACH_VAL_ IN VARCHAR2) 
  IS
      /********************************************************************************
      \ INSERT_NEW_ATTACH - Inserts new attachment
      ********************************************************************************/
    D_NUM_ORD_ NUMBER;
  BEGIN
        SELECT MAX(DOCUMENT_NUMBER) INTO D_NUM_ORD_
        FROM EQUIP_DOCUMENTS
        WHERE EQUIP_OPERATOR_ID = EQUIP_ID_;
        D_NUM_ORD_ := D_NUM_ORD_ + 1;
  
        INSERT INTO EQUIP_DOCUMENTS 
            (EQUIP_OPERATOR_ID, DOCUMENT_NUMBER, DOCUMENT_ORDER, DOCUMENT_TYPE, 
            DOCUMENT_DESCRIPTION, DOCUMENT_FILENAME)
        VALUES
            (EQUIP_ID_, D_NUM_ORD_, D_NUM_ORD_, 2, 
            TRIM(SUBSTR(ATTACH_LAB_, 1, 150)), TRIM(SUBSTR(ATTACH_VAL_, 1, 500)));
      
        INSERT INTO NEK_DCM_EQUIP_PROPERTIES 
            (EQUIP_ID, DESCRIPTION, DESC_TYPE)
        VALUES
            (EQUIP_ID_, TRIM(SUBSTR(ATTACH_LAB_, 1, 150)), 'ATT');
      
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'INSERT_NEW_ATTACH');
  END INSERT_NEW_ATTACH; 
       
  PROCEDURE NEK_LOG (VALUE_ IN VARCHAR2, LOC_ IN VARCHAR2)
   IS
      /********************************************************************************
      \ NEK_LOG - function logs inproper interface functionality. All exceptions are written through log function.
      ********************************************************************************/
   PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      INSERT INTO NEK_LOG_MAIN (LOG_DATETIME,
                                LOG_TYPE,
                                LOG_OWNER,
                                LOG_LOCATION,
                                LOG_VALUE,
                                LOG_DESCRIPTION)
           VALUES (SYSTIMESTAMP,
                   'ERR',
                   'DCM',
                   LOC_,
                   TRIM (SUBSTR (VALUE_, 1, 4000)),
                   TRIM (SUBSTR (LOC_, 1, 1000)));
      NULL;
      COMMIT;                                         
   END NEK_LOG;                
END NEK_PACKAGE_DCM_ESOMS;
/