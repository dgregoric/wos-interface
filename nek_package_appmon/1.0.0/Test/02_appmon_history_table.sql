CREATE SEQUENCE NEK_APPMON_HIST_AS
START WITH 0
INCREMENT BY 1
MINVALUE 0
NOCACHE 
NOCYCLE 
NOORDER;

CREATE TABLE NEK_APPMON_HIST
(
  KEYINDEX    INTEGER   NOT NULL,
  INSTIME     TIMESTAMP(6),
  DT          DATE,
  TOUR        NUMBER,
  SHIFT_NO    NUMBER,
  REV_NO      NUMBER,
  FW          VARCHAR2(1 BYTE),
  SW          VARCHAR2(1 BYTE),
  FW_DATE     TIMESTAMP(6),
  SW_DATE     TIMESTAMP(6),
  SIGNEE      VARCHAR2(45 BYTE),
  ENTRY_TYPE  VARCHAR2(3 BYTE),
  LOG_ID      NUMBER,
  SHIFT_ID    NUMBER,
  SHIFT_DATE  DATE  
);

CREATE UNIQUE INDEX NEK_APPMON_HIST_PK ON NEK_APPMON_HIST
(KEYINDEX);

CREATE OR REPLACE TRIGGER NEK_APPMON_HIST_KEY
BEFORE INSERT
ON NEK_APPMON_HIST
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DECLARE
BEGIN
   SELECT NEK_APPMON_HIST_AS.nextval INTO :NEW.KEYINDEX FROM dual;
   :NEW.INSTIME := SYSTIMESTAMP;
   EXCEPTION
     WHEN OTHERS THEN
       RAISE;
END NEK_APPMON_HIST;

ALTER TABLE NEK_APPMON_HIST ADD (
  CONSTRAINT NEK_APPMON_HIST_PK
  PRIMARY KEY
  (KEYINDEX)
  USING INDEX NEK_APPMON_HIST_PK
  ENABLE VALIDATE);

