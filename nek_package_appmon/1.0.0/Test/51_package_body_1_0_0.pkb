CREATE OR REPLACE PACKAGE BODY NEK_PACKAGE_APPMON AS
/******************************************************************************
   NAME:       NEK_PACKAGE_APPMON
   PURPOSE:  Monitoring Logs and Rounds with NOC and email notification

   1.0.0: 1.8.2013:M. Clemente: Initial version per doc 5.4 specification
******************************************************************************/

  PROCEDURE START_FULL_CHECK IS
   /********************************************************************************
   /  START_FULL_CHECK - Main procedure to start the system.
   /********************************************************************************/
  BEGIN
    IF (LOG_VERBOSE = '1') THEN
        NEK_LOG('Preparing queues for new check.', 'START_FULL_CHECK', 'INFO');    
    END IF;
    PREPARE_QUEUES;

    NEK_LOG('Starting check for Rounds', 'START_FULL_CHECK', 'INFO');    

    CHECK_ROUNDS;
    
    IF (LOG_VERBOSE = '1') THEN
        NEK_LOG('Starting the NOCs processing for rounds.', 'START_FULL_CHECK', 'INFO');    
    END IF;
    PROCESS_MESSAGES_ROUNDS;

    IF (LOG_VERBOSE = '1') THEN
        NEK_LOG('Starting check for Logs', 'START_FULL_CHECK', 'INFO');    
    END IF;
    CHECK_LOGS;   

    IF (LOG_VERBOSE = '1') THEN
        NEK_LOG('Starting the NOCs processing for logs.', 'START_FULL_CHECK', 'INFO');    
    END IF;
    PROCESS_MESSAGES_LOGS;
    
    NEK_LOG('Finished with items checking', 'START_FULL_CHECK', 'INFO');    

  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'START_FULL_CHECK');
  END START_FULL_CHECK;   
  
  PROCEDURE PREPARE_QUEUES IS
  /********************************************************************************
  /   PREPARE_QUEUES - Empties the queue table.
  /********************************************************************************/ 
  BEGIN
    DELETE FROM NEK_APPMON_QUEUE;
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'PREPARE_QUEUES');
  END PREPARE_QUEUES;   
  
  PROCEDURE CHECK_ROUNDS IS
  /********************************************************************************
  /   CHECK_ROUNDS - Check for unsigned rounds.
  /********************************************************************************/ 
  CURSOR getUnsignedRoundsFW IS
    SELECT DT, TOUR, SHIFT_NO, REV_NO  
    FROM TOUR_APPROVAL 
    WHERE 
        (DT >= (SYSDATE - ((ROUND_SECOND_WARN_TIME_MIN/60)/24)) 
            AND DT <=  (SYSDATE - ((ROUND_FIRST_WARN_TIME_MIN/60)/24)) )
        AND TOUR_APPROVAL_LEVEL = ROUND_APPROVE_LVL
        AND TOUR_VERIFIED_BY IS NULL
        AND TOUR_VERIFIED_DATE IS NULL
        AND TOUR_VERIF_RECORDED_BY IS NULL
        AND TOUR_VERIF_RECORDED_DATE IS NULL
        AND TOUR NOT IN (12) -- CONFIGURATION
        ORDER BY DT DESC, SHIFT_NO ASC, TOUR_APPROVAL_LEVEL ASC;

  CURSOR getUnsignedRoundsSW IS
    SELECT DT, TOUR, SHIFT_NO, REV_NO 
    FROM TOUR_APPROVAL 
    WHERE 
        (DT >=  (SYSDATE - ((ROUND_SECOND_WARN_TIME_MIN/60)/24))-ROUND_MAX_CHECK_DATE
            AND DT <= (SYSDATE - ((ROUND_SECOND_WARN_TIME_MIN/60)/24)) )
        AND TOUR_APPROVAL_LEVEL = ROUND_APPROVE_LVL
        AND TOUR_VERIFIED_BY IS NULL
        AND TOUR_VERIFIED_DATE IS NULL
        AND TOUR_VERIF_RECORDED_BY IS NULL
        AND TOUR_VERIF_RECORDED_DATE IS NULL
        AND TOUR NOT IN (12)  -- CONFIGURATION
        ORDER BY DT DESC, SHIFT_NO ASC, TOUR_APPROVAL_LEVEL ASC;
  
  loDT                          TOUR_APPROVAL.DT%TYPE;
  loTOUR                      TOUR_APPROVAL.TOUR%TYPE;
  loSHIFT_NO                TOUR_APPROVAL.SHIFT_NO%TYPE;
  loREV_NO                  TOUR_APPROVAL.REV_NO%TYPE;
  loTOUR_VERIF_DATE   TOUR_APPROVAL.TOUR_VERIFIED_DATE%TYPE;
  SIGNEE                      VARCHAR2(40);

  BEGIN 
    -- First warning
    OPEN getUnsignedRoundsFW;
    LOOP
      FETCH getUnsignedRoundsFW INTO loDT, loTOUR, loSHIFT_NO, loREV_NO;
         IF getUnsignedRoundsFW%NOTFOUND
         THEN
            EXIT;
         ELSE
            loTOUR_VERIF_DATE := MAKE_VERIF_DATE_ROUND(loDT, loTOUR, loSHIFT_NO, loREV_NO);
            SIGNEE := GET_ROUND_SIGNEE(loDT, loTOUR, loSHIFT_NO, loREV_NO, loTOUR_VERIF_DATE);
            IF (IS_ROUND_FOR_FIRST_SIGN(loDT, loTOUR, loSHIFT_NO, loREV_NO) = TRUE) THEN
                ADD_TO_FIRST_ROUND_QUEUE(loDT, loTOUR, loSHIFT_NO, loREV_NO, SIGNEE);
            END IF;   
         END IF;
    END LOOP;
    CLOSE getUnsignedRoundsFW;
    
    -- Second warning
    OPEN getUnsignedRoundsSW;
    LOOP
      FETCH getUnsignedRoundsSW INTO loDT, loTOUR, loSHIFT_NO, loREV_NO;
         IF getUnsignedRoundsSW%NOTFOUND
         THEN
            EXIT;
         ELSE
            loTOUR_VERIF_DATE := MAKE_VERIF_DATE_ROUND(loDT, loTOUR, loSHIFT_NO, loREV_NO);
            SIGNEE := GET_ROUND_SIGNEE(loDT, loTOUR, loSHIFT_NO, loREV_NO, loTOUR_VERIF_DATE);
            IF (IS_ROUND_FOR_SECOND_SIGN(loDT, loTOUR, loSHIFT_NO, loREV_NO) = TRUE) THEN
                ADD_TO_SECOND_ROUND_QUEUE(loDT, loTOUR, loSHIFT_NO, loREV_NO, SIGNEE);
            END IF;   
         END IF;
    END LOOP;
    CLOSE getUnsignedRoundsSW;

  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'CHECK_ROUNDS');
  END CHECK_ROUNDS;   
  
  FUNCTION MAKE_VERIF_DATE_ROUND(DT_ IN DATE, TOUR_ IN NUMBER, SHIFT_NO_ IN NUMBER, REV_NO_ IN NUMBER) RETURN DATE IS
    /********************************************************************************
  /   MAKE_VERIF_DATE_ROUND - Process the Messages from the QUEUE
  /********************************************************************************/  
    MAX_SHIFT_N NUMBER;
    END_TIME_ VARCHAR2(10);
    NEXT_DAY_ VARCHAR2(30);
    DT2 DATE;
    H_ NUMBER;
    M_ NUMBER;
    SHIFT_NO_NEW_ NUMBER;
    DT_NEW_ DATE;

    CURSOR getAllShifts IS
        SELECT SHIFT_NO
        FROM SHIFT_TIME
        WHERE TOUR = TOUR_
            AND REV_NO = REV_NO_
            AND SHIFT_NO > SHIFT_NO_
        ORDER BY SHIFT_NO ASC;
    
    BEGIN

    SHIFT_NO_NEW_ := SHIFT_NO_;
    DT_NEW_ := DT_;
    
    OPEN getAllShifts;
    LOOP
      FETCH getAllShifts INTO SHIFT_NO_NEW_;
         IF getAllShifts%NOTFOUND
         THEN
            SELECT SHIFT_NO INTO SHIFT_NO_NEW_ 
            FROM SHIFT_TIME 
            WHERE TOUR = TOUR_
                AND REV_NO = REV_NO_
                AND SHIFT_NO = (SELECT MIN(SHIFT_NO) FROM SHIFT_TIME WHERE TOUR = TOUR_ AND REV_NO = REV_NO_);
            EXIT;
         ELSE
            EXIT;
         END IF;
    END LOOP;
    CLOSE getAllShifts;
    
    SELECT NORMAL_START_TIME, NORMAL_START_DAY  INTO END_TIME_, NEXT_DAY_ 
    FROM SHIFT_TIME
    WHERE TOUR = TOUR_
    AND REV_NO = REV_NO_
    AND SHIFT_NO = SHIFT_NO_NEW_; 

    IF (NEXT_DAY_ = '1') THEN
        DT_NEW_ := DT_NEW_+1;
    END IF;
   
    DT2 := TO_DATE(END_TIME_, 'hh24:mi');
    H_ := TO_NUMBER(TO_CHAR(DT2, 'HH24'));
    M_ := TO_NUMBER(TO_CHAR(DT2, 'MI'));
  
    DT_NEW_ := DT_NEW_ + H_/24 + M_/1440; 
    RETURN DT_NEW_;
  
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'MAKE_VERIF_DATE_ROUND');
      RETURN NULL;
  END MAKE_VERIF_DATE_ROUND;   
    
  PROCEDURE PROCESS_MESSAGES_ROUNDS IS
    /********************************************************************************
  /   PROCESS_MESSAGES_ROUNDS - Process the Messages from the QUEUE
  /********************************************************************************/  
   DATA_TYPE_                VARCHAR2(3);
   REC_                          VARCHAR2(35);
   
  CURSOR getRecipientsFW IS 
    SELECT DISTINCT SIGNEE 
    FROM NEK_APPMON_QUEUE
    WHERE FW = '1' AND 
    ENTRY_TYPE = DATA_TYPE_;    

  CURSOR getRecipientsSW IS 
    SELECT DISTINCT SIGNEE 
    FROM NEK_APPMON_QUEUE
    WHERE SW = '1' AND ENTRY_TYPE = DATA_TYPE_;    
  
  CURSOR getSigneeItemsFW IS
    SELECT TOUR, REV_NO, SHIFT_NO, DT
    FROM NEK_APPMON_QUEUE
    WHERE FW ='1' 
        AND ENTRY_TYPE = DATA_TYPE_
        AND SIGNEE = REC_
    ORDER BY DT ASC, TOUR ASC, REV_NO ASC;
  
    CURSOR getSigneeItemsSW IS
    SELECT TOUR, REV_NO, SHIFT_NO, DT
    FROM NEK_APPMON_QUEUE
    WHERE SW ='1' 
        AND ENTRY_TYPE = DATA_TYPE_
        AND SIGNEE = REC_
    ORDER BY DT ASC, TOUR ASC, REV_NO ASC;
  
   loDT                          TOUR_APPROVAL.DT%TYPE;
   loTOUR                      TOUR_APPROVAL.TOUR%TYPE;
   loSHIFT_NO                TOUR_APPROVAL.SHIFT_NO%TYPE;
   loREV_NO                  TOUR_APPROVAL.REV_NO%TYPE;
   
   CRLF CHAR(2) :=  CHR(13) || CHR(10);
   ROUND_MESSAGE_     VARCHAR2(4000);
   
   TOUR_NAME_ VARCHAR2(100);
   SHIFT_NAME_  VARCHAR2(100);
   FOUND_ROUND_ENTRY_ BOOLEAN := FALSE;
  BEGIN
    DATA_TYPE_ := 'R';
       
    OPEN getRecipientsFW;
    LOOP
      FETCH getRecipientsFW INTO REC_;
         IF getRecipientsFW%NOTFOUND
         THEN
            IF ((LOG_VERBOSE = '1') AND FOUND_ROUND_ENTRY_ = FALSE) THEN
                NEK_LOG('There are no recipients/overdues for rounds with first warning.', 'PROCESS_MESSAGES', 'INFO');    
            END IF;
            EXIT;
         ELSE
             FOUND_ROUND_ENTRY_ := TRUE;
             ROUND_MESSAGE_ := 'Opozorilo!' || CRLF || CRLF || 'Spodaj navedeni obhodi niso potrjeni.'  || CRLF || CRLF;
             OPEN getSigneeItemsFW;
                LOOP
                  FETCH getSigneeItemsFW INTO loTOUR, loREV_NO, loSHIFT_NO, loDT;
                     IF getSigneeItemsFW%NOTFOUND
                     THEN
                        EXIT;
                     ELSE
                        GET_ROUND_MESSAGE_ITEM_TEXT(loTOUR, loSHIFT_NO, loREV_NO, SHIFT_NAME_, TOUR_NAME_);
                        ROUND_MESSAGE_ := ROUND_MESSAGE_   || 'Obhod: '  ||
                            TOUR_NAME_ || ', za dan: '  || TO_CHAR(loDT, 'dd.mm.yyyy')    || ', izmena: '  || SHIFT_NAME_  ||  CRLF;  
                     END IF;
                END LOOP;
                CLOSE getSigneeItemsFW;
                
                IF (CREATE_NOC = '1') THEN
                    ROUND_MESSAGE_ := ROUND_MESSAGE_  || CRLF; 
                    CREATE_NEW_NOC(REC_, 'Opozorilo - obhodi', ROUND_MESSAGE_, 'Potrdite obhode.', ROUND_FIRST_WARN_TYPE, TRUE, 'R');             
                END IF;
         
         END IF;
    END LOOP;
    CLOSE getRecipientsFW;

    FOUND_ROUND_ENTRY_ := FALSE;
    OPEN getRecipientsSW;
    LOOP
      FETCH getRecipientsSW INTO REC_;
         IF getRecipientsSW%NOTFOUND
         THEN
            IF ((LOG_VERBOSE = '1') AND FOUND_ROUND_ENTRY_ = FALSE) THEN
                NEK_LOG('There are no recipients/overdues for rounds with second warning.', 'PROCESS_MESSAGES_ROUNDS', 'INFO');    
            END IF;
            EXIT;
         ELSE
             FOUND_ROUND_ENTRY_ := TRUE;
             ROUND_MESSAGE_ := 'Opozorilo!!' || CRLF || CRLF || 'Spodaj navedeni obhodi �e zmeraj niso potrjeni!!' || CRLF || CRLF;
             OPEN getSigneeItemsSW;
                LOOP
                  FETCH getSigneeItemsSW INTO loTOUR, loREV_NO, loSHIFT_NO, loDT;
                     IF getSigneeItemsSW%NOTFOUND
                     THEN
                        EXIT;
                     ELSE
                        GET_ROUND_MESSAGE_ITEM_TEXT(loTOUR, loSHIFT_NO, loREV_NO, SHIFT_NAME_, TOUR_NAME_);
                        ROUND_MESSAGE_ := ROUND_MESSAGE_   || 'Obhod: ' ||
                            TOUR_NAME_ || ', za dan: '  || TO_CHAR(loDT, 'dd.mm.yyyy') || ', izmena: '  || SHIFT_NAME_  ||  CRLF;  
                     END IF;
                END LOOP;
                CLOSE getSigneeItemsSW;

                IF (CREATE_NOC = '1') THEN   
                    ROUND_MESSAGE_ := ROUND_MESSAGE_  || CRLF;                     
                    CREATE_NEW_NOC(REC_, 'Opozorilo - obhodi', ROUND_MESSAGE_, 'Potrdite obhode.', ROUND_SECOND_WARN_TYPE, FALSE, 'R');             
                END IF;
                
                IF (ROUND_SECOND_WARN_SEND_MAIL = '1') THEN
                    SEND_EMAIL(REC_, EMAIL_ROUND_SUB, ROUND_MESSAGE_);    
                END IF;        
       END IF;
    END LOOP;
    CLOSE getRecipientsSW;
  
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'PROCESS_MESSAGES_ROUNDS');
  END PROCESS_MESSAGES_ROUNDS;   
  
  PROCEDURE SEND_EMAIL(RECIPIENT_ID_ IN VARCHAR2, SUBJECT_ IN VARCHAR2, MESSAGE_ IN VARCHAR2) IS
  /********************************************************************************
  /   SEND_EMAIL - Sends mail to the responsible person
  /********************************************************************************/  
    EMAIL_ USERLOG.EMAIL%TYPE;
  BEGIN 
    SELECT EMAIL INTO EMAIL_
    FROM USERLOG 
    WHERE TRIM(USER_ID) = TRIM(RECIPIENT_ID_);
    
    UTL_MAIL.send(sender=> SENDER_ADD, recipients=> EMAIL_, subject=>SUBJECT_, message=> MESSAGE_);
    
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'SEND_EMAIL');
  END SEND_EMAIL;   
  
  PROCEDURE GET_ROUND_MESSAGE_ITEM_TEXT(TOUR_ IN NUMBER, SHIFT_NO_ IN NUMBER, REV_NO_ IN NUMBER, DESC_ OUT VARCHAR2, NAME_ OUT VARCHAR2)  IS
   /********************************************************************************
  /   GET_MESSAGE_TEXT - Returns specified text for an Round item.
  /********************************************************************************/   
  CURSOR getDescAndName IS 
      SELECT st.SHIFT_DESC, tr.TOUR_NAME 
      FROM SHIFT_TIME st
      INNER JOIN TOUR tr ON tr.TOUR = st.TOUR AND tr.REV_NO = st.REV_NO
      WHERE 
         st.TOUR = TOUR_
         AND st.REV_NO = REV_NO_
         AND st.SHIFT_NO = SHIFT_NO_;
  BEGIN
    OPEN getDescAndName;
    FETCH getDescAndName INTO DESC_, NAME_;
    CLOSE getDescAndName;
     
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'GET_ROUND_MESSAGE_ITEM_TEXT');
  END GET_ROUND_MESSAGE_ITEM_TEXT; 
  
  PROCEDURE CREATE_NEW_NOC(REC_ID_ IN VARCHAR2, TITLE_ IN VARCHAR2, TEXT_ IN VARCHAR2, RESOL_ IN VARCHAR2, MESSAGE_TYPE_ IN NUMBER, MESSAGE_ONLY_ IN BOOLEAN, TYPE_ IN VARCHAR2) IS
   /********************************************************************************
  /   CREATE_NEW_NOC - Create single NOC
  /********************************************************************************/ 
    NOC_ID_ NUMBER;
    TYPE_FOLDER_ VARCHAR2(35);  
BEGIN
    NOC_ID_ := NULL;
    IF (TYPE_ = 'R') THEN
        TYPE_FOLDER_ := ROUND_SECOND_WARN_FOLDER;
    END IF;
    IF (TYPE_ = 'L') THEN
        TYPE_FOLDER_ := LOG_SECOND_WARN_FOLDER;
    END IF;
    
    IF (MESSAGE_ONLY_ = FALSE) THEN
        SELECT MAX(NOTICE_OF_CHANGE_ID) INTO NOC_ID_ FROM NOTICE_OF_CHANGE; 
        NOC_ID_ := NOC_ID_+1;

        INSERT INTO NOTICE_OF_CHANGE
            (NOTICE_OF_CHANGE_ID, NOTICE_OF_CHANGE_TYPE_ID, 
            NOTICE_OF_CHANGE_TITLE, NOTICE_OF_CHANGE_TEXT, 
            NOTICE_OF_CHANGE_INSTRUCTIONS, NOTICE_OF_CHANGE_FROM, 
            MODULE_ID, MESSAGE_TYPE, NOTICE_OF_CHANGE_CREATED_BY, 
            NOTICE_OF_CHANGE_CREATED_DT, NOTICE_OF_CHANGE_APPROVED_BY, 
            NOTICE_OF_CHANGE_APPROVED_DT, ATTACHMENTS_MUST_BE_VIEWED)
        VALUES
            (NOC_ID_, TYPE_FOLDER_,
            TITLE_, TEXT_, 
            RESOL_, NOC_INSERT_USER, 
            'NOC', MESSAGE_TYPE_, NOC_INSERT_USER, 
            SYSTIMESTAMP, NOC_INSERT_USER, 
            SYSTIMESTAMP, 0);
            
        INSERT INTO NOTICE_OF_CHANGE_USERS (NOTICE_OF_CHANGE_ID, USER_ID)
        VALUES (NOC_ID_, REC_ID_);
    END IF;
    
    INSERT INTO USER_MESSAGES
        (MESSAGE_TO, MESSAGE_CREATED_DT, MESSAGE_FROM, 
        MESSAGE_TITLE, MESSAGE_TEXT, MESSAGE_INSTRUCTIONS, 
        MODULE_ID, NOTICE_OF_CHANGE_ID, MESSAGE_TYPE)
    VALUES
        (REC_ID_, SYSTIMESTAMP, NOC_INSERT_USER, 
         TITLE_, TEXT_, RESOL_, 
         'NOC', NOC_ID_, MESSAGE_TYPE_); 
         
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'CREATE_NEW_NOC');
  END CREATE_NEW_NOC;   
  
  FUNCTION GET_ROUND_SIGNEE(DT_ IN DATE, TOUR_ IN NUMBER, SHIFT_NO_ IN NUMBER, REV_NO_ IN NUMBER, TOUR_VERIF_DATE_ IN DATE) RETURN VARCHAR2 IS
  /********************************************************************************
  /   GET_ROUND_SIGNEE - Check the correct signee for the round.
  /********************************************************************************/        
    SHIFT_POS_ VARCHAR2(3);
    USER_ID_ SHIFT_STAFFING.USER_ID%TYPE;
    TOUR_ID_ NUMBER;
  BEGIN
    SELECT TOUR INTO TOUR_ID_ FROM TOUR
        WHERE TOUR_NAME = VI_SHIFT_NAME
        AND REV_NO = ( SELECT MAX(REV_NO) FROM TOUR WHERE TOUR_NAME = VI_SHIFT_NAME);
    
    IF (TOUR_ID_ = TOUR_) THEN 
        SHIFT_POS_ := SHIFT_POS_VI;
    ELSE 
        SHIFT_POS_ := SHIFT_POS_GO;
    END IF;    
    
    SELECT USER_ID INTO USER_ID_ 
        FROM SHIFT_STAFFING
        WHERE LOG_ID = ROSTER_LOG_ID 
        AND SHIFT_ID = GET_SHIFT_ID(TOUR_VERIF_DATE_) 
        AND TO_CHAR(SHIFT_DATE) = TO_CHAR(DT_)
        AND SHIFT_POSITION_DESCR = SHIFT_POS_ 
        ORDER BY SHIFT_POSITION_NO ASC;
    
    RETURN USER_ID_;
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'GET_ROUND_SIGNEE');
      RETURN '-1';
  END GET_ROUND_SIGNEE;    
  
    FUNCTION GET_LOG_SIGNEE(LOG_ID_ IN NUMBER, SHIFT_ID_ IN NUMBER, SHIFT_DATE_ IN DATE, LOG_VERIF_DATE_ IN DATE) RETURN VARCHAR2 IS
  /********************************************************************************
  /   GET_LOG_SIGNEE - Check the correct signee for the log.
  /********************************************************************************/        
    SHIFT_POS_ VARCHAR2(3);
    USER_ID_ SHIFT_STAFFING.USER_ID%TYPE;
    VI_LOG_ID_ NUMBER;
  BEGIN
    SELECT LOG_ID INTO VI_LOG_ID_ FROM LOG_LIST
        WHERE LOG_TITLE = VI_LOG_NAME;
    
    IF (VI_LOG_ID_ = LOG_ID_) THEN 
        SHIFT_POS_ := SHIFT_POS_VI;
    ELSE 
        SHIFT_POS_ := SHIFT_POS_GO;
    END IF;    
    
    SELECT USER_ID INTO USER_ID_ 
        FROM SHIFT_STAFFING
        WHERE LOG_ID = ROSTER_LOG_ID
        AND SHIFT_ID = GET_SHIFT_ID(LOG_VERIF_DATE_) 
        AND TO_CHAR(SHIFT_DATE) = TO_CHAR(SHIFT_DATE_)
        AND SHIFT_POSITION_DESCR = SHIFT_POS_ 
        ORDER BY SHIFT_POSITION_NO ASC;
    
    RETURN USER_ID_;
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'GET_LOG_SIGNEE');
      RETURN '-1';
  END GET_LOG_SIGNEE;    
  
  FUNCTION GET_SHIFT_ID(TOUR_DATE_ IN DATE) RETURN NUMBER IS 
  /********************************************************************************
  /   GET_SHIFT_ID - Returns the shift id based on the tour date
  /********************************************************************************/
  CURSOR getShifts IS
    SELECT SHIFT_ID, START_TIME, END_TIME 
    FROM SHIFT_LIST
    WHERE LOG_ID = ROSTER_LOG_ID
    ORDER BY SHIFT_ID;
    
    SHIFT_ID_          SHIFT_LIST.SHIFT_ID%TYPE;
    START_TIME_     SHIFT_LIST.START_TIME%TYPE;
    END_TIME_       SHIFT_LIST.END_TIME%TYPE;
    
    TOUR_TIME_ NUMBER;
    st_ NUMBER;
    et_ NUMBER;

 BEGIN
    TOUR_TIME_ := TO_NUMBER(TO_CHAR(TOUR_DATE_, 'hh24mi'));
 
    OPEN getShifts;
        LOOP
          FETCH getShifts INTO SHIFT_ID_, START_TIME_, END_TIME_;
             IF getShifts%NOTFOUND
             THEN
                EXIT;
             ELSE
                st_ := TO_NUMBER(TO_CHAR(TO_TIMESTAMP(START_TIME_, 'hh24:mi'), 'hh24mi'));
                et_ := TO_NUMBER(TO_CHAR(TO_TIMESTAMP(END_TIME_, 'hh24:mi'), 'hh24mi'));
                IF (st_ < et_) THEN                    
                    IF (st_ < TOUR_TIME_) AND (et_ >= TOUR_TIME_) THEN
                       EXIT;
                    END IF;
                ELSE
                    IF (((st_ < TOUR_TIME_) AND (TOUR_TIME_ <= 2400)) OR ((TOUR_TIME_ > 0) AND (TOUR_TIME_ <= et_))) THEN
                        EXIT;
                    END IF;
                END IF;
             END IF;
        END LOOP;
    CLOSE getShifts;
    
    RETURN SHIFT_ID_;
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'GET_SHIFT_ID');
      RETURN -1;
  END GET_SHIFT_ID; 
  
  FUNCTION IS_ROUND_FOR_FIRST_SIGN(DT_ IN DATE, TOUR_ IN NUMBER, SHIFT_NO_ IN NUMBER, REV_NO_ IN NUMBER) RETURN BOOLEAN IS
  /********************************************************************************
  /   IS_ROUND_FOR_FIRST_SIGN - Check if the round has not been signed yet.
  /********************************************************************************/
  CURSOR findEntry IS
    SELECT COUNT(*)
    FROM NEK_APPMON_HIST
    WHERE TO_CHAR(DT) = TO_CHAR(DT_) AND
        TOUR = TOUR_ AND
        SHIFT_NO = SHIFT_NO_ AND 
        REV_NO = REV_NO_  AND
        FW = '1' AND 
        ENTRY_TYPE = 'R' AND
        FW_DATE IS NOT NULL;
        
  CURSOR findEmptyEntry IS
    SELECT COUNT(*)
    FROM NEK_APPMON_HIST
    WHERE TO_CHAR(DT) = TO_CHAR(DT_) AND
        TOUR = TOUR_ AND
        SHIFT_NO = SHIFT_NO_ AND 
        REV_NO = REV_NO_ AND
        ENTRY_TYPE = 'R' ;
                 
    FOUND_ENTRY NUMBER;
  BEGIN
    OPEN findEntry;
    FETCH findEntry INTO FOUND_ENTRY;
    CLOSE findEntry;
    
    IF FOUND_ENTRY > 0 THEN
        RETURN FALSE;
    ELSE 
        OPEN findEmptyEntry;
        FETCH findEmptyEntry INTO FOUND_ENTRY;
        CLOSE findEmptyEntry;    
    
        IF FOUND_ENTRY > 0 THEN
            RETURN TRUE;
        ELSE 
            INSERT INTO NEK_APPMON_HIST (DT, TOUR, SHIFT_NO, REV_NO, ENTRY_TYPE) 
            VALUES (DT_, TOUR_, SHIFT_NO_, REV_NO_, 'R');
            RETURN TRUE;
        END IF;
   END IF;
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'IS_ROUND_FOR_FIRST_SIGN');
      RETURN FALSE;
  END IS_ROUND_FOR_FIRST_SIGN; 
  
    PROCEDURE ADD_TO_FIRST_ROUND_QUEUE(DT_ IN DATE, TOUR_ IN NUMBER, SHIFT_NO_ IN NUMBER, REV_NO_ IN NUMBER, SIGNEE_ IN VARCHAR2) IS
  /********************************************************************************
  /   ADD_TO_FIRST_ROUND_QUEUE - adds the round to queue.
  /********************************************************************************/  
  BEGIN
            INSERT INTO NEK_APPMON_QUEUE (DT, TOUR, SHIFT_NO, REV_NO, FW, FW_DATE, SIGNEE, ENTRY_TYPE) 
            VALUES (DT_, TOUR_, SHIFT_NO_, REV_NO_, '1', SYSDATE, SIGNEE_, 'R');
            
            UPDATE NEK_APPMON_HIST 
            SET FW = '1', FW_DATE = SYSDATE
            WHERE TO_CHAR(DT) = TO_CHAR(DT_) AND TOUR = TOUR_ AND SHIFT_NO = SHIFT_NO_ AND REV_NO = REV_NO_ AND ENTRY_TYPE='R';
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'ADD_TO_FIRST_ROUND_QUEUE');
  END ADD_TO_FIRST_ROUND_QUEUE;    
  
     FUNCTION IS_ROUND_FOR_SECOND_SIGN(DT_ IN DATE, TOUR_ IN NUMBER, SHIFT_NO_ IN NUMBER, REV_NO_ IN NUMBER) RETURN BOOLEAN IS
  /********************************************************************************
  /   IS_ROUND_FOR_SECOND_SIGN - Check if the round has not been signed yet.
  /********************************************************************************/  
   CURSOR findEntry IS
    SELECT COUNT(*)
    FROM NEK_APPMON_HIST
    WHERE TO_CHAR(DT) = TO_CHAR(DT_) AND
        TOUR = TOUR_ AND
        SHIFT_NO = SHIFT_NO_ AND 
        REV_NO = REV_NO_  AND
        SW = '1' AND 
        ENTRY_TYPE = 'R' AND
        SW_DATE IS NOT NULL;
    
   CURSOR findEntryBefore IS  -- This is only because we're starting where the statuses are already passed the first chekup.
    SELECT COUNT(*)
    FROM NEK_APPMON_HIST
    WHERE 
        TO_CHAR(DT) = TO_CHAR(DT_) AND
        TOUR = TOUR_ AND
        SHIFT_NO = SHIFT_NO_ AND 
        REV_NO = REV_NO_  AND
        FW = '1' AND 
        ENTRY_TYPE = 'R' AND
        FW_DATE IS NOT NULL;    
        
    FOUND_ENTRY NUMBER;
  BEGIN
    OPEN findEntry;
    FETCH findEntry INTO FOUND_ENTRY;
    CLOSE findEntry;
    
    IF FOUND_ENTRY > 0 THEN
        RETURN FALSE;
    ELSE
        OPEN findEntryBefore;
        FETCH findEntryBefore INTO FOUND_ENTRY;
        CLOSE findEntryBefore; 
        
        IF (FOUND_ENTRY > 0) THEN 
            RETURN TRUE;
        ELSE
            INSERT INTO NEK_APPMON_HIST (DT, TOUR, SHIFT_NO, REV_NO, FW, FW_DATE, ENTRY_TYPE) 
                VALUES (DT_, TOUR_, SHIFT_NO_, REV_NO_, '1', DT_, 'R');
        END IF;
        RETURN TRUE;
    END IF;
    
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'IS_ROUND_FOR_SECOND_SIGN');
      RETURN FALSE;
  END IS_ROUND_FOR_SECOND_SIGN; 
  
    PROCEDURE ADD_TO_SECOND_ROUND_QUEUE(DT_ IN DATE, TOUR_ IN NUMBER, SHIFT_NO_ IN NUMBER, REV_NO_ IN NUMBER, SIGNEE_ IN VARCHAR2) IS
  /********************************************************************************
  /   ADD_TO_SECOND_ROUND_QUEUE - adds the round to queue.
  /********************************************************************************/  
  BEGIN
            INSERT INTO NEK_APPMON_QUEUE (DT, TOUR, SHIFT_NO, REV_NO, SW, SW_DATE, SIGNEE, ENTRY_TYPE) 
            VALUES (DT_, TOUR_, SHIFT_NO_, REV_NO_, '1', SYSDATE, SIGNEE_, 'R');
            
            UPDATE NEK_APPMON_HIST 
            SET SW = '1', SW_DATE = SYSDATE
            WHERE TO_CHAR(DT) = TO_CHAR(DT_) AND TOUR = TOUR_ AND SHIFT_NO = SHIFT_NO_ AND REV_NO = REV_NO_ AND ENTRY_TYPE='R';
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'ADD_TO_SECOND_ROUND_QUEUE');
  END ADD_TO_SECOND_ROUND_QUEUE;  
  
  PROCEDURE CHECK_LOGS IS
  /********************************************************************************
  /   CHECK_LOGS - Check for unsigned logs.
  /********************************************************************************/ 
  CURSOR getUnsignedLogEntriesFW IS
      SELECT LOG_ID, SHIFT_ID, SHIFT_DATE, END_TIME
        FROM SHIFT_APPROVAL_LIST sal
        INNER JOIN LOG_LIST ll on ll.LOG_ID = sal.LOG_ID
        INNER JOIN SHIFT_LIST sl on sl.LOG_ID = sal.LOG_ID AND sl.SHIFT_ID = sal.SHIFT_ID
        WHERE ll.LOG_ID NOT IN (2, 3, 4, 8, 6, 7, 9, 10) AND  -- CONFIGURATION
            ll.DEACTIVATION_DT IS NULL AND
            sal.APPROVAL_DT IS NULL AND
            sal.START_DT IS NOT NULL AND 
            sal.TURNOVER_DT IS NOT NULL AND
            (sal.TURNOVER_DT >= (SYSDATE - ((LOG_SECOND_WARN_TIME_MIN/60)/24)) 
                        AND sal.TURNOVER_DT  <=  (SYSDATE - (LOG_FIRST_WARN_TIME_MIN/60)/24)) 
        order by sal.SHIFT_DATE desc, LOG_ID asc, sal.SHIFT_ID asc; 
  
    CURSOR getUnsignedLogEntriesSW IS
      SELECT LOG_ID, SHIFT_ID, SHIFT_DATE, END_TIME
        FROM SHIFT_APPROVAL_LIST sal
        INNER JOIN LOG_LIST ll on ll.LOG_ID = sal.LOG_ID
        INNER JOIN SHIFT_LIST sl on sl.LOG_ID = sal.LOG_ID AND sl.SHIFT_ID = sal.SHIFT_ID
        WHERE ll.LOG_ID NOT IN (2, 3, 4, 8, 6, 7, 9, 10) AND  -- CONFIGURATION
            ll.DEACTIVATION_DT IS NULL AND
            sal.APPROVAL_DT IS NULL AND
            sal.START_DT IS NOT NULL AND 
            sal.TURNOVER_DT IS NOT NULL AND
            (sal.TURNOVER_DT >= (SYSDATE - ((LOG_SECOND_WARN_TIME_MIN/60)/24))-LOG_MAX_CHECK_DATE 
                        AND sal.TURNOVER_DT  <=  (SYSDATE - (LOG_SECOND_WARN_TIME_MIN/60)/24)) 
        order by sal.SHIFT_DATE desc, LOG_ID asc, sal.SHIFT_ID asc; 
  
  loLOG_ID          SHIFT_APPROVAL_LIST.LOG_ID%TYPE;
  loSHIFT_ID        SHIFT_APPROVAL_LIST.SHIFT_ID%TYPE;
  loSHIFT_DATE   SHIFT_APPROVAL_LIST.SHIFT_DATE%TYPE;
  loEND_TIME     SHIFT_LIST.END_TIME%TYPE;
  
  SIGNEE VARCHAR2(35);
  BEGIN
 -- First warning
    OPEN getUnsignedLogEntriesFW;
    LOOP
      FETCH getUnsignedLogEntriesFW INTO loLOG_ID, loSHIFT_ID, loSHIFT_DATE, loEND_TIME;
         IF getUnsignedLogEntriesFW%NOTFOUND
         THEN
            EXIT;
         ELSE
            SIGNEE := GET_LOG_SIGNEE(loLOG_ID, loSHIFT_ID, loSHIFT_DATE, TO_DATE(loEND_TIME, 'hh24:mi'));
            IF (IS_LOG_FOR_FIRST_SIGN(loLOG_ID, loSHIFT_ID, loSHIFT_DATE) = TRUE) THEN
                ADD_TO_FIRST_LOG_QUEUE(loLOG_ID, loSHIFT_ID, loSHIFT_DATE, SIGNEE);
            END IF;   
         END IF;
    END LOOP;
    CLOSE getUnsignedLogEntriesFW;
    
    -- Second warning
    OPEN getUnsignedLogEntriesSW;
    LOOP
      FETCH getUnsignedLogEntriesSW INTO loLOG_ID, loSHIFT_ID, loSHIFT_DATE, loEND_TIME;
         IF getUnsignedLogEntriesSW%NOTFOUND
         THEN
            EXIT;
         ELSE
            SIGNEE := GET_LOG_SIGNEE(loLOG_ID, loSHIFT_ID, loSHIFT_DATE, TO_DATE(loEND_TIME, 'hh24:mi'));
            IF (IS_LOG_FOR_SECOND_SIGN(loLOG_ID, loSHIFT_ID, loSHIFT_DATE) = TRUE) THEN
                ADD_TO_SECOND_LOG_QUEUE(loLOG_ID, loSHIFT_ID, loSHIFT_DATE, SIGNEE);
            END IF;   
         END IF;
    END LOOP;
    CLOSE getUnsignedLogEntriesSW;
    
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'CHECK_LOGS');
  END CHECK_LOGS;   
  
    FUNCTION IS_LOG_FOR_FIRST_SIGN(LOG_ID_ IN NUMBER, SHIFT_ID_ IN NUMBER, SHIFT_DATE_ IN DATE) RETURN BOOLEAN IS
  /********************************************************************************
  /   IS_LOG_FOR_FIRST_SIGN - Check if the log has not been signed yet.
  /********************************************************************************/
  CURSOR findEntry IS
    SELECT COUNT(*)
    FROM NEK_APPMON_HIST
    WHERE 
        LOG_ID = LOG_ID_ AND
        SHIFT_ID = SHIFT_ID_ AND
        TO_CHAR(SHIFT_DATE) = TO_CHAR(SHIFT_DATE_) AND
        FW = '1' AND 
        ENTRY_TYPE = 'L' AND
        FW_DATE IS NOT NULL;
        
  CURSOR findEmptyEntry IS
    SELECT COUNT(*)
    FROM NEK_APPMON_HIST
    WHERE 
        TO_CHAR(SHIFT_DATE) = TO_CHAR(SHIFT_DATE_) AND
        LOG_ID = LOG_ID_ AND
        SHIFT_ID = SHIFT_ID_ AND
        ENTRY_TYPE = 'L' ;
                 
    FOUND_ENTRY NUMBER;
  BEGIN
    OPEN findEntry;
    FETCH findEntry INTO FOUND_ENTRY;
    CLOSE findEntry;
    
    IF FOUND_ENTRY > 0 THEN
        RETURN FALSE;
    ELSE 
        OPEN findEmptyEntry;
        FETCH findEmptyEntry INTO FOUND_ENTRY;
        CLOSE findEmptyEntry;    
    
        IF FOUND_ENTRY > 0 THEN
            RETURN TRUE;
        ELSE 
            INSERT INTO NEK_APPMON_HIST (LOG_ID, SHIFT_ID, SHIFT_DATE, ENTRY_TYPE) 
            VALUES (LOG_ID_, SHIFT_ID_, SHIFT_DATE_, 'L');
            RETURN TRUE;
        END IF;
   END IF;
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'IS_LOG_FOR_FIRST_SIGN');
      RETURN FALSE;
  END IS_LOG_FOR_FIRST_SIGN; 
  
   FUNCTION IS_LOG_FOR_SECOND_SIGN(LOG_ID_ IN NUMBER, SHIFT_ID_ IN NUMBER, SHIFT_DATE_ IN DATE) RETURN BOOLEAN IS
  /********************************************************************************
  /   IS_LOG_FOR_SECOND_SIGN - Check if the log has not been signed yet for second warning.
  /********************************************************************************/  
   CURSOR findEntry IS
    SELECT COUNT(*)
    FROM NEK_APPMON_HIST
    WHERE 
        LOG_ID = LOG_ID_ AND
        SHIFT_ID = SHIFT_ID_ AND
        TO_CHAR(SHIFT_DATE) = TO_CHAR(SHIFT_DATE_) AND
        SW = '1' AND 
        ENTRY_TYPE = 'L' AND
        SW_DATE IS NOT NULL;
   
   CURSOR findEntryBefore IS
    SELECT COUNT(*)
    FROM NEK_APPMON_HIST
    WHERE 
        LOG_ID = LOG_ID_ AND
        SHIFT_ID = SHIFT_ID_ AND
        TO_CHAR(SHIFT_DATE) = TO_CHAR(SHIFT_DATE_) AND
        FW = '1' AND 
        ENTRY_TYPE = 'L' AND
        FW_DATE IS NOT NULL;
        
    FOUND_ENTRY NUMBER;
  BEGIN
    OPEN findEntry;
    FETCH findEntry INTO FOUND_ENTRY;
    CLOSE findEntry;
    
    IF FOUND_ENTRY > 0 THEN
        RETURN FALSE;
    ELSE 
        OPEN findEntryBefore;
        FETCH findEntryBefore INTO FOUND_ENTRY;
        CLOSE findEntryBefore;
        
        IF (FOUND_ENTRY > 0) THEN
            RETURN TRUE;
        ELSE
            INSERT INTO NEK_APPMON_HIST (LOG_ID, SHIFT_ID, SHIFT_DATE, FW, FW_DATE, ENTRY_TYPE) 
            VALUES (LOG_ID_, SHIFT_ID_, SHIFT_DATE_, '1', SHIFT_DATE_, 'L');
        END IF;        
        RETURN TRUE;
    END IF;
    
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'IS_LOG_FOR_SECOND_SIGN');
      RETURN FALSE;
  END IS_LOG_FOR_SECOND_SIGN; 

   PROCEDURE ADD_TO_FIRST_LOG_QUEUE(LOG_ID_ IN NUMBER, SHIFT_ID_ IN NUMBER, SHIFT_DATE_ IN DATE, SIGNEE_ IN VARCHAR2) IS
  /********************************************************************************
  /   ADD_TO_FIRST_LOG_QUEUE - adds the log to the queue.
  /********************************************************************************/  
  BEGIN
            INSERT INTO NEK_APPMON_QUEUE (LOG_ID, SHIFT_ID, SHIFT_DATE, FW, FW_DATE, SIGNEE, ENTRY_TYPE) 
            VALUES (LOG_ID_, SHIFT_ID_, SHIFT_DATE_, '1', SYSDATE, SIGNEE_, 'L');
            
            UPDATE NEK_APPMON_HIST 
            SET FW = '1', FW_DATE = SYSDATE
            WHERE LOG_ID = LOG_ID_ AND SHIFT_ID = SHIFT_ID_ AND TO_CHAR(SHIFT_DATE) = TO_CHAR(SHIFT_DATE_) 
                AND ENTRY_TYPE='L';
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'ADD_TO_FIRST_LOG_QUEUE');
  END ADD_TO_FIRST_LOG_QUEUE;    

   PROCEDURE ADD_TO_SECOND_LOG_QUEUE(LOG_ID_ IN NUMBER, SHIFT_ID_ IN NUMBER, SHIFT_DATE_ IN DATE, SIGNEE_ IN VARCHAR2) IS
  /********************************************************************************
  /   ADD_TO_SECOND_LOG_QUEUE - adds the round to queue.
  /********************************************************************************/  
  BEGIN
            INSERT INTO NEK_APPMON_QUEUE (LOG_ID, SHIFT_ID, SHIFT_DATE, SW, SW_DATE, SIGNEE, ENTRY_TYPE) 
            VALUES (LOG_ID_, SHIFT_ID_, SHIFT_DATE_, '1', SYSDATE, SIGNEE_, 'L');
            
            UPDATE NEK_APPMON_HIST 
            SET SW = '1', SW_DATE = SYSDATE
            WHERE LOG_ID = LOG_ID_ AND SHIFT_ID = SHIFT_ID_ AND TO_CHAR(SHIFT_DATE) = TO_CHAR(SHIFT_DATE_) 
                AND ENTRY_TYPE='L';
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'ADD_TO_SECOND_LOG_QUEUE');
  END ADD_TO_SECOND_LOG_QUEUE;  

  PROCEDURE PROCESS_MESSAGES_LOGS IS
    /********************************************************************************
  /   PROCESS_MESSAGES_LOGS - Process the Messages from the QUEUE for logs.
  /********************************************************************************/  
   DATA_TYPE_                VARCHAR2(3);
   REC_                          VARCHAR2(35);
   
   CURSOR getRecipientsFW IS 
    SELECT DISTINCT SIGNEE 
    FROM NEK_APPMON_QUEUE
    WHERE FW = '1' AND ENTRY_TYPE = DATA_TYPE_;    

  CURSOR getRecipientsSW IS 
    SELECT DISTINCT SIGNEE 
    FROM NEK_APPMON_QUEUE
    WHERE SW = '1' AND ENTRY_TYPE = DATA_TYPE_;    
  
  CURSOR getSigneeItemsFW IS
    SELECT LOG_ID, SHIFT_ID, SHIFT_DATE
    FROM NEK_APPMON_QUEUE
    WHERE FW ='1' 
        AND ENTRY_TYPE = DATA_TYPE_
        AND SIGNEE = REC_
    ORDER BY SHIFT_DATE ASC, LOG_ID ASC, SHIFT_ID ASC;
  
    CURSOR getSigneeItemsSW IS
    SELECT LOG_ID, SHIFT_ID, SHIFT_DATE
    FROM NEK_APPMON_QUEUE
    WHERE SW ='1' 
        AND ENTRY_TYPE = DATA_TYPE_
        AND SIGNEE = REC_
    ORDER BY SHIFT_DATE ASC, LOG_ID ASC, SHIFT_ID ASC;
   
   loLOG_ID                      NEK_APPMON_QUEUE.LOG_ID%TYPE;
   loSHIFT_ID                    NEK_APPMON_QUEUE.SHIFT_ID%TYPE;
   loSHIFT_DATE               NEK_APPMON_QUEUE.SHIFT_DATE%TYPE;
   
   CRLF CHAR(2) :=  CHR(13) || CHR(10);
   LOG_MESSAGE_     VARCHAR2(4000);
   
   LOG_NAME_ VARCHAR2(100);
   SHIFT_NAME_  VARCHAR2(100);
   FOUND_LOG_ENTRY_ BOOLEAN := FALSE;
  BEGIN
    DATA_TYPE_ := 'L';
       
    OPEN getRecipientsFW;
    LOOP
      FETCH getRecipientsFW INTO REC_;
         IF getRecipientsFW%NOTFOUND
         THEN
            IF ((LOG_VERBOSE = '1') AND FOUND_LOG_ENTRY_ = FALSE) THEN
                NEK_LOG('There are no recipients/overdues for logs with first warning.', 'PROCESS_MESSAGES_LOGS', 'INFO');    
            END IF;
            EXIT;
         ELSE
             FOUND_LOG_ENTRY_ := TRUE;
             LOG_MESSAGE_ := 'Opozorilo!' || CRLF || CRLF || 'Spodaj navedeni dnevniki niso potrjeni.'  || CRLF || CRLF;
             OPEN getSigneeItemsFW;
                LOOP
                  FETCH getSigneeItemsFW INTO loLOG_ID, loSHIFT_ID, loSHIFT_DATE;
                     IF getSigneeItemsFW%NOTFOUND
                     THEN
                        EXIT;
                     ELSE
                        GET_LOG_MESSAGE_ITEM_TEXT(loLOG_ID, loSHIFT_ID, loSHIFT_DATE, LOG_NAME_, SHIFT_NAME_);
                        LOG_MESSAGE_ := LOG_MESSAGE_  || 'Dnevnik: '  || LOG_NAME_  || 
                            ' za dan: '  || TO_CHAR(loSHIFT_DATE, 'dd.mm.yyyy')  || ', izmena: '  || SHIFT_NAME_  ||  CRLF;  
                     END IF;
                END LOOP;
                CLOSE getSigneeItemsFW;
                
                IF (CREATE_NOC = '1') THEN
                    LOG_MESSAGE_ := LOG_MESSAGE_  || CRLF; 
                    CREATE_NEW_NOC(REC_, 'Opozorilo - dnevniki', LOG_MESSAGE_, 'Potrdite dnevnike.', LOG_FIRST_WARN_TYPE, TRUE, 'L');             
                END IF;
                
         END IF;
    END LOOP;
    CLOSE getRecipientsFW;

    FOUND_LOG_ENTRY_ := FALSE;
    OPEN getRecipientsSW;
    LOOP
      FETCH getRecipientsSW INTO REC_;
         IF getRecipientsSW%NOTFOUND
         THEN
            IF ((LOG_VERBOSE = '1') AND FOUND_LOG_ENTRY_ = FALSE) THEN
                NEK_LOG('There are no recipients/overdues for logs with second warning.', 'PROCESS_MESSAGES_LOGS', 'INFO');    
            END IF;
            EXIT;
         ELSE
             FOUND_LOG_ENTRY_ := TRUE;
             LOG_MESSAGE_ := 'Opozorilo!!' || CRLF || CRLF || 'Spodaj navedeni dnevniki �e zmeraj niso potrjeni!!'  || CRLF || CRLF;
             OPEN getSigneeItemsSW;
                LOOP
                  FETCH getSigneeItemsSW INTO loLOG_ID, loSHIFT_ID, loSHIFT_DATE;
                     IF getSigneeItemsSW%NOTFOUND
                     THEN
                        EXIT;
                     ELSE
                        GET_LOG_MESSAGE_ITEM_TEXT(loLOG_ID, loSHIFT_ID, loSHIFT_DATE, LOG_NAME_, SHIFT_NAME_);
                        LOG_MESSAGE_ := LOG_MESSAGE_  || 'Dnevnik: '  || LOG_NAME_  || 
                            ' za dan: '  || TO_CHAR(loSHIFT_DATE, 'dd.mm.yyyy')  || ', izmena: '  || SHIFT_NAME_  ||  CRLF;   
                     END IF;
                END LOOP;
                CLOSE getSigneeItemsSW;
                       
                IF (CREATE_NOC = '1') THEN
                    LOG_MESSAGE_ := LOG_MESSAGE_  || CRLF; 
                    CREATE_NEW_NOC(REC_, 'Opozorilo - dnevniki', LOG_MESSAGE_, 'Potrdite dnevnike.', LOG_SECOND_WARN_TYPE, FALSE, 'L');             
                END IF;
                
                IF (LOG_SECOND_WARN_SEND_MAIL = '1') THEN
                    SEND_EMAIL(REC_, EMAIL_LOG_SUB, LOG_MESSAGE_);    
                END IF;        
       END IF;
    END LOOP;
    CLOSE getRecipientsSW;
  
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'PROCESS_MESSAGES_LOGS');
  END PROCESS_MESSAGES_LOGS;
  
  PROCEDURE GET_LOG_MESSAGE_ITEM_TEXT(LOG_ID_ IN NUMBER, SHIFT_ID_ IN NUMBER, SHIFT_DATE_ IN DATE, DESC_ OUT VARCHAR2, NAME_ OUT VARCHAR2)  IS
   /********************************************************************************
  /   GET_LOG_MESSAGE_ITEM_TEXT - Returns specified text for an log item.
  /********************************************************************************/   
  CURSOR getDescAndName IS 
    SELECT LOG_TITLE, SHIFT_DESCR
    FROM SHIFT_APPROVAL_LIST sal
    INNER JOIN log_list ll ON ll.LOG_ID = sal.LOG_ID
    INNER JOIN shift_list sl ON sl.LOG_ID = sal.LOG_ID AND sl.SHIFT_ID = sal.SHIFT_ID
    WHERE
        ll.LOG_ID = LOG_ID_ AND
        SHIFT_ID = SHIFT_ID_ AND
        TO_CHAR(SHIFT_DATE) = TO_CHAR(SHIFT_DATE_);
  BEGIN
    OPEN getDescAndName;
    FETCH getDescAndName INTO DESC_, NAME_;
    CLOSE getDescAndName;
     
  EXCEPTION
      WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'GET_LOG_MESSAGE_ITEM_TEXT');
  END GET_LOG_MESSAGE_ITEM_TEXT;    

  PROCEDURE NEK_LOG (VALUE_ IN VARCHAR2, LOC_ IN VARCHAR2)
   IS
      /********************************************************************************
      \ NEK_LOG - function logs inproper interface functionality. All exceptions are written through log function.
      ********************************************************************************/
   PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      INSERT INTO NEK_LOG_MAIN (LOG_DATETIME,
                                LOG_TYPE,
                                LOG_OWNER,
                                LOG_LOCATION,
                                LOG_VALUE,
                                LOG_DESCRIPTION)
           VALUES (SYSTIMESTAMP,
                   'ERR',
                   'APPMON',
                   LOC_,
                   TRIM (SUBSTR (VALUE_, 1, 4000)),
                   TRIM (SUBSTR (LOC_, 1, 1000)));
      NULL;
      COMMIT;                                         
   END NEK_LOG;            
  PROCEDURE NEK_LOG (VALUE_ IN VARCHAR2, LOC_ IN VARCHAR2, TYPE_ IN VARCHAR2)
   IS
      /********************************************************************************
      \ NEK_LOG - function logs inproper interface functionality. All exceptions are written through log function.
      ********************************************************************************/
   PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      INSERT INTO NEK_LOG_MAIN (LOG_DATETIME,
                                LOG_TYPE,
                                LOG_OWNER,
                                LOG_LOCATION,
                                LOG_VALUE,
                                LOG_DESCRIPTION)
           VALUES (SYSTIMESTAMP,
                   TYPE_,
                   'APPMON',
                   LOC_,
                   TRIM (SUBSTR (VALUE_, 1, 4000)),
                   TRIM (SUBSTR (LOC_, 1, 1000)));
      NULL;
      COMMIT;                                         
   END NEK_LOG;        
   PROCEDURE CLEAR_CONFIRMED_NOC
   IS
      /********************************************************************************
      \ CLEAR_CONFIRMED_NOC - clears all confirmed NOCs to completed
      ********************************************************************************/
   BEGIN

        UPDATE NOTICE_OF_CHANGE noc
        SET
            NOTICE_OF_CHANGE_COMPLETED_BY = NOC_INSERT_USER,
            NOTICE_OF_CHANGE_COMPLETED_DT = SYSDATE
        WHERE 
            (NOTICE_OF_CHANGE_TYPE_ID = ROUND_SECOND_WARN_FOLDER OR 
                NOTICE_OF_CHANGE_TYPE_ID = LOG_SECOND_WARN_FOLDER)
            AND (SELECT COUNT(*) FROM 
                    USER_MESSAGES um
                    WHERE 
                        um.NOTICE_OF_CHANGE_ID = noc.NOTICE_OF_CHANGE_ID
                        AND um.MESSAGE_ACKNOWLEDGED IS NOT NULL)
                    > 0
             AND noc.NOTICE_OF_CHANGE_COMPLETED_BY IS NULL
             AND noc.NOTICE_OF_CHANGE_COMPLETED_DT IS NULL;
                                 
   END CLEAR_CONFIRMED_NOC;     
      
END NEK_PACKAGE_APPMON;
/