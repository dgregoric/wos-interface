CREATE OR REPLACE PACKAGE ESM_DEV_ITF.NEK_INTERFACE_MEL_TO_ESOMS AS
/******************************************************************************
   This is general comment header for MEL-eSOMS interface at NEK and it is   
 valid packages NEK_INTERFACE_MEL_TO_ESOMS and NEK_INTERFACE_CLEANUP.    
 Those two pacakages forms eSOMS part of MEL-eSOMS interface at NEK.         
   This general header has a general notes and history about MEL-eSOMS       
 interface. Specific notes and history for items (procedures, functions,     
 etc.) in interface shall be writen comment header in those items            
*******************************************************************************
 NAME:    NEK MEL to eSOMS Equipment Database interface package              
 PURPOSE: This package contains procedures and functions to implement        
          specified functionality of the MEL->ESOMS interface.
 SUPPORITNG DOCUMENTS: - NEK-P-L-2-1208-1; Interface Specification MEL to eSOMS, 
                                           revision 1.0, January 2009.               
 DESCRIPTOIN: Main (start) procedure: START_TRANSFER                         
              Interface schema:       /                                      
              Interface table:        MEL_VW_EQUIP_TO_ESOMS                  
              Log table:              MEL_TU_LOG                             
              eSOMS schema:   eSOMS ADM user:           eSOMS ITF User:  
                              --------------------      -------------------          
              eSOMS tables:   EQUIP                     MEL_TU_LOG
                              EQUIP_DOCUMENTS
                              PLANT_TYPE
                              SYSTEM_TYPE
                              BUILDING_TYPE
                              ELEVATION_TYPE
                              COLUMN_TYPE
                              ROOM_TYPE
                              COMPONENT_TYPE
                              CONFIG_TYPE
                              TRAIN_TYPE
                              TAG_SIZES                                                                                          
 INITIAL AUTHOR: CENTRADESIGN d.o.o.                                         
                 MARKO CLEMENTE, marko.clemente@centradesign.net             
                 LJUBLJANA , 9.1.2009                                        
 INTERFACE REVISIONS:                                                                  
 Rev.  Date        Author           Description                                     
 ----  ----------  ---------------  -------------------------------------------
 1.0   16.6.2009   M. Clemente      Spremenjen pogoj v DELETE_INTF, sinhro. z 
                                    NEK.  
       18.6.2009   M. Clemente      Dodano izvajanje CleanUp funkcije 
                                    avtomatsko ob zagonu vmesnika.                                
       22.6.2009   M. Clemente      Dodan Commit za brisanje vrstic tudi takrat,    
                                    ko ni obdelave.                                
 2.0   22.6.2009   M. Clemente      - Dodana baza MEL_TU_LOG namenjena logiranju   
                                    - Odstranjeni virtualni direktoriji (aliasi) 
                                      za tektovne loge.                                
                                    - Spremenjene funkcije za logiranje            
 2.1   28.7.2009   M. Clemente      - Popravek procedure za oznacevanje vrstic
                                      za izbris.                                       
 3.0   2.11.2009   M. Clemente      - Odstranjeni commit stavki -- optimizacija    
                                      hitrosti.                                     
 3.1   23.1.2010   D. Gregoric      Prilagojeni komantarji paketa in komentarji    
                                    programov.
 4.0   3.2.2010    D. Gregoric      Popravljena konverzija elevacij, da se 
                                    zagotovi vejica kot decimalni znak 
 4.1   2.6.2011    D. Gregoric      Z vspostavitvijo povezave z eBS se je iskazalo, 
                                    da je URL polje (150 dol�ine) prekratko. v eSOMS
                                    je to polje dol�ine 500. Deklaracije so se 
                                    popravile na sklicevanje na eSOMS.
 4.2  23.12.2011   D.Gregoric       V niz za lokacijo je dodano equipment elevacija
                                    Sprememba v INSERT_EQUIPMENT in UPDATE_EQUIPMENT.                                                                      
 WARNIRNG: Before interface run please check schema/table names.
 NOTES: /                                    
******************************************************************************/

    PROCEDURE START_TRANSFER;
    PROCEDURE CHECK_ID(KEY_ IN MEL_VW_EQUIP_TO_ESOMS.EQUIP_NO%TYPE, RES_ OUT Boolean);
    PROCEDURE UPDATE_EQUIPMENT(KEY_ IN MEL_VW_EQUIP_TO_ESOMS.EQUIP_NO%TYPE);
    PROCEDURE INSERT_EQUIPMENT(KEY_ IN MEL_VW_EQUIP_TO_ESOMS.EQUIP_NO%TYPE);
    PROCEDURE SET_EQUIP_TO_VOID(KEY_ IN MEL_VW_EQUIP_TO_ESOMS.EQUIP_NO%TYPE, SET_VALUE IN BOOLEAN);
    PROCEDURE NEW_PKEY(OLD_KEY_ IN MEL_VW_EQUIP_TO_ESOMS.EQUIP_NO%TYPE, KEY_ IN MEL_VW_EQUIP_TO_ESOMS.EQUIP_NO%TYPE);
    PROCEDURE CHECK_LOOKUP_DATA(KEY_ IN MEL_VW_EQUIP_TO_ESOMS.EQUIP_NO%TYPE, INSERTING_ IN BOOLEAN);
    FUNCTION  GENERATE_ADDR(KEY_ IN MEL_VW_EQUIP_TO_ESOMS.EQUIP_NO%TYPE) RETURN VARCHAR2;
    PROCEDURE WRITE_ERROR_CODE(ERROR_ IN VARCHAR2, KEY_ IN MEL_VW_EQUIP_TO_ESOMS.EQUIP_NO%TYPE); 
    PROCEDURE WRITE_EXECUTION_LOG(KEY_ IN VARCHAR, MESSAGE_ IN VARCHAR);
    PROCEDURE DELETE_INTF(VAL_ IN INTEGER);    
    PROCEDURE WRITE_SUMMARY_LOG(SUMMARY_ IN VARCHAR); 
END NEK_INTERFACE_MEL_TO_ESOMS;
/