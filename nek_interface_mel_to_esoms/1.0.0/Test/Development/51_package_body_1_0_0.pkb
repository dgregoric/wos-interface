CREATE OR REPLACE PACKAGE BODY NEK_INTERFACE_MEL_TO_ESOMS AS
    
 -- GLOBAL VARIABLES FOR SUMMARY REPORT DEFINITIONS    
    COUNT_OK    INT;  -- SUCESSFULL OPERATIONS COUNT
    COUNT_UPD   INT;  -- UPDATE OPERATIONS COUNT
    COUNT_INS   INT;  -- INSERT OPERATIONS COUNT
    COUNT_ERR   INT;  -- ERRORS COUNT

PROCEDURE START_TRANSFER IS
/*****************************************************************************
 START_TRANSFER:   This is main procedure of the package and should be the   
                   only procedure ever called from outside the interface     
                   object.                                                   
 REVISIONS:                                                                  
 Date        Author           Description                                     
 ----------  ---------------  -----------------------------------------------
 23.1.2010    D. Gregoric      Modified dcmment header.                      
******************************************************************************/
 
 -- CURSOR RETURNS ALL ROWS FROM INTERFACE TABLE THAT HAVE NOT YET BEEN PROCESSED
 -- AND ARE SORTED BY THE ENTRY_SEQUIENCE_NO.           
    CURSOR RET_NEW_INT_ROWS IS                  
        SELECT EQUIP_NO 
        FROM MEL_VW_EQUIP_TO_ESOMS
        WHERE EQUIP_NO IS NOT NULL 
        AND (PROCESSED IS NULL OR PROCESSED != 'T')
        ORDER BY ENTRY_SEQUENCE_NO;
    EQUIP_IS_NEW    BOOLEAN := FALSE;
    FETCHED_KEY     MEL_VW_EQUIP_TO_ESOMS.EQUIP_NO%TYPE;
          
BEGIN
 -- BEFORE WE DO ANYTHING LETS JUST CLEAN THE OLD ROWS 
    NEK_INTERFACE_CLEANUP.DELETE_OLD_ROWS();
    
    COUNT_OK := 0;
    COUNT_UPD:= 0;
    COUNT_INS:= 0;
    COUNT_ERR:= 0;
 
    WRITE_SUMMARY_LOG('Zacetek zagona '|| TO_CHAR(SYSTIMESTAMP,'DD.MM.YYYY HH24:MI') ||'.');

 -- READ ALL ENTRIES THAT DON'T HAVE PROCESSED = 'T' AND ORDER BY SEQ.
    OPEN RET_NEW_INT_ROWS;                      
    FETCH RET_NEW_INT_ROWS INTO FETCHED_KEY;
    
    IF NOT RET_NEW_INT_ROWS%NOTFOUND THEN
        LOOP
         -- FIRST CHECK IF THAT'S AN EXISTING EQUIPMENT OR NOT.     
            CHECK_ID(FETCHED_KEY, EQUIP_IS_NEW);
         -- IF EQUIP_IS_NEW THEN WE HAVE A NEW PIECE AND ARE INSERTING 
         -- OTHERWISE WE'RE UPDATING EXISTING PIECE
            IF EQUIP_IS_NEW = TRUE THEN         
                INSERT_EQUIPMENT(FETCHED_KEY);
            ELSE 
             -- TODO: CHECK IF MEL DELETED PIECE AND WE HAVE TO REMOVE 
             --       IT COMPLETELY
                UPDATE_EQUIPMENT(FETCHED_KEY);
            END IF;
        
         -- GET THE NEXT ROW
            FETCH RET_NEW_INT_ROWS INTO FETCHED_KEY;
            EXIT WHEN RET_NEW_INT_ROWS%NOTFOUND;
        END LOOP;
    END IF;
 -- ALL THE ROWS ARE PROCESSED AT THIS POINT. WRITE THE SUMMARY.
 -- IF THERE WAS AN ERROR SOMEWHERE A DIFFERENT SUMMARY IS NEEDED.
    IF COUNT_ERR = 0 THEN
       WRITE_SUMMARY_LOG(
         'Izvedeno brez napak. Posodobljeno '|| TO_CHAR(COUNT_UPD) || ' vrstic. Vstavljenih '|| TO_CHAR(COUNT_INS) ||' novih vrstic.' || CHR(13) || CHR(10) ||
         'Konec zagona '|| TO_CHAR(SYSTIMESTAMP,'DD.MM.YYYY HH24:MI') ||'.' || CHR(13) || CHR(10));   
    ELSE
 -- AND THE GOOD ONE
       WRITE_SUMMARY_LOG(
         'Izvedeno Z NAPAKAMI. Posodobljeno '|| TO_CHAR(COUNT_UPD) || ' vrstic. Vstavljenih '|| TO_CHAR(COUNT_INS) ||' novih vrstic. ' ||
         'Med izvajanjem je pri�lo do '|| TO_CHAR(COUNT_ERR) ||' napak.'|| CHR(13) || CHR(10) || 'Konec zagona '|| TO_CHAR(SYSTIMESTAMP,'DD.MM.YYYY HH24:MI') ||'.' || CHR(13) || CHR(10));    
    END IF;
 
 -- JUST CHECK THE OLD ROWS FOR POSSIBLE CLEAN UP TAGGING.
 -- THE DEFAULT DAYS TO LEAVE DATA ROWS IN TABLE IS 31.   
    DELETE_INTF(31);  
    COMMIT;
          
END;
PROCEDURE CHECK_ID(KEY_ IN MEL_VW_EQUIP_TO_ESOMS.EQUIP_NO%TYPE, RES_ OUT Boolean) IS
/******************************************************************************
 CHECK_ID: Check_ID procedure return boolean value as a result of a check   
           on the existance of the passed key (equipment) in the eSOMS table.
 REVISIONS:                                                                 
 Date        Author           Description                                    
 ----------  ---------------  -----------------------------------------------
 23.1.2010    D. Gregoric      Modified dcmment header.                     
*******************************************************************************/
    
    CURSOR ESOMS_EQUIPMENT IS
        SELECT EQUIP_OPERATOR_ID 
        FROM EQUIP
        WHERE EQUIP_OPERATOR_ID = KEY_;       
    ESOMS_FETCHED_KEY   EQUIP.EQUIP_OPERATOR_ID%TYPE;
BEGIN

    OPEN ESOMS_EQUIPMENT;
    FETCH ESOMS_EQUIPMENT INTO 
        ESOMS_FETCHED_KEY;
    IF ESOMS_FETCHED_KEY IS NULL THEN
        RES_ := true;
    ELSE
        RES_ := false;
    END IF;

EXCEPTION
    WHEN OTHERS THEN 
        WRITE_ERROR_CODE(SQLERRM || ' (' || SQLCODE || ').', '');
END CHECK_ID;

PROCEDURE UPDATE_EQUIPMENT(KEY_ IN MEL_VW_EQUIP_TO_ESOMS.EQUIP_NO%TYPE) IS
/******************************************************************************
 UPDATE_EQUIPMENT: Equipment is updated here. KEY is sent as a parameter for
                  an eSOMS table.                                             
 REVISIONS:                                                                 
 Date        Author           Description                                    
 ----------  ---------------  -----------------------------------------------
 23.1.2010   D. Gregoric      Modified comment header.
 3.2.2010    D. Gregoric      Popravljena konverzija elevacij, da se 
                              zagotovi vejica kot decimalni znak. 
 2.6.2011    D. Gregoric      Spremenjena deklaracija za URL.  
 23.12.2011  D. Gregoric      Dodana elevacija opreme v niz EQUIP_LOC                                                
*******************************************************************************/
 
 -- IF THIS CURSOR IS NOT NULL THEN EQUIP IS VOIDED    
    CURSOR RET_VOID_EQUIP IS                            
        SELECT VOID 
        FROM MEL_VW_EQUIP_TO_ESOMS
        WHERE EQUIP_NO = KEY_ 
        AND VOID IS NOT NULL
        AND PROCESSED IS NULL
        ORDER BY ENTRY_SEQUENCE_NO
        FOR UPDATE;
        
 -- GETS THE PASSED URL   
    CURSOR RET_EQUIP_URL IS 
        SELECT EQUIP_URL 
        FROM MEL_VW_EQUIP_TO_ESOMS
        WHERE EQUIP_NO = KEY_ 
        AND (PROCESSED IS NULL OR PROCESSED != 'T')
        ORDER BY ENTRY_SEQUENCE_NO;        
    --EQUIPMENT_URL_  VARCHAR(150);
    EQUIPMENT_URL_ EQUIP_DOCUMENTS.DOCUMENT_FILENAME%type;
    
 -- GETS CURRENT FREEFORM LOCATION
    CURSOR RET_FREEFORM_LOC IS
        SELECT FREEFORM_LOC
        FROM EQUIP
        WHERE EQUIP_OPERATOR_ID = KEY_;
    RET_FREEFORM_LOC_ VARCHAR2(150);
    
 -- GETS THE WHOLE DATA ROW FROM INTERFACE TABLE  
    CURSOR RET_EQUIP IS  
        SELECT  
                UNIT_ID,
                FUNCTIONAL_DESCRIPTION,
                LABEL,
                SYSTEM_CODE,
                SYSTEM_DESC,
                SAFETY_RELATED,
                BUILDING_ID,
                BUILDING_DESC,
                BUILDING_ROOM_ELEVATION,
                EQUIP_ELEVATION,
                ROOM_NO,
                ROOM_DESC,
                WIRE_MARK,
                EQUIP_CATEGORY,
                EQUIP_CATEGORY_DESC,
                OPERATOR,
                OPERATOR_DESC,
                EQUIP_STATUS_CODE,
                ELECTRIC_TRAIN,
                TAG_SIZE_CODE,
                TAG_SIZE_DESC
        FROM MEL_VW_EQUIP_TO_ESOMS
        WHERE EQUIP_NO = KEY_
        AND (PROCESSED IS NULL OR PROCESSED != 'T')
        ORDER BY ENTRY_SEQUENCE_NO
        FOR UPDATE;
    UNIT_ID_            EQUIP.PLANT_ID%TYPE;
    FUNC_DESC_          EQUIP.EQUIP_ATTRIBUTE_01%TYPE;
    LABEL_              EQUIP.EQUIP_DESCRIPTION%TYPE;
    SYSTEM_CODE_        EQUIP.SYSTEM_ID%TYPE;
    SYSTEM_DESCRIPTION_ SYSTEM_TYPE.SYSTEM_DESC%TYPE;
    SAFETY_RELATED_     MEL_VW_EQUIP_TO_ESOMS.SAFETY_RELATED%TYPE;
    BUILDING_ID_        EQUIP.BUILDING_ID%TYPE;
    BUILDING_DESC_      BUILDING_TYPE.BUILDING_DESC%TYPE; 
    NUM_B_ROOM_ELEV_    MEL_VW_EQUIP_TO_ESOMS.BUILDING_ROOM_ELEVATION%TYPE;
    NUM_EQUIPMENT_ELEV_ MEL_VW_EQUIP_TO_ESOMS.EQUIP_ELEVATION%TYPE;       
    BUILDING_ROOM_ELEV_ CHAR(10);
    EQUIP_ELEVATION_    EQUIP.COLUMN_ID%TYPE;
    ROOM_NO_            EQUIP.ROOM_ID%TYPE;   
    ROOM_DESC_          ROOM_TYPE.ROOM_DESC%TYPE;
    WIRE_MARK_          EQUIP.EQUIP_ATTRIBUTE_03%TYPE;
    EQUIP_CATEGORY_     EQUIP.COMPONENT_ID%TYPE;
    EQUIP_CAT_DESC_     COMPONENT_TYPE.COMPONENT_DESC%TYPE;
    OPERATOR_           EQUIP.EQUIP_ATTRIBUTE_04%TYPE;
    OPERATOR_DESC_      EQUIP.EQUIP_ATTRIBUTE_04%TYPE;   
    EQUIP_STATUS_CODE_  EQUIP.EQUIP_ATTRIBUTE_05%TYPE;
    ELECTRIC_TRAIN_     EQUIP.TRAIN_ID%TYPE;
    TAG_SIZE_CODE_      EQUIP.TAG_SIZE%TYPE;
    TAG_SIZE_DESC_      TAG_SIZES.TAG_SIZE_DESCRIPTION%TYPE;
 -- STRING USED TO CONCATINATE OPERATOR INFO 
    OPER_CONC       VARCHAR2(150);
    IS_EQUIP_VOID   BOOLEAN := FALSE;
 -- STRING USED TO CONCATINATE PROPER ROOM_ID 
    CONC_ROOM_ID    VARCHAR2(150);
    VOID_KEY        MEL_VW_EQUIP_TO_ESOMS.EQUIP_NO%TYPE;
    EQUIP_LOC       VARCHAR2(150);
        
BEGIN
    WRITE_EXECUTION_LOG(KEY_,'Posodabljam napravo: ' || KEY_);

    OPEN RET_VOID_EQUIP;
    FETCH RET_VOID_EQUIP INTO VOID_KEY;
    
 -- 1. CHECK IF THE EQUIP. IS VOIDED   
    IF RET_VOID_EQUIP%ROWCOUNT != 0 THEN   
        
        IF  VOID_KEY = 'T' THEN         
            IS_EQUIP_VOID := TRUE;
            SET_EQUIP_TO_VOID(KEY_,TRUE);
        
         -- 1.3 MARK THAT ROW AS PROCESSED              
            UPDATE MEL_VW_EQUIP_TO_ESOMS
            SET PROCESSED = 'T',
                PROCESS_DATE = SYSTIMESTAMP
            WHERE CURRENT OF RET_VOID_EQUIP;

            COUNT_UPD := COUNT_UPD + 1;
        ELSE 
            IS_EQUIP_VOID := FALSE;
            SET_EQUIP_TO_VOID(KEY_,FALSE);
        END IF;
                     
        -- WE END WITH AN UPDATE HERE
    END IF;
    
   IF IS_EQUIP_VOID = FALSE THEN
   
     -- 2. CHECK ALL LOOKUP TABLES IN ESOMS. 
        CHECK_LOOKUP_DATA(KEY_, FALSE); 
        
     -- 3. UPDATE EQUIPMENT IN ESOMS    
        OPEN RET_EQUIP;                 
        FETCH RET_EQUIP INTO 
            UNIT_ID_,
            FUNC_DESC_,
            LABEL_,
            SYSTEM_CODE_,
            SYSTEM_DESCRIPTION_,
            SAFETY_RELATED_,
            BUILDING_ID_, 
            BUILDING_DESC_,
            NUM_B_ROOM_ELEV_,
            NUM_EQUIPMENT_ELEV_,
            ROOM_NO_,
            ROOM_DESC_,
            WIRE_MARK_,
            EQUIP_CATEGORY_,
            EQUIP_CAT_DESC_,
            OPERATOR_,
            OPERATOR_DESC_,
            EQUIP_STATUS_CODE_,
            ELECTRIC_TRAIN_,
            TAG_SIZE_CODE_,
            TAG_SIZE_DESC_;

         -- THIS IS FOR THE CONVERSION PURPOSES
         -- Popravljena konverzija elevacij, da se  zagotovi vejica kot.
         -- decimalni znak.
            BUILDING_ROOM_ELEV_ := NUM_B_ROOM_ELEV_;
            BUILDING_ROOM_ELEV_ := TRANSLATE (BUILDING_ROOM_ELEV_, '.', ',');
            
            EQUIP_ELEVATION_    := NUM_EQUIPMENT_ELEV_; 
            EQUIP_ELEVATION_    := TRANSLATE (EQUIP_ELEVATION_, '.', ',');
            
        -- GET FREEFORM LOCATION    
        OPEN RET_FREEFORM_LOC;
        FETCH RET_FREEFORM_LOC INTO RET_FREEFORM_LOC_;
        CLOSE RET_FREEFORM_LOC;            
                      
     -- EVERY FIELD GETS CHECKED AND IF NECESSARY UPDATED       
        IF UNIT_ID_ IS NOT NULL THEN 
            UPDATE EQUIP SET PLANT_ID            = TRIM(UNIT_ID_) WHERE EQUIP_OPERATOR_ID = TRIM(KEY_); 
            WRITE_EXECUTION_LOG(KEY_,'  - Unit ('|| UNIT_ID_ || ') OK.');        
        END IF;
        
        IF FUNC_DESC_ IS NOT NULL THEN 
            UPDATE EQUIP SET EQUIP_ATTRIBUTE_01  = TRIM(SUBSTR(FUNC_DESC_,1,150)) WHERE EQUIP_OPERATOR_ID = TRIM(KEY_); 
            WRITE_EXECUTION_LOG(KEY_,'  - Func des. ('|| SUBSTR(FUNC_DESC_,1,10) || ') OK.');                
        END IF;
        
        IF LABEL_ IS NOT NULL THEN 
            UPDATE EQUIP SET EQUIP_DESCRIPTION   = TRIM(SUBSTR(LABEL_,1,150)) WHERE EQUIP_OPERATOR_ID = TRIM(KEY_); 
            WRITE_EXECUTION_LOG(KEY_,'  - Equip des. ('|| SUBSTR(LABEL_,1,10) || ') OK.');                        
        END IF;
        
        IF SYSTEM_CODE_ IS NOT NULL THEN 
            UPDATE EQUIP SET SYSTEM_ID           = TRIM(SUBSTR(SYSTEM_CODE_,1,10)) WHERE EQUIP_OPERATOR_ID = TRIM(KEY_); 
            WRITE_EXECUTION_LOG(KEY_,'  - Sys ID ('|| SYSTEM_CODE_ ||') OK.');                        
        END IF;
        
--        IF SYSTEM_CODE_ IS NOT NULL AND SYSTEM_DESCRIPTION_ IS NOT NULL THEN
--            UPDATE 
--                SYSTEM_TYPE 
--            SET 
--                SYSTEM_DESC = SUBSTR(SYSTEM_DESCRIPTION_,1,40) 
--            WHERE 
--                TRIM(SYSTEM_ID) = SUBSTR(SYSTEM_CODE_,1,10);
--            WRITE_EXECUTION_LOG(KEY_,'  - Tabela SYSTEM ima vrednost. Posodabljam z:'|| SUBSTR(SYSTEM_DESCRIPTION_,1,40) || SUBSTR(SYSTEM_,1,10));             
--        END IF;
                
        IF SAFETY_RELATED_ IS NOT NULL THEN
            IF SAFETY_RELATED_ = 'Y' THEN 
                UPDATE EQUIP SET MAINTENANCE_RULE_RELATED = 1 WHERE EQUIP_OPERATOR_ID = TRIM(KEY_);
                WRITE_EXECUTION_LOG(KEY_,'  - Safety rel. (1) OK.');    
            ELSE 
                UPDATE EQUIP SET MAINTENANCE_RULE_RELATED = 0 WHERE EQUIP_OPERATOR_ID = TRIM(KEY_);
                WRITE_EXECUTION_LOG(KEY_,'  - Safety rel. (0) OK.');   
            END IF;                          
        END IF;
        
        IF BUILDING_ID_ IS NOT NULL THEN 
            UPDATE EQUIP SET BUILDING_ID         = TRIM(SUBSTR(BUILDING_ID_,1,10)) WHERE EQUIP_OPERATOR_ID = TRIM(KEY_); 
            WRITE_EXECUTION_LOG(KEY_,'  - Building ID ('|| SUBSTR(BUILDING_ID_,1,10) ||') OK.');                        
        END IF;
        
        IF BUILDING_ROOM_ELEV_ IS NOT NULL THEN 
            UPDATE EQUIP SET ELEVATION_ID        = TRIM(SUBSTR(BUILDING_ROOM_ELEV_,1,5)) WHERE EQUIP_OPERATOR_ID = TRIM(KEY_); 
            WRITE_EXECUTION_LOG(KEY_,'  - Build. Room el. ('|| SUBSTR(BUILDING_ROOM_ELEV_,1,5) ||') OK.');                        
        END IF;

        IF EQUIP_ELEVATION_ IS NOT NULL THEN 
            UPDATE EQUIP SET COLUMN_ID           = TRIM(SUBSTR(EQUIP_ELEVATION_,1,10)) WHERE EQUIP_OPERATOR_ID = TRIM(KEY_); 
            WRITE_EXECUTION_LOG(KEY_,'  - Equip. elev. ('|| SUBSTR(EQUIP_ELEVATION_,1,10) ||') OK.');                
        END IF;
        
        CONC_ROOM_ID := TRIM(SUBSTR(BUILDING_ID_,1,10)) || '' || TRIM(SUBSTR(ROOM_NO_,1,10));    
        
        IF BUILDING_ID_ IS NOT NULL OR ROOM_NO_ IS NOT NULL THEN 
            UPDATE EQUIP SET ROOM_ID             = TRIM(SUBSTR(CONC_ROOM_ID,1,20)) WHERE EQUIP_OPERATOR_ID = TRIM(KEY_); 
            WRITE_EXECUTION_LOG(KEY_,'  - Build. ID ('|| SUBSTR(CONC_ROOM_ID,1,10) || ') OK.');                
        END IF;

        IF WIRE_MARK_ IS NOT NULL THEN 
            UPDATE EQUIP SET EQUIP_ATTRIBUTE_03  = TRIM(SUBSTR(WIRE_MARK_,1,150)) WHERE EQUIP_OPERATOR_ID = TRIM(KEY_); 
            WRITE_EXECUTION_LOG(KEY_,'  - Wire mark ('|| SUBSTR(WIRE_MARK_,1,10) || ') OK.');                
        END IF;

        IF EQUIP_CATEGORY_ IS NOT NULL THEN 
            UPDATE EQUIP SET COMPONENT_ID        = TRIM(SUBSTR(EQUIP_CATEGORY_,1,20)) WHERE EQUIP_OPERATOR_ID = TRIM(KEY_); 
            WRITE_EXECUTION_LOG(KEY_,'  - Equip cat. ('|| SUBSTR(EQUIP_CATEGORY_,1,10) || ') OK.');                        
        END IF;
           
        OPER_CONC := TRIM(OPERATOR_) || ' - ' || TRIM(OPERATOR_DESC_);
        IF OPERATOR_ IS NOT NULL OR OPERATOR_DESC_ IS NOT NULL THEN 
            UPDATE EQUIP SET EQUIP_ATTRIBUTE_04  = TRIM(SUBSTR(OPER_CONC,1,150)) WHERE EQUIP_OPERATOR_ID = TRIM(KEY_); 
            WRITE_EXECUTION_LOG(KEY_,'  - Oper. ('|| SUBSTR(OPER_CONC,1,10) || ') OK.');                
        END IF;

        IF EQUIP_STATUS_CODE_ IS NOT NULL THEN
            IF EQUIP_STATUS_CODE_ = 'INSTLD' OR 
               EQUIP_STATUS_CODE_ = 'ACTIVE' OR
               EQUIP_STATUS_CODE_ = 'PRELIM' 
            THEN 
                UPDATE EQUIP SET DISABLED        = 0 WHERE EQUIP_OPERATOR_ID = TRIM(KEY_); 
                WRITE_EXECUTION_LOG(KEY_,'  - Equip code (1) (0) OK.');                
            ELSE 
                UPDATE EQUIP SET DISABLED        = 1 WHERE EQUIP_OPERATOR_ID = TRIM(KEY_);
                WRITE_EXECUTION_LOG(KEY_,'  - Equip code (1) (1) OK.');                
            END IF;
        
            UPDATE EQUIP SET EQUIP_ATTRIBUTE_05  = TRIM(SUBSTR(EQUIP_STATUS_CODE_,1,10)) WHERE EQUIP_OPERATOR_ID = TRIM(KEY_);
            WRITE_EXECUTION_LOG(KEY_,'  - Equip code (2) ('|| SUBSTR(EQUIP_STATUS_CODE_,1,10) ||') OK.');                            
        END IF;
        
        IF ELECTRIC_TRAIN_ IS NOT NULL THEN 
            UPDATE EQUIP SET TRAIN_ID            = TRIM(SUBSTR(ELECTRIC_TRAIN_,1,3)) WHERE EQUIP_OPERATOR_ID = TRIM(KEY_); 
            WRITE_EXECUTION_LOG(KEY_,'  - El. train ('|| SUBSTR(ELECTRIC_TRAIN_,1,3) ||') OK.');                
        END IF;

        IF TAG_SIZE_CODE_ IS NOT NULL THEN 
            UPDATE EQUIP SET TAG_SIZE            = TRIM(TAG_SIZE_CODE_) WHERE EQUIP_OPERATOR_ID = TRIM(KEY_); 
            WRITE_EXECUTION_LOG(KEY_,'  - Tag ID ('|| TAG_SIZE_CODE_ || ') OK.');                
        ELSE
            UPDATE EQUIP SET TAG_SIZE            = 'L' WHERE EQUIP_OPERATOR_ID = TRIM(KEY_);
            WRITE_EXECUTION_LOG(KEY_,'  - Tag ID (L) OK.');         
        END IF;

        -- Dodana elevacija opreme v niz za lokacijo. D. Gregoric (23.12.2011)
        EQUIP_LOC := TRIM(SUBSTR(BUILDING_ID_,1,4)) || ' ' ||
                     TRIM(SUBSTR(BUILDING_ROOM_ELEV_,1,5)) || '/' ||
                     TRIM(SUBSTR(ROOM_NO_,1,7)) || '/' ||
                     TRIM(SUBSTR(EQUIP_ELEVATION_,1,5));
        UPDATE EQUIP SET EQUIP_LOCATION = EQUIP_LOC, 
                         FREEFORM_LOC = EQUIP_LOC 
                     WHERE EQUIP_OPERATOR_ID = TRIM(KEY_);    
               
     -- 4. UPDATE URL ADRRESS IN ESOMS EQUIPMENT.   
        OPEN RET_EQUIP_URL;             
        FETCH RET_EQUIP_URL INTO EQUIPMENT_URL_;
        UPDATE EQUIP_DOCUMENTS
            SET DOCUMENT_NUMBER = 1, 
                DOCUMENT_ORDER = 1,
                DOCUMENT_TYPE = 2,
                DOCUMENT_DESCRIPTION = GENERATE_ADDR(KEY_),
                DOCUMENT_FILENAME = EQUIPMENT_URL_
            WHERE EQUIP_OPERATOR_ID = KEY_;
        CLOSE RET_EQUIP_URL;
        
     -- 5. MARK STATUS FIELDS IN INTERFACE TABLE
     -- 5.1 SINCE WE GOT THIS FAR IT's ALL OK.     
        IF RET_EQUIP%NOTFOUND = FALSE THEN
            UPDATE MEL_VW_EQUIP_TO_ESOMS 
                SET PROCESSED = 'T',
                    PROCESS_DATE = SYSTIMESTAMP
                WHERE CURRENT OF RET_EQUIP;
        END IF;
            
     -- 6. FOR SUMMARY
        COUNT_UPD := COUNT_UPD + 1;
        
        CLOSE RET_EQUIP;               
    END IF;  
    CLOSE RET_VOID_EQUIP;
    
EXCEPTION
    WHEN OTHERS THEN 
        WRITE_ERROR_CODE(SQLERRM || ' (' || SQLCODE || ').', KEY_);
END UPDATE_EQUIPMENT;

PROCEDURE INSERT_EQUIPMENT(KEY_ IN MEL_VW_EQUIP_TO_ESOMS.EQUIP_NO%TYPE) IS 
/******************************************************************************
 INSERT_EQUIPMENT: Equipment is inserted here. KEY is sent as a parameter for
                  an eSOMS table.                                          
 REVISIONS:                                                                 
 Date        Author           Description                                    
 ----------  ---------------  -----------------------------------------------
 23.1.2010    D. Gregoric      Modified comment header.
  3.2.2010    D. Gregoric      Popravljena konverzija elevacij, da se 
                              zagotovi vejica kot decimalni znak.
  2.6.2011    D. Gregoric     Spremenjena deklaracija za URL!
 23.12.2011  D. Gregoric      Dodana elevacija opreme v niz EQUIP_LOC                              
*******************************************************************************/

    CURSOR RET_OLDKEY IS                        
        SELECT OLD_EQUIP_NO 
        FROM MEL_VW_EQUIP_TO_ESOMS
        WHERE EQUIP_NO = KEY_
        AND (PROCESSED IS NULL OR PROCESSED != 'T')
        ORDER BY ENTRY_SEQUENCE_NO
        FOR UPDATE; 
        
    EQUIP_HAS_OLDKEY    BOOLEAN := FALSE;
    OLD_KEY             MEL_VW_EQUIP_TO_ESOMS.OLD_EQUIP_NO%TYPE;

    CURSOR RET_EQUIP_URL IS 
        SELECT EQUIP_URL 
        FROM MEL_VW_EQUIP_TO_ESOMS
        WHERE EQUIP_NO = KEY_ 
        AND (PROCESSED IS NULL OR PROCESSED != 'T')
        ORDER BY ENTRY_SEQUENCE_NO;
        
    --EQUIPMENT_URL_  VARCHAR(150);
    EQUIPMENT_URL_ EQUIP_DOCUMENTS.DOCUMENT_FILENAME%type;
    
    CURSOR RET_EQUIP IS  
        SELECT  
                UNIT_ID,
                FUNCTIONAL_DESCRIPTION,
                LABEL,
                SYSTEM_CODE,
                SYSTEM_DESC,
                SAFETY_RELATED,
                BUILDING_ID,
                BUILDING_DESC,
                BUILDING_ROOM_ELEVATION,
                EQUIP_ELEVATION,
                ROOM_NO,
                ROOM_DESC,
                WIRE_MARK,
                EQUIP_CATEGORY,
                EQUIP_CATEGORY_DESC,
                OPERATOR,
                OPERATOR_DESC,
                EQUIP_STATUS_CODE,
                ELECTRIC_TRAIN,
                TAG_SIZE_CODE,
                TAG_SIZE_DESC
        FROM MEL_VW_EQUIP_TO_ESOMS
        WHERE EQUIP_NO = KEY_
        AND (PROCESSED IS NULL OR PROCESSED != 'T')
        ORDER BY ENTRY_SEQUENCE_NO
        FOR UPDATE;
    UNIT_ID_            EQUIP.PLANT_ID%TYPE;
    FUNC_DESC_          EQUIP.EQUIP_ATTRIBUTE_01%TYPE;
    LABEL_              EQUIP.EQUIP_DESCRIPTION%TYPE;
    SYSTEM_CODE_        EQUIP.SYSTEM_ID%TYPE;
    SYSTEM_DESCRIPTION_ SYSTEM_TYPE.SYSTEM_DESC%TYPE;
    SAFETY_RELATED_     MEL_VW_EQUIP_TO_ESOMS.SAFETY_RELATED%TYPE;
    BUILDING_ID_        EQUIP.BUILDING_ID%TYPE;
    BUILDING_DESC_      BUILDING_TYPE.BUILDING_DESC%TYPE; 
    NUM_B_ROOM_ELEV_    MEL_VW_EQUIP_TO_ESOMS.BUILDING_ROOM_ELEVATION%TYPE;
    NUM_EQUIPMENT_ELEV_ MEL_VW_EQUIP_TO_ESOMS.EQUIP_ELEVATION%TYPE;       
    BUILDING_ROOM_ELEV_ CHAR(10);
    EQUIP_ELEVATION_    EQUIP.COLUMN_ID%TYPE;
    ROOM_NO_            EQUIP.ROOM_ID%TYPE;   
    ROOM_DESC_          ROOM_TYPE.ROOM_DESC%TYPE;
    WIRE_MARK_          EQUIP.EQUIP_ATTRIBUTE_03%TYPE;
    EQUIP_CATEGORY_     EQUIP.COMPONENT_ID%TYPE;
    EQUIP_CAT_DESC_     COMPONENT_TYPE.COMPONENT_DESC%TYPE;
    OPERATOR_           EQUIP.EQUIP_ATTRIBUTE_04%TYPE;
    OPERATOR_DESC_      EQUIP.EQUIP_ATTRIBUTE_04%TYPE;   
    EQUIP_STATUS_CODE_  EQUIP.EQUIP_ATTRIBUTE_05%TYPE;
    ELECTRIC_TRAIN_     EQUIP.TRAIN_ID%TYPE;
    TAG_SIZE_CODE_      EQUIP.TAG_SIZE%TYPE;
    TAG_SIZE_DESC_      TAG_SIZES.TAG_SIZE_DESCRIPTION%TYPE;
   
    OPER_CONC           VARCHAR2(150); 
    SAFETY_REL_VALUE    NUMBER(6);
    DISABLED_VALUE      NUMBER(6);
    ROOM_NO_CONC        VARCHAR2(150);
    OPERATOR_CONC       VARCHAR2(150);
    
    EQUIP_LOC           VARCHAR(150);    
        
BEGIN
    WRITE_EXECUTION_LOG(KEY_,'Vstavljam napravo: ' || KEY_);
    
    OPEN RET_OLDKEY;
    FETCH RET_OLDKEY INTO OLD_KEY;
    
    IF OLD_KEY IS NOT NULL THEN
        EQUIP_HAS_OLDKEY := TRUE;
    ELSE 
        EQUIP_HAS_OLDKEY := FALSE;
    END IF;
    
    IF EQUIP_HAS_OLDKEY = TRUE THEN
        NEW_PKEY(OLD_KEY, KEY_);                -- 1. OLD KEY? 
    END IF;
    
    CHECK_LOOKUP_DATA(KEY_, TRUE);              -- 2. CHECK ALL LOOKUP TABLES
                                                   
    OPEN RET_EQUIP;                             -- 3. INSERT NEW EQUIP. IN ESOMS TABLE
    FETCH RET_EQUIP INTO 
        UNIT_ID_,
        FUNC_DESC_,
        LABEL_,
        SYSTEM_CODE_,
        SYSTEM_DESCRIPTION_,
        SAFETY_RELATED_,
        BUILDING_ID_, 
        BUILDING_DESC_,
        NUM_B_ROOM_ELEV_,
        NUM_EQUIPMENT_ELEV_,
        ROOM_NO_,
        ROOM_DESC_,
        WIRE_MARK_,
        EQUIP_CATEGORY_,
        EQUIP_CAT_DESC_,
        OPERATOR_,
        OPERATOR_DESC_,
        EQUIP_STATUS_CODE_,
        ELECTRIC_TRAIN_,
        TAG_SIZE_CODE_,
        TAG_SIZE_DESC_;
    
    -- THIS IS FOR THE CONVERSION PURPOSES
    -- Popravljena konverzija elevacij, da se  zagotovi vejica kot.
    -- decimalni znak.
    BUILDING_ROOM_ELEV_ := NUM_B_ROOM_ELEV_;
    BUILDING_ROOM_ELEV_ := TRANSLATE (BUILDING_ROOM_ELEV_, '.', ',');

    EQUIP_ELEVATION_    := NUM_EQUIPMENT_ELEV_;
    EQUIP_ELEVATION_    := TRANSLATE (EQUIP_ELEVATION_, '.', ',');
       
    IF SAFETY_RELATED_ = 'Y' THEN
        SAFETY_REL_VALUE := 1;
    ELSE
        SAFETY_REL_VALUE := 0;
    END IF;
     
    ROOM_NO_CONC := TRIM(SUBSTR(BUILDING_ID_,1,10)) || '' || TRIM(SUBSTR(ROOM_NO_,1,10)); 
    OPERATOR_CONC := OPERATOR_ || ' - ' || OPERATOR_DESC_;
    
    IF EQUIP_STATUS_CODE_ = 'INSTLD' OR 
       EQUIP_STATUS_CODE_ = 'ACTIVE' OR
       EQUIP_STATUS_CODE_ = 'PRELIM' 
    THEN
        DISABLED_VALUE := 0;
    ELSE 
        DISABLED_VALUE := 1;
    END IF;

    IF TAG_SIZE_CODE_ IS NULL THEN 
        TAG_SIZE_CODE_ := 'L';
    END IF;

 -- CONCATENATE EQUIPMENT LOCATION STRING. THIS ONE DOES NOT HAVE FREE_FORM 
 -- LOCATION SINCE WE'RE INSERTING AND FREEFORM LOCATION DOES NOT EXIST.
    -- Dodana elevacija opreme v niz za lokacijo. D. Gregoric (23.12.2011)
        EQUIP_LOC := TRIM(SUBSTR(BUILDING_ID_,1,4)) || ' ' ||
                     TRIM(SUBSTR(BUILDING_ROOM_ELEV_,1,5)) || '/' ||
                     TRIM(SUBSTR(ROOM_NO_,1,7)) || '/' ||
                     TRIM(SUBSTR(EQUIP_ELEVATION_,1,5));
                  
    INSERT INTO EQUIP (
        PLANT_ID,
        EQUIP_OPERATOR_ID, FREEFORM_ID, EQUIP_ALTERNATE_ID, 
        EQUIP_ATTRIBUTE_01, 
        EQUIP_DESCRIPTION, 
        SYSTEM_ID, 
        MAINTENANCE_RULE_RELATED, 
        BUILDING_ID, 
        ELEVATION_ID,
        COLUMN_ID, 
        ROOM_ID, 
        EQUIP_ATTRIBUTE_03, 
        COMPONENT_ID, TYPE_ID, 
        EQUIP_ATTRIBUTE_04, 
        DISABLED,
        EQUIP_ATTRIBUTE_05, 
        TRAIN_ID, 
        TAG_SIZE,
        EQUIP_LOCK_DT,
        EQUIP_LOCATION,
        FREEFORM_LOC,
        PENETRATES_CONTAINMENT,
        APPENDIX_R_RELATED)
    VALUES (
        UNIT_ID_,
        SUBSTR(KEY_,1,150), SUBSTR(KEY_,1,150), SUBSTR(KEY_,1,100),
        SUBSTR(FUNC_DESC_,1,150),
        SUBSTR(LABEL_,1,150),
        SUBSTR(SYSTEM_CODE_,1,10),
        SAFETY_REL_VALUE,
        SUBSTR(BUILDING_ID_,1,10),
        SUBSTR(BUILDING_ROOM_ELEV_,1,5),
        SUBSTR(EQUIP_ELEVATION_,1,10),
        SUBSTR(ROOM_NO_CONC,1,20),
        SUBSTR(WIRE_MARK_,1,150),
        SUBSTR(EQUIP_CATEGORY_,1,20),
        SUBSTR(EQUIP_CATEGORY_,1,20),
        SUBSTR(OPERATOR_CONC,1,150),
        DISABLED_VALUE,
        SUBSTR(EQUIP_STATUS_CODE_,1,150),
        SUBSTR(ELECTRIC_TRAIN_,1,3),
        TAG_SIZE_CODE_,
        SYSDATE,
        SUBSTR(EQUIP_LOC,1,150),
        SUBSTR(EQUIP_LOC,1,150),
        0,
        0);    
           
    WRITE_EXECUTION_LOG(KEY_, 'Naprava uspe�no vstavljena.');
    
    OPEN RET_EQUIP_URL;             -- 4. INSERT URL ADRRESS IN ESOMS EQUIPMENT.
    FETCH RET_EQUIP_URL INTO EQUIPMENT_URL_;
    INSERT INTO EQUIP_DOCUMENTS (
        EQUIP_OPERATOR_ID,
        DOCUMENT_NUMBER,
        DOCUMENT_ORDER,
        DOCUMENT_TYPE,
        DOCUMENT_DESCRIPTION,
        DOCUMENT_FILENAME)
    VALUES (
        KEY_,
        1,
        1,
        2,
        GENERATE_ADDR(KEY_),
        EQUIPMENT_URL_);
    CLOSE RET_EQUIP_URL;
               
    -- 5. MARK STATUS FIELDS IN INTERFACE TABLE
    -- 5.1 SINCE WE GOT THIS FAR IT's ALL OK.            
    IF RET_EQUIP%NOTFOUND = FALSE THEN
        UPDATE MEL_VW_EQUIP_TO_ESOMS 
            SET PROCESSED = 'T',
                PROCESS_DATE = SYSTIMESTAMP
            WHERE CURRENT OF RET_EQUIP;    
    END IF;
    
    --  6. FOR SUMMARY
    COUNT_INS := COUNT_INS + 1;
    
    
EXCEPTION
    WHEN OTHERS THEN 
        WRITE_ERROR_CODE(SQLERRM || ' (' || SQLCODE || ').', KEY_);    
END INSERT_EQUIPMENT;
PROCEDURE SET_EQUIP_TO_VOID(KEY_ IN MEL_VW_EQUIP_TO_ESOMS.EQUIP_NO%TYPE, 
                            SET_VALUE IN BOOLEAN) IS
/******************************************************************************
 SET_EQUIP_TO_VOID: Equipment is set to void or valid. KEY is sent as a     
                   parameter for an eSOMS table and value sets the void opt.
 REVISIONS:                                                                 
 Date        Author           Description                                    
 ----------  ---------------  -----------------------------------------------
 23.1.2010    D. Gregoric      Modified comment header.                     
*******************************************************************************/

BEGIN
    IF SET_VALUE = TRUE THEN
        WRITE_EXECUTION_LOG(KEY_,'Onemogocam napravo: '|| KEY_ || ' (1).');
        UPDATE EQUIP
            SET DISABLED = 1
            WHERE EQUIP_OPERATOR_ID = KEY_;          
    ELSE 
        WRITE_EXECUTION_LOG(KEY_,'Omogocam napravo: '|| KEY_ || ' (0).');
        UPDATE EQUIP
            SET DISABLED = 0
            WHERE EQUIP_OPERATOR_ID = KEY_;    
    END IF;
        
EXCEPTION
    WHEN OTHERS THEN 
        WRITE_ERROR_CODE(SQLERRM || ' (' || SQLCODE || ').', KEY_);    
END SET_EQUIP_TO_VOID;
PROCEDURE NEW_PKEY(OLD_KEY_ IN MEL_VW_EQUIP_TO_ESOMS.EQUIP_NO%TYPE,
                   KEY_  IN MEL_VW_EQUIP_TO_ESOMS.EQUIP_NO%TYPE) IS
/******************************************************************************
 NEW_PKEY: Old equipment is set to void and mesasge is added. This represents
           a logical delete from MEL database.                              
 REVISIONS:                                                                 
 Date        Author           Description                                    
 ----------  ---------------  -----------------------------------------------
 23.1.2010    D. Gregoric      Modified comment header.                     
*******************************************************************************/
                   
BEGIN
    SET_EQUIP_TO_VOID(OLD_KEY_, TRUE);
    
    UPDATE EQUIP 
        SET EQUIP_ATTRIBUTE_10 = 'This equipment should be inactivated and replaced by ' || KEY_
        WHERE EQUIP_OPERATOR_ID = OLD_KEY_;

EXCEPTION
    WHEN OTHERS THEN 
        WRITE_ERROR_CODE(SQLERRM || ' (' || SQLCODE || ').', KEY_);    
END NEW_PKEY;
PROCEDURE CHECK_LOOKUP_DATA(KEY_ IN MEL_VW_EQUIP_TO_ESOMS.EQUIP_NO%TYPE,
                            INSERTING_ IN BOOLEAN) IS
/******************************************************************************
 CHECK_LOOKUP_DATA: All lookup tables for the current interface table row and
                    eSOMS equipment are checked in this procedure.          
 REVISIONS:                                                                 
 Date        Author           Description                                    
 ----------  ---------------  -----------------------------------------------
 23.1.2010    D. Gregoric      Modified comment header.
  3.2.2010    D. Gregoric      Popravljena konverzija elevacij, da se 
                               zagotovi vejica kot decimalni znak.
********************************************************************************/
                            
   CURSOR RET_LOOKUP IS                            -- Retrieves all lookup data
        SELECT 
            UNIT_ID,
            SYSTEM_CODE, SYSTEM_DESC,
            BUILDING_ID, BUILDING_DESC, 
            BUILDING_ROOM_ELEVATION,
            EQUIP_ELEVATION,
            ROOM_NO, ROOM_DESC,
            EQUIP_CATEGORY, EQUIP_CATEGORY_DESC, 
            ELECTRIC_TRAIN,
            TAG_SIZE_CODE, TAG_SIZE_DESC
        FROM MEL_VW_EQUIP_TO_ESOMS
        WHERE EQUIP_NO = KEY_
        AND (PROCESSED IS NULL OR PROCESSED != 'T')
        ORDER BY ENTRY_SEQUENCE_NO;

    UNIT_                       EQUIP.PLANT_ID%TYPE;
    SYSTEM_                     EQUIP.SYSTEM_ID%TYPE;
    SYSTEM_DESCRIPTION_         SYSTEM_TYPE.SYSTEM_DESC%TYPE;
    BUILDING_                   EQUIP.BUILDING_ID%TYPE;
    BUILDING_DESCRIPTION_       BUILDING_TYPE.BUILDING_DESC%TYPE; 
    NUM_B_ROOM_ELEVATION_       MEL_VW_EQUIP_TO_ESOMS.BUILDING_ROOM_ELEVATION%TYPE;
    NUM_EQUIPMENT_ELEVATION_    MEL_VW_EQUIP_TO_ESOMS.EQUIP_ELEVATION%TYPE;       
    BUILDING_ROOM_ELEVATION_    VARCHAR2(10);
    EQUIPMENT_ELEVATION_        EQUIP.COLUMN_ID%TYPE;    
    ROOM_                       EQUIP.ROOM_ID%TYPE;   
    ROOM_DESCRIPTION_           ROOM_TYPE.ROOM_DESC%TYPE;
    EQUIPMENT_CATEGORY_         EQUIP.COMPONENT_ID%TYPE;
    EQUIPMENT_CATEGORY_DESC_    COMPONENT_TYPE.COMPONENT_DESC%TYPE;
    ELECTRICAL_TRAIN_           EQUIP.TRAIN_ID%TYPE;
    TAG_SIZE_CODE_              EQUIP.TAG_SIZE%TYPE;
    TAG_SIZE_DESC_              TAG_SIZES.TAG_SIZE_DESCRIPTION%TYPE;
    
    CUR_VAR                     VARCHAR(50);
    
    CURSOR ES_UNIT IS                   -- Plant  (unit) cursor
        SELECT PLANT_ID
        FROM PLANT_TYPE
        WHERE TRIM(PLANT_ID) = TRIM(UNIT_);
   
    CURSOR ES_SYSTEM IS                 -- System cursor
        SELECT SYSTEM_ID
        FROM SYSTEM_TYPE
        WHERE TRIM(SYSTEM_ID) = TRIM(SUBSTR(SYSTEM_,1,10));
    
    CURSOR ES_BUILDING IS               -- Building cursor
        SELECT BUILDING_ID
        FROM BUILDING_TYPE
        WHERE TRIM(BUILDING_ID) = TRIM(SUBSTR(BUILDING_,1,10));
        
    CURSOR ES_BUILD_ELEVATION IS         -- Building elevation cursor
        SELECT ELEVATION_ID
        FROM ELEVATION_TYPE
        WHERE TRIM(ELEVATION_ID) = TRIM(SUBSTR(BUILDING_ROOM_ELEVATION_,1,5));
        
    CURSOR ES_EQUIP_ELEVATION IS         -- Equipment elevation cursor
        SELECT COLUMN_ID
        FROM COLUMN_TYPE
        WHERE TRIM(COLUMN_ID) = TRIM(SUBSTR(EQUIPMENT_ELEVATION_,1,10));
        
    CONC_ROOM_ID        VARCHAR2(255);
    CURSOR ES_ROOM IS                   -- Room cursor
        SELECT ROOM_ID
        FROM ROOM_TYPE
        WHERE TRIM(ROOM_ID) = TRIM(SUBSTR(CONC_ROOM_ID,1,20));
        
    CURSOR ES_EQUIP_CAT_A IS             -- Two Equipment category cursors
        SELECT COMPONENT_ID
        FROM COMPONENT_TYPE
        WHERE TRIM(COMPONENT_ID) = TRIM(SUBSTR(EQUIPMENT_CATEGORY_,1,20));
        
    CURSOR ES_EQUIP_CAT_B IS
        SELECT TYPE_ID 
        FROM CONFIG_TYPE
        WHERE TRIM(TYPE_ID) = TRIM(SUBSTR(EQUIPMENT_CATEGORY_,1,20));
        
    CURSOR ES_TRAIN IS
        SELECT TRAIN_ID
        FROM TRAIN_TYPE
        WHERE TRIM(TRAIN_ID) = TRIM(SUBSTR(ELECTRICAL_TRAIN_,1,3));
        
    CURSOR ES_TAGSIZE IS 
        SELECT TAG_SIZE
        FROM TAG_SIZES
        WHERE TRIM(TAG_SIZE) = TAG_SIZE_CODE_;       

BEGIN
    WRITE_EXECUTION_LOG(KEY_,'Preverjam ostale podatkovne tabele za napravo (' || KEY_ || ')');
    
    OPEN RET_LOOKUP;
    FETCH RET_LOOKUP INTO
        UNIT_,
        SYSTEM_, SYSTEM_DESCRIPTION_,
        BUILDING_, BUILDING_DESCRIPTION_, 
        NUM_B_ROOM_ELEVATION_, NUM_EQUIPMENT_ELEVATION_, 
        ROOM_, ROOM_DESCRIPTION_, 
        EQUIPMENT_CATEGORY_, EQUIPMENT_CATEGORY_DESC_, 
        ELECTRICAL_TRAIN_,
        TAG_SIZE_CODE_, TAG_SIZE_DESC_;
    CLOSE RET_LOOKUP;
    
    -- THIS IS FOR THE CONVERSION PURPOSES 
    -- Popravljena konverzija elevacij, da se  zagotovi vejica kot.
    -- decimalni znak.
    BUILDING_ROOM_ELEVATION_    := NUM_B_ROOM_ELEVATION_;
    BUILDING_ROOM_ELEVATION_    := TRANSLATE (BUILDING_ROOM_ELEVATION_, '.', ',');
    
    EQUIPMENT_ELEVATION_        := NUM_EQUIPMENT_ELEVATION_;
    EQUIPMENT_ELEVATION_        := TRANSLATE (EQUIPMENT_ELEVATION_, '.', ',');

    -- Unit
    OPEN ES_UNIT;
    FETCH ES_UNIT INTO CUR_VAR;
    
    IF UNIT_ IS NOT NULL AND ES_UNIT%ROWCOUNT = 0 THEN
        WRITE_EXECUTION_LOG(KEY_,'  - Tabela UNIT nima vrednosti. Vstavljam vrednost.');
        INSERT INTO PLANT_TYPE
            (PLANT_ID, PLANT_DESC)
            VALUES
            (UNIT_, UNIT_);   
    END IF;
    CLOSE ES_UNIT;
    
    -- System
    OPEN ES_SYSTEM;
    FETCH ES_SYSTEM INTO CUR_VAR;
    
    IF SUBSTR(SYSTEM_,1,10) IS NOT NULL THEN
    
        IF SYSTEM_DESCRIPTION_ IS NULL THEN
            SYSTEM_DESCRIPTION_ := SYSTEM_;
        END IF;
        
        IF ES_SYSTEM%ROWCOUNT = 0 THEN
            WRITE_EXECUTION_LOG(KEY_,'  - Tabela SYSTEM nima vrednosti. Vstavljam vrednost.');      
        
            INSERT INTO SYSTEM_TYPE
                (SYSTEM_ID, SYSTEM_DESC)
                VALUES
                (SUBSTR(SYSTEM_,1,10), SUBSTR(SYSTEM_DESCRIPTION_,1,40));
        ELSE
            WRITE_EXECUTION_LOG(KEY_,'  - Tabela SYSTEM ima vrednost. Posodabljam z:'|| SUBSTR(SYSTEM_DESCRIPTION_,1,40) || SUBSTR(SYSTEM_,1,10));             
            UPDATE SYSTEM_TYPE SET SYSTEM_DESC = SUBSTR(SYSTEM_DESCRIPTION_,1,40) WHERE TRIM(SYSTEM_ID) = TRIM(SUBSTR(SYSTEM_,1,10));
        END IF;
        
    END IF;
    CLOSE ES_SYSTEM;
    
    -- Building
    OPEN ES_BUILDING;
    FETCH ES_BUILDING INTO CUR_VAR;
    
    IF SUBSTR(BUILDING_,1,10) IS NOT NULL THEN

        IF BUILDING_DESCRIPTION_ IS NULL THEN
            BUILDING_DESCRIPTION_ := BUILDING_;
        END IF; 

        IF ES_BUILDING%ROWCOUNT = 0 THEN
            WRITE_EXECUTION_LOG(KEY_,'  - Tabela BUILDING nima vrednosti. Vstavljam vrednost.');
            
            INSERT INTO BUILDING_TYPE
                (BUILDING_ID, BUILDING_DESC)
                VALUES
                (SUBSTR(BUILDING_,1,10), SUBSTR(BUILDING_DESCRIPTION_,1,40));  
        ELSE
            UPDATE BUILDING_TYPE SET BUILDING_DESC = SUBSTR(BUILDING_DESCRIPTION_,1,40) WHERE TRIM(BUILDING_ID) = SUBSTR(BUILDING_,1,10);    
        END IF;
        
    END IF;
    CLOSE ES_BUILDING;
    
    -- Building elevation
    OPEN ES_BUILD_ELEVATION;
    FETCH ES_BUILD_ELEVATION INTO CUR_VAR;
    
    IF SUBSTR(BUILDING_ROOM_ELEVATION_,1,5) IS NOT NULL AND ES_BUILD_ELEVATION%ROWCOUNT = 0 THEN
        WRITE_EXECUTION_LOG(KEY_,'  - Tabela BUILDING ELEVATION nima vrednosti. Vstavljam vrednost.');
    
        INSERT INTO ELEVATION_TYPE
            (ELEVATION_ID, ELEVATION_DESC)
            VALUES
            (SUBSTR(BUILDING_ROOM_ELEVATION_,1,5), SUBSTR(BUILDING_ROOM_ELEVATION_,1,5));
    END IF;
    CLOSE ES_BUILD_ELEVATION;
    
    -- Equipment elevation
    OPEN ES_EQUIP_ELEVATION;
    FETCH ES_EQUIP_ELEVATION INTO CUR_VAR;
    
    IF SUBSTR(EQUIPMENT_ELEVATION_,1,10) IS NOT NULL AND ES_EQUIP_ELEVATION%ROWCOUNT = 0 THEN
        WRITE_EXECUTION_LOG(KEY_,'  - Tabela EQUIPMENT ELEVATION nima vrednosti. Vstavljam vrednost.');
    
        INSERT INTO COLUMN_TYPE
            (COLUMN_ID, COLUMN_DESC)
            VALUES
            (SUBSTR(EQUIPMENT_ELEVATION_,1,10),SUBSTR(EQUIPMENT_ELEVATION_,1,10));
    END IF;
    CLOSE ES_EQUIP_ELEVATION;
    
    -- Concatenate ROOM ID
    CONC_ROOM_ID := TRIM(SUBSTR(BUILDING_,1,10)) || '' || TRIM(SUBSTR(ROOM_,1,10));
    OPEN ES_ROOM;
    FETCH ES_ROOM INTO CUR_VAR;
    
    IF SUBSTR(CONC_ROOM_ID,1,20) IS NOT NULL THEN
    
        IF ROOM_DESCRIPTION_ IS NULL THEN
            ROOM_DESCRIPTION_ := CONC_ROOM_ID;
        END IF;
    
        IF ES_ROOM%ROWCOUNT = 0 THEN
            WRITE_EXECUTION_LOG(KEY_,'  - Tabela ROOM nima vrednosti. Vstavljam vrednost.');
    
            INSERT INTO ROOM_TYPE
                (ROOM_ID, ROOM_DESC)
                VALUES
                (SUBSTR(CONC_ROOM_ID,1,20),SUBSTR(ROOM_DESCRIPTION_,1,40));
        ELSE 
            UPDATE ROOM_TYPE SET ROOM_DESC = SUBSTR(ROOM_DESCRIPTION_,1,40) WHERE TRIM(ROOM_ID) = SUBSTR(CONC_ROOM_ID,1,20);
        END IF;
        
    END IF;    
    CLOSE ES_ROOM;
    
    -- Equipment Component category
    OPEN ES_EQUIP_CAT_A;
    FETCH ES_EQUIP_CAT_A INTO CUR_VAR;
    
    IF SUBSTR(EQUIPMENT_CATEGORY_,1,20) IS NOT NULL THEN
    
        IF EQUIPMENT_CATEGORY_DESC_ IS NULL THEN
            EQUIPMENT_CATEGORY_DESC_ := EQUIPMENT_CATEGORY_;
        END IF;
    
        IF ES_EQUIP_CAT_A%ROWCOUNT = 0 THEN
            WRITE_EXECUTION_LOG(KEY_,'  - Tabela EQUIPMNET CATEGORY/COMPONENT TYPE nima vrednosti. Vstavljam vrednost.');
            
            INSERT INTO COMPONENT_TYPE
                (COMPONENT_ID, COMPONENT_DESC)
                VALUES
                (SUBSTR(EQUIPMENT_CATEGORY_,1,20), SUBSTR(EQUIPMENT_CATEGORY_DESC_,1,40));
        ELSE
            UPDATE COMPONENT_TYPE SET COMPONENT_DESC = SUBSTR(EQUIPMENT_CATEGORY_DESC_,1,40)
                WHERE TRIM(COMPONENT_ID) = SUBSTR(EQUIPMENT_CATEGORY_,1,20);
        END IF;
    
    END IF;
    CLOSE ES_EQUIP_CAT_A;
    
    -- Equipment Type category
    IF INSERTING_ = TRUE THEN   -- Only processed at insert
        OPEN ES_EQUIP_CAT_B;
        FETCH ES_EQUIP_CAT_B INTO CUR_VAR;
        
        IF SUBSTR(EQUIPMENT_CATEGORY_,1,20) IS NOT NULL AND ES_EQUIP_CAT_B%ROWCOUNT = 0 THEN
            WRITE_EXECUTION_LOG(KEY_,'  - Tabela EQUIPMENT CATEGORY/CONFIG TYPE nima vrednosti. Vstavljam vrednost.');

            IF EQUIPMENT_CATEGORY_DESC_ IS NULL THEN
                EQUIPMENT_CATEGORY_DESC_ := EQUIPMENT_CATEGORY_;
            END IF;
    
            INSERT INTO CONFIG_TYPE
                (TYPE_ID, TYPE_DESC)
                VALUES
                (SUBSTR(EQUIPMENT_CATEGORY_,1,20), SUBSTR(EQUIPMENT_CATEGORY_DESC_,1,40));
        END IF;
        CLOSE ES_EQUIP_CAT_B;
    END IF;
    
    -- Electrical train
    OPEN ES_TRAIN;
    FETCH ES_TRAIN INTO CUR_VAR;
    
    IF SUBSTR(ELECTRICAL_TRAIN_,1,3) IS NOT NULL AND ES_TRAIN%ROWCOUNT = 0 THEN
        WRITE_EXECUTION_LOG(KEY_,'  - Tabela TRAIN nima vrednosti. Vstavljam vrednost.');
    
        INSERT INTO TRAIN_TYPE
            (TRAIN_ID, TRAIN_DESC)
            VALUES
            (SUBSTR(ELECTRICAL_TRAIN_,1,3),SUBSTR(ELECTRICAL_TRAIN_,1,40));
    END IF;    
    CLOSE ES_TRAIN;    

    IF TAG_SIZE_CODE_ IS NULL THEN
        TAG_SIZE_CODE_ := 'L';
    END IF;

    OPEN ES_TAGSIZE;
    FETCH ES_TAGSIZE INTO CUR_VAR;
    
    -- Tag sizes
    IF TAG_SIZE_CODE_ IS NOT NULL THEN
        IF TAG_SIZE_DESC_ IS NULL THEN
            TAG_SIZE_DESC_ := TAG_SIZE_CODE_;
        END IF;
    
        IF ES_TAGSIZE%ROWCOUNT = 0 THEN
            WRITE_EXECUTION_LOG(KEY_,'  - Tabela TAG_SIZE nima vrednosti. Vstavljam vrednost.');
            
            INSERT INTO TAG_SIZES
                (TAG_SIZE, TAG_SIZE_DESCRIPTION)
                VALUES
                (TAG_SIZE_CODE_, SUBSTR(TAG_SIZE_DESC_,1,40));
        ELSE
            UPDATE TAG_SIZES SET TAG_SIZE_DESCRIPTION = SUBSTR(TAG_SIZE_DESC_,1,40)
                WHERE TRIM(TAG_SIZE) = TAG_SIZE_CODE_;
        END IF;
    
    END IF;
    CLOSE ES_TAGSIZE;    
    
    WRITE_EXECUTION_LOG(KEY_,'Podatkovne tabele za napravo (' || KEY_ || ') uspe�no preverjene.' );

EXCEPTION
    WHEN OTHERS THEN 
        WRITE_ERROR_CODE(SQLERRM || ' (' || SQLCODE || ').', KEY_);    
END CHECK_LOOKUP_DATA;

FUNCTION GENERATE_ADDR(KEY_ IN MEL_VW_EQUIP_TO_ESOMS.EQUIP_NO%TYPE) RETURN VARCHAR2 IS
/******************************************************************************
 GENERATE_ADDR: URL address description is generated and passed back.       
 REVISIONS:                                                                 
 Date        Author           Description                                    
 ----------  ---------------  -----------------------------------------------
 23.1.2010    D. Gregoric      Modified comment header.                     
*******************************************************************************/

BEGIN
    RETURN 'WEB pregledovalnik opreme za ' || KEY_;
END GENERATE_ADDR;
PROCEDURE WRITE_ERROR_CODE(ERROR_ IN VARCHAR2, KEY_ IN MEL_VW_EQUIP_TO_ESOMS.EQUIP_NO%TYPE) IS
/******************************************************************************
 WRITE_ERROR_CODE: Writes error to error buffer                             
 REVISIONS:                                                                 
 Date        Author           Description                                    
 ----------  ---------------  -----------------------------------------------
 23.1.2010    D. Gregoric      Modified comment header.                     
********************************************************************************/

BEGIN
        INSERT INTO MEL_TU_LOG (
            ENTRY_TS,
            INTF_SRC,
            MSG_TYPE,
            ENV,
            MSG,
            EQUIP_ID)
        VALUES (
            SYSTIMESTAMP,
            1,
            1,
            0,
            TRIM(SUBSTR(ERROR_,1,4000)),
            TRIM(SUBSTR(KEY_,1,150))
            );
        COMMIT;
    COUNT_ERR := COUNT_ERR + 1;
END WRITE_ERROR_CODE;
PROCEDURE WRITE_EXECUTION_LOG(KEY_ IN VARCHAR, MESSAGE_ IN VARCHAR) IS
/******************************************************************************
 WRITE_EXECUTION_LOG: Messages are written to execution log file.           
 REVISIONS:                                                                 
 Date        Author           Description                                    
 ----------  ---------------  -----------------------------------------------
 23.1.2010   D. Gregoric      Modified comment header.                     
*******************************************************************************/

BEGIN
        INSERT INTO MEL_TU_LOG (
            ENTRY_TS,
            INTF_SRC,
            MSG_TYPE,
            ENV,
            MSG,
            EQUIP_ID)
        VALUES (
            SYSTIMESTAMP,
            1,
            2,
            0,
            TRIM(SUBSTR(MESSAGE_,1,4000)),
            TRIM(SUBSTR(KEY_,1,150))
            );
END WRITE_EXECUTION_LOG;
PROCEDURE WRITE_SUMMARY_LOG(SUMMARY_ IN VARCHAR) IS
/******************************************************************************
 WRITE_SUMMARY_LOG: Summary is written to database as summary entry.        
 REVISIONS:                                                                 
 Date        Author           Description                                    
 ----------  ---------------  -----------------------------------------------
 23.1.2010   D. Gregoric      Modified comment header.                     
*******************************************************************************/

BEGIN
        INSERT INTO MEL_TU_LOG (
            ENTRY_TS,
            INTF_SRC,
            MSG_TYPE,
            ENV,
            MSG,
            EQUIP_ID)
        VALUES (
            SYSTIMESTAMP,
            1,
            3,
            0,
            TRIM(SUBSTR(SUMMARY_,1,4000)),
            ''
            );
END WRITE_SUMMARY_LOG;
PROCEDURE DELETE_INTF(VAL_ IN INTEGER) AS
/******************************************************************************
 DELETE_INTF: This procedure compares process dates with setting for tagging
              and sets the mark_for_delete field if the condition is met.   
 REVISIONS:                                                                 
 Date        Author           Description                                     
 ----------  ---------------  -----------------------------------------------
 23.1.2010    D. Gregoric      Modified comment header.                     
*******************************************************************************/

    CURSOR RET_OLD_ROWS IS
        SELECT EQUIP_NO 
        FROM MEL_VW_EQUIP_TO_ESOMS 
        WHERE SYSTIMESTAMP >= (PROCESS_DATE + VAL_)
            AND ((PROCESSED = 'T')
            AND ((MARK_FOR_DELETE IS NULL) OR (MARK_FOR_DELETE <> 'T')));
        
    EQUIPMENT  VARCHAR2(150);
BEGIN  
    WRITE_EXECUTION_LOG('','Oznacevanje vrstic za izbris po poteku obdobja ' || VAL_ || ' dni.');    

    OPEN RET_OLD_ROWS;
    LOOP
        FETCH RET_OLD_ROWS INTO EQUIPMENT;
        IF RET_OLD_ROWS%NOTFOUND
        THEN 
          EXIT;
        ELSE
            WRITE_EXECUTION_LOG(EQUIPMENT, '  - Oznacitev vrstice naprave ' || EQUIPMENT ||' za izbris.');
        END IF;
    END LOOP;
    CLOSE RET_OLD_ROWS;
    
    UPDATE MEL_VW_EQUIP_TO_ESOMS 
    SET MARK_FOR_DELETE = 'T'
    WHERE SYSTIMESTAMP >= (PROCESS_DATE + VAL_)
            AND ((PROCESSED = 'T')
            AND ((MARK_FOR_DELETE IS NULL) OR (MARK_FOR_DELETE <> 'T')));
    
END DELETE_INTF;
END NEK_INTERFACE_MEL_TO_ESOMS;
/