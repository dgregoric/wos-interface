CREATE OR REPLACE TRIGGER CATCH_ROUND_DATA AFTER UPDATE ON
/******************************************************************************
  NAME:     CATCH_ROUND_DATA
  PURPOSE:  Trigger for catching tour uploaded data for eSOMS - PIS transfer.
  
  REVISIONS:
  Date        Author           Description
  ----------  ---------------  -------------------------------------------------
  10.8.2013   M. Clemente    Updated for easier schema transfer
  18.1.2010   S. Fuks          Transfare files will be created only for TOURS 
                               that are below tour 7.
  
******************************************************************************/
   TOUR_APPROVAL FOR EACH ROW
DECLARE
    TOUR_   HISTORY.TOUR%TYPE;
    SHIF_    HISTORY.SHIFT_NO%TYPE;
    REVI_    HISTORY.REV_NO%TYPE;
    DATET_  HISTORY.DT%TYPE;
    USER_    TOUR_APPROVAL.TOUR_VERIFIED_BY%TYPE;
    UDATET_  TOUR_APPROVAL.TOUR_VERIFIED_DATE%TYPE;
    APPLVL_  TOUR_APPROVAL.TOUR_APPROVAL_LEVEL%TYPE;
BEGIN
    TOUR_ := :new.TOUR;
    SHIF_ := :new.SHIFT_NO;
    REVI_ := :new.REV_NO;
    DATET_:= :new.DT;
    USER_ := :new.TOUR_VERIFIED_BY;
    UDATET_:=:new.TOUR_VERIFIED_DATE;
    APPLVL_:=:new.TOUR_APPROVAL_LEVEL;

    IF (APPLVL_ = 1) AND (UDATET_ IS NOT NULL) AND (USER_ != '' OR USER_ IS NOT NULL) AND (TOUR_ < 7)
    THEN
        PIS_ROUND_DATA.MAKE_FILES(TOUR_, SHIF_, REVI_, DATET_, TRIM(USER_));
        -- IF TRIGGER NEEDS A DEBUGING, FUNCTION IS PROVIDED
        PIS_ROUND_DATA.DEBUG_ITF(TOUR_, SHIF_, REVI_, DATET_, USER_);
    END IF;
END CATCH_ROUND_DATA;
/

ALTER TRIGGER CATCH_ROUND_DATA DISABLE
/