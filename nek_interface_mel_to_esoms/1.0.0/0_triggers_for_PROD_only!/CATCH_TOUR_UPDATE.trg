CREATE OR REPLACE TRIGGER CATCH_TOUR_UPDATE AFTER UPDATE
/******************************************************************************
  NAME:     CATCH_TOUR_UPDATE
  PURPOSE:  Trigger for catching tour revision change, which updates tour 
            revision in ESM_PROD_ITF.PIS_PMAP table. 
  
  REVISIONS:
  Date        Author           Description
  ----------  ---------------  -------------------------------------------------
  10.8.2013  M. Clemente   Updated for easier schema transfer
  4.1.2010   S. Fuks          Trigger created.
  
******************************************************************************/
OF REV_NO ON CURRENT_TOUR_REVISION FOR EACH ROW
DECLARE
    TOUR_   CURRENT_TOUR_REVISION.TOUR%TYPE;
    REV_   CURRENT_TOUR_REVISION.REV_NO%TYPE;
BEGIN
    TOUR_ := :new.TOUR;
    REV_ := :new.REV_NO;
    
    IF (TOUR_ IS NOT NULL) AND (REV_ IS NOT NULL)
    THEN
       PIS_PMAP_UPDATE.UPDATE_PMAP(TOUR_, REV_);      
    END IF;
END CATCH_TOUR_UPDATE;
/

ALTER TRIGGER CATCH_TOUR_UPDATE DISABLE
/