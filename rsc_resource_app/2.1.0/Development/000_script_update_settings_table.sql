INSERT INTO NEK_ITF_SETTINGS 
    (USER_ID, APP_ID, SECTION_ID, SETTING_ID, VALUE_ID, DESCRIPTION)
VALUES
    ('ADMIN', 'RSC', 'CONTROL', 'ROUND_CONTR_ATTR', '4', 'Attribute number that controls rounding.');


COMMIT;

