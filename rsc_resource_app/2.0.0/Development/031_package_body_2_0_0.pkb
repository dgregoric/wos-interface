CREATE OR REPLACE PACKAGE BODY ESM_DEV_ITF.RSC_RESOURCE_APP AS
/******************************************************************************
   NAME:       RSC_RESOURCE_APP
   PURPOSE:    Resource application

   1.0.0: 22.8.2013:M. Clemente: Initial version per specification
   2.0.0: 06.7.2013:M. Clemente: Changes to new specification
                                - Added table for better equipment without
                                  assigned responsibility group management
                                - Time normalization to the hourly blocks
                                - New table for final results
******************************************************************************/

PROCEDURE CHECK_TAGOUTS
/********************************************************************************
\ CHECK_TAGOUTS - Checks the TAGOUTS for certain folder.
********************************************************************************/
IS  
    VALUE_ VARCHAR(500);       
    FULL_LOAD_ RSC_THREE_STR_TABLE;
    FL_SINGLE_ NUMBER;
    CNT_ NUMBER;
    
    TAG_HANG_ID_ INTEGER;
    TAG_REMOVE_ID_ INTEGER;
    
    TAG_HANG_ACTIVITY_ VARCHAR2(150);
    TAG_REMOVE_ACTIVITY_ VARCHAR2(150); 
    
    PARAMS_ RSC_THREE_STR_TABLE;
    
    FULL_RESULTS_ RSC_CALC_TABLE;
    CURR_RESULTS_ RSC_CALC_TABLE;
    
    DISPL_RES_  RSC_THREE_STR_TABLE;
    TI_3_ NUMBER;
    
     FULL_TIME_ NUMBER; NORM_FULL_TIME_ NUMBER; TIME_CH_RATIO_ NUMBER;
    
    JOB_ID_ NUMBER;        
BEGIN
    SELECT
       RSC_THR_STR_RECORD(ats.TAGOUT_TYPE_ID, ats.ACT_TAGOUT_NUMBER, ats.ACT_TAGOUT_SECTION_NUMBER)
        BULK COLLECT INTO PARAMS_
    FROM ACT_TAGOUT_SECTIONS ats
    INNER JOIN ACT_TAGOUT_PROJECT atp ON
        ats.TAGOUT_TYPE_ID = atp.TAGOUT_TYPE_ID AND 
        ats.ACT_TAGOUT_NUMBER = atp.ACT_TAGOUT_NUMBER
    WHERE 
     TRIM(ats.TAGOUT_TYPE_ID) = TRIM(ats.TAGOUT_TYPE_ID) AND
     TRIM(ats.ACT_TAGOUT_NUMBER) = TRIM(atp.ACT_TAGOUT_NUMBER)
     AND ARCHIVED = 0
        AND atp.SEND_AT_VERIF_LEVEL_NO_WOS IS NOT NULL AND 
            atp.SEND_AT_VERIF_LEVEL_WITH_WOS IS NOT NULL;
   
    SELECT RSC_JOB_ID.nextval INTO JOB_ID_ FROM DUAL;

    IF (PARAMS_ IS NULL OR PARAMS_.COUNT = 0) THEN 
        dbms_output.put_line('Did not found the available Tagouts.');
        RETURN; 
    END IF;
    
    FULL_RESULTS_ := RSC_CALC_TABLE();
    DISPL_RES_ := RSC_THREE_STR_TABLE();
    FOR i IN PARAMS_.FIRST .. PARAMS_.LAST -- Postavitev
    LOOP
        CURR_RESULTS_ := CHECK_TAGOUT(PARAMS_(i).val1, PARAMS_(i).val2, PARAMS_(i).val3, 'HANG', TI_3_, JOB_ID_);  
        FULL_TIME_ := CALCULATE_TAGOUT_TIME(CURR_RESULTS_, TI_3_, FULL_RESULTS_);
        
        NORM_FULL_TIME_ := CEIL(FULL_TIME_/60);
        IF (FULL_TIME_ > 0) THEN TIME_CH_RATIO_ := NORM_FULL_TIME_ / FULL_TIME_; ELSE TIME_CH_RATIO_ := 0; END IF;

            -- is entry already in?
            SELECT COUNT(*) INTO CNT_ FROM RSC_RESULT_TAGOUT WHERE 
                TA_ID = PARAMS_(i).val1 AND FONAME = PARAMS_(i).val2 AND TONAME =  PARAMS_(i).val3;
                
            SELECT TAG_HANG_OBJECT_ID, TAG_HANG_ACTIVITY INTO TAG_HANG_ID_, TAG_HANG_ACTIVITY_ FROM 
                ACT_TAGOUT_SECTION_SCHEDULE 
            WHERE TAGOUT_TYPE_ID = PARAMS_(i).val1 AND ACT_TAGOUT_NUMBER = PARAMS_(i).val2
                AND ACT_TAGOUT_SECTION_NUMBER =PARAMS_(i).val3;
            
            IF (CNT_ > 0) THEN -- update
                UPDATE RSC_RESULT_TAGOUT SET SET_FULL_TIME = FULL_TIME_/60, DATE_ENTRY = SYSDATE,
                    TAG_HANG_ACTION_ID = TAG_HANG_ID_, JOB_ID = JOB_ID_, TAG_HANG_ACTIVITY = TAG_HANG_ACTIVITY_, SET_FULL_TIME_NORM = NORM_FULL_TIME_
                    WHERE TA_ID = PARAMS_(i).val1 AND FONAME = PARAMS_(i).val2 AND TONAME = PARAMS_(i).val3;  
            ELSE
                INSERT INTO RSC_RESULT_TAGOUT (TA_ID, FONAME, TONAME, SET_FULL_TIME, TAG_HANG_ACTION_ID, JOB_ID, TAG_HANG_ACTIVITY, SET_FULL_TIME_NORM) 
                VALUES (PARAMS_(i).val1, PARAMS_(i).val2, PARAMS_(i).val3, FULL_TIME_/60, TAG_HANG_ID_, JOB_ID_, TAG_HANG_ACTIVITY_, NORM_FULL_TIME_);
            END IF; 
            
            IF (RSC_WRITE_TO_TAGOUT = 1) THEN
                UPDATE ACT_TAGOUT_SECTION_SCHEDULE
                    SET TAG_HANG_TIME_TO_HANG = NORM_FULL_TIME_
                WHERE TAGOUT_TYPE_ID = PARAMS_(i).val1 AND ACT_TAGOUT_NUMBER = PARAMS_(i).val2
                AND ACT_TAGOUT_SECTION_NUMBER =PARAMS_(i).val3;
                
                INSERT INTO ACT_TAGOUT_SECTION_CHANGES (TAGOUT_TYPE_ID, ACT_TAGOUT_NUMBER, 
                        ACT_TAGOUT_SECTION_NUMBER, USER_ID, CHANGE_DATE, CHANGE_TEXT) VALUES
                        (PARAMS_(i).val1, PARAMS_(i).val2, PARAMS_(i).val3, RSC_LOG_WRITE_USER, SYSDATE+((1/(24*60*60))*i), 
                        'Time to Hang changed to '  ||  NORM_FULL_TIME_  || ' by RSC Application!');
            END IF;
    END LOOP;  
     
    TI_3_ := 0;
    CURR_RESULTS_ := NULL;
    FULL_RESULTS_ := RSC_CALC_TABLE();
    DISPL_RES_ := RSC_THREE_STR_TABLE();
    FOR i IN PARAMS_.FIRST .. PARAMS_.LAST -- Odstranitev
    LOOP
        CURR_RESULTS_ := CHECK_TAGOUT(PARAMS_(i).val1, PARAMS_(i).val2, PARAMS_(i).val3,  'REMOVE', TI_3_, JOB_ID_);
        FULL_TIME_ := CALCULATE_TAGOUT_TIME(CURR_RESULTS_, TI_3_, FULL_RESULTS_);
        
        NORM_FULL_TIME_ := CEIL(FULL_TIME_/60);
        IF (FULL_TIME_ > 0) THEN TIME_CH_RATIO_ := NORM_FULL_TIME_ / FULL_TIME_; ELSE TIME_CH_RATIO_ := 0; END IF;
               
            -- is entry already in?
            SELECT COUNT(*) INTO CNT_ FROM RSC_RESULT_TAGOUT WHERE 
                TA_ID = PARAMS_(i).val1 AND FONAME = PARAMS_(i).val2 AND TONAME =  PARAMS_(i).val3;
            
            SELECT TAG_REMOVE_OBJECT_ID, TAG_REMOVE_ACTIVITY INTO TAG_REMOVE_ID_, TAG_REMOVE_ACTIVITY_ FROM 
                ACT_TAGOUT_SECTION_SCHEDULE 
            WHERE TAGOUT_TYPE_ID = PARAMS_(i).val1 AND ACT_TAGOUT_NUMBER = PARAMS_(i).val2
                AND ACT_TAGOUT_SECTION_NUMBER =PARAMS_(i).val3;
            
            IF (CNT_ > 0) THEN -- update
                UPDATE RSC_RESULT_TAGOUT SET REM_FULL_TIME = FULL_TIME_/60, DATE_ENTRY = SYSDATE,
                    TAG_REMOVE_ACTION_ID = TAG_REMOVE_ID_, JOB_ID = JOB_ID_, TAG_REMOVE_ACTIVITY = TAG_REMOVE_ACTIVITY_, REM_FULL_TIME_NORM = NORM_FULL_TIME_
                    WHERE TA_ID = PARAMS_(i).val1 AND FONAME = PARAMS_(i).val2 AND TONAME = PARAMS_(i).val3;  
            ELSE
                INSERT INTO RSC_RESULT_TAGOUT (TA_ID, FONAME, TONAME, REM_FULL_TIME, TAG_REMOVE_ACTION_ID, JOB_ID, TAG_REMOVE_ACTIVITY, REM_FULL_TIME_NORM) 
                VALUES (PARAMS_(i).val1, PARAMS_(i).val2, PARAMS_(i).val3, FULL_TIME_/60, TAG_REMOVE_ID_, JOB_ID_, TAG_REMOVE_ACTIVITY_, NORM_FULL_TIME_);
            END IF;
                        
            IF (RSC_WRITE_TO_TAGOUT = 1) THEN
                UPDATE ACT_TAGOUT_SECTION_SCHEDULE
                    SET TAG_REMOVE_TIME_TO_REMOVE =NORM_FULL_TIME_
                WHERE TAGOUT_TYPE_ID = PARAMS_(i).val1 AND ACT_TAGOUT_NUMBER = PARAMS_(i).val2
                AND ACT_TAGOUT_SECTION_NUMBER =PARAMS_(i).val3;
                
                INSERT INTO ACT_TAGOUT_SECTION_CHANGES (TAGOUT_TYPE_ID, ACT_TAGOUT_NUMBER, 
                        ACT_TAGOUT_SECTION_NUMBER, USER_ID, CHANGE_DATE, CHANGE_TEXT) VALUES
                        (PARAMS_(i).val1, PARAMS_(i).val2, PARAMS_(i).val3, RSC_LOG_WRITE_USER, SYSDATE+((1/(24*60*60))*i), 
                        'Time to Remove changed to '  ||  NORM_FULL_TIME_  || ' by RSC Application!');                
            END IF;
    END LOOP;

EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'CHECK_TAGOUTS');
END CHECK_TAGOUTS;

FUNCTION CHECK_TAGOUT(TO_ID_ IN VARCHAR2, FONAME_ IN VARCHAR2, TONAME_ IN VARCHAR2, OPER_TYPE_ IN VARCHAR2, ST3_MAX_TIME_ OUT NUMBER, JOB_ID_ IN NUMBER) RETURN RSC_CALC_TABLE
/********************************************************************************
\ CHECK_TAGOUT - Checks the designated TAGOUT for Usergroups and responsibility times
********************************************************************************/
IS         
    TO_EQUIP_ VARCHAR2(500); TO_RESP_   VARCHAR2(500); TO_PLACE_ NUMBER;
    TO_EQUIP_CONFIG_  VARCHAR2(500);
    GID_ VARCHAR2(150);
    RULE_KEY_SEARCH_ NUMBER;
    
    CURRENT_USER_GROUP_ VARCHAR2(150);
    
    CARDS_ON_TA_WITH_RESP_                    RSC_TO_RESP_TABLE;
    CARDS_WO_RESP_                                  RSC_TO_RESP_TABLE;
    DISTINCTIVE_USER_GROUPS_IN_TO_        RSC_STR_TABLE;
    SUM_TI_                                                RSC_NUM_VAL_TABLE;
    
    CUR_CARD_NUM_ NUMBER;
    CURRENT_CARDS_FOR_GROUP_              RSC_TO_RESP_TABLE;
    
    UG_STEP_TIME_ NUMBER; 
    UG_STEP3_TIME_ NUMBER; 
    UG_STEP_1_LAG_ NUMBER; UG_STEP_2_LAG_ NUMBER; UG_STEP_3_LAG_ NUMBER;
    UG_STEP_4_LAG_ NUMBER; UG_STEP_5_LAG_ NUMBER;
    
    UG_PREFORM_ST_1_ NUMBER; UG_PREFORM_ST_2_ NUMBER; UG_PREFORM_ST_3_ NUMBER; 
    UG_PREFORM_ST_4_ NUMBER; UG_PREFORM_ST_5_ NUMBER;
    
    CARD_STEP_3_CALC_                              RSC_CARD_CALC_TABLE;
    ST_3_RESULT_CARDS_                             RSC_CARD_CALC_TABLE;
    
    CALC_                                                   RSC_CALC_TABLE;
    FIN_RES_                                                   RSC_CALC_TABLE;
    
    FULL_TIME_ NUMBER; NORM_FULL_TIME_ NUMBER; TIME_CH_RATIO_ NUMBER;
BEGIN
    --NEK_LOG('Starting resource check for Tagout '  || FONAME_  || ', '  || TONAME_  || ' with ID '  || TO_ID_  || '.', 'CHECK_TAGOUT', 'INFO');  
     CALC_ := RSC_CALC_TABLE();
     FIN_RES_ := RSC_CALC_TABLE();
           
     SELECT 
            RSC_TO_RESP_RECORD
            (atst.EQUIP_OPERATOR_ID, 
            eq.EQUIP_DESCRIPTION,
            GET_EQUIP_RESPONSIBILITY(eq.EQUIP_OPERATOR_ID),
            atst.TAG_PLACE_SEQ,
            atst.PLACEMENT_CONFIG,
            eq.BUILDING_ID,
            PREP_ELEV(eq.ELEVATION_ID),
            eq.ROOM_ID,
            eq.SYSTEM_ID,
            eq.TYPE_ID) 
         BULK COLLECT INTO CARDS_ON_TA_WITH_RESP_       
         FROM ACT_TAGOUT_SECTION_TAGS atst
         INNER JOIN EQUIP eq ON eq.EQUIP_OPERATOR_ID  = atst.EQUIP_OPERATOR_ID
         WHERE 
             TRIM(atst.TAGOUT_TYPE_ID) = TRIM(TO_ID_)
             AND TRIM(atst.ACT_TAGOUT_NUMBER) = TRIM(FONAME_)
             AND TRIM(atst.ACT_TAGOUT_SECTION_NUMBER) = TRIM(TONAME_)
             AND (eq.EQUIP_OPERATOR_ID IS NOT NULL OR LENGTH(eq.EQUIP_OPERATOR_ID) > 0)  
             ORDER BY atst.TAG_PLACE_SEQ ASC, atst.TAG_SORT_ORDER ASC;
     
     -- Move Cards without responsibility to another table... 
     SELECT RSC_TO_RESP_RECORD (val1, val2, val3, val4, val5, val6, val7, val8, val9, val10)
        BULK COLLECT INTO CARDS_WO_RESP_
        FROM TABLE(CARDS_ON_TA_WITH_RESP_)
        WHERE TRIM(val3) = '' OR val3 IS NULL;
     
     IF (CARDS_WO_RESP_ IS NOT NULL AND CARDS_WO_RESP_.COUNT > 0) THEN    
        FOR u IN CARDS_WO_RESP_.FIRST .. CARDS_WO_RESP_.LAST
        LOOP
            INSERT INTO RSC_EQUIP_NO_RESP (EQUIP_ID, EQUIP_DESC, BUILDING_ID, ELEVATION_ID, ROOM_ID, SYSTEM_ID, TYPE_ID, JOB_ID)
                VALUES (CARDS_WO_RESP_(u).val1, CARDS_WO_RESP_(u).val2, CARDS_WO_RESP_(u).val6, CARDS_WO_RESP_(u).val7, 
                CARDS_WO_RESP_(u).val8, CARDS_WO_RESP_(u).val9, CARDS_WO_RESP_(u).val10, JOB_ID_);
        END LOOP;
     END IF;
     -- and delete the empty ones.
     FOR i IN CARDS_ON_TA_WITH_RESP_.FIRST .. CARDS_ON_TA_WITH_RESP_.LAST
     LOOP
        IF ((CARDS_ON_TA_WITH_RESP_(i).val3 IS NOT NULL AND LENGTH(TRIM(CARDS_ON_TA_WITH_RESP_(i).val3)) = 0) OR CARDS_ON_TA_WITH_RESP_(i).val3 IS NULL) THEN
            --dbms_output.put_line('The equipment ' || CARDS_ON_TA_WITH_RESP_(i).val1 || ' does not have a responsibility attached. All cards applied to this equipment will not be processed.');
            CARDS_ON_TA_WITH_RESP_.DELETE(i);
        END IF; 
     END LOOP;
        
     SELECT DISTINCT(val3) BULK COLLECT INTO DISTINCTIVE_USER_GROUPS_IN_TO_
     FROM TABLE(CARDS_ON_TA_WITH_RESP_) WHERE val3 IN 
        (SELECT USERGROUP_NAME FROM RSC_TIME_KEYS); -- KOND is currently not here!
     
     IF (DISTINCTIVE_USER_GROUPS_IN_TO_ IS NULL OR DISTINCTIVE_USER_GROUPS_IN_TO_.COUNT = 0) THEN
        dbms_output.put_line('The TO '  || FONAME_  || ' - ' || TONAME_ ||  ' does not have any cards with responsibility attached.'); 
        RETURN NULL;       
     END IF;
   
   -- CALCULATES ALL CARDS FOR STEP 3 (ONLY ONCE, SAVE TIME AND SPACE :))
    CARD_STEP_3_CALC_ := CALCULATE_UG_STEP_3(DISTINCTIVE_USER_GROUPS_IN_TO_, CARDS_ON_TA_WITH_RESP_, OPER_TYPE_, UG_STEP3_TIME_);
    
    IF (DISTINCTIVE_USER_GROUPS_IN_TO_ IS NOT NULL AND DISTINCTIVE_USER_GROUPS_IN_TO_.COUNT > 0) THEN
        FOR i IN DISTINCTIVE_USER_GROUPS_IN_TO_.FIRST .. DISTINCTIVE_USER_GROUPS_IN_TO_.LAST
        LOOP
            CURRENT_USER_GROUP_ := DISTINCTIVE_USER_GROUPS_IN_TO_(i);
            IF (CURRENT_USER_GROUP_ IS NULL OR CURRENT_USER_GROUP_ = '') THEN
                dbms_output.put_line('Empty group.');
                GOTO ST_LOOP;
            END IF;
            
            GID_ := '';
            CURRENT_CARDS_FOR_GROUP_ := NULL;
            SELECT COUNT(*) INTO CUR_CARD_NUM_ FROM TABLE(CARDS_ON_TA_WITH_RESP_) WHERE val3 = DISTINCTIVE_USER_GROUPS_IN_TO_(i);
            SELECT RSC_TO_RESP_RECORD (val1, val2, val3, val4, val5, val6, val7, val8, val9, val10) BULK COLLECT INTO CURRENT_CARDS_FOR_GROUP_
                FROM TABLE(CARDS_ON_TA_WITH_RESP_) WHERE val3 = CURRENT_USER_GROUP_;
            SELECT USE_STEP1, USE_STEP2, USE_STEP3, USE_STEP4, USE_STEP5, USERGROUP_CODE INTO UG_PREFORM_ST_1_,
                UG_PREFORM_ST_2_, UG_PREFORM_ST_3_, UG_PREFORM_ST_4_, UG_PREFORM_ST_5_ , GID_ 
                FROM RSC_TIME_KEYS WHERE USERGROUP_NAME = CURRENT_USER_GROUP_;   
                
           UG_STEP_TIME_ := 0;        
                    
            IF (UG_PREFORM_ST_2_ = 1) THEN
                CALCULATE_UG_STEP_2(CURRENT_USER_GROUP_, CURRENT_CARDS_FOR_GROUP_, UG_STEP_TIME_);        
                CALC_.EXTEND;       
                CALC_(CALC_.COUNT) := RSC_CALC_RECORD(CURRENT_USER_GROUP_, GID_, 2, TO_ID_, FONAME_, TONAME_, 
                                                        UG_STEP_TIME_, 0, 100, 0, 0, 0, 0, 0, 0, 0, 0); -- Lag  se racuna kasneje
                UG_STEP_TIME_ := 0;
            END IF;
            IF (UG_PREFORM_ST_3_ = 1) THEN
               SELECT RSC_CARD_CALC_RECORD(IDx, USER_GROUP, CARD_ID, BUILDING_ID, CARD_SEQUENCE, 
                                                                 CARD_POSTITION, WALK_TIME, TIME_MIN, LOC_LAG) 
                BULK COLLECT INTO ST_3_RESULT_CARDS_ 
                FROM TABLE(CARD_STEP_3_CALC_) WHERE USER_GROUP = CURRENT_USER_GROUP_;
               
               IF (ST_3_RESULT_CARDS_ IS NOT NULL) THEN
                   FOR j IN ST_3_RESULT_CARDS_.FIRST .. ST_3_RESULT_CARDS_.LAST
                   LOOP
                       CALC_.EXTEND;       
                       CALC_(CALC_.COUNT) := RSC_CALC_RECORD(CURRENT_USER_GROUP_, GID_, 3, TO_ID_, FONAME_, TONAME_, 
                                                            ST_3_RESULT_CARDS_(j).TIME_MIN, 0, 100, ST_3_RESULT_CARDS_(j).LOC_LAG, 0, 0, 0, 0, 0, 0, 0); -- Lag in Load se racuna kasneje
                   END LOOP;
                END IF;
            END IF;
            IF (UG_PREFORM_ST_4_ = 1) THEN
                CALCULATE_UG_STEP_4(CURRENT_USER_GROUP_, CURRENT_CARDS_FOR_GROUP_, UG_STEP_TIME_);
                CALC_.EXTEND;       
                CALC_(CALC_.COUNT) := RSC_CALC_RECORD(CURRENT_USER_GROUP_, GID_, 4, TO_ID_, FONAME_, TONAME_, 
                                                        UG_STEP_TIME_, 0, 100, 0, 0, 0, 0, 0, 0, 0, 0); -- Lag se racuna kasneje
                UG_STEP_TIME_ := 0;
            END IF;
            IF (UG_PREFORM_ST_5_ = 1) THEN
               CALCULATE_UG_STEP_5(CURRENT_USER_GROUP_, UG_STEP_TIME_);
               CALC_.EXTEND;       
               CALC_(CALC_.COUNT) := RSC_CALC_RECORD(CURRENT_USER_GROUP_, GID_, 5, TO_ID_, FONAME_, TONAME_, 
                                                        UG_STEP_TIME_, 0, 100, 0, 0, 0, 0, 0, 0, 0, 0); -- Lag  se racuna kasneje
               UG_STEP_TIME_ := 0;
            END IF;

            <<ST_LOOP>>
            NULL;
        END LOOP;  
     END IF;
    
    -- CALCS FOR VI
    CURRENT_USER_GROUP_ := 'VI';
    SELECT USE_STEP1, USERGROUP_CODE INTO UG_PREFORM_ST_1_, GID_ 
        FROM RSC_TIME_KEYS WHERE USERGROUP_NAME = CURRENT_USER_GROUP_;   
     UG_STEP_TIME_ := 0;
     IF (UG_PREFORM_ST_1_ = 1) THEN
         CALCULATE_UG_STEP_1(CURRENT_USER_GROUP_, UG_STEP_TIME_);
         CALC_.EXTEND;
         CALC_(CALC_.COUNT) := RSC_CALC_RECORD(CURRENT_USER_GROUP_, GID_, 1, TO_ID_, FONAME_, TONAME_, 
                    UG_STEP_TIME_, 0, 100, 0, 0, 0, 0, 0, 0, 0, 0);
     END IF;    

    -- CALCS FOR GO   
    CURRENT_USER_GROUP_ := 'GO';
    CURRENT_CARDS_FOR_GROUP_ := NULL;
    SELECT RSC_TO_RESP_RECORD (val1, val2, val3, val4, val5, val6, val7, val8, val9, val10) BULK COLLECT INTO CURRENT_CARDS_FOR_GROUP_
        FROM TABLE(CARDS_ON_TA_WITH_RESP_);
    SELECT USE_STEP1, USE_STEP2, USE_STEP3, USE_STEP4, USE_STEP5, USERGROUP_CODE INTO UG_PREFORM_ST_1_,
        UG_PREFORM_ST_2_, UG_PREFORM_ST_3_, UG_PREFORM_ST_4_, UG_PREFORM_ST_5_ , GID_ 
        FROM RSC_TIME_KEYS WHERE USERGROUP_NAME = CURRENT_USER_GROUP_;   
     UG_STEP_TIME_ := 0; 
     
     IF (UG_PREFORM_ST_2_ = 1) THEN
        CALCULATE_UG_STEP_2(CURRENT_USER_GROUP_, CURRENT_CARDS_FOR_GROUP_, UG_STEP_TIME_);
        CALC_.EXTEND;
        CALC_(CALC_.COUNT) := RSC_CALC_RECORD(CURRENT_USER_GROUP_, GID_, 2, TO_ID_, FONAME_, TONAME_, 
            UG_STEP_TIME_, 0, 100, 0, 0, 0, 0, 0, 0, 0, 0);
     END IF;
     IF (UG_PREFORM_ST_3_ = 1) THEN   
        CALC_.EXTEND;
        CALC_(CALC_.COUNT) := RSC_CALC_RECORD(CURRENT_USER_GROUP_, GID_, 3, TO_ID_, FONAME_, TONAME_, 
            UG_STEP3_TIME_, 0, 25, 0, 0, 0, 0, 0, 0, 0, 0);
     END IF;
     IF (UG_PREFORM_ST_4_ = 1) THEN
        SUM_TI_ := NULL;
        SELECT RSC_NUM_VAL_RECORD(SUM(TI)) BULK COLLECT INTO SUM_TI_ FROM TABLE(CALC_) WHERE STEP_ID = 4
            GROUP BY USER_GROUP;
        SELECT MAX(val) INTO UG_STEP_TIME_ FROM TABLE(SUM_TI_);
        CALC_.EXTEND;
        CALC_(CALC_.COUNT) := RSC_CALC_RECORD(CURRENT_USER_GROUP_, GID_, 4, TO_ID_, FONAME_, TONAME_, 
            UG_STEP_TIME_, 0, 25, 0, 0, 0, 0, 0, 0, 0, 0);            
     END IF;
     IF (UG_PREFORM_ST_5_ = 1) THEN
        CALCULATE_UG_STEP_5(CURRENT_USER_GROUP_, UG_STEP_TIME_);
        CALC_.EXTEND;
        CALC_(CALC_.COUNT) := RSC_CALC_RECORD(CURRENT_USER_GROUP_, GID_, 5, TO_ID_, FONAME_, TONAME_, 
            UG_STEP_TIME_, 0, 100, 0, 0, 0, 0, 0, 0, 0, 0);        
     END IF;   
     
     FULL_TIME_ := CALCULATE_TAGOUT_TIME_NOTAB(CALC_, UG_STEP3_TIME_);
     NORM_FULL_TIME_ := CEIL(FULL_TIME_/60);
     IF (FULL_TIME_ > 0) THEN TIME_CH_RATIO_ := NORM_FULL_TIME_ / (FULL_TIME_ / 60); ELSE TIME_CH_RATIO_ := 0; END IF;
    
     CALC_LAG(CALC_); -- CALCULTE LAGS
     
     STRETCH_TIMES(CALC_, TIME_CH_RATIO_); -- STRECH TIMES (NORMALIZATION STEP 1) 

     NORMALIZE_RESOURCES(CALC_, NORM_FULL_TIME_, OPER_TYPE_, TO_ID_, JOB_ID_);

     WRITE_TO_DB(CALC_, OPER_TYPE_, TO_ID_, JOB_ID_);   
    -- FOR DEBUG ONLY
    --DEBUG_CALC_TABLE(CALC_); 
    ST3_MAX_TIME_ := UG_STEP3_TIME_;
    
    RETURN CALC_;    
EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || '). Tagout info: '  || TO_ID_  || ', '  || FONAME_  || ', '  || TONAME_  ||
    '. Equip: '  || TO_EQUIP_  || '.' , 'CHECK_TAGOUT');
    RETURN NULL;
END CHECK_TAGOUT;  

PROCEDURE NORMALIZE_RESOURCES(CALC_ IN OUT RSC_CALC_TABLE, SUM_TIME IN NUMBER, OPER_TYPE_ IN VARCHAR2, TA_ IN VARCHAR2, JOB_ID_ IN NUMBER)
/********************************************************************************
\ STRETCH_TIMES - Stretch times and lags as a normalization process
********************************************************************************/         
IS
    REC_                RSC_CALC_RECORD;
    RES_REC_         RSC_CALC_RECORD;
    SUM_RECORD_ RSC_CALC_RECORD;
    RESULTS_         RSC_CALC_TABLE;
    FIN_RESULTS_   RSC_CALC_TABLE;
    CURR_RES_       RSC_CALC_TABLE;
    
    GROUPS_         RSC_STR_TABLE;
    
    BEGIN_H_ NUMBER; END_H_ NUMBER; i NUMBER; k NUMBER; MULTI_STEPS_ NUMBER;
    BLOCK_DUR_ NUMBER; CNT_ NUMBER; BLOCK_LAG_ NUMBER; F_LOAD_ NUMBER; BLOCK_LOA_ NUMBER;
    z NUMBER;
    CURSOR_GRP_ VARCHAR2(150);
    
    CURSOR getResForHour IS 
        SELECT
            RSC_CALC_RECORD(USER_GROUP, USER_GROUP_ID, STEP_ID, TO_ID, FONAME, TONAME, TI,
            LA, LO, LOC_LAG, TI_STRECH, LA_STRECH, LO_STRECH, TI_NORMAL, LA_NORMAL, LO_NORMAL, HOURLY_BLOCK)
         FROM TABLE (CALC_) 
         WHERE 
            LA_STRECH/60 >= BEGIN_H_ AND LA_STRECH/60 < END_H_;

    CURSOR getNormResForHour IS 
        SELECT
            RSC_CALC_RECORD(USER_GROUP, USER_GROUP_ID, STEP_ID, TO_ID, FONAME, TONAME, TI,
            LA, LO, LOC_LAG, TI_STRECH, LA_STRECH, LO_STRECH, TI_NORMAL, LA_NORMAL, LO_NORMAL, HOURLY_BLOCK)
         FROM TABLE (RESULTS_) 
         WHERE 
            LA_NORMAL = i AND USER_GROUP = CURSOR_GRP_;
            
    CURSOR getDistGroups IS 
        SELECT
            DISTINCT(USER_GROUP)
         FROM TABLE (RESULTS_)
         WHERE 
            LA_NORMAL = i;  
BEGIN
    i := 1;
    RESULTS_ := RSC_CALC_TABLE();
    
    -- Normalizes LAG/TIME and calculates LOAD for every tag into hourly blocks
    WHILE i <= SUM_TIME 
    LOOP
        BEGIN_H_ := i-1;
        END_H_ := i;
    
        CURR_RES_ := RSC_CALC_TABLE();
        OPEN getResForHour;
        FETCH getResForHour BULK COLLECT INTO CURR_RES_;
        CLOSE getResForHour;
        
        IF (CURR_RES_ IS NOT NULL AND CURR_RES_.COUNT > 0) THEN
            FOR j IN CURR_RES_.FIRST .. CURR_RES_.LAST 
            LOOP
                REC_ := CURR_RES_(j);
                k := BEGIN_H_; MULTI_STEPS_ := 0;
                               
                IF (((REC_.TI_STRECH+REC_.LA_STRECH)/60)-BEGIN_H_ < 1) THEN -- The complete resource is in the same hour block
                    RES_REC_ := RSC_CALC_RECORD(REC_.USER_GROUP, REC_.USER_GROUP_ID, REC_.STEP_ID, REC_.TO_ID, REC_.FONAME, REC_.TONAME, 
                                                                    REC_.TI, REC_.LA, REC_.LO, REC_.LOC_LAG, REC_.TI_STRECH, REC_.LA_STRECH, REC_.LO_STRECH, 1, BEGIN_H_, (REC_.TI_STRECH/60)*REC_.LO_STRECH, i);
                    RESULTS_.EXTEND;
                    RESULTS_(RESULTS_.COUNT) := RES_REC_;
                ELSE -- The complete resource is streched over multiple hour blocks
                    MULTI_STEPS_ := CEIL((REC_.TI_STRECH+REC_.LA_STRECH)/60) - CEIL((REC_.LA_STRECH)/60) + 1;
                    CNT_ := 1;
                    z := 0;
                    WHILE z < MULTI_STEPS_
                    LOOP
                        IF (CNT_ = 1) THEN
                            BLOCK_DUR_ := CEIL(REC_.LA_STRECH/60)-(REC_.LA_STRECH/60); -- 1st block load
                            BLOCK_LAG_ := BEGIN_H_+z;
                            BLOCK_LOA_ := BLOCK_DUR_*REC_.LO_STRECH;
                        ELSIF (z+1 >= MULTI_STEPS_) THEN -- last block load
                            BLOCK_DUR_ := ((REC_.LA_STRECH+REC_.TI_STRECH)/60)  - FLOOR((REC_.LA_STRECH+REC_.TI_STRECH)/60);
                            BLOCK_LAG_ := BEGIN_H_+z;
                            BLOCK_LOA_ := BLOCK_DUR_*REC_.LO_STRECH;
                        ELSE -- all in between
                            BLOCK_DUR_ := 1;
                            BLOCK_LAG_ := BEGIN_H_+z;
                            BLOCK_LOA_ := REC_.LO_STRECH;
                        END IF;
                        RES_REC_ := RSC_CALC_RECORD(REC_.USER_GROUP, REC_.USER_GROUP_ID, REC_.STEP_ID, REC_.TO_ID, REC_.FONAME, REC_.TONAME, 
                                                                         REC_.TI, REC_.LA, REC_.LO, REC_.LOC_LAG, REC_.TI_STRECH, REC_.LA_STRECH, REC_.LO_STRECH, 1, BLOCK_LAG_, BLOCK_LOA_, i);
                        RESULTS_.EXTEND;
                        RESULTS_(RESULTS_.COUNT) := RES_REC_;                 
                        CNT_ := CNT_ +1;
                        z := z+1;
                    END LOOP;
                END IF;
            END LOOP;
        END IF; 
        i := i+1; -- next hour
    END LOOP;
    CALC_ := RESULTS_;
    
--    IF (REC_.TONAME = 'CW-0016- -CW SISTEM DRENIRANJE') THEN
--    null;
--    END IF;
    -- Joins blocks with the same group in lag hour segment into one
    FIN_RESULTS_ := RSC_CALC_TABLE();
    i := 0;
    WHILE i < SUM_TIME 
    LOOP
        GROUPS_ := RSC_STR_TABLE();
        OPEN getDistGroups;
        FETCH getDistGroups BULK COLLECT INTO GROUPS_;
        CLOSE getDIstGroups;
    
        IF (GROUPS_ IS NOT NULL AND GROUPS_.COUNT > 0) THEN
            FOR m IN GROUPS_.FIRST .. GROUPS_.LAST
            LOOP          
                CURSOR_GRP_ := GROUPS_(m);
            
                CURR_RES_ := RSC_CALC_TABLE();
                OPEN getNormResForHour;
                FETCH getNormResForHour BULK COLLECT INTO CURR_RES_;
                CLOSE getNormResForHour;
                
                F_LOAD_ := 0;
                IF (CURR_RES_ IS NOT NULL AND CURR_RES_.COUNT > 0) THEN
                    FOR n IN CURR_RES_.FIRST .. CURR_RES_.LAST
                    LOOP
                        REC_ := CURR_RES_(n);
                        F_LOAD_ := F_LOAD_ + CURR_RES_(n).LO_NORMAL;
                    END LOOP;
                   
                    SUM_RECORD_ := RSC_CALC_RECORD(REC_.USER_GROUP, REC_.USER_GROUP_ID, REC_.STEP_ID, REC_.TO_ID, REC_.FONAME, REC_.TONAME, 
                                                                        REC_.TI, REC_.LA, REC_.LO, REC_.LOC_LAG, REC_.TI_STRECH, REC_.LA_STRECH, REC_.LO_STRECH, 
                                                                        REC_.TI_NORMAL, i, F_LOAD_, i+1);
                    FIN_RESULTS_.EXTEND;
                    FIN_RESULTS_(FIN_RESULTS_.COUNT) := SUM_RECORD_; 
                
--                    dbms_output.put_line('P: '  || TO_CHAR(-99) || ', '  || TO_CHAR(SUM_RECORD_.USER_GROUP) || ', ' || TO_CHAR(SUM_RECORD_.USER_GROUP_ID) || ', ' || TO_CHAR(SUM_RECORD_.FONAME) || ', '
--                                                          || TO_CHAR(SUM_RECORD_.TONAME) || ', ' || TO_CHAR('TEST') || ', ' || TO_CHAR(SUM_RECORD_.TO_ID) || ', ' || TO_CHAR('TESTID') || ', ' || TO_CHAR('TEST') || ', '
--                                                          || TO_CHAR(SUM_RECORD_.TI_NORMAL) || ', ' || TO_CHAR(SUM_RECORD_.LA_NORMAL) || ', ' || TO_CHAR(SUM_RECORD_.LO_NORMAL) || '. ');
                END IF;           
            END LOOP;
        END IF; 
        i := i+1; -- next hour
    END LOOP;

    WRITE_TO_DB_FINAL(FIN_RESULTS_, OPER_TYPE_, TA_, JOB_ID_);
    
EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NORMALIZE_RESOURCES');
END NORMALIZE_RESOURCES; 

PROCEDURE STRETCH_TIMES(CALC_ IN OUT RSC_CALC_TABLE, TIME_CH_RATIO_ IN NUMBER) 
/********************************************************************************
\ STRETCH_TIMES - Stretch times and lags as a normalization process
********************************************************************************/         
IS 
    REC_ RSC_CALC_RECORD;
BEGIN

    IF (CALC_ IS NOT NULL AND CALC_.COUNT > 0) THEN
        FOR i IN CALC_.FIRST .. CALC_.LAST 
        LOOP
            REC_ := CALC_(i);
                        
            REC_.TI_STRECH := REC_.TI * TIME_CH_RATIO_;
            REC_.LA_STRECH := REC_.LA * TIME_CH_RATIO_;
            IF (REC_.TI_STRECH > 0) THEN
                REC_.LO_STRECH := (REC_.LO/100)-((REC_.LO/100) * (1 - (REC_.TI  / REC_.TI_STRECH)));
            ELSE 
                REC_.LO_STRECH := 0;
            END IF;
            CALC_(i) := REC_;
        END LOOP;
    END IF; 
    
EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'STRETCH_TIMES');
END STRETCH_TIMES; 

PROCEDURE CALC_LAG(TABLE_ IN OUT RSC_CALC_TABLE)
/********************************************************************************
\ CALC_LAG - calculate lag
********************************************************************************/         
IS 
    UG_ VARCHAR2(50);
    MAX_TI_ NUMBER;
    
    GEN_CUR_LAG_ NUMBER;
    STEP_RES_ RSC_CALC_TABLE;
    RESULT_ RSC_CALC_TABLE;
BEGIN
    GEN_CUR_LAG_ := 0;
    RESULT_ := RSC_CALC_TABLE();   

    FOR i IN 1 .. 5
    LOOP
        IF (i = 1) THEN           
            SELECT RSC_CALC_RECORD(USER_GROUP, USER_GROUP_ID, STEP_ID, TO_ID, FONAME, TONAME, 
                        TI, LA, LO, LOC_LAG, TI_STRECH, LA_STRECH, LO_STRECH, TI_NORMAL, LA_NORMAL, LO_NORMAL, HOURLY_BLOCK) 
            BULK COLLECT INTO STEP_RES_ FROM 
                TABLE(TABLE_) WHERE STEP_ID = 1;
            FOR j IN STEP_RES_.FIRST .. STEP_RES_.LAST
            LOOP
                STEP_RES_(j).LA := 0;
                RESULT_.EXTEND;
                RESULT_(RESULT_.COUNT) := STEP_RES_(j);
            END LOOP;
        END IF;        
        IF (i = 2)  THEN
            STEP_RES_ := NULL;
            SELECT MAX(TI) INTO MAX_TI_ FROM TABLE(TABLE_)  -- max time in step one is gen leg for step 2
                    WHERE STEP_ID = 1;
            GEN_CUR_LAG_ := MAX_TI_;
            SELECT RSC_CALC_RECORD(USER_GROUP, USER_GROUP_ID, STEP_ID, TO_ID, FONAME, TONAME, 
                        TI, LA, LO, LOC_LAG, TI_STRECH, LA_STRECH, LO_STRECH, TI_NORMAL, LA_NORMAL, LO_NORMAL, HOURLY_BLOCK) 
            BULK COLLECT INTO STEP_RES_ FROM 
                TABLE(TABLE_) WHERE STEP_ID = 2;
            FOR j IN STEP_RES_.FIRST .. STEP_RES_.LAST
            LOOP
                STEP_RES_(j).LA :=GEN_CUR_LAG_ ;
                RESULT_.EXTEND;
                RESULT_(RESULT_.COUNT) := STEP_RES_(j);
            END LOOP;
        END IF;
        IF (i = 3) THEN
            STEP_RES_ := NULL;
            SELECT MAX(TI) INTO MAX_TI_ FROM TABLE(TABLE_)  -- max time in step two
                    WHERE STEP_ID = 2;        
            GEN_CUR_LAG_ := GEN_CUR_LAG_ + MAX_TI_; -- and one frome step 1 are gen. lag for step 3.         
            SELECT RSC_CALC_RECORD(USER_GROUP, USER_GROUP_ID, STEP_ID, TO_ID, FONAME, TONAME, 
                        TI, LA, LO, LOC_LAG, TI_STRECH, LA_STRECH, LO_STRECH, TI_NORMAL, LA_NORMAL, LO_NORMAL, HOURLY_BLOCK) 
            BULK COLLECT INTO STEP_RES_ FROM 
                TABLE(TABLE_) WHERE STEP_ID = 3;
            FOR j IN STEP_RES_.FIRST .. STEP_RES_.LAST
            LOOP
                STEP_RES_(j).LA :=GEN_CUR_LAG_ + STEP_RES_(j).LOC_LAG ;
                RESULT_.EXTEND;
                RESULT_(RESULT_.COUNT) := STEP_RES_(j);            
            END LOOP;            
        END IF;
        IF (i = 4) THEN 
            STEP_RES_ := NULL;
            SELECT MAX(TI) INTO MAX_TI_ FROM TABLE(TABLE_)  -- max time in step two
                    WHERE STEP_ID = 3;        
            GEN_CUR_LAG_ := GEN_CUR_LAG_ + MAX_TI_; -- and one frome step 1 are gen. lag for step 3.         
            SELECT RSC_CALC_RECORD(USER_GROUP, USER_GROUP_ID, STEP_ID, TO_ID, FONAME, TONAME, 
                    TI, LA, LO, LOC_LAG, TI_STRECH, LA_STRECH, LO_STRECH, TI_NORMAL, LA_NORMAL, LO_NORMAL, HOURLY_BLOCK) 
            BULK COLLECT INTO STEP_RES_ FROM 
                TABLE(TABLE_) WHERE STEP_ID = 4;
            FOR j IN STEP_RES_.FIRST .. STEP_RES_.LAST
            LOOP
                STEP_RES_(j).LA :=GEN_CUR_LAG_;
                RESULT_.EXTEND;
                RESULT_(RESULT_.COUNT) := STEP_RES_(j);
            END LOOP;                   
        END IF ;
        IF (i = 5) THEN
            STEP_RES_ := NULL;
            SELECT MAX(TI) INTO MAX_TI_ FROM TABLE(TABLE_)  -- max time in step two
                    WHERE STEP_ID = 4;        
            GEN_CUR_LAG_ := GEN_CUR_LAG_ + MAX_TI_; -- and one frome step 1 are gen. lag for step 3.         
            SELECT RSC_CALC_RECORD(USER_GROUP, USER_GROUP_ID, STEP_ID, TO_ID, FONAME, TONAME, 
                    TI, LA, LO, LOC_LAG, TI_STRECH, LA_STRECH, LO_STRECH, TI_NORMAL, LA_NORMAL, LO_NORMAL, HOURLY_BLOCK) 
            BULK COLLECT INTO STEP_RES_ FROM 
                TABLE(TABLE_) WHERE STEP_ID = 5;
            FOR j IN STEP_RES_.FIRST .. STEP_RES_.LAST
            LOOP
                STEP_RES_(j).LA :=GEN_CUR_LAG_;
                RESULT_.EXTEND;
                RESULT_(RESULT_.COUNT) := STEP_RES_(j);
            END LOOP;          
        END IF;
    END LOOP;
   
    TABLE_ := RESULT_;
    
EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'CALC_LAG');
END CALC_LAG;  

FUNCTION CALCULATE_TAGOUT_TIME(CURR_RESULTS_ IN RSC_CALC_TABLE, TI_3_ IN NUMBER, FULL_RESULTS_ IN OUT RSC_CALC_TABLE) RETURN NUMBER
/********************************************************************************
\ CALCULATE_TAGOUT_TIME - calculates Tagout time
********************************************************************************/         
IS 
    TI_1_ NUMBER; TI_2_ NUMBER; TI_4_ NUMBER; TI_5_ NUMBER;   

BEGIN
        IF (CURR_RESULTS_ IS NOT NULL) THEN
            FOR j IN CURR_RESULTS_.FIRST .. CURR_RESULTS_.LAST
            LOOP
                FULL_RESULTS_.EXTEND;
                FULL_RESULTS_(FULL_RESULTS_.COUNT) := CURR_RESULTS_(j);
            END LOOP;
        
            SELECT MAX(TI) INTO TI_1_ FROM TABLE(CURR_RESULTS_) 
                WHERE STEP_ID = 1;
            SELECT MAX(TI) INTO TI_2_ FROM TABLE(CURR_RESULTS_) 
                WHERE STEP_ID = 2;
            SELECT MAX(TI) INTO TI_4_ FROM TABLE(CURR_RESULTS_) 
                WHERE STEP_ID = 4;
            SELECT MAX(TI) INTO TI_5_ FROM TABLE(CURR_RESULTS_) 
                WHERE STEP_ID = 5;
                
           RETURN TI_1_ + TI_2_ + TI_3_ + TI_4_ + TI_5_;
        END IF;
        
        RETURN 0;
EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'CALCULATE_TAGOUT_TIME');
    RETURN 0;
END CALCULATE_TAGOUT_TIME;  

FUNCTION CALCULATE_TAGOUT_TIME_NOTAB(CURR_RESULTS_ IN RSC_CALC_TABLE, TI_3_ IN NUMBER) RETURN NUMBER
/********************************************************************************
\ CALCULATE_TAGOUT_TIME_NOTAB - calculates Tagout time and does not return table
********************************************************************************/         
IS 
    TI_1_ NUMBER; TI_2_ NUMBER; TI_4_ NUMBER; TI_5_ NUMBER;   

BEGIN
        IF (CURR_RESULTS_ IS NOT NULL) THEN      
            SELECT MAX(TI) INTO TI_1_ FROM TABLE(CURR_RESULTS_) 
                WHERE STEP_ID = 1;
            SELECT MAX(TI) INTO TI_2_ FROM TABLE(CURR_RESULTS_) 
                WHERE STEP_ID = 2;
            SELECT MAX(TI) INTO TI_4_ FROM TABLE(CURR_RESULTS_) 
                WHERE STEP_ID = 4;
            SELECT MAX(TI) INTO TI_5_ FROM TABLE(CURR_RESULTS_) 
                WHERE STEP_ID = 5;
                
           RETURN TI_1_ + TI_2_ + TI_3_ + TI_4_ + TI_5_;
        END IF;
        
        RETURN 0;
EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'CALCULATE_TAGOUT_TIME_NOTAB');
    RETURN 0;
END CALCULATE_TAGOUT_TIME_NOTAB;  

PROCEDURE CALC_LOAD(TABLE_ IN OUT RSC_CALC_TABLE)
/********************************************************************************
\ CALC_LOAD - calculates load
********************************************************************************/         
IS 
    MAX_T2_ NUMBER;
    MAX_T3_ NUMBER;
    MAX_T4_ NUMBER;
    MAX_T5_ NUMBER;
BEGIN
--    SELECT MAX(T2) INTO MAX_T2_ FROM TABLE(TABLE_);
--    SELECT MAX(T3) INTO MAX_T3_ FROM TABLE(TABLE_);
--    SELECT MAX(T4) INTO MAX_T4_ FROM TABLE(TABLE_);
--    SELECT MAX(T4) INTO MAX_T5_ FROM TABLE(TABLE_);
    
--    FOR i IN TABLE_.FIRST .. TABLE_.LAST
--    LOOP
--        IF (TABLE_(i).USER_GROUP <> 'VI' AND TABLE_(i).USER_GROUP <> 'VI(CNA)' AND TABLE_(i).USER_GROUP <> 'GO') THEN       
--            --TABLE_(i).LO1 := TABLE_(i).T1 / MAX_T1_;    
--            IF (MAX_T2_ <> 0) THEN TABLE_(i).LO2 := TABLE_(i).T2 / MAX_T2_; END IF;
--            IF (MAX_T3_ <> 0) THEN TABLE_(i).LO3 := TABLE_(i).T3 / MAX_T3_; END IF;
--            IF (MAX_T4_ <> 0) THEN TABLE_(i).LO4 := TABLE_(i).T4 / MAX_T4_; END IF;
--            IF (MAX_T5_ <> 0) THEN TABLE_(i).LO5 := TABLE_(i).T5 / MAX_T5_; END IF;
--        END IF;
--    END LOOP;
   NULL;
EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'CALC_LOAD');
END CALC_LOAD;  

PROCEDURE WRITE_TO_DB(TABLE_ IN RSC_CALC_TABLE, OPER_TYPE_ IN VARCHAR2, TA_ID_ IN VARCHAR2, JOB_ID_ IN NUMBER)         
/********************************************************************************
\ WRITE_TO_DB - writes results to the database.
********************************************************************************/
IS 
    TAG_ACTION_ID_ INTEGER;
    LOAD_SUM_ NUMBER;
    ACTIVITY_   VARCHAR2(150);
BEGIN
    FOR i IN TABLE_.FIRST..TABLE_.LAST
    LOOP
            
            IF (OPER_TYPE_ = 'REMOVE') THEN 
                SELECT TAG_REMOVE_OBJECT_ID, TAG_REMOVE_ACTIVITY INTO TAG_ACTION_ID_, ACTIVITY_ FROM 
                    ACT_TAGOUT_SECTION_SCHEDULE 
                WHERE TAGOUT_TYPE_ID = TA_ID_ AND ACT_TAGOUT_NUMBER =  TABLE_(i).FONAME
                    AND ACT_TAGOUT_SECTION_NUMBER =TABLE_(i).TONAME;
            ELSIF (OPER_TYPE_ = 'HANG') THEN         
                 SELECT TAG_HANG_OBJECT_ID, TAG_HANG_ACTIVITY INTO TAG_ACTION_ID_, ACTIVITY_ FROM 
                    ACT_TAGOUT_SECTION_SCHEDULE 
                WHERE TAGOUT_TYPE_ID = TA_ID_ AND ACT_TAGOUT_NUMBER =  TABLE_(i).FONAME
                    AND ACT_TAGOUT_SECTION_NUMBER =TABLE_(i).TONAME;
            END IF;
                    
            INSERT INTO RSC_RESULT_GROUP (GROUP_NAME, GROUP_NAME_ID, TO_FOLDER, TO_NAME, 
                OPER, TA_ID, RES_LAG_MINUTE, RES_TIME_MINUTE, RES_LOAD, LOC_RES_LAG, RES_STEP, TAG_ACTION_ID, JOB_ID, RES_LAG, RES_TIME, RES_ACTIVITY_VALUE,
                RES_LAG_STRECH, RES_TIME_STRECH, RES_LAG_NORMAL, RES_TIME_NORMAL, RES_LOAD_NORMAL, HOURLY_BLOCK)
            VALUES (TABLE_(i).USER_GROUP, TABLE_(i).USER_GROUP_ID, TABLE_(i).FONAME, TABLE_(i).TONAME, 
                OPER_TYPE_, TABLE_(i).TO_ID, TABLE_(i).LA, TABLE_(i).TI, TABLE_(i).LO, TABLE_(i).LOC_LAG, TABLE_(i).STEP_ID,
                TAG_ACTION_ID_, JOB_ID_, TABLE_(i).LA_STRECH/60, TABLE_(i).TI_STRECH/60, ACTIVITY_, TABLE_(i).LA_STRECH, TABLE_(i).TI_STRECH, 
                TABLE_(i).LA_NORMAL, TABLE_(i).TI_NORMAL, TABLE_(i).LO_NORMAL, TABLE_(i).HOURLY_BLOCK); 
           
    END LOOP;
    
EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'WRITE_TO_DB');
END WRITE_TO_DB;  

PROCEDURE WRITE_TO_DB_FINAL(TABLE_ IN RSC_CALC_TABLE, OPER_TYPE_ IN VARCHAR2, TA_ID_ IN VARCHAR2, JOB_ID_ IN NUMBER)         
/********************************************************************************
\ WRITE_TO_DB_FINAL - writes results to the database.
********************************************************************************/
IS 
    TAG_ACTION_ID_ INTEGER;
    LOAD_SUM_ NUMBER;
    ACTIVITY_   VARCHAR2(150);
BEGIN
    FOR i IN TABLE_.FIRST..TABLE_.LAST
    LOOP
            
            IF (OPER_TYPE_ = 'REMOVE') THEN 
                SELECT TAG_REMOVE_OBJECT_ID, TAG_REMOVE_ACTIVITY INTO TAG_ACTION_ID_, ACTIVITY_ FROM 
                    ACT_TAGOUT_SECTION_SCHEDULE 
                WHERE TAGOUT_TYPE_ID = TA_ID_ AND ACT_TAGOUT_NUMBER =  TABLE_(i).FONAME
                    AND ACT_TAGOUT_SECTION_NUMBER =TABLE_(i).TONAME;
            ELSIF (OPER_TYPE_ = 'HANG') THEN         
                 SELECT TAG_HANG_OBJECT_ID, TAG_HANG_ACTIVITY INTO TAG_ACTION_ID_, ACTIVITY_ FROM 
                    ACT_TAGOUT_SECTION_SCHEDULE 
                WHERE TAGOUT_TYPE_ID = TA_ID_ AND ACT_TAGOUT_NUMBER =  TABLE_(i).FONAME
                    AND ACT_TAGOUT_SECTION_NUMBER =TABLE_(i).TONAME;
            END IF;
                    
                   INSERT INTO RSC_RESULT_GROUP_FINAL (JOB_ID, GROUP_NAME, GROUP_NAME_ID, TO_FOLDER, TO_NAME, 
                OPER, TA_ID, TAG_ACTION_ID, RES_ACTIVITY_VALUE, RES_LAG, RES_TIME, RES_LOAD)
            VALUES (JOB_ID_, TABLE_(i).USER_GROUP, TABLE_(i).USER_GROUP_ID, TABLE_(i).FONAME, TABLE_(i).TONAME, 
                OPER_TYPE_, TABLE_(i).TO_ID, TAG_ACTION_ID_, ACTIVITY_, TABLE_(i).LA_NORMAL, TABLE_(i).TI_NORMAL, TABLE_(i).LO_NORMAL); 
          
--            dbms_output.put_line('T: '  || TO_CHAR(JOB_ID_) || ', '  || TO_CHAR(TABLE_(i).USER_GROUP) || ', '     || TO_CHAR(TABLE_(i).USER_GROUP_ID) || ', '     || TO_CHAR(TABLE_(i).FONAME) || ', '    
--             || TO_CHAR(TABLE_(i).TONAME) || ', '     || TO_CHAR(OPER_TYPE_) || ', '     || TO_CHAR(TABLE_(i).TO_ID) || ', '     || TO_CHAR(TAG_ACTION_ID_) || ', '     || TO_CHAR(ACTIVITY_) || ', '
--              || TO_CHAR(RES_(i).TI_NORMAL) || ', ' || TO_CHAR(RES_(i).LA_NORMAL) || ', ' || TO_CHAR(RES_(i).LO_NORMAL) || '. '  );
    
    END LOOP;  
    
EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'WRITE_TO_DB_FINAL');
END WRITE_TO_DB_FINAL;  


FUNCTION GENERATE_PV_STR(ID_ IN VARCHAR2, LAG_ IN NUMBER, TIME_ IN NUMBER, LOAD_ IN NUMBER) RETURN VARCHAR2
/********************************************************************************
\ GENERATE_PV_STR - generates string for primavera
********************************************************************************/
IS 
BEGIN

    --RETURN ID_ || '(' || TRIM(TO_CHAR(LAG_, '990.99')) || '/' || TRIM(TO_CHAR(TIME_, '990.99')) || '\' || TRIM(TO_CHAR(LOAD_, '990.99')) || ')';
    RETURN '';

EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'GENERATE_PV_STR');
    RETURN '';
END GENERATE_PV_STR;

PROCEDURE DEBUG_CALC_TABLE(TABLE_ IN RSC_CALC_TABLE)         
/********************************************************************************
\ DEBUG_CALC_TABLE - debug printout
********************************************************************************/
IS 
BEGIN
--    FOR i IN TABLE_.FIRST..TABLE_.LAST
--    LOOP
--        dbms_output.put_line('Grupa: ' || RPAD(TABLE_(i).USER_GROUP, 5) || ', ID grupe: ' || RPAD(TABLE_(i).USER_GROUP_ID, 7) ||
--        ', Osamitev: ' || RPAD(TABLE_(i).FONAME || '-' || TABLE_(i).TONAME, 50) ||
--        ', T1: ' || TO_CHAR(TABLE_(i).T1/60,'99.99')||'h' || ', LO1: ' || TO_CHAR(TABLE_(i).LO1*100, '999')||'%' || 
--        ', T2: ' || TO_CHAR(TABLE_(i).T2/60,'99.99')||'h' || ', LO2: ' || TO_CHAR(TABLE_(i).LO2*100, '999')||'%' || ', L2: ' || TO_CHAR(TABLE_(i).L2/60,'99.99')||'h'|| 
--        ', T3: ' || TO_CHAR(TABLE_(i).T3/60,'99.99')||'h' || ', LO3: ' || TO_CHAR(TABLE_(i).LO3*100, '999')||'%' || ', L3: ' || TO_CHAR(TABLE_(i).L3/60,'99.99')||'h'|| 
--        ', T4: ' || TO_CHAR(TABLE_(i).T4/60,'99.99')||'h' || ', LO4: ' || TO_CHAR(TABLE_(i).LO4*100, '999')||'%' || ', L4: ' || TO_CHAR(TABLE_(i).L4/60,'99.99')||'h'||
--        ', T5: ' || TO_CHAR(TABLE_(i).T5/60,'99.99')||'h' || ', LO5: ' || TO_CHAR(TABLE_(i).LO5*100, '999')||'%' || ', L5: ' || TO_CHAR(TABLE_(i).L5/60,'99.99')||'h'); 
--        
--    END LOOP;
null;
EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'DEBUG_CALC_TABLE');
END DEBUG_CALC_TABLE;  

PROCEDURE CALCULATE_UG_STEP_1(USER_GROUP_ IN VARCHAR2, TIME_ OUT NUMBER) 
IS
/********************************************************************************
\ CALCULATE_UG_STEP_1 - calculate step 1
********************************************************************************/
LOW_VAL_ NUMBER;     
BEGIN
    SELECT 
        rtr.SIMPLE_VALUE ST_VAL
    INTO LOW_VAL_
    FROM RSC_TIME_RULES rtr 
    INNER JOIN RSC_TIME_KEYS rtk ON rtr.FOR_TK_ID = rtk.TK_ID
    WHERE 
    rtk.USERGROUP_NAME = USER_GROUP_ AND rtr.FOR_STEP = 1;

    TIME_ := LOW_VAL_;

EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'CALCULATE_UG_STEP_1');
END CALCULATE_UG_STEP_1;  

PROCEDURE CALCULATE_UG_STEP_2(USER_GROUP_ IN VARCHAR2, GROUP_CARDS_ IN RSC_TO_RESP_TABLE, TIME_ OUT NUMBER) 
IS
/********************************************************************************
\ CALCULATE_UG_STEP_2 - calculate step 2
********************************************************************************/
LOW_VAL_ NUMBER;
HI_VAL_ NUMBER;
CONDITION_ NUMBER;

CARD_NUM_ NUMBER;

BEGIN
    SELECT 
        rtrg.VALUE AFFECTIVE_RULE, 
        rtr.SIMPLE_VALUE ST_VAL,
        rtr.COMPLEX_VALUE CT_VAL
    INTO CONDITION_, LOW_VAL_, HI_VAL_
    FROM RSC_TIME_RULE_GROUPS rtrg
    INNER JOIN RSC_TIME_RULES rtr ON rtr.TR_ID = rtrg.FOR_TR_ID
    INNER JOIN RSC_TIME_KEYS rtk ON rtr.FOR_TK_ID = rtk.TK_ID
    WHERE 
    rtk.USERGROUP_NAME = USER_GROUP_ AND rtr.FOR_STEP = 2 AND rtrg.GROUP_SEGMENT = 'T';
    
    IF (GROUP_CARDS_ IS NULL) THEN
        RETURN;
    ELSE 
        CARD_NUM_ := GROUP_CARDS_.COUNT;    
    END IF;
    
    IF (CARD_NUM_ > CONDITION_) THEN
        TIME_ := HI_VAL_;
    ELSE
        TIME_ := LOW_VAL_;
    END IF;

EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'CALCULATE_UG_STEP_2');
END CALCULATE_UG_STEP_2; 

PROCEDURE GET_STEP_3_TIME(USER_GROUP_ IN VARCHAR2, ALL_CARDS_ IN RSC_CARD_CALC_TABLE, TIME_ OUT NUMBER) 
IS
/********************************************************************************
\ GET_STEP_3_TIME - Returns time and lag for current group
********************************************************************************/
SUM_TIME_ NUMBER;
WALK_TIME_ NUMBER;
MY_SUMM_ NUMBER;
MAX_SUMM_ NUMBER;

SEQ_LIST_ RSC_NUM_TABLE;
INTER_CALC_ RSC_THREE_STR_TABLE;
BEGIN

    SELECT SUM(TIME_MIN) INTO SUM_TIME_
    FROM TABLE(ALL_CARDS_) 
    WHERE USER_GROUP = USER_GROUP_;
    
    SELECT MAX(WALK_TIME) INTO WALK_TIME_
    FROM TABLE(ALL_CARDS_) 
    WHERE USER_GROUP = USER_GROUP_;

    TIME_ := SUM_TIME_ + WALK_TIME_;

EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'GET_STEP_3_TIME');
END GET_STEP_3_TIME;  

FUNCTION CALCULATE_UG_STEP_3(GROUPS_ IN RSC_STR_TABLE, ALL_CARDS_ IN RSC_TO_RESP_TABLE, OPER_TYPE_ IN VARCHAR2, ST3_MAX_TIME_ OUT NUMBER) RETURN RSC_CARD_CALC_TABLE
IS
/********************************************************************************
\ CALCULATE_UG_STEP_3 - calculate step 3 for all cards
********************************************************************************/
CURRENT_USER_GROUP_             VARCHAR2(50);
CURRENT_CARDS_FOR_GROUP_    RSC_TO_RESP_TABLE;
CWT_                                          NUMBER;
PART_WALK_TIME_                        NUMBER;

RS_TABLE_FIRST            RSC_CARD_CALC_TABLE;
RS_TABLE_SECOND            RSC_CARD_CALC_TABLE;
CURR_CARD_              RSC_TO_RESP_RECORD;
CURR_CARD_RESULT_  RSC_CARD_CALC_RECORD;
SEQ_TABLE                 RSC_NUM_TABLE;
CURR_SEQUENCE        NUMBER;
SEQ_CARDS_              RSC_CARD_CALC_TABLE;
CUR_LAG_                  NUMBER;
PAR_STEP_3_               NUMBER;
STEP_3_TIME_             NUMBER;

SEQ_NUMBER_           NUMBER;
MAX_SEQ_TIME_         NUMBER;
CUR_ALL_LAG_            NUMBER;
PREV_SEQ_NUM_        NUMBER;

IDx_MAX                     NUMBER;
IDx_MIN                     NUMBER;

CURSOR getMaxOnSeqSet IS -- This is for SET
    SELECT MAX(SUM(TIME_MIN))  FROM TABLE(RS_TABLE_FIRST) 
    WHERE CARD_SEQUENCE = SEQ_NUMBER_
    GROUP BY USER_GROUP;

BEGIN
    RS_TABLE_FIRST := RSC_CARD_CALC_TABLE();
    CURR_CARD_RESULT_ := RSC_CARD_CALC_RECORD(null,null,null,null,null,null,null,null,null);
    RS_TABLE_SECOND := RSC_CARD_CALC_TABLE();
    
    IF (GROUPS_ IS NOT NULL AND GROUPS_.COUNT > 0) THEN
        FOR i IN GROUPS_.FIRST .. GROUPS_.LAST
        LOOP
            CURRENT_USER_GROUP_ := GROUPS_(i);
            CURRENT_CARDS_FOR_GROUP_ := NULL;
            SELECT RSC_TO_RESP_RECORD (val1, val2, val3, val4, val5, val6, val7, val8, val9, val10) 
                BULK COLLECT INTO CURRENT_CARDS_FOR_GROUP_
                FROM TABLE(ALL_CARDS_) WHERE val3 = CURRENT_USER_GROUP_
                ORDER BY val4;
        
            IF (CURRENT_CARDS_FOR_GROUP_ IS NOT NULL) THEN
                FOR j IN CURRENT_CARDS_FOR_GROUP_.FIRST .. CURRENT_CARDS_FOR_GROUP_.LAST
                LOOP
                    CURR_CARD_RESULT_ := CALCULATE_CARD_TIME_S3(CURRENT_CARDS_FOR_GROUP_(j), OPER_TYPE_);                
                    CURR_CARD_RESULT_.IDx := j;
                    RS_TABLE_FIRST.EXTEND;
                    RS_TABLE_FIRST(RS_TABLE_FIRST.COUNT) := CURR_CARD_RESULT_;
                END LOOP;                  

                SELECT -- reset times for first and last item and gives half walking time (based on max walking from the whole group).
                    MAX(WALK_TIME) INTO CWT_ 
                    FROM TABLE(RS_TABLE_FIRST)
                    WHERE USER_GROUP = CURRENT_USER_GROUP_;         
                PART_WALK_TIME_ := CWT_ / 2;
                IF (PART_WALK_TIME_ IS NULL) 
                    THEN PART_WALK_TIME_ := 0;
                END IF;
                     
                SELECT 
                    MAX(IDx) INTO IDx_MAX 
                    FROM TABLE(RS_TABLE_FIRST)
                    WHERE USER_GROUP = CURRENT_USER_GROUP_;         
                SELECT 
                    MIN(IDx) INTO IDx_MIN
                    FROM TABLE(RS_TABLE_FIRST)
                    WHERE USER_GROUP = CURRENT_USER_GROUP_;         
                    
                FOR h IN RS_TABLE_FIRST.FIRST .. RS_TABLE_FIRST.LAST
                LOOP
                    IF (RS_TABLE_FIRST(h).IDx = IDx_MIN AND RS_TABLE_FIRST(h).USER_GROUP = CURRENT_USER_GROUP_)
                    THEN
                        RS_TABLE_FIRST(h).TIME_MIN := RS_TABLE_FIRST(h).TIME_MIN + PART_WALK_TIME_;
                    END IF;
                    IF (RS_TABLE_FIRST(h).IDx = IDx_MAX AND RS_TABLE_FIRST(h).USER_GROUP = CURRENT_USER_GROUP_)
                    THEN
                        RS_TABLE_FIRST(h).TIME_MIN := RS_TABLE_FIRST(h).TIME_MIN + PART_WALK_TIME_;
                    END IF;
                END LOOP;                                  
            END IF;      
        END LOOP; 
    END IF;
    
    CUR_ALL_LAG_ := 0;
    RS_TABLE_SECOND := RSC_CARD_CALC_TABLE();
    IF (OPER_TYPE_ = 'HANG') THEN
        SELECT DISTINCT(CARD_SEQUENCE) BULK COLLECT INTO SEQ_TABLE FROM TABLE(RS_TABLE_FIRST) 
        ORDER BY CARD_SEQUENCE ASC;
    ELSIF (OPER_TYPE_ = 'REMOVE') THEN
        SELECT DISTINCT(CARD_SEQUENCE) BULK COLLECT INTO SEQ_TABLE FROM TABLE(RS_TABLE_FIRST) 
        ORDER BY CARD_SEQUENCE DESC;    
    END IF;    
                 
    STEP_3_TIME_ := 0;
    PREV_SEQ_NUM_ := 0;
    IF (SEQ_TABLE IS NOT NULL AND SEQ_TABLE.COUNT > 0) THEN
        FOR k IN SEQ_TABLE.FIRST .. SEQ_TABLE.LAST
        LOOP    
            CURR_SEQUENCE := SEQ_TABLE(k);
                        
            SEQ_NUMBER_ := PREV_SEQ_NUM_; -- we're checking the max time for PREVIOUS sequence    
            OPEN getMaxOnSeqSet;
            FETCH getMaxOnSeqSet INTO MAX_SEQ_TIME_;
            CLOSE getMaxOnSeqSet;
                    
            IF (MAX_SEQ_TIME_ IS NULL) THEN MAX_SEQ_TIME_ := 0; END IF;
            CUR_ALL_LAG_ := CUR_ALL_LAG_ + MAX_SEQ_TIME_;
                    
            SELECT MAX(SUM(TIME_MIN)) INTO PAR_STEP_3_ FROM TABLE(RS_TABLE_FIRST) 
                WHERE CARD_SEQUENCE = CURR_SEQUENCE        
                GROUP BY USER_GROUP;
            IF (PAR_STEP_3_ IS NULL) THEN PAR_STEP_3_ := 0; END IF;
                STEP_3_TIME_ := STEP_3_TIME_ + PAR_STEP_3_;
                          
            IF (GROUPS_ IS NOT NULL AND GROUPS_.COUNT > 0) THEN      
                FOR l IN GROUPS_.FIRST .. GROUPS_.LAST
                LOOP
                    CURRENT_USER_GROUP_ := GROUPS_(l);
                    IF (OPER_TYPE_ = 'HANG') THEN    
                        SELECT 
                            RSC_CARD_CALC_RECORD(IDx, USER_GROUP, CARD_ID, BUILDING_ID, CARD_SEQUENCE, CARD_POSTITION,WALK_TIME,TIME_MIN, LOC_LAG)
                            BULK COLLECT INTO SEQ_CARDS_ 
                            FROM TABLE(RS_TABLE_FIRST)
                            WHERE USER_GROUP = CURRENT_USER_GROUP_ AND CARD_SEQUENCE = CURR_SEQUENCE
                            ORDER BY IDx ASC, CARD_SEQUENCE ASC;
                    END IF;
                    IF (OPER_TYPE_ = 'REMOVE') THEN
                        SELECT 
                            RSC_CARD_CALC_RECORD(IDx, USER_GROUP, CARD_ID, BUILDING_ID, CARD_SEQUENCE, CARD_POSTITION,WALK_TIME,TIME_MIN, LOC_LAG)
                            BULK COLLECT INTO SEQ_CARDS_ 
                            FROM TABLE(RS_TABLE_FIRST)
                            WHERE USER_GROUP = CURRENT_USER_GROUP_ AND CARD_SEQUENCE = CURR_SEQUENCE
                            ORDER BY IDx DESC, CARD_SEQUENCE DESC;
                    END IF;

                    CUR_LAG_ := 0; -- for start of sinle card computation for step 3
                    IF (SEQ_CARDS_ IS NOT NULL AND SEQ_CARDS_.COUNT > 0) THEN
                        FOR m IN SEQ_CARDS_.FIRST .. SEQ_CARDS_.LAST 
                        LOOP
                            CURR_CARD_RESULT_ := SEQ_CARDS_(m);
                            CURR_CARD_RESULT_.LOC_LAG := CUR_LAG_ + CUR_ALL_LAG_;
                                    
                            CUR_LAG_ := CUR_LAG_ + CURR_CARD_RESULT_.TIME_MIN;
                            RS_TABLE_SECOND.EXTEND;
                            RS_TABLE_SECOND(RS_TABLE_SECOND.COUNT) := CURR_CARD_RESULT_;
                        END LOOP;
                    END IF;
                END LOOP;           
            END IF;                
            
            PREV_SEQ_NUM_ := CURR_SEQUENCE;
        END LOOP;    
    END IF;
    
    ST3_MAX_TIME_ := STEP_3_TIME_;
    RETURN RS_TABLE_SECOND;

EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || '). Seq: '  || SEQ_NUMBER_  || ', UG: '  || CURRENT_USER_GROUP_  || 
            ', Card: '  || CURR_CARD_RESULT_.CARD_ID , 'CALCULATE_UG_STEP_3');
    RETURN NULL;
END CALCULATE_UG_STEP_3;  

FUNCTION CALCULATE_CARD_TIME_S3(CURR_CARD_ IN RSC_TO_RESP_RECORD, OPER_TYPE_ IN VARCHAR2) RETURN RSC_CARD_CALC_RECORD
IS
/********************************************************************************
\ CALCULATE_CARD_TIME_S3 - calculate single card time for Step3
********************************************************************************/

TIME_ NUMBER;
CARD_UG_ VARCHAR2(50);

STA_VALUE_ NUMBER;
CTA_VALUE_ NUMBER;
EQUIP_POS_ VARCHAR2(500);
TA_RULES_   RSC_STR_TABLE;

SWA_VALUE_ NUMBER;
CWA_VALUE_ NUMBER;
EQUIP_BUILD_ VARCHAR2(500);
WA_RULES_  RSC_STR_TABLE;

RESULT_ RSC_CARD_CALC_RECORD;

CURSOR getValues IS 
    SELECT  -- For values
        rtr.SIMPLE_VALUE, rtr.COMPLEX_VALUE, rtr.SIMPLE_PATH_VALUE,
        rtr.COMPLEX_PATH_VALUE
    FROM RSC_TIME_RULES rtr 
    INNER JOIN RSC_TIME_KEYS rtk ON rtr.FOR_TK_ID = rtk.TK_ID
    WHERE rtk.USERGROUP_NAME = CARD_UG_ AND rtr.FOR_STEP = 3;

BEGIN
    IF (CURR_CARD_ IS NULL) THEN RETURN RSC_CARD_CALC_RECORD(null,null,null,null,null,null,null,null,null); END IF;
    
    CARD_UG_ := CURR_CARD_.val3;
    EQUIP_POS_ := CURR_CARD_.val5;
    EQUIP_BUILD_ := CURR_CARD_.val6;
    
    RESULT_ := RSC_CARD_CALC_RECORD(null,null,null,null,null,null,null,null,null);
    RESULT_.USER_GROUP := CARD_UG_;
    RESULT_.CARD_ID := CURR_CARD_.val1;
    RESULT_.BUILDING_ID := EQUIP_BUILD_;
    RESULT_.CARD_SEQUENCE := CURR_CARD_.val4;
    RESULT_.CARD_POSTITION := EQUIP_POS_;
    RESULT_.LOC_LAG := 0; -- default

    OPEN getValues;
    FETCH getValues INTO STA_VALUE_, CTA_VALUE_, SWA_VALUE_, CWA_VALUE_;
    CLOSE getValues;

    IF (STA_VALUE_ IS NULL) THEN
        STA_VALUE_ := 0;
    END IF;

    IF (CTA_VALUE_ IS NULL) THEN
        CTA_VALUE_ := 0;
    END IF;

    IF (SWA_VALUE_ IS NULL) THEN
        SWA_VALUE_ := 0;
    END IF;

    IF (CWA_VALUE_ IS NULL) THEN
        CWA_VALUE_ := 0;
    END IF;

    SELECT -- TAGOUT RULES
        rtrg.VALUE
    BULK COLLECT INTO TA_RULES_
    FROM RSC_TIME_RULE_GROUPS rtrg
    INNER JOIN RSC_TIME_RULES rtr ON rtr.TR_ID = rtrg.FOR_TR_ID
    INNER JOIN RSC_TIME_KEYS rtk ON rtr.FOR_TK_ID = rtk.TK_ID
    WHERE rtk.USERGROUP_NAME = CARD_UG_ AND rtr.FOR_STEP = 3 AND rtrg.GROUP_SEGMENT = 'T';
    
    SELECT -- PATH - Walking - RULES
        rtrg.VALUE
    BULK COLLECT INTO WA_RULES_
    FROM RSC_TIME_RULE_GROUPS rtrg
    INNER JOIN RSC_TIME_RULES rtr ON rtr.TR_ID = rtrg.FOR_TR_ID
    INNER JOIN RSC_TIME_KEYS rtk ON rtr.FOR_TK_ID = rtk.TK_ID
    WHERE rtk.USERGROUP_NAME = CARD_UG_ AND rtr.FOR_STEP = 3 AND rtrg.GROUP_SEGMENT = 'P';

    IF (TRIM(EQUIP_POS_) MEMBER OF TA_RULES_ AND OPER_TYPE_ = 'HANG') THEN -- These rules are for SETTING the cards ONLY!
        RESULT_.TIME_MIN := CTA_VALUE_;
    ELSE 
        RESULT_.TIME_MIN := STA_VALUE_;
    END IF;

    IF (TRIM(EQUIP_BUILD_) MEMBER OF WA_RULES_) THEN
        RESULT_.WALK_TIME := CWA_VALUE_;
    ELSE 
        RESULT_.WALK_TIME := SWA_VALUE_;
    END IF;

RETURN RESULT_;

EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || '). Card: '  ||
        RESULT_.USER_GROUP  || ', '  ||
        RESULT_.CARD_ID  || ', '  ||
        RESULT_.BUILDING_ID  || ', '  ||
        RESULT_.CARD_SEQUENCE  || ', '  ||
        RESULT_.CARD_POSTITION  || '.' , 'CALCULATE_CARD_TIME_S3');
    RETURN NULL;
END CALCULATE_CARD_TIME_S3;  

PROCEDURE CALCULATE_UG_STEP_4(USER_GROUP_ IN VARCHAR2, GROUP_CARDS_ IN RSC_TO_RESP_TABLE, TIME_ OUT NUMBER) 
IS
/********************************************************************************
\ CALCULATE_UG_STEP_4 - calculate step 4
********************************************************************************/
    CARD_STAND_TIME_ NUMBER;
    MIN_SUM_TIME_ NUMBER;
    WALKING_STAND_TIME_ NUMBER;
    WALKING_COMP_TIME_ NUMBER;
    CURR_CARD_BUILDING_ID_ VARCHAR2(500);
    
    RULES_  RSC_STR_TABLE;
    CURR_CARD_  RSC_TO_RESP_RECORD;
    
    USE_COMPLEX_WALKING_ BOOLEAN;
BEGIN
    IF (GROUP_CARDS_ IS NULL) THEN
        TIME_ := 0;
        RETURN;
    END IF;

    USE_COMPLEX_WALKING_ := FALSE;
    
    SELECT -- Rules
        rtrg.VALUE 
    BULK COLLECT INTO RULES_
    FROM RSC_TIME_RULE_GROUPS rtrg
    INNER JOIN RSC_TIME_RULES rtr ON rtr.TR_ID = rtrg.FOR_TR_ID
    INNER JOIN RSC_TIME_KEYS rtk ON rtr.FOR_TK_ID = rtk.TK_ID
    WHERE 
    rtk.USERGROUP_NAME = USER_GROUP_ AND rtr.FOR_STEP = 4;
    
    SELECT  -- For values
        rtr.SIMPLE_VALUE ST_VAL,
        rtr.SIMPLE_PATH_VALUE SP_VAL,
        rtr.COMPLEX_PATH_VALUE CP_VAL, 
        rtr.MIN_OVERALL_VALUE MIN_VAL
    INTO
        CARD_STAND_TIME_, WALKING_STAND_TIME_, WALKING_COMP_TIME_, MIN_SUM_TIME_    
    FROM RSC_TIME_RULES rtr 
    INNER JOIN RSC_TIME_KEYS rtk ON rtr.FOR_TK_ID = rtk.TK_ID
    WHERE 
    rtk.USERGROUP_NAME = USER_GROUP_ AND rtr.FOR_STEP = 4;
    
    FOR i IN GROUP_CARDS_.FIRST..GROUP_CARDS_.LAST
    LOOP
        CURR_CARD_ := GROUP_CARDS_(i);
        CURR_CARD_BUILDING_ID_ := TRIM(CURR_CARD_.val6);
        IF (RULES_ IS NOT NULL AND CURR_CARD_BUILDING_ID_ MEMBER OF RULES_) THEN
            USE_COMPLEX_WALKING_ := TRUE;
        END IF;
    END LOOP;    
    
    TIME_ := GROUP_CARDS_.COUNT * CARD_STAND_TIME_;
    IF (MIN_SUM_TIME_ IS NOT NULL AND MIN_SUM_TIME_ > TIME_) THEN
        TIME_ := MIN_SUM_TIME_;
    END IF;
    
    IF (WALKING_COMP_TIME_ IS NULL) THEN WALKING_COMP_TIME_ := 0; END IF;
    IF (WALKING_STAND_TIME_ IS NULL) THEN WALKING_STAND_TIME_ := 0; END IF;
    
    IF (USE_COMPLEX_WALKING_ = TRUE) THEN
        TIME_ := TIME_ + WALKING_COMP_TIME_;
    ELSE
        TIME_ := TIME_ + WALKING_STAND_TIME_;
    END IF;
    
EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'CALCULATE_UG_STEP_4');
END CALCULATE_UG_STEP_4;

PROCEDURE CALCULATE_UG_STEP_5(USER_GROUP_ IN VARCHAR2, TIME_ OUT NUMBER) 
IS
/********************************************************************************
\ CALCULATE_UG_STEP_5 - calculate step 5
********************************************************************************/
SIMPLE_VAL NUMBER;
BEGIN
    SELECT 
        rtr.SIMPLE_VALUE ST_VAL
    INTO SIMPLE_VAL
    FROM RSC_TIME_RULES rtr 
    INNER JOIN RSC_TIME_KEYS rtk ON rtr.FOR_TK_ID = rtk.TK_ID
    WHERE 
    rtk.USERGROUP_NAME = USER_GROUP_ AND rtr.FOR_STEP = 5;

    TIME_ := SIMPLE_VAL;

EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'CALCULATE_UG_STEP_5');
END CALCULATE_UG_STEP_5; 

FUNCTION PREP_LOCATION_STRING(V_ IN VARCHAR2, V_2 IN VARCHAR2, V_3 IN VARCHAR2) RETURN VARCHAR2
/********************************************************************************
\ PREP_LOCATION_STRING - Prepares location key string
********************************************************************************/
IS
    V_2_ VARCHAR2(150);
BEGIN
    
    IF (V_ IS NULL AND V_2 IS NULL AND V_3 IS NULL) THEN
        RETURN NULL;
    END IF;

    IF (V_2 IS NULL) THEN
        V_2_ := '*';
    ELSE 
        V_2_ := PREP_ELEV(V_2);
    END IF;
    RETURN NVL(V_, '*') || '-' || V_2_ || '-' || NVL(V_3, '*');

EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'PREP_LOCATION_STRING');
    RETURN '';
END PREP_LOCATION_STRING; 

FUNCTION GET_EQUIP_RESPONSIBILITY(EQUIP_ID_ IN VARCHAR2) RETURN VARCHAR2
/********************************************************************************
\ GET_EQUIP_RESPONSIBILITY - Returns the usergroup responsible for this equipment
********************************************************************************/
IS
    CURSOR getUserGroups IS 
    SELECT 
            USERGROUP,
            LOC_ID_INDEX, NOT_LOC_ID_INDEX, 
            SYSTEM_ID_INDEX, NOT_SYSTEM_ID_INDEX, 
            TYPE_ID_INDEX, NOT_TYPE_ID_INDEX
        FROM RSC_USER_GROUP_KEYS;
    
    E_BUILDING          VARCHAR2(50);
    E_ELEVATION        VARCHAR2(50);
    E_ROOM              VARCHAR2(50);
    E_SYSTEM             VARCHAR2(50);
    E_TYPE                 VARCHAR2(50);
    
    E_LOCATION VARCHAR(150);
    
    USERGROUP_ VARCHAR2(50);
    KL NUMBER; KNL NUMBER;
    KS NUMBER; KNS NUMBER;
    KT NUMBER; KNT NUMBER;
    
    LOCATION_T RSC_THREE_STR_TABLE;
    NLOCATION_T RSC_THREE_STR_TABLE;
    SYSTEM_T RSC_STR_TABLE;
    NSYSTEM_T RSC_STR_TABLE;
    TYPE_T RSC_STR_TABLE;
    NTYPE_T RSC_STR_TABLE;
    RESULT_TABLE_ RSC_STR_TABLE;
    
    COUNT_RESULT_ NUMBER;
    
    MSG_ VARCHAR2(1500);
    ZERO_ BOOLEAN := FALSE;
    MULTIPLE_ BOOLEAN := FALSE;
BEGIN

USERGROUP_ := '';

SELECT
        BUILDING_ID, 
        ELEVATION_ID, 
        ROOM_ID, 
        SYSTEM_ID, 
        TYPE_ID 
    INTO 
        E_BUILDING,
        E_ELEVATION,
        E_ROOM,
        E_SYSTEM,
        E_TYPE    
    FROM 
        EQUIP
    WHERE 
        EQUIP_OPERATOR_ID = EQUIP_ID_;

    E_LOCATION := PREP_LOCATION_STRING(TRIM(E_BUILDING), TRIM(E_ELEVATION), TRIM(E_ROOM));

OPEN getUserGroups;
    LOOP
        LOCATION_T := NULL;
        NLOCATION_T := NULL;
        SYSTEM_T := NULL;
        NSYSTEM_T := NULL;
        TYPE_T := NULL;
        NTYPE_T := NULL;
        RESULT_TABLE_ := NULL;        
   
        KL := NULL; KNL := NULL;
        KS := NULL; KNS := NULL;
        KT := NULL; KNT := NULL;
        
        MSG_ := '';

        FETCH getUserGroups INTO USERGROUP_, KL, KNL, KS, KNS, KT, KNT;
            IF getUserGroups%NOTFOUND THEN
                USERGROUP_ := '';
                EXIT;
            ELSE
                IF (KL IS NOT NULL) THEN                
                    SELECT RSC_THR_STR_RECORD(VALUE, VALUE_2, VALUE_3) BULK COLLECT INTO LOCATION_T
                    FROM RSC_USER_GROUP_VALS 
                    WHERE GROUP_ID = KL;                  
                END IF;

                IF (KNL IS NOT NULL) THEN
                    SELECT RSC_THR_STR_RECORD(VALUE, VALUE_2, VALUE_3) BULK COLLECT INTO NLOCATION_T 
                    FROM RSC_USER_GROUP_VALS 
                    WHERE GROUP_ID = KNL;
                END IF;

                IF (KS IS NOT NULL) THEN
                    SELECT VALUE BULK COLLECT INTO SYSTEM_T 
                    FROM RSC_USER_GROUP_VALS 
                    WHERE GROUP_ID = KS;
                END IF;

                IF (KNS IS NOT NULL) THEN
                    SELECT VALUE BULK COLLECT INTO NSYSTEM_T 
                    FROM RSC_USER_GROUP_VALS 
                    WHERE GROUP_ID = KNS;
                END IF;

                IF (KT IS NOT NULL) THEN
                    SELECT VALUE BULK COLLECT INTO TYPE_T 
                    FROM RSC_USER_GROUP_VALS 
                    WHERE GROUP_ID = KT;
                END IF;

                IF (KNT IS NOT NULL) THEN
                    SELECT VALUE BULK COLLECT INTO NTYPE_T 
                    FROM RSC_USER_GROUP_VALS 
                    WHERE GROUP_ID = KNT;
                END IF;            
                  
                SELECT USERGROUP BULK COLLECT INTO RESULT_TABLE_
                FROM RSC_USER_GROUP_KEYS ugk
                WHERE 
                    (((CHECK_LOCATION(E_BUILDING, PREP_ELEV(E_ELEVATION), E_ROOM, LOCATION_T) = 1 OR E_LOCATION IS NULL OR LOCATION_T IS NULL) 
                    AND
                      (CHECK_LOCATION(E_BUILDING, PREP_ELEV(E_ELEVATION), E_ROOM, NLOCATION_T) = 0 OR E_LOCATION IS NULL OR NLOCATION_T IS NULL))
                        AND ((LOC_ID_INDEX = KL OR LOC_ID_INDEX IS NULL) AND (NOT_LOC_ID_INDEX = KNL OR NOT_LOC_ID_INDEX IS NULL)))                    
                    AND
                    (((TRIM(E_SYSTEM) MEMBER OF SYSTEM_T OR E_SYSTEM IS NULL OR SYSTEM_T IS NULL) AND
                        (TRIM(E_SYSTEM) NOT MEMBER OF NSYSTEM_T OR E_SYSTEM IS NULL OR NSYSTEM_T IS NULL))
                        AND ((SYSTEM_ID_INDEX = KS OR SYSTEM_ID_INDEX IS NULL) AND (NOT_SYSTEM_ID_INDEX = KNS OR NOT_SYSTEM_ID_INDEX IS NULL)))
                    AND
                    (((TRIM(E_TYPE) MEMBER OF TYPE_T OR E_TYPE IS NULL OR TYPE_T IS NULL) AND
                        (TRIM(E_TYPE) NOT MEMBER OF NTYPE_T OR E_TYPE IS NULL OR NTYPE_T IS NULL))
                        AND ((TYPE_ID_INDEX = KT OR TYPE_ID_INDEX IS NULL) AND (NOT_TYPE_ID_INDEX = KNT OR NOT_TYPE_ID_INDEX IS NULL)));
                
                IF (RESULT_TABLE_ IS NOT NULL) THEN
                    COUNT_RESULT_ := RESULT_TABLE_.COUNT;
                ELSE 
                    COUNT_RESULT_ := 0;
                END IF;   
                         
                IF (COUNT_RESULT_ > 0) THEN
                    IF (COUNT_RESULT_ > 1) THEN
                        FOR k IN RESULT_TABLE_.FIRST .. RESULT_TABLE_.LAST
                        LOOP
                            MSG_ := MSG_  || ', ';
                        END LOOP;
                        MSG_ := SUBSTR(MSG_, 1, LENGTH(MSG_)-2);
                        MSG_ := 'Found responsibility groups '  || MSG_ ||  ' on the equipment '  || EQUIP_ID_ || '.';
                        
                        NEK_LOG(MSG_, 'GET_EQUIP_RESPONSIBILITY');
                        dbms_output.put_line(MSG_);   
                        
                        RETURN ''; -- This finds ALL groups simultaniously. So we can exit.
                    END IF;
                    
                    RETURN USERGROUP_;
                END IF;
            END IF;
    END LOOP;
CLOSE getUserGroups;

IF (USERGROUP_ IS NULL OR USERGROUP_ = '') THEN
    MSG_ := 'Have not found any responsibility groups on the equipment '  || EQUIP_ID_ || '.';
    NEK_LOG(MSG_, 'GET_EQUIP_RESPONSIBILITY');
    dbms_output.put_line(MSG_);  
END IF;       

RETURN USERGROUP_;

EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'GET_EQUIP_RESPONSIBILITY');
    RETURN '';
END GET_EQUIP_RESPONSIBILITY; 

FUNCTION CHECK_LOCATION(BUI_ IN VARCHAR2, ELE_ IN VARCHAR2, ROOM_ IN VARCHAR2, LOC_TAB_ IN RSC_THREE_STR_TABLE) RETURN NUMBER
/********************************************************************************
\ CHECK_LOCATION - Checks location string
********************************************************************************/
IS
    TAB_STR_ RSC_THR_STR_RECORD;
    IS_FIRST BOOLEAN;
    IS_SECOND BOOLEAN;
    IS_THIRD BOOLEAN;
    
BEGIN

    IF (LOC_TAB_ IS NULL) THEN 
        RETURN 0;
    END IF;

    FOR i IN LOC_TAB_.FIRST .. LOC_TAB_.LAST
    LOOP
        TAB_STR_ := LOC_TAB_(i);
        IF (TRIM(BUI_) LIKE TRIM(TAB_STR_.val1) OR (TAB_STR_.val1 IS NULL OR TAB_STR_.val1 = '')) THEN IS_FIRST := TRUE; ELSE IS_FIRST := FALSE; END IF;
        IF (TRIM(ELE_) LIKE TRIM(TAB_STR_.val2) OR (TAB_STR_.val2 IS NULL OR TAB_STR_.val2 = '')) THEN IS_SECOND := TRUE; ELSE IS_SECOND := FALSE; END IF;
        IF (TRIM(ROOM_) LIKE TRIM(TAB_STR_.val1  || TAB_STR_.val3) OR (TAB_STR_.val3 IS NULL OR TAB_STR_.val3 = '')) THEN IS_THIRD := TRUE; ELSE IS_THIRD := FALSE; END IF;
        
        IF (IS_FIRST=TRUE AND IS_SECOND=TRUE AND IS_THIRD=TRUE) THEN
            RETURN 1;
        END IF;
    END LOOP;
    
    RETURN 0;

EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'CHECK_LOCATION');
    RETURN 0;
END CHECK_LOCATION;       

FUNCTION LIST_EQUIP_RESPONSIBILITY(EQUIP_ID IN VARCHAR2) RETURN SYS_REFCURSOR
IS 
    REF_CUR SYS_REFCURSOR;
BEGIN
    OPEN REF_CUR FOR 
    SELECT 
        EQUIP_OPERATOR_ID Eid,
        RSC_RESOURCE_APP.GET_EQUIP_RESPONSIBILITY(EQUIP_OPERATOR_ID) Responsibility,
        EQUIP_DESCRIPTION Descr,
        BUILDING_ID Building,
        ELEVATION_ID Elevation, 
        ROOM_ID Room, 
        SYSTEM_ID System,
        TYPE_ID Type
    FROM EQUIP
    WHERE EQUIP_OPERATOR_ID = EQUIP_ID;

    RETURN REF_CUR;

EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'LIST_EQUIP_RESPONSIBILITY');
    RETURN null;
END LIST_EQUIP_RESPONSIBILITY;

FUNCTION LIST_USER_GROUPS RETURN SYS_REFCURSOR
/********************************************************************************
\ LIST_USER_GROUPS - Display all user groups with properties for human readibility.
********************************************************************************/
IS 
    REF_CUR SYS_REFCURSOR;
BEGIN    
    OPEN REF_CUR FOR 
       SELECT 
        KEYID Key, 
        USERGROUP User_group,
        GET_USER_GROUP_VALUES_S(rsc.LOC_ID_INDEX, 1) Location,
        GET_USER_GROUP_VALUES_S(rsc.NOT_LOC_ID_INDEX, 1) Not_Location,
        GET_USER_GROUP_VALUES_S(rsc.SYSTEM_ID_INDEX, 0) System,
        GET_USER_GROUP_VALUES_S(rsc.NOT_SYSTEM_ID_INDEX, 0) Not_System,
        GET_USER_GROUP_VALUES_S(rsc.TYPE_ID_INDEX, 0) Type,
        GET_USER_GROUP_VALUES_S(rsc.NOT_TYPE_ID_INDEX, 0) Not_Type
    FROM RSC_USER_GROUP_KEYS rsc
    ORDER BY USERGROUP;
    
  RETURN REF_CUR;

EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'LIST_USER_GROUPS');
    RETURN null;
END LIST_USER_GROUPS;

FUNCTION PREP_ELEV(ELEV_ IN VARCHAR2) RETURN VARCHAR2 
/********************************************************************************
\ PREP_ELEV - Prepares elevation
********************************************************************************/
IS
    FOUND_DEC_ NUMBER;
BEGIN
    IF (ELEVATION_BASE_ONLY = '0') THEN
        RETURN ELEV_;
    END IF;

    FOUND_DEC_ := INSTR(ELEV_, ',');
    IF (FOUND_DEC_ > 0) THEN
        RETURN SUBSTR(ELEV_, 1, FOUND_DEC_ -1);
    ELSE 
        RETURN ELEV_;
    END IF;
    
EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'PREP_ELEV');
    RETURN '';
END PREP_ELEV;

FUNCTION GET_USER_GROUP_VALUES_S(GR_ID_ IN VARCHAR2, LOC_ IN NUMBER) RETURN VARCHAR2 
/********************************************************************************
\ GET_USER_GROUP_VALUES_S - Returns values for a group in a string
********************************************************************************/
IS
CURSOR getValues IS 
    SELECT VALUE, VALUE_2, VALUE_3 
    FROM RSC_USER_GROUP_VALS
    WHERE GROUP_ID = GR_ID_;

VALUE_ VARCHAR2(50);
VALUE_2_ VARCHAR2(50);
VALUE_3_ VARCHAR2(50);

RET_STRING_ VARCHAR2(5000);
BEGIN

    OPEN getValues;
        LOOP
            FETCH getValues INTO VALUE_, VALUE_2_, VALUE_3_;
            IF getValues%NOTFOUND THEN
                IF (LENGTH(RET_STRING_) > 0) THEN
                    RET_STRING_ := SUBSTR(RET_STRING_, 1, LENGTH(RET_STRING_)-2);
                END IF;
                EXIT;
            ELSE
                IF (LOC_ = 1) THEN
                    RET_STRING_ := RET_STRING_ || PREP_LOCATION_STRING(VALUE_, VALUE_2_, VALUE_3_) || ', ';
                ELSE  
                    RET_STRING_ := RET_STRING_ || VALUE_ || ', ';
                END IF;
            END IF;   
        END LOOP;
    CLOSE getValues;

    RETURN RET_STRING_;
EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'GET_USER_GROUP_VALUES_S');
    RETURN '';
END GET_USER_GROUP_VALUES_S;

PROCEDURE ADD_USER_GROUP_KEY(NEW_ID_ IN VARCHAR2)
/********************************************************************************
\ ADD_USER_GROUP_KEY - Adds new user group key
********************************************************************************/
IS
BEGIN
    INSERT INTO RSC_USER_GROUP_KEYS (USERGROUP)
    VALUES (NEW_ID_);
EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'ADD_USER_GROUP_KEY');
END ADD_USER_GROUP_KEY;

PROCEDURE REMOVE_USER_GROUP_KEY(UG_ID_ IN VARCHAR2)
/********************************************************************************
\ REMOVE_USER_GROUP_KEY - Removes user group key
********************************************************************************/
IS
    SEG_NAME_ VARCHAR2(50);
    GRP_CNT_ NUMBER;
    CURR_GROUP_ID_ NUMBER;
    DEL_ELEMENTS_ NUMBER;
BEGIN
    SELECT COUNT(*) INTO GRP_CNT_ FROM RSC_USER_GROUP_KEYS WHERE KEYID = UG_ID_;
    IF (GRP_CNT_= 0) THEN
        dbms_output.put_line('Key '  || TO_CHAR(UG_ID_) || ' is not valid. Group not found. Nothing was deleted.');
    ELSE 
        DELETE FROM RSC_USER_GROUP_VALS
        WHERE 
            GROUP_ID = (SELECT LOC_ID_INDEX FROM RSC_USER_GROUP_KEYS WHERE KEYID = UG_ID_)
            OR GROUP_ID = (SELECT NOT_LOC_ID_INDEX FROM RSC_USER_GROUP_KEYS WHERE KEYID = UG_ID_)
            OR GROUP_ID = (SELECT SYSTEM_ID_INDEX FROM RSC_USER_GROUP_KEYS WHERE KEYID = UG_ID_)
            OR GROUP_ID = (SELECT NOT_SYSTEM_ID_INDEX FROM RSC_USER_GROUP_KEYS WHERE KEYID = UG_ID_)
            OR GROUP_ID = (SELECT TYPE_ID_INDEX FROM RSC_USER_GROUP_KEYS WHERE KEYID = UG_ID_)
            OR GROUP_ID = (SELECT NOT_TYPE_ID_INDEX FROM RSC_USER_GROUP_KEYS WHERE KEYID = UG_ID_); 
    
            DELETE FROM RSC_USER_GROUP_KEYS WHERE KEYID = UG_ID_;
    END IF;

EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'REMOVE_USER_GROUP_KEY');
END REMOVE_USER_GROUP_KEY;

PROCEDURE REMOVE_USER_GROUP_VALUE(UG_ID_ IN NUMBER, SEGMENT_ IN VARCHAR2, VALUE_ IN VARCHAR2, VALUE_2_ IN VARCHAR2, VALUE_3_ IN VARCHAR2)
/********************************************************************************
\ REMOVE_USER_GROUP_KEY - Removes existing group value
********************************************************************************/
IS
    SEG_NAME_ VARCHAR2(50);
    GRP_CNT_ NUMBER;
    CURR_GROUP_ID_ NUMBER;
    DEL_ELEMENTS_ NUMBER;
BEGIN

    SELECT COUNT(*) INTO GRP_CNT_ FROM RSC_USER_GROUP_KEYS WHERE KEYID = UG_ID_;
    IF (GRP_CNT_= 0) THEN
        dbms_output.put_line('Key '  || TO_CHAR(UG_ID_) || ' is not valid. Group not found. Nothing was deleted.');
    ELSE 
        CASE UPPER(SEGMENT_) 
            WHEN 'LOCATION' THEN 
                SEG_NAME_ := 'LOC_ID_INDEX';
            WHEN 'NLOCATION' THEN 
                SEG_NAME_ := 'NOT_LOC_ID_INDEX';
            WHEN 'SYSTEM' THEN 
                SEG_NAME_ := 'SYSTEM_ID_INDEX';
            WHEN 'NSYSTEM' THEN 
                SEG_NAME_ := 'NOT_SYSTEM_ID_INDEX';
            WHEN 'TYPE' THEN 
                SEG_NAME_ := 'TYPE_ID_INDEX';
            WHEN 'NTYPE' THEN 
                SEG_NAME_ := 'NOT_TYPE_ID_INDEX';
            ELSE SEG_NAME_ := 'NOT_FOUND';    
        END CASE;

        IF (SEG_NAME_ = 'NOT_FOUND') THEN
            dbms_output.put_line('Segment name '  || SEGMENT_ || ' is not a proper one. Check spelling. Aborting.');
            RETURN;
        END IF;

        SELECT DECODE(SEG_NAME_, 'LOC_ID_INDEX', LOC_ID_INDEX, 'NOT_LOC_ID_INDEX', NOT_LOC_ID_INDEX, 
            'SYSTEM_ID_INDEX', SYSTEM_ID_INDEX, 'NOT_SYSTEM_ID_INDEX', NOT_SYSTEM_ID_INDEX, 
            'TYPE_ID_INDEX', TYPE_ID_INDEX, 'NOT_TYPE_ID_INDEX', NOT_TYPE_ID_INDEX) 
        INTO CURR_GROUP_ID_
        FROM RSC_USER_GROUP_KEYS 
        WHERE KEYID = UG_ID_;
       
        IF (CURR_GROUP_ID_ IS NULL) THEN 
                dbms_output.put_line('Segment '  || SEGMENT_ || ' of the requested user group ID '   || TO_CHAR(UG_ID_) 
                || ' does not yet have any elements inside at all. Nothing can be deleted. Aborting.');                
        ELSE
            SELECT COUNT(*) INTO DEL_ELEMENTS_ 
            FROM RSC_USER_GROUP_VALS
            WHERE GROUP_ID = CURR_GROUP_ID_ AND 
                (VALUE = VALUE_ OR (VALUE IS NULL AND VALUE_ IS NULL)) AND
                (VALUE_2 = VALUE_2_ OR (VALUE_2 IS NULL AND VALUE_2_ IS NULL)) AND
                (VALUE_3 = VALUE_3_ OR (VALUE_3 IS NULL AND VALUE_3_ IS NULL));
            
            IF (DEL_ELEMENTS_ = 0) THEN
                dbms_output.put_line('Value set (' || VALUE_  ||', '|| VALUE_2_  || ', ' || VALUE_3_ || ') was not found for group ID ' || TO_CHAR(UG_ID_) 
                || ' and segment ' || SEGMENT_ || '. Nothing was deleted.');
            ELSE
                DELETE FROM RSC_USER_GROUP_VALS
                WHERE GROUP_ID = CURR_GROUP_ID_ AND 
                    (VALUE = VALUE_ OR (VALUE IS NULL AND VALUE_ IS NULL)) AND
                    (VALUE_2 = VALUE_2_ OR (VALUE_2 IS NULL AND VALUE_2_ IS NULL)) AND
                    (VALUE_3 = VALUE_3_ OR (VALUE_3 IS NULL AND VALUE_3_ IS NULL));
                
                SELECT COUNT(*) INTO DEL_ELEMENTS_ 
                FROM RSC_USER_GROUP_VALS
                WHERE GROUP_ID = CURR_GROUP_ID_;
                
                IF (DEL_ELEMENTS_ = 0) THEN
                    EXECUTE IMMEDIATE 'UPDATE RSC_USER_GROUP_KEYS SET '  || SEG_NAME_  || ' = NULL'  
                    || ' WHERE KEYID = '  || UG_ID_;                            
                END IF;                
            END IF;
        END IF;
    END IF;

EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'REMOVE_USER_GROUP_KEY');
END REMOVE_USER_GROUP_VALUE;

PROCEDURE ADD_USER_GROUP_VALUE(UG_ID_ IN NUMBER, SEGMENT_ IN VARCHAR2, VALUE_ IN VARCHAR2)
/********************************************************************************
\ ADD_USER_GROUP_VALUE - Adds new user value to the segment of the user group.
********************************************************************************/
IS
BEGIN
    ADD_USER_GROUP_VALUE(UG_ID_, SEGMENT_, VALUE_, NULL, NULL);
EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'ADD_USER_GROUP_VALUE');
END ADD_USER_GROUP_VALUE;

PROCEDURE ADD_USER_GROUP_VALUE(UG_ID_ IN NUMBER, SEGMENT_ IN VARCHAR2, VALUE_ IN VARCHAR2, VALUE_2_ IN VARCHAR2, VALUE_3_ IN VARCHAR2)
/********************************************************************************
\ ADD_USER_GROUP_VALUE - Adds new user value to the segment of the user group.
********************************************************************************/
IS
    ALL_OK_ BOOLEAN := TRUE;
    SEG_NAME_ VARCHAR2(50);
    GRP_CNT_ NUMBER;
    CURR_GROUP_ID_ NUMBER;
    VALID_GROUP_ID_ NUMBER;
    ITEM_EXISTS_ NUMBER;
    ITEM_TABLE_ VARCHAR2(50);
    ITEM_FIELD_ VARCHAR2(50);
    
    GEN_ROOM_ID_ VARCHAR2(150);
BEGIN

    SELECT COUNT(*) INTO GRP_CNT_ FROM RSC_USER_GROUP_KEYS WHERE KEYID = UG_ID_;
    IF (GRP_CNT_= 0) THEN
        ALL_OK_ := FALSE;
        dbms_output.put_line('Key '  || TO_CHAR(UG_ID_) || ' is not valid. Group not found. Nothing was added');
    ELSE 
        CASE UPPER(SEGMENT_) 
            WHEN 'LOCATION' THEN 
                SEG_NAME_ := 'LOC_ID_INDEX';
            WHEN 'NLOCATION' THEN 
                SEG_NAME_ := 'NOT_LOC_ID_INDEX';
            WHEN 'SYSTEM' THEN 
                SEG_NAME_ := 'SYSTEM_ID_INDEX';
                ITEM_TABLE_ := 'SYSTEM_TYPE';
                ITEM_FIELD_ := 'SYSTEM_ID';  
            WHEN 'NSYSTEM' THEN 
                SEG_NAME_ := 'NOT_SYSTEM_ID_INDEX';
                ITEM_TABLE_ := 'SYSTEM_TYPE';
                ITEM_FIELD_ := 'SYSTEM_ID';  
            WHEN 'TYPE' THEN 
                SEG_NAME_ := 'TYPE_ID_INDEX';
                ITEM_TABLE_ := 'COMPONENT_TYPE';
                ITEM_FIELD_ := 'COMPONENT_ID';
            WHEN 'NTYPE' THEN 
                SEG_NAME_ := 'NOT_TYPE_ID_INDEX';    
                ITEM_TABLE_ := 'COMPONENT_TYPE';
                ITEM_FIELD_ := 'COMPONENT_ID';
            ELSE SEG_NAME_ := 'NOT_FOUND';   
        END CASE;
        
        IF (SEG_NAME_ = 'NOT_FOUND') THEN
            dbms_output.put_line('Segment name '  || SEGMENT_ || ' is not a proper one. Check spelling. Aborting.');
            RETURN;
        END IF;
        
         IF (UPPER(SEGMENT_)  != 'LOCATION' AND UPPER(SEGMENT_) != 'NLOCATION') THEN
            IF (VALUE_ IS NULL) THEN
                dbms_output.put_line('Location must have building property filled in. Aborting.');  
                RETURN;             
            END IF;
            IF (VALUE_ IS NULL AND VALUE_2_ IS NULL AND VALUE_3_ IS NOT NULL) THEN
                dbms_output.put_line('Location must have building and elevation property filled in. Aborting.');  
                RETURN;             
            END IF;            
         END IF;

        SELECT DECODE(SEG_NAME_, 'LOC_ID_INDEX', LOC_ID_INDEX, 'NOT_LOC_ID_INDEX', NOT_LOC_ID_INDEX, 
            'SYSTEM_ID_INDEX', SYSTEM_ID_INDEX, 'NOT_SYSTEM_ID_INDEX', NOT_SYSTEM_ID_INDEX, 
            'TYPE_ID_INDEX', TYPE_ID_INDEX, 'NOT_TYPE_ID_INDEX', NOT_TYPE_ID_INDEX) 
        INTO CURR_GROUP_ID_
        FROM RSC_USER_GROUP_KEYS 
        WHERE KEYID = UG_ID_;
       
        IF (CURR_GROUP_ID_ IS NULL) THEN 
            VALID_GROUP_ID_ := GET_VALID_GROUP_ID;
            EXECUTE IMMEDIATE 'UPDATE RSC_USER_GROUP_KEYS SET '  || SEG_NAME_  || '='  || VALID_GROUP_ID_  || ' WHERE KEYID = '  || UG_ID_;            
        ELSE 
            VALID_GROUP_ID_ := CURR_GROUP_ID_;
        END IF;
        
        -- Check for existing IDs excpet for the elevation
        IF (UPPER(SEGMENT_)  != 'LOCATION' AND UPPER(SEGMENT_) != 'NLOCATION') THEN
            EXECUTE IMMEDIATE  'SELECT COUNT(*)  FROM '  || ITEM_TABLE_  || ' WHERE ' || 
                ITEM_FIELD_  || ' = '''  || VALUE_  || '''' INTO ITEM_EXISTS_ ;             
            IF (ITEM_EXISTS_ = 0) THEN
                ALL_OK_ := FALSE;
                dbms_output.put_line('Segment '  || SEGMENT_ || ' or its negated counterpart does not have the value '   || VALUE_ || ' entered in eSOMS.');    
            END IF;
        ELSE
            IF (VALUE_ IS NOT NULL) THEN
                EXECUTE IMMEDIATE  'SELECT COUNT(*)  FROM BUILDING_TYPE WHERE BUILDING_ID'  || ' = '''  || VALUE_  || '''' INTO ITEM_EXISTS_ ;             
                IF (ITEM_EXISTS_ = 0) THEN
                    --ALL_OK_ := FALSE; 
                    dbms_output.put_line('Segment LOCATION part BUILDING or its negated counterpart does not have the value '   || VALUE_ || ' entered in eSOMS.');    
                END IF;            
            END IF;
            IF (VALUE_2_ IS NOT NULL) THEN
                EXECUTE IMMEDIATE  'SELECT COUNT(*)  FROM ELEVATION_TYPE WHERE ELEVATION_ID'  || ' = '''  || PREP_ELEV(VALUE_2_)  || '''' INTO ITEM_EXISTS_ ;             
                IF (ITEM_EXISTS_ = 0) THEN
                    --ALL_OK_ := FALSE; 
                    dbms_output.put_line('Segment LOCATION part ELEVATION or its negated counterpart does not have the value '   || VALUE_2_ || ' entered in eSOMS.');    
                END IF;             
            END IF;
            IF (VALUE_3_ IS NOT NULL) THEN
                -- must generate a room name
                GEN_ROOM_ID_ := TRIM(VALUE_) || (VALUE_3_);

                EXECUTE IMMEDIATE  'SELECT COUNT(*)  FROM ROOM_TYPE WHERE ROOM_ID'  || ' = '''  || GEN_ROOM_ID_  || '''' INTO ITEM_EXISTS_ ;             
                IF (ITEM_EXISTS_ = 0) THEN
                    --ALL_OK_ := FALSE; 
                    dbms_output.put_line('Segment LOCATION part ROOM or its negated counterpart does not have the value '   || GEN_ROOM_ID_ || ' entered in eSOMS.');    
                END IF; 
            END IF;        
        END IF;

        IF (ALL_OK_ = TRUE) THEN
            INSERT INTO RSC_USER_GROUP_VALS (GROUP_ID, VALUE, VALUE_2, VALUE_3) 
            VALUES (VALID_GROUP_ID_, VALUE_, VALUE_2_, VALUE_3_);
        END IF;
    END IF;
    
EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'ADD_USER_GROUP_VALUE');
END ADD_USER_GROUP_VALUE;

FUNCTION GET_VALID_GROUP_ID RETURN NUMBER
/********************************************************************************
\ GET_VALID_GROUP_ID - Returns group id for this key index
********************************************************************************/
IS
    CURR_MAX_GRP_ID_ NUMBER;
BEGIN
    SELECT MAX(GROUP_ID) INTO CURR_MAX_GRP_ID_ FROM RSC_USER_GROUP_VALS;
        
    IF (CURR_MAX_GRP_ID_ IS NULL) THEN
        CURR_MAX_GRP_ID_ := 0;
    ELSE
        CURR_MAX_GRP_ID_ := CURR_MAX_GRP_ID_ + 1;
    END IF;
    
    RETURN CURR_MAX_GRP_ID_;
EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'GET_VALID_GROUP_ID');
    RETURN 0;
END GET_VALID_GROUP_ID;

FUNCTION LIST_RULES_MATRIX RETURN SYS_REFCURSOR
/********************************************************************************
\ LIST_RULES_MATRIX - Lists complete rulebook.
********************************************************************************/
IS
    REF_C SYS_REFCURSOR;
BEGIN
    OPEN REF_C FOR
        SELECT
        USERGROUP_NAME GRP,
        LIST_RULE_VALUES(TK_ID, 1) "STEP 1 VALUES",
        LIST_RULE_RESTRICTION(TK_ID, 1) "STEP 1 RULES",
        LIST_RULE_VALUES(TK_ID, 2) "STEP 2 VALUES",
        LIST_RULE_RESTRICTION(TK_ID, 2) "STEP 2 RULES",
        LIST_RULE_VALUES(TK_ID, 3) "STEP 3 VALUES",
        LIST_RULE_RESTRICTION(TK_ID, 3) "STEP 3 RULES",
        LIST_RULE_VALUES(TK_ID, 4) "STEP 4 VALUES",
        LIST_RULE_RESTRICTION(TK_ID, 4) "STEP 4 RULES",
        LIST_RULE_VALUES(TK_ID, 5) "STEP 5 VALUES",
        LIST_RULE_RESTRICTION(TK_ID, 5) "STEP 5 RULES"
    FROM RSC_TIME_KEYS;
    
    RETURN REF_C;

EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'LIST_RULES_MATRIX');
    RETURN NULL;
END LIST_RULES_MATRIX;

FUNCTION LIST_RULE_RESTRICTION(KEY_ID IN NUMBER, STEP_ID IN NUMBER) RETURN VARCHAR2
/********************************************************************************
\ LIST_RULE_RESTRICTION - Lists the rules for the current key and its step.
********************************************************************************/
IS

SEGMENT_ VARCHAR2(1);

CURSOR getRule IS 
    SELECT VALUE 
    FROM RSC_TIME_RULE_GROUPS rtrg 
    INNER JOIN RSC_TIME_RULES rtr ON rtrg.FOR_TR_ID = rtr.TR_ID
    WHERE rtr.FOR_TK_ID = KEY_ID AND rtr.FOR_STEP = STEP_ID AND rtrg.GROUP_SEGMENT = SEGMENT_;
  
   REC_ VARCHAR2(50);
   RES_STR VARCHAR2(4000);
   CNT NUMBER;
BEGIN
    SEGMENT_ := 'T';
    CNT := 0;
    OPEN getRule;
    LOOP
      FETCH getRule INTO REC_;
         IF getRule%NOTFOUND
         THEN
            IF (LENGTH(RES_STR) > 0) THEN
                RES_STR := SUBSTR(RES_STR, 1, LENGTH(RES_STR)-2) || CHR(13) || CHR(10) || CHR(13) || CHR(10);
            END IF;
            EXIT;
         ELSE
            IF (CNT = 0) THEN
                RES_STR := 'Tagout complex groups: ' || CHR(13) || CHR(10);
            END IF;
            RES_STR := RES_STR || REC_ || ', ';
            CNT := CNT + 1;
         END IF;
    END LOOP;
    CLOSE getRule;  
    
    SEGMENT_ := 'P';
    CNT := 0;
    OPEN getRule;
    LOOP
      FETCH getRule INTO REC_;
         IF getRule%NOTFOUND
         THEN
            IF (LENGTH(RES_STR) > 0) THEN
                RES_STR := SUBSTR(RES_STR, 1, LENGTH(RES_STR)-2) || CHR(13) || CHR(10) || CHR(13) || CHR(10);
            END IF;
            EXIT;
         ELSE
            IF (CNT = 0) THEN
                RES_STR := RES_STR  || 'Path complex groups: ' || CHR(13) || CHR(10);
            END IF;
            RES_STR := RES_STR || REC_ || ', ';
            CNT := CNT + 1;
         END IF;
    END LOOP;
    CLOSE getRule; 

    RETURN RES_STR;
EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'LIST_RULE_RESTRICTION');
    RETURN '';
END LIST_RULE_RESTRICTION;

FUNCTION LIST_RULE_VALUES(KEY_ID IN NUMBER, STEP_ID IN NUMBER) RETURN VARCHAR2
/********************************************************************************
\ LIST_RULE_VALUES - Lists values for current key and its step.
********************************************************************************/
IS
    RES_STR VARCHAR2(4000);
BEGIN

      SELECT 
         NVL2(TO_CHAR(SIMPLE_VALUE), 'S(t)=' || TO_CHAR(SIMPLE_VALUE)  || CHR(13) || CHR(10), '') ||
         NVL2(TO_CHAR(COMPLEX_VALUE), 'C(t)=' || TO_CHAR(COMPLEX_VALUE)  || CHR(13) || CHR(10) ,'')  || 
         NVL2(TO_CHAR(SIMPLE_PATH_VALUE), 'S(p)=' || TO_CHAR(SIMPLE_PATH_VALUE)  || CHR(13) || CHR(10), '') ||
         NVL2(TO_CHAR(COMPLEX_PATH_VALUE), 'C(p)=' || TO_CHAR(COMPLEX_PATH_VALUE)  || CHR(13) || CHR(10) ,'')  || 
         NVL2(TO_CHAR(MIN_OVERALL_VALUE), 'C(def)=' || TO_CHAR(MIN_OVERALL_VALUE)  || CHR(13) || CHR(10) ,'') INTO RES_STR
      FROM RSC_TIME_RULES WHERE FOR_TK_ID = KEY_ID AND FOR_STEP = STEP_ID;        

    RETURN RES_STR;
EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'LIST_RULE_VALUES');
    RETURN '';
END LIST_RULE_VALUES;

PROCEDURE CHECK_ALL_EQUIPMENT
/************************************************************************************************
\ CHECK_ALL_EQUIPMENT - Checks all the equipment and writes responsibility to it.
************************************************************************************************/
IS
    EQUIP_ RSC_STR_TABLE;
    EQUIP_RSP_ VARCHAR2(150);
    RSP_CODE_ VARCHAR2(50);
    MSG_ VARCHAR2(1500);  
    CURSOR getCode IS 
          SELECT USERGROUP_CODE 
          FROM RSC_TIME_KEYS WHERE USERGROUP_NAME = EQUIP_RSP_;
           
BEGIN
    SELECT EQUIP_OPERATOR_ID BULK COLLECT INTO EQUIP_ FROM
        EQUIP WHERE DISABLED=0 AND SYSTEM_ID NOT IN ('MSR', 'REL', 'CBL');
  
    FOR i IN EQUIP_.FIRST .. EQUIP_.LAST
    LOOP
        EQUIP_RSP_ := GET_EQUIP_RESPONSIBILITY(EQUIP_(i));
        IF (LENGTH(EQUIP_RSP_) > 0 AND RSC_WRITE_TO_EQUIP = 1) THEN

          OPEN getCode;
          FETCH getCode INTO RSP_CODE_;
          CLOSE getCode;
          
          IF (RSP_CODE_ IS NULL OR RSP_CODE_ = '') THEN
              MSG_ := 'Equipment '  || EQUIP_RSP_  ||  ' has a r.g. '  || EQUIP_RSP_ ||  
                '. This group is not found in TIME_KEY table!';
              dbms_output.put_line(MSG_);
              NEK_LOG(MSG_, 'CHECK_ALL_EQUIPMENT');
          END IF;

          UPDATE EQUIP SET EQUIP_ATTRIBUTE_06 = EQUIP_RSP_  || ' | '  || RSP_CODE_
                WHERE EQUIP_OPERATOR_ID = EQUIP_(i);
        END IF;
    END LOOP;

EXCEPTION
    WHEN OTHERS THEN NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'CHECK_ALL_EQUIPMENT');
END CHECK_ALL_EQUIPMENT;
   
  PROCEDURE NEK_LOG (VALUE_ IN VARCHAR2, LOC_ IN VARCHAR2)
   IS
      /********************************************************************************
      \ NEK_LOG - function logs inproper interface functionality. All exceptions are written through log function.
      ********************************************************************************/
   PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      INSERT INTO NEK_LOG_MAIN (LOG_DATETIME,
                                LOG_TYPE,
                                LOG_OWNER,
                                LOG_LOCATION,
                                LOG_VALUE,
                                LOG_DESCRIPTION)
           VALUES (SYSTIMESTAMP,
                   'ERR',
                   'RSC',
                   LOC_,
                   TRIM (SUBSTR (VALUE_, 1, 4000)),
                   TRIM (SUBSTR (LOC_, 1, 1000)));
      NULL;
      COMMIT;                                         
   END NEK_LOG;
                   
  PROCEDURE NEK_LOG (VALUE_ IN VARCHAR2, LOC_ IN VARCHAR2, TYPE_ IN VARCHAR2)
   IS
      /********************************************************************************
      \ NEK_LOG - function logs interface functionality.
      ********************************************************************************/
   PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      INSERT INTO NEK_LOG_MAIN (LOG_DATETIME,
                                LOG_TYPE,
                                LOG_OWNER,
                                LOG_LOCATION,
                                LOG_VALUE,
                                LOG_DESCRIPTION)
           VALUES (SYSTIMESTAMP,
                   TYPE_,
                   'APPMON',
                   LOC_,
                   TRIM (SUBSTR (VALUE_, 1, 4000)),
                   TRIM (SUBSTR (LOC_, 1, 1000)));
      NULL;
      COMMIT;                                         
   END NEK_LOG;           
   
END RSC_RESOURCE_APP;
/