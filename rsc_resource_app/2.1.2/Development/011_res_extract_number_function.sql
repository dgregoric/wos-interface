CREATE OR REPLACE FUNCTION RSC_EXTRACT_NUMBER(STR_ IN VARCHAR2) RETURN NUMBER IS
L1_ NUMBER;
L2_ NUMBER;
CUT_STR_ VARCHAR2(4000);
/******************************************************************************
   1.0        18/9/2013   Marko Clemente   
                                    Func. returns number when found in string
 ******************************************************************************/
BEGIN
    
   L1_ := INSTR(STR_, '[');
   IF (L1_ IS NOT NULL AND L1_ > -1) THEN
        L2_ := INSTR(STR_, ']', L1_); 
        IF (L2_ IS NOT NULL AND L2_ > -1) THEN
            
            CUT_STR_ := SUBSTR(STR_, L1_+1, L2_-L1_-1);

            IF (LENGTH(TRIM(TRANSLATE (CUT_STR_, ' +-.,0123456789',' '))) IS NULL) THEN
                CUT_STR_ := REPLACE(CUT_STR_, '.', ',');
                RETURN TO_NUMBER(CUT_STR_);
            END IF;
        END IF;
   END IF;
    
   RETURN 0;
EXCEPTION
     WHEN OTHERS THEN
        NULL;
END RSC_EXTRACT_NUMBER;



/
