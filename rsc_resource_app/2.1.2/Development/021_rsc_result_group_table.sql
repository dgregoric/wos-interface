CREATE TABLE RSC_RESULT_GROUP
(
  RG_ID          NUMBER                         NOT NULL,
  ENTRY_TIME     DATE                           NOT NULL,
  JOB_ID         NUMBER,
  GROUP_NAME     VARCHAR2(50 BYTE),
  GROUP_NAME_ID  VARCHAR2(50 BYTE),
  TA_ID          VARCHAR2(50 BYTE),
  TO_FOLDER      VARCHAR2(250 BYTE),
  TO_NAME        VARCHAR2(250 BYTE),
  EQUIP_ID      VARCHAR2(500 BYTE),
  CARD_POS   VARCHAR2(150 BYTE), 
  CARD_TYPE  VARCHAR2(50 BYTE),
  OPER           VARCHAR2(250 BYTE),
  TAG_ACTION_ID  INTEGER,
  RES_ACTIVITY_VALUE VARCHAR2(150 BYTE),
  RES_LAG        NUMBER,
  RES_LAG_MINUTE    NUMBER,
  RES_LAG_STRECH NUMBER,
  RES_LAG_NORMAL NUMBER,
  RES_TIME       NUMBER,
  RES_TIME_MINUTE   NUMBER,
  RES_TIME_STRECH NUMBER,
  RES_TIME_NORMAL NUMBER,
  RES_LOAD       NUMBER,
  RES_LOAD_NORMAL NUMBER,
  RES_STEP       NUMBER,
  LOC_RES_LAG    NUMBER, 
  HOURLY_BLOCK NUMBER
);

CREATE UNIQUE INDEX RSC_RESULT_GROUP_PK ON RSC_RESULT_GROUP
(RG_ID);

CREATE OR REPLACE TRIGGER RSC_RESULT_GROUP_AUTOINC
BEFORE INSERT
ON RSC_RESULT_GROUP
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DECLARE
BEGIN
   SELECT RSC_RESULT_GROUPS_SQ.nextval INTO :NEW.RG_ID FROM dual;
   :NEW.ENTRY_TIME := SYSDATE;
   EXCEPTION
     WHEN OTHERS THEN
       RAISE;
END RSC_RESULT_GROUP;

ALTER TABLE RSC_RESULT_GROUP ADD (
  CONSTRAINT RSC_RESULT_GROUP_PK
  PRIMARY KEY
  (RG_ID)
  USING INDEX RSC_RESULT_GROUP_PK
  ENABLE VALIDATE);
