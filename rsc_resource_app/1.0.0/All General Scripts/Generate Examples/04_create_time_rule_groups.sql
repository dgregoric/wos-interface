-- GO 2/2
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (2, 'T', '5');
-- RO 3/2
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (6, 'T', '5');
-- RO 3/3
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (7, 'T', 'ODPRT DRENAZA');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (7, 'T', 'ODPRT VENT');
-- BOP 4/2
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (10, 'T', '5');
-- BOP 4/3
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (11, 'T', 'ODPRT DRENAZA');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (11, 'T', 'ODPRT VENT');
-- DOOS 5/2
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (14, 'T', '5');
-- DOOS 5/3
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (15, 'T', 'ODPRT DRENAZA');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (15, 'T', 'ODPRT VENT');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (15, 'P', 'AB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (15, 'P', 'CCB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (15, 'P', 'SW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (15, 'P', 'PW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (15, 'P', 'CT');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (15, 'P', 'SY');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (15, 'P', 'RB');
-- DOOS 5/4
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (16, 'P', 'AB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (16, 'P', 'CCB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (16, 'P', 'SW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (16, 'P', 'PW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (16, 'P', 'CT');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (16, 'P', 'SY');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (16, 'P', 'RB');
-- AB 6/2
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (18, 'T', '5');
-- AB 6/3
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (19, 'T', 'ODPRT DRENAZA');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (19, 'T', 'ODPRT VENT');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (19, 'P', 'AB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (19, 'P', 'CCB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (19, 'P', 'SW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (19, 'P', 'PW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (19, 'P', 'CT');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (19, 'P', 'SY');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (19, 'P', 'RB');
-- AB 6/4
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (20, 'P', 'AB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (20, 'P', 'CCB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (20, 'P', 'SW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (20, 'P', 'PW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (20, 'P', 'CT');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (20, 'P', 'SY');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (20, 'P', 'RB');
-- SAVA 7/2
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (22, 'T', '5');
-- SAVA 7/3
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (23, 'T', 'ODPRT DRENAZA');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (23, 'T', 'ODPRT VENT');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (23, 'P', 'AB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (23, 'P', 'CCB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (23, 'P', 'SW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (23, 'P', 'PW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (23, 'P', 'CT');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (23, 'P', 'SY');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (23, 'P', 'RB');
-- SAVA 5/4
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (24, 'P', 'AB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (24, 'P', 'CCB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (24, 'P', 'SW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (24, 'P', 'PW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (24, 'P', 'CT');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (24, 'P', 'SY');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (24, 'P', 'RB');
-- DG 8/2
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (26, 'T', '5');
-- DG 8/3
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (27, 'T', 'ODPRT DRENAZA');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (27, 'T', 'ODPRT VENT');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (27, 'P', 'AB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (27, 'P', 'CCB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (27, 'P', 'SW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (27, 'P', 'PW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (27, 'P', 'CT');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (27, 'P', 'SY');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (27, 'P', 'RB');
-- DG 8/4
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (28, 'P', 'AB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (28, 'P', 'CCB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (28, 'P', 'SW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (28, 'P', 'PW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (28, 'P', 'CT');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (28, 'P', 'SY');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (28, 'P', 'RB');
-- TU 9/2
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (30, 'T', '5');
-- TU 9/3
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (31, 'T', 'ODPRT DRENAZA');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (31, 'T', 'ODPRT VENT');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (31, 'P', 'AB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (31, 'P', 'CCB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (31, 'P', 'SW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (31, 'P', 'PW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (31, 'P', 'CT');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (31, 'P', 'SY');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (31, 'P', 'RB');
-- TU 9/4
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (32, 'P', 'AB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (32, 'P', 'CCB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (32, 'P', 'SW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (32, 'P', 'PW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (32, 'P', 'CT');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (32, 'P', 'SY');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (32, 'P', 'RB');
-- PW 10/2
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (34, 'T', '5');
-- PW 10/3
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (35, 'T', 'ODPRT DRENAZA');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (35, 'T', 'ODPRT VENT');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (35, 'P', 'AB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (35, 'P', 'CCB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (35, 'P', 'SW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (35, 'P', 'PW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (35, 'P', 'CT');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (35, 'P', 'SY');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (35, 'P', 'RB');
-- PW 10/4
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (36, 'P', 'AB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (36, 'P', 'CCB');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (36, 'P', 'SW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (36, 'P', 'PW');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (36, 'P', 'CT');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (36, 'P', 'SY');
INSERT INTO RSC_TIME_RULE_GROUPS (FOR_TR_ID, GROUP_SEGMENT, VALUE) VALUES (36, 'P', 'RB');
