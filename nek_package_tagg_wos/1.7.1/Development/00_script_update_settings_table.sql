INSERT INTO NEK_ITF_SETTINGS 
    (USER_ID, APP_ID, SECTION_ID, SETTING_ID, VALUE_ID, DESCRIPTION)
VALUES
('ADMIN', 'WOS_ITF', 'TAGOUT', 'EXECUTE_SIGN_CHECK', '0', 'Controls the checking of the sign authorization. 0 -> no; 1 -> yes');

COMMIT;