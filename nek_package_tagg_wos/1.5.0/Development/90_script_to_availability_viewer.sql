/*
Spodnji dve vrstici sta namenjeni zgolj testu.
View deluje enako kot v tocki 1.3, se pravi mu lahko podamo parameter.
V tem primeru je parameter le en in to je vrednost attributne vrednosti
Recimo, sedaj lahko testiramo, kaj bi se zgodilo, ce bi namesto "da" iskali "ne"
(seveda mora imeti kak project tudi atribut nastavljen na ne :)) 
*/
/*
EXEC ESM_DEV_ITF.NEK_PACKAGE_TAGG_WOS.NEK_WOREP_CLEAR_VAL;
EXEC ESM_DEV_ITF.NEK_PACKAGE_TAGG_WOS.NEK_WOREP_SET_STR_VAL('proj_attribute','Da');
EXEC ESM_DEV_ITF.NEK_PACKAGE_TAGG_WOS.NEK_WOREP_SET_STR_VAL('proj_attribute_num','1')

*/
SELECT * FROM ESM_DEV_ITF.TO_ALLOWED_TO_SIGN;
EXEC ESM_DEV_ITF.NEK_PACKAGE_TAGG_WOS.NEK_WOREP_CLEAR_VAL;