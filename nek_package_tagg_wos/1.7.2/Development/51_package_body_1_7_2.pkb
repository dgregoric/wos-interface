CREATE OR REPLACE PACKAGE BODY ESM_DEV_ITF.NEK_PACKAGE_TAGG_WOS
AS
   /********************************************************************************
   PACKAGE BODY HEADER
   CHANGE
   1.7.2: 07.6.2013:M. Clemente: Modification accepted with doc 1.2 paragraph
                    + trigger modification on the main exchange table NEK_TAG_WO
                     to support WO closure when needed 
   1.7.1: 06.6.2013: M. Clemente: Modification accepted with doc 1.12 paragraph
                    + added variable to control signing authorization
                    + modified the signing checking logic to support the control
   1.7.0: 22.5.2013: M. Clemente: Modifications accepted with doc. 1.11 paragraph
                    + added trigger to TEMP_LIFT_VERIF
                    + added support methods to support the functionality of 
                      removing TO signs on temporary lifts that do not have WOh
                      signs.
   1.6.0: 18.4.2013: M. Clemente: Modifications accepted with doc. 1.7 paragraph
                    + changed the TAG verification levels logic from positional
                     to internal logic
                    + added internal verification levels to the configuration
                      table
                    + updated header with required variables   
   1.5.0; 15.3.2013; M. Clemente; Modifications accepted with doc. 1.6 paragraph
                    + added dynamic checks for project groups TOs are in
                    + log message if the project group does not confirm to the rules
                      at Sign ON/OFF
                    + added configuration for the attributes value for the Project
                    + added synonims
                    + added view showing TOs allowed to be signed on/off based on
                      the attribute on the project(TO_ALLOWED_TO_SIGN)
                    + added configuration variable representing attribute positive value
                                         
   1.4.11; 22.2.2013; M. Clemente; Modifications accepted with doc. 1.3 paragraph 
                    + modified NEK_TRIG_INSERT_REPORT_WO to support WO Reporting 
                    + added table NEK_TAG_WO_REPORT
                        + added index
                        + added sequencer
                        + denormalized data
                    + WO generating trigger modified to support reporting
                    + Added functionality to support Context modification for Vievs
                    + Extra settings in the header for the contex
                    
   1.4.10; 19.12.2012; D. Gregoric;  Changes in NEK_TO_SIGN_ON_API (NEK issue 50)
   ********************************************************************************/

   FUNCTION NEK_GET_RETURN_MESSAGE
      RETURN VARCHAR2
   IS
   /********************************************************************************
   \  NEK_GET_RETURN_MESSAGE - return what is defined as an OK message.
   ********************************************************************************/
   BEGIN
      RETURN OK_MESSAGE;
   END NEK_GET_RETURN_MESSAGE;

   FUNCTION NEK_GET_RETURN_MESSAGE (MESSAGE_ IN VARCHAR2)
      RETURN BOOLEAN
   IS
   /********************************************************************************
   \  NEK_GET_RETURN_MESSAGE - return what is defined as an OK message.
   ********************************************************************************/
   BEGIN
      IF MESSAGE_ = OK_MESSAGE
      THEN
         RETURN TRUE;
      ELSIF OK_MESSAGE IS NULL AND MESSAGE_ IS NULL
      THEN
         RETURN TRUE;
      ELSE
         RETURN FALSE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_GET_RETURN_MESSAGE');
         RETURN FALSE;
   END NEK_GET_RETURN_MESSAGE;

   PROCEDURE SET_USER (USER_ IN VARCHAR2)
   IS
      /********************************************************************************
      \  NEK_W_VIEW_TO_SIGN_ON - Full parameter Table type return function
      ********************************************************************************/
      USER_EXISTS   BOOLEAN := FALSE;
   BEGIN
      USER_EXISTS := NEK_FIND_USER_API (USER_);

      IF USER_EXISTS = FALSE
      THEN
         NEK_ADD_USER_API (USER_);
      END IF;
   END SET_USER;

   /* Wrapper functions supporting DB Link functionality */
   FUNCTION NEK_W_VIEW_TO_SIGN_ON (
      WONAME_          IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM       IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TAGOUTSECT_NUM   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_ID          IN USERLOG.USER_ID%TYPE,
      TIME_STAMP       IN TIMESTAMP)
      RETURN TAB_TO_SIGN_ON
   IS
      /********************************************************************************
      \  NEK_W_VIEW_TO_SIGN_ON - Full parameter Table type return function
      ********************************************************************************/
      cVTOn    TAB_TO_SIGN_ON;
      cVTOnR   REC_TO_SIGN_ON;

      cCURS    RETDATA;
      iCOUNT   NUMBER (6) := 1;
   BEGIN
      cCURS :=
         NEK_VIEW_TO_SIGN_ON (WONAME_,
                              TAGOUT_NUM,
                              TAGOUTSECT_NUM,
                              USER_ID,
                              TIME_STAMP);

      LOOP
         FETCH cCURS
         INTO cVTOnR.USER,
              cVTOnR.TAGOUT_NUMBER,
              cVTOnR.TAGOUT_SECT_NUM,
              cVTOnR.RESULT,
              cVTOnR.REASON,
              cVTOnR.TIME_STAMP;

         IF cCURS%NOTFOUND
         THEN
            EXIT;
         ELSE
            cVTOn (iCOUNT) := cVTOnR;
            iCOUNT := iCOUNT + 1;
         END IF;
      END LOOP;

      CLOSE cCURS;

      RETURN cVTOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_W_VIEW_TO_SIGN_ON');
         RETURN cVTOn;
   END NEK_W_VIEW_TO_SIGN_ON;

   FUNCTION NEK_W_VIEW_TO_SIGN_ON (
      WONAME_      IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      USER_ID      IN USERLOG.USER_ID%TYPE,
      TIME_STAMP   IN TIMESTAMP)
      RETURN TAB_TO_SIGN_ON
   IS
      /********************************************************************************
      \  NEK_W_VIEW_TO_SIGN_ON - Partial parameter Table type return function
      ********************************************************************************/
      cVTOn    TAB_TO_SIGN_ON;
      cVTOnR   REC_TO_SIGN_ON;

      cCURS    RETDATA;
      iCOUNT   NUMBER (6) := 1;
   BEGIN
      cCURS :=
         NEK_VIEW_TO_SIGN_ON (WONAME_,
                              TAGOUT_NUM,
                              USER_ID,
                              TIME_STAMP);

      LOOP
         FETCH cCURS
         INTO cVTOnR.USER,
              cVTOnR.TAGOUT_NUMBER,
              cVTOnR.TAGOUT_SECT_NUM,
              cVTOnR.RESULT,
              cVTOnR.REASON,
              cVTOnR.TIME_STAMP;

         IF cCURS%NOTFOUND
         THEN
            EXIT;
         ELSE
            cVTOn (iCOUNT) := cVTOnR;
            iCOUNT := iCOUNT + 1;
         END IF;
      END LOOP;

      CLOSE cCURS;

      RETURN cVTOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_W_VIEW_TO_SIGN_ON');
         RETURN cVTOn;
   END NEK_W_VIEW_TO_SIGN_ON;

   FUNCTION NEK_W_VIEW_TO_SIGN_ON_REASON (
      WONAME_          IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM       IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TAGOUTSECT_NUM   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_ID          IN USERLOG.USER_ID%TYPE,
      TIME_STAMP       IN TIMESTAMP)
      RETURN TAB_TO_SIGN_ON_REASON
   IS
      /********************************************************************************
      \ NEK_W_VIEW_TO_SIGN_ON - Full parameter Table type return function with REASON only
      ********************************************************************************/
      cVTOn    TAB_TO_SIGN_ON_REASON;
      cVTOnR   REC_TO_SIGN_ON_REASON;

      cCURS    RETDATA;
      iCOUNT   NUMBER (6) := 1;
      DUMMY    VARCHAR2 (4000);
   BEGIN
      cCURS :=
         NEK_VIEW_TO_SIGN_ON_REASON (WONAME_,
                                     TAGOUT_NUM,
                                     TAGOUTSECT_NUM,
                                     USER_ID,
                                     TIME_STAMP);

      LOOP
         FETCH cCURS
         INTO DUMMY, cVTOnR.REASON;

         IF cCURS%NOTFOUND
         THEN
            EXIT;
         ELSE
            cVTOn (iCOUNT) := cVTOnR;
            iCOUNT := iCOUNT + 1;
         END IF;
      END LOOP;

      CLOSE cCURS;

      RETURN cVTOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_W_VIEW_TO_SIGN_ON_REASON');
         RETURN cVTOn;
   END NEK_W_VIEW_TO_SIGN_ON_REASON;

   FUNCTION NEK_W_VIEW_WO_SIGN_ON (
      WORKORDER_NUM    IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM       IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TAGOUTSECT_NUM   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_ID          IN USERLOG.USER_ID%TYPE,
      TIME_STAMP       IN TIMESTAMP)
      RETURN TAB_WO_SIGN_ON
   IS
      /********************************************************************************
      \ NEK_W_VIEW_WO_SIGN_ON - Full parameter Table type return function
      ********************************************************************************/
      cVWOn    TAB_WO_SIGN_ON;
      cVWOnR   REC_WO_SIGN_ON;

      cCURS    RETDATA;
      iCOUNT   NUMBER (6) := 1;
   BEGIN
      cCURS :=
         NEK_VIEW_WO_SIGN_ON (WORKORDER_NUM,
                              TAGOUT_NUM,
                              TAGOUTSECT_NUM,
                              USER_ID,
                              TIME_STAMP);

      LOOP
         FETCH cCURS
         INTO cVWOnR.USER,
              cVWOnR.WORKORDER_NUMBER,
              cVWOnR.TAGOUT_NUMBER,
              cVWOnR.TAGOUT_SECT_NUM,
              cVWOnR.RESULT,
              cVWOnR.REASON,
              cVWOnR.TIME_STAMP;

         IF cCURS%NOTFOUND
         THEN
            EXIT;
         ELSE
            cVWOn (iCOUNT) := cVWOnR;
            iCOUNT := iCOUNT + 1;
         END IF;
      END LOOP;

      CLOSE cCURS;

      RETURN cVWOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_W_VIEW_WO_SIGN_ON');
         RETURN cVWOn;
   END NEK_W_VIEW_WO_SIGN_ON;

   FUNCTION NEK_W_VIEW_WO_SIGN_ON (
      WORKORDER_NUM   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM      IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      USER_ID         IN USERLOG.USER_ID%TYPE,
      TIME_STAMP      IN TIMESTAMP)
      RETURN TAB_WO_SIGN_ON
   IS
      /********************************************************************************
      \ NEK_W_VIEW_WO_SIGN_ON - Partial parameter Table type return function
      ********************************************************************************/
      cVWOn    TAB_WO_SIGN_ON;
      cVWOnR   REC_WO_SIGN_ON;

      cCURS    RETDATA;
      iCOUNT   NUMBER (6) := 1;
   BEGIN
      cCURS :=
         NEK_VIEW_WO_SIGN_ON (WORKORDER_NUM,
                              TAGOUT_NUM,
                              USER_ID,
                              TIME_STAMP);

      LOOP
         FETCH cCURS
         INTO cVWOnR.USER,
              cVWOnR.WORKORDER_NUMBER,
              cVWOnR.TAGOUT_NUMBER,
              cVWOnR.TAGOUT_SECT_NUM,
              cVWOnR.RESULT,
              cVWOnR.REASON,
              cVWOnR.TIME_STAMP;

         IF cCURS%NOTFOUND
         THEN
            EXIT;
         ELSE
            cVWOn (iCOUNT) := cVWOnR;
            iCOUNT := iCOUNT + 1;
         END IF;
      END LOOP;

      CLOSE cCURS;

      RETURN cVWOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_W_VIEW_WO_SIGN_ON');
         RETURN cVWOn;
   END NEK_W_VIEW_WO_SIGN_ON;

   FUNCTION NEK_W_VIEW_WO_SIGN_ON (
      WORKORDER_NUM   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      USER_ID         IN USERLOG.USER_ID%TYPE,
      TIME_STAMP      IN TIMESTAMP)
      RETURN TAB_WO_SIGN_ON
   IS
      /********************************************************************************
      \ NEK_W_VIEW_WO_SIGN_ON - Minimum parameter Table type return function
      ********************************************************************************/
      cVWOn    TAB_WO_SIGN_ON;
      cVWOnR   REC_WO_SIGN_ON;

      cCURS    RETDATA;
      iCOUNT   NUMBER (6) := 1;
   BEGIN
      cCURS := NEK_VIEW_WO_SIGN_ON (WORKORDER_NUM, USER_ID, TIME_STAMP);

      LOOP
         FETCH cCURS
         INTO cVWOnR.USER,
              cVWOnR.WORKORDER_NUMBER,
              cVWOnR.TAGOUT_NUMBER,
              cVWOnR.TAGOUT_SECT_NUM,
              cVWOnR.RESULT,
              cVWOnR.REASON,
              cVWOnR.TIME_STAMP;

         IF cCURS%NOTFOUND
         THEN
            EXIT;
         ELSE
            cVWOn (iCOUNT) := cVWOnR;
            iCOUNT := iCOUNT + 1;
         END IF;
      END LOOP;

      CLOSE cCURS;

      RETURN cVWOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_W_VIEW_WO_SIGN_ON');
         RETURN cVWOn;
   END NEK_W_VIEW_WO_SIGN_ON;

   FUNCTION NEK_W_VIEW_WO_SIGN_ON_REASON (
      WORKORDER_NUM    IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM       IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TAGOUTSECT_NUM   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_ID          IN USERLOG.USER_ID%TYPE,
      TIME_STAMP       IN TIMESTAMP)
      RETURN TAB_WO_SIGN_ON_REASON
   IS
      /********************************************************************************
      \ NEK_W_VIEW_WO_SIGN_ON - Full parameter Table type return function REASON only
      ********************************************************************************/
      cVWOn    TAB_WO_SIGN_ON_REASON;
      cVWOnR   REC_WO_SIGN_ON_REASON;

      cCURS    RETDATA;
      iCOUNT   NUMBER (6) := 1;
      DUMMY    VARCHAR (10);
   BEGIN
      cCURS :=
         NEK_VIEW_WO_SIGN_ON_REASON (WORKORDER_NUM,
                                     TAGOUT_NUM,
                                     TAGOUTSECT_NUM,
                                     USER_ID,
                                     TIME_STAMP);

      LOOP
         FETCH cCURS
         INTO DUMMY, cVWOnR.REASON;

         IF cCURS%NOTFOUND
         THEN
            EXIT;
         ELSE
            cVWOn (iCOUNT) := cVWOnR;
            iCOUNT := iCOUNT + 1;
         END IF;
      END LOOP;

      CLOSE cCURS;

      RETURN cVWOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_W_VIEW_WO_SIGN_ON_REASON');
         RETURN cVWOn;
   END NEK_W_VIEW_WO_SIGN_ON_REASON;

   FUNCTION NEK_W_VIEW_WO_LIST_TO (
      WORKORDER_NUM   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      USER_ID         IN USERLOG.USER_ID%TYPE,
      TIME_STAMP      IN TIMESTAMP)
      RETURN TAB_WO_LIST_TO
   IS
      /********************************************************************************
      \ NEK_W_VIEW_WO_LIST_TO - Table type return function
      ********************************************************************************/
      cVTOn    TAB_WO_LIST_TO;
      cVTOnR   REC_WO_LIST_TO;

      cCURS    RETDATA;
      iCOUNT   NUMBER (6) := 1;
   BEGIN
      cCURS := NEK_VIEW_WO_LIST_TO (WORKORDER_NUM, USER_ID, TIME_STAMP);

      LOOP
         FETCH cCURS
         INTO cVTOnR.TAGOUT_NUMBER,
              cVTOnR.TAGOUT_SECT_NUM,
              cVTOnR.DESCRIPTION,
              cVTOnR.HAZARDS,
              cVTOnR.USERC,
              cVTOnR.TIME_STAMP,
              cVTOnR.STATUS,
              cVTOnR.PREPARER;

         IF cCURS%NOTFOUND
         THEN
            EXIT;
         ELSE
            cVTOn (iCOUNT) := cVTOnR;
            iCOUNT := iCOUNT + 1;
         END IF;
      END LOOP;

      CLOSE cCURS;

      RETURN cVTOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_W_VIEW_WO_LIST_TO');
         RETURN cVTOn;
   END NEK_W_VIEW_WO_LIST_TO;

   FUNCTION NEK_W_VIEW_TAGS_LIST_WO (
      WORKORDER_NUM    IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM       IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TAGOUTSECT_NUM   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_ID          IN USERLOG.USER_ID%TYPE,
      TIME_STAMP       IN TIMESTAMP)
      RETURN TAB_TAGS_LIST_WO
   IS
      /********************************************************************************
      \ NEK_W_VIEW_TAGS_LIST_WO - Table type
      ********************************************************************************/
      cVTOn    TAB_TAGS_LIST_WO;
      cVTOnR   REC_TAGS_LIST_WO;

      cCURS    RETDATA;
      iCOUNT   NUMBER (6) := 1;
   BEGIN
      cCURS :=
         NEK_VIEW_TAGS_LIST_WO (WORKORDER_NUM,
                                TAGOUT_NUM,
                                TAGOUTSECT_NUM,
                                USER_ID,
                                TIME_STAMP);

      LOOP
         FETCH cCURS
         INTO cVTOnR.TAGOUT_NUMBER,
              cVTOnR.TAGOUT_SECT_NUM,
              cVTOnR.CONFIG,
              cVTOnR.IS_ON_TEMP_LIFT,
              cVTOnR.SERIAL,
              cVTOnR.JOINED_SERIAL,
              cVTOnR.EQUIPMENT,
              cVTOnR.TAG_HUNG,
              cVTOnR.TEMP_LIFT;

         IF cCURS%NOTFOUND
         THEN
            EXIT;
         ELSE
            cVTOn (iCOUNT) := cVTOnR;
            iCOUNT := iCOUNT + 1;
         END IF;
      END LOOP;

      CLOSE cCURS;

      RETURN cVTOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_W_VIEW_TAGS_LIST_WO');
         RETURN cVTOn;
   END NEK_W_VIEW_TAGS_LIST_WO;

   FUNCTION NEK_W_VIEW_TAGS_LIST_WO (
      WORKORDER_NUM   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM      IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      USER_ID         IN USERLOG.USER_ID%TYPE,
      TIME_STAMP      IN TIMESTAMP)
      RETURN TAB_TAGS_LIST_WO
   IS
      /********************************************************************************
      \ NEK_W_VIEW_TAGS_LIST_WO - Table type
      ********************************************************************************/
      cVTOn    TAB_TAGS_LIST_WO;
      cVTOnR   REC_TAGS_LIST_WO;

      cCURS    RETDATA;
      iCOUNT   NUMBER (6) := 1;
   BEGIN
      cCURS :=
         NEK_VIEW_TAGS_LIST_WO (WORKORDER_NUM,
                                TAGOUT_NUM,
                                USER_ID,
                                TIME_STAMP);

      LOOP
         FETCH cCURS
         INTO cVTOnR.TAGOUT_NUMBER,
              cVTOnR.TAGOUT_SECT_NUM,
              cVTOnR.CONFIG,
              cVTOnR.IS_ON_TEMP_LIFT,
              cVTOnR.SERIAL,
              cVTOnR.JOINED_SERIAL,
              cVTOnR.EQUIPMENT,
              cVTOnR.TAG_HUNG,
              cVTOnR.TEMP_LIFT;

         IF cCURS%NOTFOUND
         THEN
            EXIT;
         ELSE
            cVTOn (iCOUNT) := cVTOnR;
            iCOUNT := iCOUNT + 1;
         END IF;
      END LOOP;

      CLOSE cCURS;

      RETURN cVTOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_W_VIEW_TAGS_LIST_WO');
         RETURN cVTOn;
   END NEK_W_VIEW_TAGS_LIST_WO;

   FUNCTION NEK_W_VIEW_TAGS_LIST_WO (
      WORKORDER_NUM   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      USER_ID         IN USERLOG.USER_ID%TYPE,
      TIME_STAMP      IN TIMESTAMP)
      RETURN TAB_TAGS_LIST_WO
   IS
      /********************************************************************************
      \ NEK_W_VIEW_TAGS_LIST_WO - Table type
      ********************************************************************************/
      cVTOn    TAB_TAGS_LIST_WO;
      cVTOnR   REC_TAGS_LIST_WO;

      cCURS    RETDATA;
      iCOUNT   NUMBER (6) := 1;
   BEGIN
      cCURS := NEK_VIEW_TAGS_LIST_WO (WORKORDER_NUM, USER_ID, TIME_STAMP);

      LOOP
         FETCH cCURS
         INTO cVTOnR.TAGOUT_NUMBER,
              cVTOnR.TAGOUT_SECT_NUM,
              cVTOnR.CONFIG,
              cVTOnR.IS_ON_TEMP_LIFT,
              cVTOnR.SERIAL,
              cVTOnR.JOINED_SERIAL,
              cVTOnR.EQUIPMENT,
              cVTOnR.TAG_HUNG,
              cVTOnR.TEMP_LIFT;

         IF cCURS%NOTFOUND
         THEN
            EXIT;
         ELSE
            cVTOn (iCOUNT) := cVTOnR;
            iCOUNT := iCOUNT + 1;
         END IF;
      END LOOP;

      CLOSE cCURS;

      RETURN cVTOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_W_VIEW_TAGS_LIST_WO');
         RETURN cVTOn;
   END NEK_W_VIEW_TAGS_LIST_WO;

   FUNCTION NEK_W_VIEW_TO_SIGN_OFF (
      WONAME_          IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM       IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TAGOUTSECT_NUM   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_ID          IN USERLOG.USER_ID%TYPE,
      TIME_STAMP       IN TIMESTAMP)
      RETURN TAB_TO_SIGN_OFF
   IS
      /********************************************************************************
      \ NEK_W_VIEW_TO_SIGN_OFF - Table type
      ********************************************************************************/
      cVTOn    TAB_TO_SIGN_OFF;
      cVTOnR   REC_TO_SIGN_OFF;

      cCURS    RETDATA;
      iCOUNT   NUMBER (6) := 1;
   BEGIN
      cCURS :=
         NEK_VIEW_TO_SIGN_OFF (WONAME_,
                               TAGOUT_NUM,
                               TAGOUTSECT_NUM,
                               USER_ID,
                               TIME_STAMP);

      LOOP
         FETCH cCURS
         INTO cVTOnR.USER,
              cVTOnR.TAGOUT_NUMBER,
              cVTOnR.TAGOUT_SECT_NUM,
              cVTOnR.RESULT,
              cVTOnR.REASON,
              cVTOnR.TIME_STAMP;

         IF cCURS%NOTFOUND
         THEN
            EXIT;
         ELSE
            cVTOn (iCOUNT) := cVTOnR;
            iCOUNT := iCOUNT + 1;
         END IF;
      END LOOP;

      CLOSE cCURS;

      RETURN cVTOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_W_VIEW_TO_SIGN_OFF');
         RETURN cVTOn;
   END NEK_W_VIEW_TO_SIGN_OFF;

   FUNCTION NEK_W_VIEW_TO_SIGN_OFF (
      WONAME_      IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      USER_ID      IN USERLOG.USER_ID%TYPE,
      TIME_STAMP   IN TIMESTAMP)
      RETURN TAB_TO_SIGN_OFF
   IS
      /********************************************************************************
      \ NEK_W_VIEW_TO_SIGN_OFF - Table type
      ********************************************************************************/
      cVTOn    TAB_TO_SIGN_OFF;
      cVTOnR   REC_TO_SIGN_OFF;

      cCURS    RETDATA;
      iCOUNT   NUMBER (6) := 1;
   BEGIN
      cCURS :=
         NEK_VIEW_TO_SIGN_OFF (WONAME_,
                               TAGOUT_NUM,
                               USER_ID,
                               TIME_STAMP);

      LOOP
         FETCH cCURS
         INTO cVTOnR.USER,
              cVTOnR.TAGOUT_NUMBER,
              cVTOnR.TAGOUT_SECT_NUM,
              cVTOnR.RESULT,
              cVTOnR.REASON,
              cVTOnR.TIME_STAMP;

         IF cCURS%NOTFOUND
         THEN
            EXIT;
         ELSE
            cVTOn (iCOUNT) := cVTOnR;
            iCOUNT := iCOUNT + 1;
         END IF;
      END LOOP;

      CLOSE cCURS;

      RETURN cVTOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_W_VIEW_TO_SIGN_OFF');
         RETURN cVTOn;
   END NEK_W_VIEW_TO_SIGN_OFF;

   FUNCTION NEK_W_VIEW_TO_SIGN_OFF_REASON (
      WONAME_          IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM       IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TAGOUTSECT_NUM   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_ID          IN USERLOG.USER_ID%TYPE,
      TIME_STAMP       IN TIMESTAMP)
      RETURN TAB_TO_SIGN_OFF_REASON
   IS
      /********************************************************************************
      \ NEK_W_VIEW_TO_SIGN_OFF _REASON- Table type
      ********************************************************************************/
      cVTOn    TAB_TO_SIGN_OFF_REASON;
      cVTOnR   REC_TO_SIGN_OFF_REASON;

      cCURS    RETDATA;
      iCOUNT   NUMBER (6) := 1;
      DUMMY    VARCHAR2 (4000);
   BEGIN
      cCURS :=
         NEK_VIEW_TO_SIGN_OFF_REASON (WONAME_,
                                      TAGOUT_NUM,
                                      TAGOUTSECT_NUM,
                                      USER_ID,
                                      TIME_STAMP);

      LOOP
         FETCH cCURS
         INTO DUMMY, cVTOnR.REASON;

         IF cCURS%NOTFOUND
         THEN
            EXIT;
         ELSE
            cVTOn (iCOUNT) := cVTOnR;
            iCOUNT := iCOUNT + 1;
         END IF;
      END LOOP;

      CLOSE cCURS;

      RETURN cVTOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_W_VIEW_TO_SIGN_OFF_REASON');
         RETURN cVTOn;
   END NEK_W_VIEW_TO_SIGN_OFF_REASON;

   FUNCTION NEK_W_VIEW_WO_SIGN_OFF (
      WORKORDER_NUM    IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM       IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TAGOUTSECT_NUM   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_ID          IN USERLOG.USER_ID%TYPE,
      TIME_STAMP       IN TIMESTAMP)
      RETURN TAB_WO_SIGN_OFF
   IS
      /********************************************************************************
      \ NEK_W_VIEW_WO_SIGN_OFF - Table type
      ********************************************************************************/
      cVWOn    TAB_WO_SIGN_OFF;
      cVWOnR   REC_WO_SIGN_OFF;

      cCURS    RETDATA;
      iCOUNT   NUMBER (6) := 1;
   BEGIN
      cCURS :=
         NEK_VIEW_WO_SIGN_OFF (WORKORDER_NUM,
                               TAGOUT_NUM,
                               TAGOUTSECT_NUM,
                               USER_ID,
                               TIME_STAMP);

      LOOP
         FETCH cCURS
         INTO cVWOnR.USER,
              cVWOnR.WORKORDER_NUMBER,
              cVWOnR.TAGOUT_NUMBER,
              cVWOnR.TAGOUT_SECT_NUM,
              cVWOnR.RESULT,
              cVWOnR.REASON,
              cVWOnR.TIME_STAMP;

         IF cCURS%NOTFOUND
         THEN
            EXIT;
         ELSE
            cVWOn (iCOUNT) := cVWOnR;
            iCOUNT := iCOUNT + 1;
         END IF;
      END LOOP;

      CLOSE cCURS;

      RETURN cVWOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_W_VIEW_WO_SIGN_OFF');
         RETURN cVWOn;
   END NEK_W_VIEW_WO_SIGN_OFF;

   FUNCTION NEK_W_VIEW_WO_SIGN_OFF (
      WORKORDER_NUM   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM      IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      USER_ID         IN USERLOG.USER_ID%TYPE,
      TIME_STAMP      IN TIMESTAMP)
      RETURN TAB_WO_SIGN_OFF
   IS
      /********************************************************************************
      \ NEK_W_VIEW_WO_SIGN_OFF - Table type
      ********************************************************************************/
      cVWOn    TAB_WO_SIGN_OFF;
      cVWOnR   REC_WO_SIGN_OFF;

      cCURS    RETDATA;
      iCOUNT   NUMBER (6) := 1;
   BEGIN
      cCURS :=
         NEK_VIEW_WO_SIGN_OFF (WORKORDER_NUM,
                               TAGOUT_NUM,
                               USER_ID,
                               TIME_STAMP);

      LOOP
         FETCH cCURS
         INTO cVWOnR.USER,
              cVWOnR.WORKORDER_NUMBER,
              cVWOnR.TAGOUT_NUMBER,
              cVWOnR.TAGOUT_SECT_NUM,
              cVWOnR.RESULT,
              cVWOnR.REASON,
              cVWOnR.TIME_STAMP;

         IF cCURS%NOTFOUND
         THEN
            EXIT;
         ELSE
            cVWOn (iCOUNT) := cVWOnR;
            iCOUNT := iCOUNT + 1;
         END IF;
      END LOOP;

      CLOSE cCURS;

      RETURN cVWOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_W_VIEW_WO_SIGN_OFF');
         RETURN cVWOn;
   END NEK_W_VIEW_WO_SIGN_OFF;

   FUNCTION NEK_W_VIEW_WO_SIGN_OFF (
      WORKORDER_NUM   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      USER_ID         IN USERLOG.USER_ID%TYPE,
      TIME_STAMP      IN TIMESTAMP)
      RETURN TAB_WO_SIGN_OFF
   IS
      /********************************************************************************
      \ NEK_W_VIEW_WO_SIGN_OFF - Table type
      ********************************************************************************/
      cVWOn    TAB_WO_SIGN_OFF;
      cVWOnR   REC_WO_SIGN_OFF;

      cCURS    RETDATA;
      iCOUNT   NUMBER (6) := 1;
   BEGIN
      cCURS := NEK_VIEW_WO_SIGN_OFF (WORKORDER_NUM, USER_ID, TIME_STAMP);

      LOOP
         FETCH cCURS
         INTO cVWOnR.USER,
              cVWOnR.WORKORDER_NUMBER,
              cVWOnR.TAGOUT_NUMBER,
              cVWOnR.TAGOUT_SECT_NUM,
              cVWOnR.RESULT,
              cVWOnR.REASON,
              cVWOnR.TIME_STAMP;

         IF cCURS%NOTFOUND
         THEN
            EXIT;
         ELSE
            cVWOn (iCOUNT) := cVWOnR;
            iCOUNT := iCOUNT + 1;
         END IF;
      END LOOP;

      CLOSE cCURS;

      RETURN cVWOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_W_VIEW_WO_SIGN_OFF');
         RETURN cVWOn;
   END NEK_W_VIEW_WO_SIGN_OFF;

   FUNCTION NEK_W_VIEW_WO_SIGN_OFF_REASON (
      WORKORDER_NUM    IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM       IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TAGOUTSECT_NUM   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_ID          IN USERLOG.USER_ID%TYPE,
      TIME_STAMP       IN TIMESTAMP)
      RETURN TAB_WO_SIGN_OFF_REASON
   IS
      /********************************************************************************
      \ NEK_W_VIEW_WO_SIGN_OFF - Table type REason only
      ********************************************************************************/
      cVWOn    TAB_WO_SIGN_OFF_REASON;
      cVWOnR   REC_WO_SIGN_OFF_REASON;

      cCURS    RETDATA;
      iCOUNT   NUMBER (6) := 1;
      DUMMY    VARCHAR2 (4000);
   BEGIN
      cCURS :=
         NEK_VIEW_WO_SIGN_OFF_REASON (WORKORDER_NUM,
                                      TAGOUT_NUM,
                                      TAGOUTSECT_NUM,
                                      USER_ID,
                                      TIME_STAMP);

      LOOP
         FETCH cCURS
         INTO DUMMY, cVWOnR.REASON;

         IF cCURS%NOTFOUND
         THEN
            EXIT;
         ELSE
            cVWOn (iCOUNT) := cVWOnR;
            iCOUNT := iCOUNT + 1;
         END IF;
      END LOOP;

      CLOSE cCURS;

      RETURN cVWOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_W_VIEW_WO_SIGN_OFF_REASON');
         RETURN cVWOn;
   END NEK_W_VIEW_WO_SIGN_OFF_REASON;

   /********************************************************************************
   /*
   /* PUBLIC a.k.a. VIEWS for eBS
   *
   *********************************************************************************/
   FUNCTION NEK_VIEW_TO_SIGN_ON_REASON (
      WONAME_          IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM       IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TAGOUTSECT_NUM   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_ID          IN USERLOG.USER_ID%TYPE,
      TIME_STAMP       IN TIMESTAMP)
      RETURN RETDATA
   IS
      /********************************************************************************
      \ NEK_VIEW_TO_SIGN_ON_REASON - When there is no section number - all are evaluated and returned
      ********************************************************************************/
      cVTOn   RETDATA;
   BEGIN
      SET_USER (USER_ID);

      OPEN cVTOn FOR
         SELECT NEK_TO_CAN_SIGN_ON_API (WONAME_,
                                        TAGOUT_NUM,
                                        TAGOUTSECT_NUM,
                                        USER_ID)
                   "RESULT",
                NEK_GET_MESSAGES "REASON"
           FROM DUAL;

      RETURN cVTOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_VIEW_TO_SIGN_ON_REASON');
         RETURN cVTOn;
   END NEK_VIEW_TO_SIGN_ON_REASON;

   FUNCTION NEK_VIEW_TO_SIGN_ON (
      WONAME_      IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      USER_ID      IN USERLOG.USER_ID%TYPE,
      TIME_STAMP   IN TIMESTAMP)
      RETURN RETDATA
   IS
      /********************************************************************************
      \ NEK_VIEW_TO_SIGN_ON(T2.ACT_TAGOUT_NUMBER, T2.ACT_TAGOUT_SECTION_NUMBER) - When there is no section number - all are evaluated and returned
      ********************************************************************************/
      cVTOn   RETDATA;
   BEGIN
      SET_USER (USER_ID);

      OPEN cVTOn FOR
         SELECT NEK_GET_USERS_ON_TO (WONAME_,
                                     TAGOUT_NUM,
                                     T1.ACT_TAGOUT_SECTION_NUMBER)
                   "USER",
                TAGOUT_NUM "TAGOUT_NUMBER",
                T1.ACT_TAGOUT_SECTION_NUMBER "TAGOUTSECT_NUM",
                NEK_TO_CAN_SIGN_ON_API (WONAME_,
                                        TAGOUT_NUM,
                                        T1.ACT_TAGOUT_SECTION_NUMBER,
                                        USER_ID)
                   "RESULT",
                NEK_GET_MESSAGES "REASON",
                TIME_STAMP "TIME"
           FROM    NEK_SIGN_TIMES T2
                INNER JOIN
                   ACT_TAGOUT_SECTIONS T1
                ON     T1.ACT_TAGOUT_NUMBER = T2.TAG_OUT_ID
                   AND T1.ACT_TAGOUT_SECTION_NUMBER = T2.TAG_OUT_NUMB_ID
          WHERE     T2.WO_NUMBER = WONAME_
                AND T2.TAG_OUT_ID = TAGOUT_NUM
                AND T2.TAG_OUT_NUMB_ID = T1.ACT_TAGOUT_SECTION_NUMBER
                AND T1.TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(TAGOUT_NUM) --CHECKED_FOLDER_TYPE_
                AND T1.ARCHIVED = 0;

      RETURN cVTOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_VIEW_TO_SIGN_ON');
         RETURN cVTOn;
   END NEK_VIEW_TO_SIGN_ON;

   FUNCTION NEK_VIEW_TO_SIGN_ON (
      WONAME_          IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM       IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TAGOUTSECT_NUM   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_ID          IN USERLOG.USER_ID%TYPE,
      TIME_STAMP       IN TIMESTAMP)
      RETURN RETDATA
   IS
      /********************************************************************************
      \ NEK_VIEW_TO_SIGN_ON - Passed all IDs
      ********************************************************************************/
      cVTOn     RETDATA;
      myUSERS   VARCHAR2 (150);
   BEGIN
      SET_USER (USER_ID);

      myUSERS := NEK_GET_USERS_ON_TO (WONAME_, TAGOUT_NUM, TAGOUTSECT_NUM);

      OPEN cVTOn FOR
         SELECT NEK_GET_USERS_ON_TO (WONAME_, TAGOUT_NUM, TAGOUTSECT_NUM)
                   "USER",
                TAGOUT_NUM "TAGOUT_NUMBER",
                TAGOUTSECT_NUM "TAGOUTSECT_NUM",
                NEK_TO_CAN_SIGN_ON_API (WONAME_,
                                        TAGOUT_NUM,
                                        TAGOUTSECT_NUM,
                                        USER_ID)
                   "RESULT",
                NEK_GET_MESSAGES "REASON",
                TIME_STAMP "TIME"
           FROM ACT_TAGOUT_SECTIONS T1
          WHERE     T1.ACT_TAGOUT_NUMBER = TAGOUT_NUM
                AND T1.ACT_TAGOUT_SECTION_NUMBER = TAGOUTSECT_NUM
                AND T1.TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(TAGOUT_NUM)--CHECKED_FOLDER_TYPE_
                AND T1.ARCHIVED = 0;

      RETURN cVTOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_VIEW_TO_SIGN_ON');
         RETURN cVTOn;
   END NEK_VIEW_TO_SIGN_ON;

   FUNCTION NEK_VIEW_WO_SIGN_ON_REASON (
      WORKORDER_NUM    IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM       IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TAGOUTSECT_NUM   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_ID          IN USERLOG.USER_ID%TYPE,
      TIME_STAMP       IN TIMESTAMP)
      RETURN RETDATA
   IS
      /********************************************************************************
      \ NEK_VIEW_WO_SIGN_ON_REASON - Only reason is returned, result is dummy but needed for eval
      ********************************************************************************/
      cVWOn   RETDATA;
   BEGIN
      SET_USER (USER_ID);

      OPEN cVWOn FOR
         SELECT NEK_WO_CAN_SIGN_ON_API (WORKORDER_NUM,
                                        TAGOUT_NUM,
                                        TAGOUTSECT_NUM,
                                        USER_ID)
                   "RESULT",
                NEK_GET_MESSAGES "REASON"
           FROM DUAL;

      RETURN cVWOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_VIEW_WO_SIGN_ON_REASON');
         RETURN cVWOn;
   END NEK_VIEW_WO_SIGN_ON_REASON;

   FUNCTION NEK_VIEW_WO_SIGN_ON (
      WORKORDER_NUM    IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM       IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TAGOUTSECT_NUM   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_ID          IN USERLOG.USER_ID%TYPE,
      TIME_STAMP       IN TIMESTAMP)
      RETURN RETDATA
   IS
      /********************************************************************************
      \ NEK_VIEW_WO_SIGN_ON - All IDs sent
      ********************************************************************************/
      cVWOn   RETDATA;
   BEGIN
      SET_USER (USER_ID);

      OPEN cVWOn FOR
         SELECT USER_ID "USER",
                WORKORDER_NUM "WORKORDER_NUMBER",
                TAGOUT_NUM "TAGOUT_NUMBER",
                TAGOUTSECT_NUM "TAGOUTSECT_NUM",
                NEK_WO_CAN_SIGN_ON_API (WORKORDER_NUM,
                                        TAGOUT_NUM,
                                        TAGOUTSECT_NUM,
                                        USER_ID)
                   "RESULT",
                NEK_GET_MESSAGES "REASON",
                TIME_STAMP "TIME"
           FROM DUAL;

      RETURN cVWOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_VIEW_WO_SIGN_ON');
         RETURN cVWOn;
   END NEK_VIEW_WO_SIGN_ON;

   FUNCTION NEK_VIEW_WO_SIGN_ON (
      WORKORDER_NUM   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM      IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      USER_ID         IN USERLOG.USER_ID%TYPE,
      TIME_STAMP      IN TIMESTAMP)
      RETURN RETDATA
   IS
      /********************************************************************************
      \ NEK_VIEW_WO_SIGN_ON - Without Tagout section
      ********************************************************************************/
      cVWOn   RETDATA;
   BEGIN
      SET_USER (USER_ID);

      OPEN cVWOn FOR
         SELECT USER_ID "USER",
                WORKORDER_NUM "WORKORDER_NUMBER",
                TAGOUT_NUM "TAGOUT_NUMBER",
                ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_SECTION_NUMBER
                   "TAGOUTSECT_NUM",
                NEK_WO_CAN_SIGN_ON_API (
                   WORKORDER_NUM,
                   TAGOUT_NUM,
                   ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_SECTION_NUMBER,
                   USER_ID)
                   "RESULT",
                NEK_GET_MESSAGES "REASON",
                TIME_STAMP "TIME"
           FROM DUAL, ACT_TAGOUT_SECTION_WORK_ORDERS
          WHERE     ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER =
                       WORKORDER_NUM
                AND ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_NUMBER =
                       TAGOUT_NUM
                AND ACT_TAGOUT_SECTION_WORK_ORDERS.TAGOUT_TYPE_ID =
                       NEK_CHECK_FOLDER_TYPE(TAGOUT_NUM);--CHECKED_FOLDER_TYPE_;

      RETURN cVWOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_VIEW_WO_SIGN_ON');
         RETURN cVWOn;
   END NEK_VIEW_WO_SIGN_ON;

   FUNCTION NEK_VIEW_WO_SIGN_ON (
      WORKORDER_NUM   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      USER_ID         IN USERLOG.USER_ID%TYPE,
      TIME_STAMP      IN TIMESTAMP)
      RETURN RETDATA
   IS
      /********************************************************************************
      \ NEK_VIEW_WO_SIGN_ON - Without Tagout section and Tagout number
      ********************************************************************************/
      cVWOn   RETDATA;
   BEGIN
      SET_USER (USER_ID);

      OPEN cVWOn FOR
         SELECT USER_ID "USER",
                WORKORDER_NUM "WORKORDER_NUMBER",
                ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_NUMBER
                   "TAGOUT_NUMBER",
                ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_SECTION_NUMBER
                   "TAGOUTSECT_NUM",
                NEK_WO_CAN_SIGN_ON_API (
                   WORKORDER_NUM,
                   ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_NUMBER,
                   ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_SECTION_NUMBER,
                   USER_ID)
                   "RESULT",
                NEK_GET_MESSAGES "REASON",
                TIME_STAMP "TIME"
           FROM DUAL, ACT_TAGOUT_SECTION_WORK_ORDERS
          WHERE     ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER =
                       WORKORDER_NUM
                AND ACT_TAGOUT_SECTION_WORK_ORDERS.TAGOUT_TYPE_ID =
                       NEK_CHECK_FOLDER_TYPE(ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_NUMBER); --CHECKED_FOLDER_TYPE_;

      RETURN cVWOn;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_VIEW_WO_SIGN_ON');
         RETURN cVWOn;
   END NEK_VIEW_WO_SIGN_ON;

   FUNCTION NEK_GET_USERS_ON_TO (
      WONAME_   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE)
      RETURN VARCHAR2
   IS
      /********************************************************************************
      \ NEK_GET_USERS_ON_TO - checks if the selected Tag out has a holder or not
      ********************************************************************************/
      CURSOR cTO_HASHOLDER
      IS
         SELECT ASSIGNED_USER
           FROM NEK_SIGN_TIMES
          WHERE     WO_NUMBER = WONAME_
                AND TAG_OUT_ID = FONAME_
                AND TAG_OUT_NUMB_ID = TONAME_;

      cUSER      ACT_TAGOUT_SECTION_HOLDERS.USER_ID%TYPE;
      USER_RES   VARCHAR2 (2000);
   BEGIN
      OPEN cTO_HASHOLDER;


      LOOP
         FETCH cTO_HASHOLDER INTO cUSER;

         IF cTO_HASHOLDER%NOTFOUND
         THEN
            RETURN USER_RES;
         ELSE
            IF USER_RES IS NULL
            THEN
               USER_RES := TRIM (cUSER);
            ELSE
               USER_RES := USER_RES || ', ' || TRIM (cUSER);
            END IF;
         END IF;
      END LOOP;

      RETURN USER_RES;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_GET_USERS_ON_TO');
         RETURN '';
   END NEK_GET_USERS_ON_TO;

   FUNCTION NEK_GET_USERS_ON_WO (
      WONAME_ IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE)
      RETURN VARCHAR2
   IS
      /********************************************************************************
      \ NEK_GET_USERS_ON_WO
      ********************************************************************************/
      CURSOR cTO_HASHOLDER
      IS
         SELECT USER_ID
           FROM ACT_WORK_ORDER_HOLDERS
          WHERE     WORK_ORDER_NUMBER = WONAME_
                AND SIGN_OFF_DATE IS NULL
                AND SIGN_OFF_BY IS NULL
                AND WORK_COMPLETE IS NULL;

      cUSER      ACT_WORK_ORDER_HOLDERS.USER_ID%TYPE;
      USER_RES   VARCHAR2 (2000);
   BEGIN
      OPEN cTO_HASHOLDER;

      LOOP
         FETCH cTO_HASHOLDER INTO cUSER;

         IF cTO_HASHOLDER%NOTFOUND
         THEN
            RETURN USER_RES;
         ELSE
            IF USER_RES IS NULL
            THEN
               USER_RES := TRIM (cUSER);
            ELSE
               USER_RES := USER_RES || ', ' || TRIM (cUSER);
            END IF;
         END IF;
      END LOOP;

      RETURN USER_RES;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_GET_USERS_ON_WO');
         RETURN '';
   END NEK_GET_USERS_ON_WO;

   FUNCTION NEK_GET_USERS_ON_WO (
      WONAME_   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE)
      RETURN VARCHAR2
   IS
      /********************************************************************************
      \ NEK_GET_USERS_ON_WO -
      ********************************************************************************/
      CURSOR cTO_HASHOLDER
      IS
         SELECT USER_ID
           FROM ACT_WORK_ORDER_HOLDERS
          WHERE     WORK_ORDER_NUMBER = WONAME_
                AND SIGN_OFF_DATE IS NULL
                AND SIGN_OFF_BY IS NULL
                AND ACT_TAGOUT_NUMBER = FONAME_
                AND ACT_TAGOUT_SECTION_NUMBER = TONAME_
                AND WORK_COMPLETE IS NULL;

      cUSER      ACT_WORK_ORDER_HOLDERS.USER_ID%TYPE;
      USER_RES   VARCHAR2 (2000);
   BEGIN
      OPEN cTO_HASHOLDER;

      LOOP
         FETCH cTO_HASHOLDER INTO cUSER;

         IF cTO_HASHOLDER%NOTFOUND
         THEN
            RETURN USER_RES;
         ELSE
            IF USER_RES IS NULL
            THEN
               USER_RES := TRIM (cUSER);
            ELSE
               USER_RES := USER_RES || ', ' || TRIM (cUSER);
            END IF;
         END IF;
      END LOOP;

      RETURN USER_RES;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_GET_USERS_ON_WO');
         RETURN '';
   END NEK_GET_USERS_ON_WO;

   FUNCTION NEK_GET_USERS_ON_WO (
      WONAME_   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE)
      RETURN VARCHAR2
   IS
      /********************************************************************************
      \ NEK_GET_USERS_ON_WO -
      ********************************************************************************/
      CURSOR cTO_HASHOLDER
      IS
         SELECT USER_ID
           FROM ACT_WORK_ORDER_HOLDERS
          WHERE     WORK_ORDER_NUMBER = WONAME_
                AND SIGN_OFF_DATE IS NULL
                AND SIGN_OFF_BY IS NULL
                AND ACT_TAGOUT_NUMBER = FONAME_
                AND WORK_COMPLETE IS NULL;

      cUSER      ACT_WORK_ORDER_HOLDERS.USER_ID%TYPE;
      USER_RES   VARCHAR2 (2000);
   BEGIN
      OPEN cTO_HASHOLDER;

      LOOP
         FETCH cTO_HASHOLDER INTO cUSER;

         IF cTO_HASHOLDER%NOTFOUND
         THEN
            RETURN USER_RES;
         ELSE
            IF USER_RES IS NULL
            THEN
               USER_RES := TRIM (cUSER);
            ELSE
               USER_RES := USER_RES || ', ' || TRIM (cUSER);
            END IF;
         END IF;
      END LOOP;

      RETURN USER_RES;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_GET_USERS_ON_WO');
         RETURN '';
   END NEK_GET_USERS_ON_WO;

   FUNCTION NEK_VIEW_WO_LIST_TO (
      WORKORDER_NUM   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      USER_ID         IN USERLOG.USER_ID%TYPE,
      TIME_STAMP      IN TIMESTAMP)
      RETURN RETDATA
   IS
      /********************************************************************************
      \ NEK_VIEW_WO_LIST_TO
      ********************************************************************************/
      cVWOLTO   RETDATA;
   BEGIN
      OPEN cVWOLTO FOR
           SELECT T2.ACT_TAGOUT_NUMBER "TAGOUT_NUMBER",
                  T2.ACT_TAGOUT_SECTION_NUMBER "TAGOUTSECT_NUM",
                  T1.ACT_TAGOUT_SECTION_DESCRIPTION "DESCRIPTION",
                  T1.ACT_TAGOUT_SECTION_HAZARDS "HAZARDS",
                  NEK_GET_USERS_ON_TO (WORKORDER_NUM,
                                       T2.ACT_TAGOUT_NUMBER,
                                       T2.ACT_TAGOUT_SECTION_NUMBER)
                     "USER",
                  TIME_STAMP "TIME",
                  NEK_GET_TO_VERIF_DESCRIPTION (T2.ACT_TAGOUT_NUMBER,
                                                T2.ACT_TAGOUT_SECTION_NUMBER)
                     "STATUS",
                  NEK_TO_VERIFICATOR (T2.ACT_TAGOUT_NUMBER,
                                      T2.ACT_TAGOUT_SECTION_NUMBER)
                     "PREPARER"
             --NEK_VERIF_TAGS_API_STRING(T2.ACT_TAGOUT_NUMBER, T2.ACT_TAGOUT_SECTION_NUMBER) "PREPARER"
             FROM    ACT_TAGOUT_SECTION_WORK_ORDERS T2
                  INNER JOIN
                     ACT_TAGOUT_SECTIONS T1
                  ON     T1.ACT_TAGOUT_NUMBER = T2.ACT_TAGOUT_NUMBER
                     AND T1.ACT_TAGOUT_SECTION_NUMBER =
                            T2.ACT_TAGOUT_SECTION_NUMBER,
                  WORK_ORDERS
            WHERE     T2.WORK_ORDER_NUMBER = WORKORDER_NUM
                  AND T1.TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(T2.ACT_TAGOUT_NUMBER) --CHECKED_FOLDER_TYPE_
                  AND T1.ARCHIVED = 0
                  AND WORK_ORDERS.WORK_ORDER_NUMBER = WORKORDER_NUM
                  AND NEK_TO_VERIF_EX_INDEX_OK_S (T2.ACT_TAGOUT_NUMBER,
                                                  T2.ACT_TAGOUT_SECTION_NUMBER) =
                         'true'
         ORDER BY T2.ACT_TAGOUT_NUMBER, T2.ACT_TAGOUT_SECTION_NUMBER;

      RETURN cVWOLTO;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_VIEW_WO_LIST_TO');
         RETURN cVWOLTO;
   END NEK_VIEW_WO_LIST_TO;

   FUNCTION NEK_TO_VERIF_EX_INDEX_OK_S (
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE)
      RETURN VARCHAR2
   IS
   BEGIN
      IF NEK_TO_VERIF_EX_INDEX_OK (FONAME_, TONAME_, TAG_VERIF_BEGIN_VAL) = TRUE
      THEN
         RETURN 'true';
      ELSE
         RETURN 'false';
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_TO_VERIF_EX_INDEX_OK_S');
         RETURN 'false';
   END NEK_TO_VERIF_EX_INDEX_OK_S;

   FUNCTION NEK_GET_TO_VERIF_DESCRIPTION (FONAME_   IN VARCHAR2,
                                          TONAME_   IN VARCHAR2)
      RETURN VARCHAR2
   IS
      /********************************************************************************
      \ NEK_GET_TO_VERIF_DESCRIPTION - Get TO verification description from number
      ********************************************************************************/
      CURSOR cFINVERIF
      IS
         SELECT MAX (SECTION_VERIF_LEVEL)
           FROM ACT_TAGOUT_SECTION_VERIF
          WHERE     ACT_TAGOUT_NUMBER = FONAME_
                AND ACT_TAGOUT_SECTION_NUMBER = TONAME_;

      vFINVERIF   NUMBER (9) := 0;

      CURSOR cFINVERIFDESC
      IS
         SELECT SECTION_LEVEL_DESC
           FROM SECTION_VERIF_LEVELS
          WHERE     TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(FONAME_)--CHECKED_FOLDER_TYPE_
                AND SECTION_VERIF_LEVEL = vFINVERIF;

      vDESC       SECTION_VERIF_LEVELS.SECTION_LEVEL_DESC%TYPE;
   BEGIN
      OPEN cFINVERIF;

      FETCH cFINVERIF INTO vFINVERIF;

      CLOSE cFINVERIF;

      IF vFINVERIF = 0
      THEN
         RETURN 'None';
      ELSE
         OPEN cFINVERIFDESC;

         FETCH cFINVERIFDESC INTO vDESC;

         CLOSE cFINVERIFDESC;

         RETURN vDESC;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_GET_TO_VERIF_DESCRIPTION');
         RETURN 'ERR';
   END NEK_GET_TO_VERIF_DESCRIPTION;

   FUNCTION NEK_VIEW_TAGS_LIST_WO (
      WORKORDER_NUM    IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM       IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TAGOUTSECT_NUM   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_ID          IN USERLOG.USER_ID%TYPE,
      TIME_STAMP       IN TIMESTAMP)
      RETURN RETDATA
   IS
      /********************************************************************************
      \ NEK_VIEW_TAGS_LIST_WO - All parameters
      ********************************************************************************/
      cVTOl   RETDATA;
   BEGIN
      OPEN cVTOl FOR
           SELECT T2.ACT_TAGOUT_NUMBER "TAGOUT_NUMBER",
                  T2.ACT_TAGOUT_SECTION_NUMBER "TAGOUT_SECTION_NUMBER",
                  T2.PLACEMENT_CONFIG "CONFIG",
                  IS_TAG_ON_TLIFT (T2.ACT_TAGOUT_NUMBER,
                                   TO_CHAR (T2.TAG_SERIAL_NUMBER))
                     "IS_ON_TEMPORAL_LIFT",
                  T2.TAG_SERIAL_NUMBER "SERIAL",
                  T2.ACT_TAGOUT_NUMBER || '/' || TO_CHAR (T2.TAG_SERIAL_NUMBER)
                     "JOINED_SERIAL",
                  T2.EQUIP_OPERATOR_ID "EQUIPMENT_ID",
                  IS_TAG_HUNG (T2.ACT_TAGOUT_NUMBER,
                               T2.ACT_TAGOUT_SECTION_NUMBER,
                               TO_CHAR (T2.TAG_SERIAL_NUMBER))
                     "TAG_HUNG",
                  TAG_TLIFT_NUMBER (T2.ACT_TAGOUT_NUMBER,
                                    TO_CHAR (T2.TAG_SERIAL_NUMBER))
                     "TEMPORAL_LIFT_NUMBER"
             FROM    ACT_TAGOUT_SECTION_WORK_ORDERS T1
                  INNER JOIN
                     ACT_TAGOUT_SECTION_TAGS T2
                  ON     T1.ACT_TAGOUT_NUMBER = T2.ACT_TAGOUT_NUMBER
                     AND T1.ACT_TAGOUT_SECTION_NUMBER =
                            T2.ACT_TAGOUT_SECTION_NUMBER,
                  ACT_TAGOUT_SECTIONS T3
            WHERE     T3.ARCHIVED = 0
                  AND T1.TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(TAGOUT_NUM) --CHECKED_FOLDER_TYPE_
                  AND T1.WORK_ORDER_NUMBER = WORKORDER_NUM
                  AND T2.ACT_TAGOUT_NUMBER = TAGOUT_NUM
                  AND T2.ACT_TAGOUT_SECTION_NUMBER = TAGOUTSECT_NUM
                  AND T3.ACT_TAGOUT_NUMBER = TAGOUT_NUM
                  AND T3.ACT_TAGOUT_SECTION_NUMBER = TAGOUTSECT_NUM
         ORDER BY T2.ACT_TAGOUT_NUMBER,
                  T2.ACT_TAGOUT_SECTION_NUMBER,
                  T2.TAG_SERIAL_NUMBER;

      RETURN cVTOl;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_VIEW_TAGS_LIST_WO');
         RETURN cVTOl;
   END NEK_VIEW_TAGS_LIST_WO;

   FUNCTION NEK_VIEW_TAGS_LIST_WO (
      WORKORDER_NUM   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM      IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      USER_ID         IN USERLOG.USER_ID%TYPE,
      TIME_STAMP      IN TIMESTAMP)
      RETURN RETDATA
   IS
      /********************************************************************************
      \ NEK_VIEW_TAGS_LIST_WO - Not provided with section number
      ********************************************************************************/
      cVTOl   RETDATA;
   BEGIN
      OPEN cVTOl FOR
           SELECT T2.ACT_TAGOUT_NUMBER "TAGOUT_NUMBER",
                  T2.ACT_TAGOUT_SECTION_NUMBER "TAGOUT_SECTION_NUMBER",
                  T2.PLACEMENT_CONFIG "CONFIG",
                  IS_TAG_ON_TLIFT (T2.ACT_TAGOUT_NUMBER,
                                   TO_CHAR (T2.TAG_SERIAL_NUMBER))
                     "IS_ON_TEMPORAL_LIFT",
                  T2.TAG_SERIAL_NUMBER "SERIAL",
                  T2.ACT_TAGOUT_NUMBER || '/' || TO_CHAR (T2.TAG_SERIAL_NUMBER)
                     "JOINED_SERIAL",
                  T2.EQUIP_OPERATOR_ID "EQUIPMENT_ID",
                  IS_TAG_HUNG (T2.ACT_TAGOUT_NUMBER,
                               T2.ACT_TAGOUT_SECTION_NUMBER,
                               TO_CHAR (T2.TAG_SERIAL_NUMBER))
                     "TAG_HUNG",
                  TAG_TLIFT_NUMBER (T2.ACT_TAGOUT_NUMBER,
                                    TO_CHAR (T2.TAG_SERIAL_NUMBER))
                     "TEMPORAL_LIFT_NUMBER"
             FROM ACT_TAGOUT_SECTION_WORK_ORDERS T1
                  INNER JOIN ACT_TAGOUT_SECTION_TAGS T2
                     ON     T1.ACT_TAGOUT_NUMBER = T2.ACT_TAGOUT_NUMBER
                        AND T1.ACT_TAGOUT_SECTION_NUMBER =
                               T2.ACT_TAGOUT_SECTION_NUMBER
                  INNER JOIN ACT_TAGOUT_SECTIONS T3
                     ON     T1.ACT_TAGOUT_NUMBER = T3.ACT_TAGOUT_NUMBER
                        AND T1.ACT_TAGOUT_SECTION_NUMBER =
                               T3.ACT_TAGOUT_SECTION_NUMBER
                        AND 0 = T3.ARCHIVED
            WHERE     T1.TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(TAGOUT_NUM)--CHECKED_FOLDER_TYPE_
                  AND T1.WORK_ORDER_NUMBER = WORKORDER_NUM
                  AND T2.ACT_TAGOUT_NUMBER = TAGOUT_NUM
         ORDER BY T2.ACT_TAGOUT_NUMBER,
                  T2.ACT_TAGOUT_SECTION_NUMBER,
                  T2.TAG_SERIAL_NUMBER;

      RETURN cVTOl;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_VIEW_TAGS_LIST_WO');
         RETURN cVTOl;
   END NEK_VIEW_TAGS_LIST_WO;

   FUNCTION NEK_VIEW_TAGS_LIST_WO (
      WORKORDER_NUM   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      USER_ID         IN USERLOG.USER_ID%TYPE,
      TIME_STAMP      IN TIMESTAMP)
      RETURN RETDATA
   IS
      /********************************************************************************
      \ NEK_VIEW_TAGS_LIST_WO - Not provided with section number and tagout number
      ********************************************************************************/
      cVTOl   RETDATA;
   BEGIN
      OPEN cVTOl FOR
           SELECT T2.ACT_TAGOUT_NUMBER "TAGOUT_NUMBER",
                  T2.ACT_TAGOUT_SECTION_NUMBER "TAGOUT_SECTION_NUMBER",
                  T2.PLACEMENT_CONFIG "CONFIG",
                  IS_TAG_ON_TLIFT (T2.ACT_TAGOUT_NUMBER,
                                   TO_CHAR (T2.TAG_SERIAL_NUMBER))
                     "IS_ON_TEMPORAL_LIFT",
                  T2.TAG_SERIAL_NUMBER "SERIAL",
                  T2.ACT_TAGOUT_NUMBER || '/' || TO_CHAR (T2.TAG_SERIAL_NUMBER)
                     "JOINED_SERIAL",
                  T2.EQUIP_OPERATOR_ID "EQUIPMENT_ID",
                  IS_TAG_HUNG (T2.ACT_TAGOUT_NUMBER,
                               T2.ACT_TAGOUT_SECTION_NUMBER,
                               TO_CHAR (T2.TAG_SERIAL_NUMBER))
                     "TAG_HUNG",
                  TAG_TLIFT_NUMBER (T2.ACT_TAGOUT_NUMBER,
                                    TO_CHAR (T2.TAG_SERIAL_NUMBER))
                     "TEMPORAL_LIFT_NUMBER"
             FROM ACT_TAGOUT_SECTION_WORK_ORDERS T1
                  INNER JOIN ACT_TAGOUT_SECTION_TAGS T2
                     ON     T1.ACT_TAGOUT_NUMBER = T2.ACT_TAGOUT_NUMBER
                        AND T1.ACT_TAGOUT_SECTION_NUMBER =
                               T2.ACT_TAGOUT_SECTION_NUMBER
                  INNER JOIN ACT_TAGOUT_SECTIONS T3
                     ON     T1.ACT_TAGOUT_NUMBER = T3.ACT_TAGOUT_NUMBER
                        AND T1.ACT_TAGOUT_SECTION_NUMBER =
                               T3.ACT_TAGOUT_SECTION_NUMBER
                        AND 0 = T3.ARCHIVED
            WHERE     T1.TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(T2.ACT_TAGOUT_NUMBER) --CHECKED_FOLDER_TYPE_
                  AND T1.WORK_ORDER_NUMBER = WORKORDER_NUM
         ORDER BY T2.ACT_TAGOUT_NUMBER,
                  T2.ACT_TAGOUT_SECTION_NUMBER,
                  T2.TAG_SERIAL_NUMBER;

      RETURN cVTOl;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_VIEW_TAGS_LIST_WO');
         RETURN cVTOl;
   END NEK_VIEW_TAGS_LIST_WO;

   FUNCTION NEK_VIEW_TO_SIGN_OFF_REASON (
      WONAME_          IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM       IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TAGOUTSECT_NUM   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_ID          IN USERLOG.USER_ID%TYPE,
      TIME_STAMP       IN TIMESTAMP)
      RETURN RETDATA
   IS
      /********************************************************************************
      \ NEK_VIEW_TO_SIGN_OFF_REASON
      ********************************************************************************/
      cVTOf   RETDATA;
      CUID    VARCHAR2 (50);
   BEGIN
      CUID := USER_ID;
      SET_USER (USER_ID);

      --  DBMS_OUTPUT.PUT_LINE('EXECUTING NEK_TO_CAN_SIGN_OFF_API');

      OPEN cVTOf FOR
         SELECT NEK_TO_CAN_SIGN_OFF_API (WONAME_,
                                         TAGOUT_NUM,
                                         TAGOUTSECT_NUM,
                                         CUID)
                   "RESULT",
                NEK_GET_MESSAGES "REASON"
           FROM DUAL;

      RETURN cVTOf;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_VIEW_TO_SIGN_OFF_REASON');
         RETURN cVTOf;
   END NEK_VIEW_TO_SIGN_OFF_REASON;

   FUNCTION NEK_VIEW_TO_SIGN_OFF (
      WONAME_          IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM       IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TAGOUTSECT_NUM   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_ID          IN USERLOG.USER_ID%TYPE,
      TIME_STAMP       IN TIMESTAMP)
      RETURN RETDATA
   IS
      /********************************************************************************
      \ NEK_VIEW_TO_SIGN_OFF
      ********************************************************************************/
      cVTOf   RETDATA;
   BEGIN
      SET_USER (USER_ID);

      OPEN cVTOf FOR
         SELECT USER_ID "USER",
                TAGOUT_NUM "TAGOUT_NUMBER",
                TAGOUTSECT_NUM "TAGOUTSECT_NUM",
                NEK_TO_CAN_SIGN_OFF_API (WONAME_,
                                         TAGOUT_NUM,
                                         TAGOUTSECT_NUM,
                                         USER_ID)
                   "RESULT",
                NEK_GET_MESSAGES "REASON",
                TIME_STAMP "TIME"
           FROM DUAL;

      RETURN cVTOf;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_VIEW_TO_SIGN_OFF');
         RETURN cVTOf;
   END NEK_VIEW_TO_SIGN_OFF;

   FUNCTION NEK_VIEW_TO_SIGN_OFF (
      WONAME_      IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      USER_ID      IN USERLOG.USER_ID%TYPE,
      TIME_STAMP   IN TIMESTAMP)
      RETURN RETDATA
   IS
      /********************************************************************************
      \ NEK_VIEW_TO_SIGN_OFF
      ********************************************************************************/
      cVTOf   RETDATA;
   BEGIN
      SET_USER (USER_ID);

      OPEN cVTOf FOR
         SELECT USER_ID "USER",
                TAGOUT_NUM "TAGOUT_NUMBER",
                ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER
                   "TAGOUTSECT_NUM",
                NEK_TO_CAN_SIGN_OFF_API (
                   WONAME_,
                   TAGOUT_NUM,
                   ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER,
                   USER_ID)
                   "RESULT",
                NEK_GET_MESSAGES "REASON",
                TIME_STAMP "TIME"
           FROM DUAL, ACT_TAGOUT_SECTIONS
          WHERE     ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER = TAGOUT_NUM
                AND ACT_TAGOUT_SECTIONS.TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(TAGOUT_NUM) --CHECKED_FOLDER_TYPE_
                AND ACT_TAGOUT_SECTIONS.ARCHIVED = 0;

      RETURN cVTOf;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_VIEW_TO_SIGN_OFF');
         RETURN cVTOf;
   END NEK_VIEW_TO_SIGN_OFF;

   FUNCTION NEK_VIEW_WO_SIGN_OFF_REASON (
      WORKORDER_NUM    IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM       IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TAGOUTSECT_NUM   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_ID          IN USERLOG.USER_ID%TYPE,
      TIME_STAMP       IN TIMESTAMP)
      RETURN RETDATA
   IS
      /********************************************************************************
      \ NEK_VIEW_WO_SIGN_OFF_REASON
      ********************************************************************************/
      cVWOf   RETDATA;
   BEGIN
      SET_USER (USER_ID);

      OPEN cVWOf FOR
         SELECT NEK_WO_CAN_SIGN_OFF_API (WORKORDER_NUM,
                                         TAGOUT_NUM,
                                         TAGOUTSECT_NUM,
                                         USER_ID)
                   "RESULT",
                NEK_GET_MESSAGES "REASON"
           FROM DUAL;

      RETURN cVWOf;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_VIEW_WO_SIGN_OFF_REASON');
         RETURN cVWOf;
   END NEK_VIEW_WO_SIGN_OFF_REASON;

   FUNCTION NEK_VIEW_WO_SIGN_OFF (
      WORKORDER_NUM    IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM       IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TAGOUTSECT_NUM   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_ID          IN USERLOG.USER_ID%TYPE,
      TIME_STAMP       IN TIMESTAMP)
      RETURN RETDATA
   IS
      /********************************************************************************
      \ NEK_VIEW_WO_SIGN_OFF
      ********************************************************************************/
      cVWOf   RETDATA;
   BEGIN
      SET_USER (USER_ID);

      OPEN cVWOf FOR
         SELECT USER_ID "USER",
                WORKORDER_NUM "WORKORDER_NUMBER",
                TAGOUT_NUM "TAGOUT_NUMBER",
                TAGOUTSECT_NUM "TAGOUTSECT_NUM",
                NEK_WO_CAN_SIGN_OFF_API (WORKORDER_NUM,
                                         TAGOUT_NUM,
                                         TAGOUTSECT_NUM,
                                         USER_ID)
                   "RESULT",
                NEK_GET_MESSAGES "REASON",
                TIME_STAMP "TIME"
           FROM DUAL;

      RETURN cVWOf;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_VIEW_WO_SIGN_OFF');
         RETURN cVWOf;
   END NEK_VIEW_WO_SIGN_OFF;

   FUNCTION NEK_VIEW_WO_SIGN_OFF (
      WORKORDER_NUM   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      TAGOUT_NUM      IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      USER_ID         IN USERLOG.USER_ID%TYPE,
      TIME_STAMP      IN TIMESTAMP)
      RETURN RETDATA
   IS
      /********************************************************************************
      \ NEK_VIEW_WO_SIGN_OFF
      ********************************************************************************/
      cVWOf   RETDATA;
   BEGIN
      SET_USER (USER_ID);

      OPEN cVWOf FOR
         SELECT USER_ID "USER",
                WORKORDER_NUM "WORKORDER_NUMBER",
                TAGOUT_NUM "TAGOUT_NUMBER",
                ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_SECTION_NUMBER
                   "TAGOUTSECT_NUM",
                NEK_WO_CAN_SIGN_OFF_API (
                   WORKORDER_NUM,
                   TAGOUT_NUM,
                   ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_SECTION_NUMBER,
                   USER_ID)
                   "RESULT",
                NEK_GET_MESSAGES "REASON",
                TIME_STAMP "TIME"
           FROM DUAL, ACT_TAGOUT_SECTION_WORK_ORDERS
          WHERE     ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER =
                       WORKORDER_NUM
                AND ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_NUMBER =
                       TAGOUT_NUM
                AND ACT_TAGOUT_SECTION_WORK_ORDERS.TAGOUT_TYPE_ID =
                       NEK_CHECK_FOLDER_TYPE(TAGOUT_NUM);--CHECKED_FOLDER_TYPE_;

      RETURN cVWOf;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_VIEW_WO_SIGN_OFF');
         RETURN cVWOf;
   END NEK_VIEW_WO_SIGN_OFF;

   FUNCTION NEK_VIEW_WO_SIGN_OFF (
      WORKORDER_NUM   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      USER_ID         IN USERLOG.USER_ID%TYPE,
      TIME_STAMP      IN TIMESTAMP)
      RETURN RETDATA
   IS
      /********************************************************************************
      \ NEK_VIEW_WO_SIGN_OFF
      ********************************************************************************/
      cVWOf   RETDATA;
   BEGIN
      SET_USER (USER_ID);

      OPEN cVWOf FOR
         SELECT USER_ID "USER",
                WORKORDER_NUM "WORKORDER_NUMBER",
                ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_NUMBER
                   "TAGOUT_NUMBER",
                ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_SECTION_NUMBER
                   "TAGOUTSECT_NUM",
                NEK_WO_CAN_SIGN_OFF_API (
                   WORKORDER_NUM,
                   ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_NUMBER,
                   ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_SECTION_NUMBER,
                   USER_ID)
                   "RESULT",
                NEK_GET_MESSAGES "REASON",
                TIME_STAMP "TIME"
           FROM DUAL, ACT_TAGOUT_SECTION_WORK_ORDERS
          WHERE     ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER =
                       WORKORDER_NUM
                AND ACT_TAGOUT_SECTION_WORK_ORDERS.TAGOUT_TYPE_ID =
                       NEK_CHECK_FOLDER_TYPE(ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_NUMBER);--CHECKED_FOLDER_TYPE_;

      RETURN cVWOf;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_VIEW_WO_SIGN_OFF');
         RETURN cVWOf;
   END NEK_VIEW_WO_SIGN_OFF;

   /* PUBLIC */
   FUNCTION ADD_MSG (ORIGINAL IN VARCHAR2, ADD_TEXT IN VARCHAR2)
      RETURN VARCHAR2
   IS
      /********************************************************************************
      \ MSG - joins messages in one global string
      ********************************************************************************/
      NEW_MESSAGE   VARCHAR2 (4000);
   BEGIN
      IF ORIGINAL IS NULL
      THEN
         IF (LENGTH(ADD_TEXT) > 0)
         THEN
            NEW_MESSAGE := TRIM (SUBSTR (ADD_TEXT, 1, 4000));
         END IF;
      ELSE
         IF (LENGTH(ADD_TEXT) > 0)
         THEN
             NEW_MESSAGE := TRIM (SUBSTR (ORIGINAL || '\n' || ADD_TEXT, 1, 4000));
         END IF; 
     END IF;
     
     IF (LENGTH(ORIGINAL) > 0 AND (ADD_TEXT IS NULL OR LENGTH(ADD_TEXT) = 0))
     THEN
        NEW_MESSAGE := ORIGINAL;
     END IF;

      RETURN NEW_MESSAGE;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'MSG');
         RETURN SQLERRM;
   END ADD_MSG;

   FUNCTION VERIFY_TO_SIGN_USER_OFF (
      WONAME_   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_     IN USERLOG.USER_ID%TYPE)
      RETURN VARCHAR2
   IS
      /********************************************************************************
      \ VERIFY_TO_SIGN_USER - Checks if  user has been alredy signed on the support table
      ********************************************************************************/
      CURSOR cUSIG
      IS
         SELECT COUNT (*)
           FROM NEK_SIGN_TIMES
          WHERE     WO_NUMBER = WONAME_
                AND TAG_OUT_ID = FONAME_
                AND TAG_OUT_NUMB_ID = TONAME_
                AND ASSIGNED_USER = TRIM (USER_);

      CNT              NUMBER (6);
      SIGNED           VARCHAR2 (50) := '';
      SINGLE_MESSAGE   VARCHAR2 (4000);
   BEGIN
      OPEN cUSIG;

      FETCH cUSIG INTO CNT;

      --    DBMS_OUTPUT.PUT_LINE('USER 0 =  '  || USER_);

      -- Ali mu dovoli kdo drug?
      SIGNED :=
         VERIFY_USER_AUTH_ON_TO_ACT (WONAME_,
                                     FONAME_,
                                     TONAME_,
                                     USER_);

      --   DBMS_OUTPUT.PUT_LINE('SIGNED =  '  || SIGNED);
      IF SIGNED = 'FALSE'
      THEN
         SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
      END IF;

      IF TO_CHAR (CNT) = '0' AND LENGTH (SIGNED) = 0
      THEN
         SINGLE_MESSAGE :=
            ADD_MSG (
               SINGLE_MESSAGE,
                  'Koordinator '
               || USER_
               || ' ni prevzel osamitve '
               || FONAME_
               || '/'
               || TONAME_
               || ' z operacijo '
               || TO_CHAR (WONAME_)
               || '!');
         FUNC_MESSAGE := SINGLE_MESSAGE;
         RETURN 'FALSE';
      ELSE
         RETURN SIGNED;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'MSG');
         RETURN 'FALSE';
   END VERIFY_TO_SIGN_USER_OFF;

   FUNCTION VERIFY_TO_SIGN_USER_ON (
      WONAME_   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_     IN USERLOG.USER_ID%TYPE)
      RETURN BOOLEAN
   IS
      /********************************************************************************
      \ VERIFY_TO_SIGN_USER_ON - Checks if  user has been alredy signed on the support table
      ********************************************************************************/
      CURSOR cUSIG
      IS
         SELECT COUNT (*)
           FROM NEK_SIGN_TIMES
          WHERE     WO_NUMBER = WONAME_
                AND TAG_OUT_ID = FONAME_
                AND TAG_OUT_NUMB_ID = TONAME_
                AND ASSIGNED_USER = TRIM (USER_);

      CNT   NUMBER (6);
   BEGIN
      OPEN cUSIG;

      FETCH cUSIG INTO CNT;

      IF TO_CHAR (CNT) = '0'
      THEN
         RETURN TRUE;
      ELSE
         -- ENG Text
         -- FUNC_MESSAGE := '- User ' || USER_  ||  ' with the Workorder ID '  || TO_CHAR(WONAME_) || ' and Tag out number ('  || FONAME_ || ') '  ||TONAME_  || ' has already been signed.';
         -- SLO Text
         FUNC_MESSAGE :=
               'Koordinator '
            || USER_
            || ' je �e prevzel osamitve '
            || FONAME_
            || '/'
            || TONAME_
            || ' z operacijo '
            || TO_CHAR (WONAME_)
            || '!';
         RETURN FALSE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'MSG');
         RETURN FALSE;
   END VERIFY_TO_SIGN_USER_ON;

   FUNCTION NEK_TOH_CHECK_USER_IN_GROUP (USER_ IN USERLOG.USER_ID%TYPE)
      RETURN BOOLEAN
   IS
      /********************************************************************************
      \ NEK_TOH_CHECK_USER_IN_GROUP - Checks if  user has been alredy signed on the support table
      ********************************************************************************/
      CURSOR cGRPCHK
      IS
         SELECT COUNT (*)
           FROM USER_GROUP_USERS
          WHERE     TRIM (USER_GROUP_ID) = TRIM (TOH_VERIF_GROUP)
                AND TRIM (USER_ID) = TRIM (USER_);

      CNT   NUMBER (9);

      CURSOR cUSER
      IS
         SELECT NAME
           FROM USERLOG
          WHERE TRIM (USER_ID) = TRIM (USER_);

      USR   USERLOG.NAME%TYPE;
   BEGIN
      OPEN cGRPCHK;

      FETCH cGRPCHK INTO CNT;

      CLOSE cGRPCHK;

      OPEN cUSER;

      FETCH cUSER INTO USR;

      CLOSE cUSER;

      IF TO_CHAR (CNT) = '1'
      THEN
         RETURN TRUE;
      ELSE
         FUNC_MESSAGE :=
               'Oseba '
            || USR
            || ' ni poobla�cena za prevzem osamitev kot koordinator!';
         RETURN FALSE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'MSG');
         RETURN FALSE;
   END NEK_TOH_CHECK_USER_IN_GROUP;

   FUNCTION NEK_TO_CAN_SIGN_ON_API (
      WONAME_   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_     IN USERLOG.USER_ID%TYPE)
      RETURN VARCHAR2
   IS
      /********************************************************************************
      \ NEK_TO_CAN_SIGN_ON_API - can sign TO?
      ********************************************************************************/
      USER_EXISTS      BOOLEAN := TRUE;
      R0               BOOLEAN := FALSE;
      R1               BOOLEAN := FALSE;
      R2               BOOLEAN := FALSE;
      R3               BOOLEAN := FALSE;
      R4               BOOLEAN := FALSE;
      R5               BOOLEAN := FALSE;
      R6               BOOLEAN := FALSE;
      R7               BOOLEAN := FALSE;
      R8               BOOLEAN := FALSE;
      SINGLE_MESSAGE   VARCHAR2 (4000) := '';
   BEGIN
      -- init
      FUNC_MESSAGE := '';
      -- rule 0) Validate user
      -- not any more
      SET_USER (USER_);

      R6 := NEK_TOH_CHECK_USER_IN_GROUP (USER_);

      IF R6 = FALSE
      THEN
         SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
      END IF;
      
      --Rule 8 - WO must be in a project that allows the signing
      -- otherwise there must be an configuration error
      IF (LENGTH(NEK_CHECK_FOLDER_TYPE(FONAME_)) > 0) THEN
        R8 := TRUE;
      ELSE 
        SINGLE_MESSAGE := ADD_MSG(SINGLE_MESSAGE, 'NAPAKA: osamitev je v projektu, ki se ne prevzema na nekomatu!');
      END IF;

      -- rule 0.1) Is user already signed with this WO, TO name?
      R0 :=
         VERIFY_TO_SIGN_USER_ON (WONAME_,
                                 FONAME_,
                                 TONAME_,
                                 USER_);

      IF R0 = FALSE
      THEN
         SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
      END IF;

      -- rule 1)  Are tags hung and verified?
      --R1 := NEK_VERIFY_TAGS_API(FONAME_, TONAME_);
      --IF R1 = false THEN
      --    SINGLE_MESSAGE := ADD_MSG(SINGLE_MESSAGE, FUNC_MESSAGE);
      --END IF;       - Marko 16.6.
      R1 := TRUE;

      -- rule 2) Is TO Locked?
      R2 := NEK_IS_TAGOUT_LOCKED_API (FONAME_, TONAME_);

      IF R2 = TRUE
      THEN
         SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
      END IF;

      -- rule 3) Is TO Verification reached?
      R3 := NEK_TAGOUT_LEVEL_REACHED_API (FONAME_, TONAME_);

      IF R3 = FALSE
      THEN
         SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
      END IF;

      -- rule 4) Is user already signed on TO/can TO be multi-signed?
      R4 := NEK_TAGOUT_SIGNING_ALLOWED_API (FONAME_, TONAME_);

      IF R4 = FALSE
      THEN
         SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
      END IF;

      -- rule 5) TO verification must not be over an index!
      R5 := NEK_TAGOUT_VERIF_INDEX_OK (FONAME_, TONAME_, TAG_VERIF_END_VAL);

      IF R5 = FALSE
      THEN
         SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
      END IF;

      R7 := NEK_TO_HAS_SIGNATURE (WONAME_, FONAME_, TONAME_);

      IF R7 = FALSE
      THEN
         SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
      END IF;

      FUNC_MESSAGE := SINGLE_MESSAGE;

      IF (    USER_EXISTS = TRUE
          AND R1 = TRUE
          AND R2 = FALSE
          AND R3 = TRUE
          AND R4 = TRUE
          AND R0 = TRUE
          AND R5 = TRUE
          AND R6 = TRUE
          AND R7 = TRUE
          AND R8 = TRUE)
      THEN
         RETURN 'TRUE';
      ELSE
         RETURN 'FALSE';
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_TO_CAN_SIGN_ON_API');
         RETURN SQLERRM;
   END NEK_TO_CAN_SIGN_ON_API;

   -- todo
   FUNCTION VERIFY_USER_AUTH_ON_WO_ACT (
      WONAME_   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_     IN USERLOG.USER_ID%TYPE)
      RETURN VARCHAR2
   IS
      /********************************************************************************
      \ VERIFY_USER_AUTH_ON_ACT - Verify if the user has priviliges to remove sign under another user
      ********************************************************************************/
      CURSOR cUSIG
      IS
         SELECT SIGN_ON_BY
           FROM ACT_WORK_ORDER_HOLDERS
          WHERE     WORK_ORDER_NUMBER = WONAME_
                AND ACT_TAGOUT_NUMBER = FONAME_
                AND ACT_TAGOUT_SECTION_NUMBER = TONAME_
                AND SIGN_OFF_DATE IS NULL;

      SIGNED    NEK_SIGN_TIMES.ASSIGNED_USER%TYPE;

      CURSOR cAUTHCHK
      IS
         SELECT REVOKED, AUTHENDS, AUTHBEGUN
           FROM NEK_AUTH_USER
          WHERE     TRIM (AUTHORIZER) = TRIM (SIGNED)
                AND TRIM (AUTHORIZED) = TRIM (USER_)
                AND --TANUMB = FONAME_ AND
                    --TASECTNUMB = TONAME_ AND
                    AUTHBEGUN <= SYSTIMESTAMP;

      REVO      NEK_AUTH_USER.REVOKED%TYPE;
      ENDS      NEK_AUTH_USER.AUTHENDS%TYPE;
      BEGINS    NEK_AUTH_USER.AUTHBEGUN%TYPE;

      CURSOR cUSER
      IS
         SELECT NAME
           FROM USERLOG
          WHERE TRIM (USER_ID) = TRIM (SIGNED);

      USR       USERLOG.NAME%TYPE;

      CURSOR cUSERAUTH
      IS
         SELECT NAME
           FROM USERLOG
          WHERE TRIM (USER_ID) = TRIM (USER_);

      USRAUTH   USERLOG.NAME%TYPE;
      
   BEGIN
      OPEN cUSIG;

      FETCH cUSIG INTO SIGNED;

      --DBMS_OUTPUT.PUT_LINE('WO ?? =  '  || WONAME_ ||' ' || FONAME_ || ' ' || TONAME_ ||' ' ||USER_ );
      --DBMS_OUTPUT.PUT_LINE('SIGNED ! =  '  || SIGNED);
      CLOSE cUSIG;

      IF (TRIM (USER_) = TRIM (SIGNED) OR (NEK_PACKAGE_SETTINGS.GET_SETTING('EXECUTE_SIGN_CHECK') = '0'))
      THEN
         RETURN SIGNED;
      ELSE
         OPEN cAUTHCHK;

         FETCH cAUTHCHK
         INTO REVO, ENDS, BEGINS;

         --DBMS_OUTPUT.PUT_LINE('Revo: ' ||TO_CHAR(REVO));
         --DBMS_OUTPUT.PUT_LINE('Begins: ' ||TO_CHAR(BEGINS));
         --DBMS_OUTPUT.PUT_LINE('Ends: ' ||TO_CHAR(ENDS));

         OPEN cUSER;

         FETCH cUSER INTO USR;

         CLOSE cUSER;

         --DBMS_OUTPUT.PUT_LINE('auth: ' || USR);

         OPEN cUSERAUTH;

         FETCH cUSERAUTH INTO USRAUTH;

         CLOSE cUSERAUTH;

         --
         --DBMS_OUTPUT.PUT_LINE('usr: ' || USRAUTH);
         --DBMS_OUTPUT.PUT_LINE('found rows: ' ||  TO_CHAR(cAUTHCHK%ROWCOUNT));

         IF TO_CHAR (cAUTHCHK%ROWCOUNT) > '0'
         THEN
            --DBMS_OUTPUT.PUT_LINE('in');

            IF ENDS < SYSTIMESTAMP
            THEN
               --  DBMS_OUTPUT.PUT_LINE('ends');
               FUNC_MESSAGE :=
                     'Oseba '
                  || USRAUTH
                  || ' ni poobla�cena za sprostitev osamitev namesto '
                  || USR
                  || '. Pooblastilo je poteklo!';

               CLOSE cAUTHCHK;

               RETURN 'FALSE';
            END IF;

            IF TO_CHAR (REVO) = '1'
            THEN
               --DBMS_OUTPUT.PUT_LINE('revoked');
               FUNC_MESSAGE :=
                     'Oseba '
                  || USRAUTH
                  || ' ni poobla�cena za sprostitev osamitev namesto '
                  || USR
                  || '. Pooblastilo je preklicano!';

               CLOSE cAUTHCHK;

               RETURN 'FALSE';
            END IF;

            CLOSE cAUTHCHK;

            --DBMS_OUTPUT.PUT_LINE('ret sign ! =  '  || SIGNED);
            RETURN SIGNED;
         ELSE
            FUNC_MESSAGE :=
                  'Oseba '
               || USRAUTH
               || ' ni poobla�cena za sprostitev osamitev namesto '
               || USR
               || '.';

            CLOSE cAUTHCHK;

            --DBMS_OUTPUT.PUT_LINE('bypass');
            RETURN 'FALSE';
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'MSG');
         RETURN 'FALSE';
   END VERIFY_USER_AUTH_ON_WO_ACT;

   FUNCTION VERIFY_USER_AUTH_ON_TO_ACT (
      WONAME_   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_     IN USERLOG.USER_ID%TYPE)
      RETURN VARCHAR2
   IS
      /********************************************************************************
      \ VERIFY_USER_AUTH_ON_ACT - Verify if the user has priviliges to remove sign under another user
      ********************************************************************************/
      CURSOR cUSIG
      IS
         SELECT ASSIGNED_USER
           FROM NEK_SIGN_TIMES
          WHERE     WO_NUMBER = WONAME_
                AND TAG_OUT_ID = FONAME_
                AND TAG_OUT_NUMB_ID = TONAME_;

      SIGNED    NEK_SIGN_TIMES.ASSIGNED_USER%TYPE;

      CURSOR cAUTHCHK
      IS
         SELECT REVOKED, AUTHENDS, AUTHBEGUN
           FROM NEK_AUTH_USER
          WHERE     TRIM (AUTHORIZER) = TRIM (SIGNED)
                AND TRIM (AUTHORIZED) = TRIM (USER_)
                AND --TANUMB = FONAME_ AND
                    --TASECTNUMB = TONAME_ AND
                    AUTHBEGUN <= SYSTIMESTAMP;

      REVO      NEK_AUTH_USER.REVOKED%TYPE;
      ENDS      NEK_AUTH_USER.AUTHENDS%TYPE;
      BEGINS    NEK_AUTH_USER.AUTHBEGUN%TYPE;

      CURSOR cUSER
      IS
         SELECT NAME
           FROM USERLOG
          WHERE TRIM (USER_ID) = TRIM (SIGNED);

      USR       USERLOG.NAME%TYPE;

      CURSOR cUSERAUTH
      IS
         SELECT NAME
           FROM USERLOG
          WHERE TRIM (USER_ID) = TRIM (USER_);

      USRAUTH   USERLOG.NAME%TYPE;
   BEGIN
      OPEN cUSIG;

      FETCH cUSIG INTO SIGNED; 

      --DBMS_OUTPUT.PUT_LINE('? ! =  '  || WONAME_ ||' ' || FONAME_ || ' ' || TONAME_ ||' ' ||USER_ );
      --DBMS_OUTPUT.PUT_LINE('SIGNED ! =  '  || SIGNED);
      CLOSE cUSIG;

      IF ((TRIM (USER_) = TRIM (SIGNED)) OR (NEK_PACKAGE_SETTINGS.GET_SETTING('EXECUTE_SIGN_CHECK') = '0')) -- if 0 all checks are irelevant
      THEN
         RETURN SIGNED;
      ELSE
         OPEN cAUTHCHK;

         FETCH cAUTHCHK
         INTO REVO, ENDS, BEGINS;

         --DBMS_OUTPUT.PUT_LINE('Revo: ' ||TO_CHAR(REVO));
         --DBMS_OUTPUT.PUT_LINE('Begins: ' ||TO_CHAR(BEGINS));
         --DBMS_OUTPUT.PUT_LINE('Ends: ' ||TO_CHAR(ENDS));

         OPEN cUSER;

         FETCH cUSER INTO USR;

         CLOSE cUSER;

         --DBMS_OUTPUT.PUT_LINE('auth: ' || USR);

         OPEN cUSERAUTH;

         FETCH cUSERAUTH INTO USRAUTH;

         CLOSE cUSERAUTH;

         --
         --DBMS_OUTPUT.PUT_LINE('usr: ' || USRAUTH);
         --DBMS_OUTPUT.PUT_LINE('found rows: ' ||  TO_CHAR(cAUTHCHK%ROWCOUNT));

         IF TO_CHAR (cAUTHCHK%ROWCOUNT) > '0'
         THEN
            --DBMS_OUTPUT.PUT_LINE('in');

            IF ENDS < SYSTIMESTAMP
            THEN
               --  DBMS_OUTPUT.PUT_LINE('ends');
               FUNC_MESSAGE :=
                     'Oseba '
                  || USRAUTH
                  || ' ni poobla�cena za sprostitev osamitev namesto '
                  || USR
                  || '. Pooblastilo je poteklo!';

               CLOSE cAUTHCHK;

               RETURN 'FALSE';
            END IF;

            IF TO_CHAR (REVO) = '1'
            THEN
               --DBMS_OUTPUT.PUT_LINE('revoked');
               FUNC_MESSAGE :=
                     'Oseba '
                  || USRAUTH
                  || ' ni poobla�cena za sprostitev osamitev namesto '
                  || USR
                  || '. Pooblastilo je preklicano!';

               CLOSE cAUTHCHK;

               RETURN 'FALSE';
            END IF;

            CLOSE cAUTHCHK;

            --DBMS_OUTPUT.PUT_LINE('ret sign ! =  '  || SIGNED);
            RETURN SIGNED;
         ELSE
            FUNC_MESSAGE :=
                  'Oseba '
               || USRAUTH
               || ' ni poobla�cena za sprostitev osamitev namesto '
               || USR
               || '.';

            CLOSE cAUTHCHK;

            --DBMS_OUTPUT.PUT_LINE('bypass');
            RETURN 'FALSE';
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'MSG');
         RETURN '';
   END VERIFY_USER_AUTH_ON_TO_ACT;

   -- todo
   FUNCTION NEK_TO_CAN_SIGN_OFF_API (
      WONAME_   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_     IN USERLOG.USER_ID%TYPE)
      RETURN VARCHAR2
   IS
      /********************************************************************************
      \ NEK_TO_SIGN_OFF_API - Checks if sign off is allowed
      ********************************************************************************/
      USER_EXISTS          BOOLEAN := TRUE;
      R0                   VARCHAR2 (50) := '';
      R1                   BOOLEAN := FALSE;
      R2                   BOOLEAN := FALSE;
      R3                   BOOLEAN := FALSE;
      R4                   BOOLEAN := FALSE;
      R5                   VARCHAR2 (50) := '';
      R6                   BOOLEAN := FALSE;
      SINGLE_MESSAGE       VARCHAR2 (4000) := '';
      NUM_SIGNED_HOLDERS   NUMBER (9);

      ENDUSER              VARCHAR (50) := '';
   BEGIN
      --DBMS_OUTPUT.PUT_LINE('USER =  '  || USER_);
      ENDUSER := USER_;

      FUNC_MESSAGE := '';
      -- rule 0) Validate user
      -- not any more
      SET_USER (USER_);


      --Rule 6 - WO must be in a project that allows the signing off
      -- otherwise there must be an configuration error
      IF (LENGTH(NEK_CHECK_FOLDER_TYPE(FONAME_)) > 0) THEN
        R6 := TRUE;
      ELSE 
        SINGLE_MESSAGE := ADD_MSG(SINGLE_MESSAGE, 'NAPAKA: osamitev je v projektu, ki se ne prevzema na nekomatu!');
      END IF;

      -- rule 0.1) Is user already signed with this WO, TO name?
      R0 :=
         VERIFY_TO_SIGN_USER_OFF (WONAME_,
                                  FONAME_,
                                  TONAME_,
                                  USER_);

      IF R0 = 'FALSE'
      THEN
         SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
      -- DBMS_OUTPUT.PUT_LINE('TO-CSO '  || SINGLE_MESSAGE);
      --ELSE
      --  ENDUSER := R0;
      --DBMS_OUTPUT.PUT_LINE('ENDUSER =  '  || ENDUSER);
      END IF;

      -- rule 1)  Is there a WO holder?
      R1 := NEK_IS_WO_HOLDER_SIGNED_API (WONAME_, FONAME_, TONAME_);

      IF R1 = TRUE
      THEN
         SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
      END IF;

      -- rule 2) Is this the last sign off?
      NUM_SIGNED_HOLDERS := NEK_NUM_TO_SIGNED_HOLDERS_API (FONAME_, TONAME_);

      IF NUM_SIGNED_HOLDERS = 1
      THEN
         R2 := TRUE;

         IF R1 = TRUE
         THEN
            SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
         END IF;
      ELSIF NUM_SIGNED_HOLDERS = 0
      THEN
         R3 := TRUE;                                         -- Must be false.
         SINGLE_MESSAGE :=
            ADD_MSG (
               SINGLE_MESSAGE,
                  'Koordinator �e ni prevzel osamitev '
               || FONAME_
               || '/'
               || TONAME_
               || '!');
      ELSE
         R2 := TRUE;
      END IF;

      FUNC_MESSAGE := SINGLE_MESSAGE;

      -- All rules together
      IF (    USER_EXISTS = TRUE
          AND (R1 = FALSE OR (R1 = TRUE AND R2 = FALSE))
          AND R3 = FALSE
          AND R0 <> 'FALSE')
      THEN
         -- rule 5) verify user authorization - only if user is signed
         R5 :=
            VERIFY_USER_AUTH_ON_TO_ACT (WONAME_,
                                        FONAME_,
                                        TONAME_,
                                        USER_); -- second user is the real one - removing sign

         IF LENGTH (R5) = 0
         THEN
            SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
            RETURN 'FALSE';
         END IF;

         IF (R6 = FALSE)
         THEN
            RETURN 'FALSE';
         END IF;
         
         --RETURN ENDUSER;
         RETURN 'TRUE';
      ELSE
         RETURN 'FALSE';
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_TO_CAN_SIGN_OFF_API');
         RETURN SQLERRM;
   END NEK_TO_CAN_SIGN_OFF_API;

   PROCEDURE NEK_TO_SIGN_ON_API (
      WONAME_    IN     ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_    IN     ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_    IN     ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_      IN     USERLOG.USER_ID%TYPE,
      NOTES_     IN     VARCHAR2,
      RESULT        OUT VARCHAR2,
      WORKINFO      OUT VARCHAR2)
   IS
      /********************************************************************************
      \ NEK_TO_SIGN_ON_API
      Changes
      19.12.2012; D. Gregoric; LOOP_COUNT for MARKTIME variable
      ********************************************************************************/
      CURSOR NUM_SIGN
      IS
         SELECT COUNT (*)
           FROM    ACT_TAGOUT_SECTION_HOLDERS
                INNER JOIN
                   NEK_SIGN_TIMES NKT
                ON     NKT.TAG_OUT_ID = ACT_TAGOUT_NUMBER
                   AND NKT.TAG_OUT_NUMB_ID = ACT_TAGOUT_SECTION_NUMBER
          WHERE     TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(FONAME_) --CHECKED_FOLDER_TYPE_
                AND ACT_TAGOUT_NUMBER = FONAME_
                AND ACT_TAGOUT_SECTION_NUMBER = TONAME_
                AND DEPARTMENT = DEFAULT_USER_DEPARTMENT_
                AND NKT.WO_NUMBER = WONAME_;

      CNT          NUMBER (9);
      MARKTIME     TIMESTAMP;
      LOC_RESULT   VARCHAR2 (4000);
   BEGIN
      LOOP_COUNT := LOOP_COUNT + 1;                   -- Dodal DG (19.12.2012)

      IF NEK_TO_CAN_SIGN_ON_API (WONAME_,
                                 FONAME_,
                                 TONAME_,
                                 USER_) = 'TRUE'
      THEN
         WORKINFO := 'WAL';
         --MARKTIME := SYSTIMESTAMP; by DG (19.12.2012)
         MARKTIME := SYSTIMESTAMP + (LOOP_COUNT / 86400); --Dloda DG (19.12.2012)

         -- Write sign on to TO
         INSERT INTO ACT_TAGOUT_SECTION_HOLDERS (TAGOUT_TYPE_ID,
                                                 ACT_TAGOUT_NUMBER,
                                                 ACT_TAGOUT_SECTION_NUMBER,
                                                 USER_ID,
                                                 ACCEPTED_DT,
                                                 SIGNED_ON_BY,
                                                 DEPARTMENT,
                                                 NOTES)
              VALUES (NEK_CHECK_FOLDER_TYPE(FONAME_), --CHECKED_FOLDER_TYPE_,
                      FONAME_,
                      TONAME_,
                      USER_,
                      MARKTIME,
                      USER_,
                      DEFAULT_USER_DEPARTMENT_,
                      NOTES_ || ' Operacija: ' || WONAME_ || ' (eBS).');

         NEK_WRITE_WO_TIMESTAMP (MARKTIME,
                                 FONAME_,
                                 TONAME_,
                                 WONAME_,
                                 USER_);

         NEK_AUDIT (
            'SIGN',
            NEK_CHECK_FOLDER_TYPE(FONAME_), --CHECKED_FOLDER_TYPE_,
            FONAME_,
            TONAME_,
            USER_,
               'User '
            || NEK_GET_USER_FULLNAME (USER_)
            || ' ('
            || TRIM (USER_)
            || ') had been signed ON on TO '
            || FONAME_
            || '-'
            || TONAME_
            || ' by eBS-eSOMS interface.');

         RESULT := OK_MESSAGE;
         LOC_RESULT := 'Sign on to TO successfull!';
      ELSE
         OPEN NUM_SIGN;

         FETCH NUM_SIGN INTO CNT;

         CLOSE NUM_SIGN;

         IF (TO_CHAR (CNT) != '0')
         THEN
            WORKINFO := 'WIP';
         ELSE
            WORKINFO := 'WNA';
         END IF;

         RESULT := FUNC_MESSAGE;
         LOC_RESULT := FUNC_MESSAGE;
      END IF;

      NEK_LOCAL_AUDIT ('TO',
                       WONAME_,
                       FONAME_,
                       TONAME_,
                       USER_,
                       'ON',
                       LOC_RESULT);
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_TO_SIGN_ON_API');
         RESULT := SQLERRM;
   END NEK_TO_SIGN_ON_API;

   PROCEDURE NEK_TO_SIGN_OFF_API (
      WONAME_   IN     ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_   IN     ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN     ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_     IN     USERLOG.USER_ID%TYPE,
      RESULT       OUT VARCHAR2)
   IS
      /********************************************************************************
      \ NEK_TO_SIGN_OFF_API
      ********************************************************************************/
      MARKTIME     NEK_SIGN_TIMES.ESOMS_TIME%TYPE;
      LOC_RESULT   VARCHAR2 (4000);
      FIN_USER     USERLOG.USER_ID%TYPE;
   BEGIN
      --DBMS_OUTPUT.PUT_LINE('USER =  '  || USER_);
      -- DBMS_OUTPUT.PUT_LINE('LOC_USER =  '  || LOC_USER);

      IF NEK_TO_CAN_SIGN_OFF_API (WONAME_,
                                  FONAME_,
                                  TONAME_,
                                  USER_) = 'TRUE'
      THEN
         --DBMS_OUTPUT.PUT_LINE('REMOVING USER =  '  || LOC_USER);

         FIN_USER :=
            TRIM (GET_SIGNED_USER_TO (WONAME_,
                                      FONAME_,
                                      TONAME_,
                                      USER_));

         --DBMS_OUTPUT.PUT_LINE('USER ZA OFF =  '  || FIN_USER);

         -- Write sign off from TO
         MARKTIME :=
            NEK_GET_TIME_FROM_TOSIGN (FONAME_,
                                      TONAME_,
                                      WONAME_,
                                      FIN_USER);

         UPDATE ACT_TAGOUT_SECTION_HOLDERS
            SET RELEASED_BY = FIN_USER,
                RELEASED_DT = SYSTIMESTAMP,
                WORK_COMPLETE = 1
          WHERE     TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(FONAME_) --CHECKED_FOLDER_TYPE_
                AND ACT_TAGOUT_NUMBER = FONAME_
                AND ACT_TAGOUT_SECTION_NUMBER = TONAME_
                AND SIGNED_ON_BY = FIN_USER
                AND ACCEPTED_DT =
                       TO_DATE (TO_CHAR (MARKTIME, 'dd.mm.yy hh24:MI:SS'),
                                'dd.mm.yy hh24:MI:SS')
                AND (RELEASED_BY IS NULL AND WORK_COMPLETE IS NULL);

         NEK_DELETE_WO_TIMESTAMP (MARKTIME,
                                  FONAME_,
                                  TONAME_,
                                  WONAME_,
                                  FIN_USER);

         NEK_AUDIT (
            'SIGN',
            NEK_CHECK_FOLDER_TYPE(FONAME_), --CHECKED_FOLDER_TYPE_,
            FONAME_,
            TONAME_,
            FIN_USER,
               'User '
            || NEK_GET_USER_FULLNAME (FIN_USER)
            || ' ('
            || TRIM (FIN_USER)
            || ') had been signed OFF from TO '
            || FONAME_
            || '-'
            || TONAME_
            || ' by eBS-eSOMS interface.');

         RESULT := OK_MESSAGE;
         LOC_RESULT := 'Sign off from TO successfull!';
      ELSE
         RESULT := FUNC_MESSAGE;
         LOC_RESULT := FUNC_MESSAGE;
      END IF;

      NEK_LOCAL_AUDIT ('TO',
                       WONAME_,
                       FONAME_,
                       TONAME_,
                       FIN_USER,
                       'OFF',
                       LOC_RESULT);
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_TO_SIGN_OFF_API');
         RESULT := SQLERRM;
   END NEK_TO_SIGN_OFF_API;

   FUNCTION NEK_WO_HAS_TO (
      WONAME_   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE)
      RETURN BOOLEAN
   IS
      /********************************************************************************
      \ NEK_WO_HAS_TO - Does it have TO?
      ********************************************************************************/
      CURSOR cHTO
      IS
         SELECT COUNT (*)
           FROM ACT_TAGOUT_SECTION_WORK_ORDERS
          WHERE     WORK_ORDER_NUMBER = WONAME_
                AND TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(FONAME_) --CHECKED_FOLDER_TYPE_
                AND ACT_TAGOUT_NUMBER = FONAME_
                AND ACT_TAGOUT_SECTION_NUMBER = TONAME_;

      COUNT_HTO   NUMBER (9);
   BEGIN
      OPEN cHTO;
      FETCH cHTO INTO COUNT_HTO;
      CLOSE cHTO;

      IF LENGTH(NEK_CHECK_FOLDER_TYPE(FONAME_)) > 0 
      THEN
          IF (TO_CHAR (COUNT_HTO) = '0' )
          THEN
             FUNC_MESSAGE :=
                   'Na osamitvi '
                || FONAME_
                || '/'
                || TONAME_
                || ' ni operacije '
                || WONAME_
                || '!';
             RETURN FALSE;
          ELSE
             RETURN TRUE;
          END IF;
      ELSE 
        RETURN FALSE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_WO_HAS_TO');
         RETURN FALSE;
   END NEK_WO_HAS_TO;

   FUNCTION NEK_WO_HAS_TO (
      WONAME_   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE)
      RETURN BOOLEAN
   IS
      /********************************************************************************
      \ NEK_WO_HAS_TO - Does it have TO?
      ********************************************************************************/
      CURSOR cHTO
      IS
         SELECT COUNT (*)
           FROM ACT_TAGOUT_SECTION_WORK_ORDERS
          WHERE     WORK_ORDER_NUMBER = WONAME_
                AND TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(FONAME_) --CHECKED_FOLDER_TYPE_
                AND ACT_TAGOUT_NUMBER = FONAME_;

      COUNT_HTO   NUMBER (9);
   BEGIN
      OPEN cHTO;

      FETCH cHTO INTO COUNT_HTO;

      IF LENGTH(NEK_CHECK_FOLDER_TYPE(FONAME_)) > 0 
      THEN
          IF TO_CHAR (COUNT_HTO) = '0'
          THEN
             FUNC_MESSAGE :=
                'Na osamitvi ' || FONAME_ || ' ni operacije ' || WONAME_ || '!';
             RETURN FALSE;
          ELSE
             RETURN TRUE;
          END IF;
      ELSE 
        RETURN FALSE;
      END IF;
      
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_WO_HAS_TO');
         RETURN FALSE;
   END NEK_WO_HAS_TO;

   FUNCTION NEK_WO_HAS_TO (
      WONAME_ IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE)
      RETURN BOOLEAN
   IS
      /********************************************************************************
      \ NEK_WO_HAS_TO - Does it have TO?
      ********************************************************************************/
      CURSOR cHTO
      IS
         SELECT COUNT (*)
           FROM ACT_TAGOUT_SECTION_WORK_ORDERS
          WHERE WORK_ORDER_NUMBER = WONAME_
                AND TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_NUMBER); --CHECKED_FOLDER_TYPE_;
      
      CURSOR cFN
      IS 
          SELECT ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_NUMBER
            FROM ACT_TAGOUT_SECTION_WORK_ORDERS
            WHERE WORK_ORDER_NUMBER = WONAME_
                  AND TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_NUMBER);

      COUNT_HTO   NUMBER (9);
      FONAME_     ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_NUMBER%TYPE;
   BEGIN
      OPEN cHTO;
      FETCH cHTO INTO COUNT_HTO;
      CLOSE cHTO;
      
      OPEN cFN;
      FETCH cFN INTO FONAME_;
      CLOSE cFN;

      IF LENGTH(NEK_CHECK_FOLDER_TYPE(FONAME_)) > 0 
      THEN
          IF TO_CHAR (COUNT_HTO) = '0'
          THEN
             FUNC_MESSAGE := 'Na osamitvi ni operacije ' || WONAME_ || '!';
             RETURN FALSE;
          ELSE
             RETURN TRUE;
          END IF;
      ELSE 
      
        RETURN FALSE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_WO_HAS_TO');
         RETURN FALSE;
   END NEK_WO_HAS_TO;

   FUNCTION NEK_WOH_CHECK_USER_IN_GROUP (USER_ IN USERLOG.USER_ID%TYPE)
      RETURN BOOLEAN
   IS
      /********************************************************************************
      \ NEK_WOH_CHECK_USER_IN_GROUP - Checks if  user has been alredy signed on the support table
      ********************************************************************************/
      CURSOR cGRPCHKW
      IS
         SELECT COUNT (*)
           FROM USER_GROUP_USERS
          WHERE     TRIM (USER_GROUP_ID) = TRIM (WOH_VERIF_GROUP)
                AND TRIM (USER_ID) = TRIM (USER_);

      CNT   NUMBER (9);

      CURSOR cUSER
      IS
         SELECT NAME
           FROM USERLOG
          WHERE TRIM (USER_ID) = TRIM (USER_);

      USR   USERLOG.NAME%TYPE;
   BEGIN
      OPEN cGRPCHKW;

      FETCH cGRPCHKW INTO CNT;

      CLOSE cGRPCHKW;

      OPEN cUSER;

      FETCH cUSER INTO USR;

      CLOSE cUSER;

      IF TO_CHAR (CNT) = '1'
      THEN
         RETURN TRUE;
      ELSE
         FUNC_MESSAGE :=
               'Oseba '
            || USR
            || ' ni poobla�cena za prevzem osamitev kot vodja del!';
         RETURN FALSE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'MSG');
         RETURN FALSE;
   END NEK_WOH_CHECK_USER_IN_GROUP;

   FUNCTION NEK_WO_CAN_SIGN_ON_API (
      WONAME_   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_     IN USERLOG.USER_ID%TYPE)
      RETURN VARCHAR2
   IS
      /********************************************************************************
      \ NEK_WO_CAN_SIGN_ON_API - can sign on WO?
      ********************************************************************************/
      USER_EXISTS      BOOLEAN := TRUE;
      R1               BOOLEAN := FALSE;
      R2               BOOLEAN := FALSE;
      R3               BOOLEAN := FALSE;
      R4               BOOLEAN := FALSE;
      R5               BOOLEAN := FALSE;
      R01              BOOLEAN := FALSE;
      R02              BOOLEAN := FALSE;
      R6               BOOLEAN := FALSE;
      R7               BOOLEAN := FALSE;
      R8               BOOLEAN := FALSE;
      SINGLE_MESSAGE   VARCHAR2 (4000) := '';
   BEGIN
      -- init
      FUNC_MESSAGE := '';
      -- rule 0) Validate user
      -- not anymore
      
            -- R8 Check the FONAME_ validity
      IF (LENGTH(NEK_CHECK_FOLDER_TYPE(FONAME_)) > 0) THEN
        R8 := TRUE;
      ELSE 
        SINGLE_MESSAGE := ADD_MSG(SINGLE_MESSAGE, 'NAPAKA: osamitev je v projektu, ki se ne prevzema na nekomatu!');
      END IF;
      --DBMS_OUTPUT.PUT_LINE('R8 '  || SINGLE_MESSAGE);
      
      SET_USER (USER_);
      
      R6 := NEK_WOH_CHECK_USER_IN_GROUP (USER_);

      IF R6 = FALSE
      THEN
         SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
      END IF;

      -- rule 0.1) Does workorder exist?
      R01 := NEK_WO_EXISTS (WONAME_);

      IF R01 = FALSE
      THEN
         SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
      END IF;

      -- rule 0.2) Does workorder have TO?
      R02 := NEK_WO_HAS_TO (WONAME_, FONAME_, TONAME_);

      IF R02 = FALSE
      THEN
         SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
      END IF;

      -- rule 1)  Are tags hung and verified?
      --R1 := NEK_VERIFY_TAGS_API(FONAME_, TONAME_);
      --IF R1 = false THEN
      --    SINGLE_MESSAGE := ADD_MSG(SINGLE_MESSAGE, FUNC_MESSAGE);
      --END IF;
      R1 := TRUE;

      -- rule 2) Is TO Locked?
      R2 := NEK_IS_TAGOUT_LOCKED_API (FONAME_, TONAME_);

      IF R2 = TRUE
      THEN
         SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
      END IF;

      -- rule 3) Is TO Verification reached?
      R3 := NEK_TAGOUT_LEVEL_REACHED_API (FONAME_, TONAME_);

      IF R3 = FALSE
      THEN
         SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
      END IF;

      -- rule 4) Is user already signed on WO/can sign?
      R4 :=
         NEK_WO_SIGNING_ON_ALLOWED_API (WONAME_,
                                        FONAME_,
                                        TONAME_,
                                        USER_);

      IF R4 = FALSE
      THEN
         SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
      END IF;

      -- Check TOh - if none, WO cannot be signed
      R5 := NEK_WO_HAS_TO_SIGN (WONAME_, FONAME_, TONAME_);

      IF R5 = FALSE
      THEN
         SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
      END IF;

      R7 := NEK_WO_HAS_SIGNATURE (WONAME_, FONAME_, TONAME_);

      IF R7 = FALSE
      THEN
         SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
      END IF;
      

      FUNC_MESSAGE := SINGLE_MESSAGE;

      IF (    USER_EXISTS = TRUE
          AND R1 = TRUE
          AND R2 = FALSE
          AND R3 = TRUE
          AND R4 = TRUE
          AND R01 = TRUE
          AND R02
          AND R5 = TRUE
          AND R6 = TRUE
          AND R7 = TRUE
          AND R8 = TRUE)
      THEN
         RETURN 'TRUE';
      ELSE
         RETURN 'FALSE';
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_WO_CAN_SIGN_ON_API');
         RETURN SQLERRM;
   END NEK_WO_CAN_SIGN_ON_API;

   FUNCTION NEK_WO_HAS_SIGNATURE (
      WONAME_   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE)
      RETURN BOOLEAN
   IS
      CURSOR cUSIG
      IS
         SELECT COUNT (*)
           FROM ACT_WORK_ORDER_HOLDERS
          WHERE     WORK_ORDER_NUMBER = WONAME_
                AND ACT_TAGOUT_NUMBER = FONAME_
                AND ACT_TAGOUT_SECTION_NUMBER = TONAME_
                AND SIGN_OFF_DATE IS NULL;

      SIGN_   NUMBER (9);
   BEGIN
      OPEN cUSIG;

      FETCH cUSIG INTO SIGN_;

      CLOSE cUSIG;

      IF SIGN_ = 0
      THEN
         RETURN TRUE;
      ELSE
         FUNC_MESSAGE :=
               'Vodja del je �e prevzel osamitev z delovnim nalogom '
            || WONAME_
            || '.';
         RETURN FALSE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'GET_SIGNED_USER');
         RETURN FALSE;
   END NEK_WO_HAS_SIGNATURE;

   FUNCTION NEK_TO_HAS_SIGNATURE (
      WONAME_   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE)
      RETURN BOOLEAN
   IS
      CURSOR cUSIG
      IS
         SELECT COUNT (*)
           FROM NEK_SIGN_TIMES
          WHERE     WO_NUMBER = WONAME_
                AND TAG_OUT_ID = FONAME_
                AND TAG_OUT_NUMB_ID = TONAME_;

      SIGN_   NUMBER (9);
   BEGIN
      OPEN cUSIG;

      FETCH cUSIG INTO SIGN_;

      CLOSE cUSIG;

      --DBMS_OUTPUT.PUT_LINE('CNT '  || SIGN_);

      IF SIGN_ = 0
      THEN
         RETURN TRUE;
      ELSE
         FUNC_MESSAGE :=
               'Koordinator je �e prevzel osamitev z delovnim nalogom '
            || WONAME_
            || '.';
         RETURN FALSE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'GET_SIGNED_USER');
         RETURN FALSE;
   END NEK_TO_HAS_SIGNATURE;

   -- todo
   FUNCTION NEK_WO_CAN_SIGN_OFF_API (
      WONAME_   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_     IN USERLOG.USER_ID%TYPE)
      RETURN VARCHAR2
   IS
      /********************************************************************************
      \ NEK_WO_CAN_SIGN_OFF_API - can sign off WO?
      ********************************************************************************/
      USER_EXISTS          BOOLEAN := TRUE;
      R0                   VARCHAR2 (50) := '';
      R1                   BOOLEAN := FALSE;
      R2                   BOOLEAN := FALSE;
      R3                   VARCHAR2 (50) := '';
      SINGLE_MESSAGE       VARCHAR2 (4000) := '';
      NUM_SIGNED_HOLDERS   NUMBER (9);
      LOC_USER             VARCHAR2 (50) := '';
      R4                   BOOLEAN := FALSE;
   BEGIN
      -- init
      LOC_USER := USER_;
      FUNC_MESSAGE := '';
      
            -- R4 Check the FONAME_ validity
      IF (LENGTH(NEK_CHECK_FOLDER_TYPE(FONAME_)) > 0) THEN
          R4 := TRUE;
      ELSE 
          SINGLE_MESSAGE := ADD_MSG(SINGLE_MESSAGE, 'NAPAKA: osamitev je v projektu, ki se ne prevzema na nekomatu!');
      END IF;
      --DBMS_OUTPUT.PUT_LINE('R0 '  || SINGLE_MESSAGE);
      
      -- rule 0) Validate user
      -- not any more,
      SET_USER (LOC_USER);

      -- rule 0) Is user already signed on WO/can sign?
      R0 :=
         NEK_WO_SIGNING_OFF_ALLOWED_API (WONAME_,
                                         FONAME_,
                                         TONAME_,
                                         LOC_USER);

      --DBMS_OUTPUT.PUT_LINE('R0 '  || R0);

      IF R0 = 'FALSE'
      THEN
         SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
      END IF;

      -- rule 1)  Is there a WO holder?
      R1 := NEK_IS_WO_HOLDER_SIGNED_API (FONAME_, TONAME_);

      IF R1 = FALSE
      THEN
         --FUNC_MESSAGE := '- There is no work order holder';
         FUNC_MESSAGE :=
               'Vodja del z operacijo '
            || WONAME_
            || ' �e ni prevzel osamitev '
            || FONAME_
            || '/'
            || TONAME_
            || '!';
         SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
      END IF;

      --IF R1 = true then
      --DBMS_OUTPUT.PUT_LINE('R1 true' );
      --else
      --DBMS_OUTPUT.PUT_LINE('R1 false' );
      --end if;
      -- rule 2) Is this the last sign off?
      NUM_SIGNED_HOLDERS := NEK_NUM_TO_SIGNED_HOLDERS_API (FONAME_, TONAME_);

      IF NUM_SIGNED_HOLDERS > 0
      THEN
         R2 := TRUE;
      ELSE
         R2 := FALSE;
      END IF;

      --IF R1 = true then
      --DBMS_OUTPUT.PUT_LINE('R2 true' );
      --else
      --DBMS_OUTPUT.PUT_LINE('R2 false' );
      --end if;
      FUNC_MESSAGE := SINGLE_MESSAGE;

      -- All rules together
      IF (USER_EXISTS = TRUE AND R1 = TRUE AND R2 = TRUE AND (R0 <> 'FALSE') AND R4 = TRUE)
      THEN
         -- rule 3) verify user authorization
         R3 :=
            VERIFY_USER_AUTH_ON_WO_ACT (WONAME_,
                                        FONAME_,
                                        TONAME_,
                                        USER_); -- second user is the real one - removing sign

         IF R3 = 'FALSE'
         THEN
            SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
            RETURN 'FALSE';
         END IF;

         --RETURN LOC_USER;
         RETURN 'TRUE';
      ELSE
         RETURN 'FALSE';
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_WO_CAN_SIGN_OFF_API');
         RETURN SQLERRM;
   END NEK_WO_CAN_SIGN_OFF_API;


   PROCEDURE NEK_WO_SIGN_ON_API (
      WONAME_    IN     ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_    IN     ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_    IN     ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_      IN     USERLOG.USER_ID%TYPE,
      RESULT        OUT VARCHAR2,
      WORKINFO      OUT VARCHAR2)
   IS
      /********************************************************************************
      \ NEK_WO_SIGN_ON_API
      ********************************************************************************/
      CURSOR NUM_SIGN
      IS
         SELECT COUNT (*)
           FROM    ACT_WORK_ORDER_HOLDERS
                INNER JOIN
                   NEK_SIGN_TIMES NKT
                ON     NKT.TAG_OUT_ID = ACT_TAGOUT_NUMBER
                   AND NKT.TAG_OUT_NUMB_ID = ACT_TAGOUT_SECTION_NUMBER
          WHERE     WORK_ORDER_NUMBER = WONAME_
                AND TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(FONAME_) --CHECKED_FOLDER_TYPE_
                AND ACT_TAGOUT_NUMBER = FONAME_
                AND ACT_TAGOUT_SECTION_NUMBER = TONAME_
                AND NKT.WO_NUMBER = WONAME_;

      CNT          NUMBER (9);
      MARKTIME     TIMESTAMP;
      LOC_RESULT   VARCHAR2 (4000);
   BEGIN
      IF NEK_WO_CAN_SIGN_ON_API (WONAME_,
                                 FONAME_,
                                 TONAME_,
                                 USER_) = 'TRUE'
      THEN
         WORKINFO := 'WAL';
         --DBMS_OUTPUT.PUT_LINE('Getting the sign on! '  || WONAME_ ||FONAME_ || TONAME_||USER_);
         MARKTIME := SYSTIMESTAMP;

         -- Write sign on to WO
         INSERT INTO ACT_WORK_ORDER_HOLDERS (WORK_ORDER_NUMBER,
                                             TAGOUT_TYPE_ID,
                                             ACT_TAGOUT_NUMBER,
                                             ACT_TAGOUT_SECTION_NUMBER,
                                             USER_ID,
                                             SIGN_ON_DATE,
                                             SIGN_ON_BY,
                                             NOTES)
              VALUES (WONAME_,
                      NEK_CHECK_FOLDER_TYPE(FONAME_), --CHECKED_FOLDER_TYPE_,
                      FONAME_,
                      TONAME_,
                      USER_,
                      MARKTIME,
                      USER_,
                      '(eBS).');

         -- DBMS_OUTPUT.PUT_LINE('Tu bi moral biti insert.');

         NEK_AUDIT (
            'SIGN',
            NEK_CHECK_FOLDER_TYPE(FONAME_), --CHECKED_FOLDER_TYPE_,
            FONAME_,
            TONAME_,
            USER_,
               'User '
            || NEK_GET_USER_FULLNAME (USER_)
            || ' ('
            || TRIM (USER_)
            || ') had been signed ON to WO '
            || WONAME_
            || ' by eBS-eSOMS interface.');
         RESULT := OK_MESSAGE;
         LOC_RESULT := 'Sign on to WO successfull!';
      ELSE
         OPEN NUM_SIGN;

         FETCH NUM_SIGN INTO CNT;

         CLOSE NUM_SIGN;

         IF (TO_CHAR (CNT) != '0')
         THEN
            WORKINFO := 'WIP';
         ELSE
            WORKINFO := 'WNA';
         END IF;

         RESULT := FUNC_MESSAGE;
         LOC_RESULT := FUNC_MESSAGE;
      --   DBMS_OUTPUT.PUT_LINE('We are not signing! ' || FUNC_MESSAGE);
      END IF;

      NEK_LOCAL_AUDIT ('WO',
                       WONAME_,
                       FONAME_,
                       TONAME_,
                       USER_,
                       'ON',
                       LOC_RESULT);
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_WO_SIGN_ON_API');
         RESULT := SQLERRM;
   END NEK_WO_SIGN_ON_API;

   FUNCTION GET_SIGNED_USER_TO (
      WONAME_   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_     IN USERLOG.USER_ID%TYPE)
      RETURN VARCHAR2
   IS
      /********************************************************************************
      \ GET_SIGNED_USER
      ********************************************************************************/
      CURSOR cUSIG
      IS
         SELECT ASSIGNED_USER
           FROM NEK_SIGN_TIMES
          WHERE     WO_NUMBER = WONAME_
                AND TAG_OUT_ID = FONAME_
                AND TAG_OUT_NUMB_ID = TONAME_;

      SIGNED   NEK_SIGN_TIMES.ASSIGNED_USER%TYPE;
   BEGIN
      OPEN cUSIG;

      FETCH cUSIG INTO SIGNED;

      CLOSE cUSIG;

      RETURN SIGNED;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'GET_SIGNED_USER');
         RETURN SQLERRM;
   END GET_SIGNED_USER_TO;

   FUNCTION GET_SIGNED_USER_WO (
      WONAME_   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_     IN USERLOG.USER_ID%TYPE)
      RETURN VARCHAR2
   IS
      /********************************************************************************
      \ GET_SIGNED_USER
      ********************************************************************************/
      CURSOR cUSIG
      IS
         SELECT SIGN_ON_BY
           FROM ACT_WORK_ORDER_HOLDERS
          WHERE     WORK_ORDER_NUMBER = WONAME_
                AND ACT_TAGOUT_NUMBER = FONAME_
                AND ACT_TAGOUT_SECTION_NUMBER = TONAME_
                AND SIGN_OFF_DATE IS NULL;

      SIGNED   NEK_SIGN_TIMES.ASSIGNED_USER%TYPE;
   BEGIN
      OPEN cUSIG;

      FETCH cUSIG INTO SIGNED;

      CLOSE cUSIG;

      RETURN SIGNED;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'GET_SIGNED_USER');
         RETURN SQLERRM;
   END GET_SIGNED_USER_WO;

   PROCEDURE NEK_WO_SIGN_OFF_API (
      WONAME_   IN     ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_   IN     ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN     ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_     IN     USERLOG.USER_ID%TYPE,
      RESULT       OUT VARCHAR2)
   IS
      /********************************************************************************
      \ NEK_WO_SIGN_OFF_API
      ********************************************************************************/
      MARKTIME     TIMESTAMP (6);
      LOC_RESULT   VARCHAR2 (4000);
      FIN_USER     USERLOG.USER_ID%TYPE;
   BEGIN
      --DBMS_OUTPUT.PUT_LINE('LOC USER ' || USER_ );

      IF NEK_WO_CAN_SIGN_OFF_API (WONAME_,
                                  FONAME_,
                                  TONAME_,
                                  USER_) = 'TRUE'
      THEN
         FIN_USER :=
            TRIM (GET_SIGNED_USER_WO (WONAME_,
                                      FONAME_,
                                      TONAME_,
                                      USER_));

         --DBMS_OUTPUT.PUT_LINE('fin USER ' || FIN_USER );

         MARKTIME := SYSTIMESTAMP;

         -- Remove sign on to WO
         UPDATE ACT_WORK_ORDER_HOLDERS
            SET SIGN_OFF_DATE = MARKTIME,
                SIGN_OFF_BY = FIN_USER,
                WORK_COMPLETE = 1
          WHERE     WORK_ORDER_NUMBER = WONAME_
                AND TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(FONAME_) --CHECKED_FOLDER_TYPE_
                AND ACT_TAGOUT_NUMBER = FONAME_
                AND ACT_TAGOUT_SECTION_NUMBER = TONAME_
                AND USER_ID = FIN_USER
                AND SIGN_ON_BY = FIN_USER
                AND SIGN_ON_DATE IS NOT NULL;

         NEK_AUDIT (
            'SIGN',
            NEK_CHECK_FOLDER_TYPE(FONAME_), --CHECKED_FOLDER_TYPE_,
            FONAME_,
            TONAME_,
            FIN_USER,
               'User '
            || NEK_GET_USER_FULLNAME (FIN_USER)
            || ' ('
            || TRIM (FIN_USER)
            || ') had been signed OFF from WO '
            || WONAME_
            || ' by eBS-eSOMS interface.');

         RESULT := OK_MESSAGE;
         LOC_RESULT := 'Sign off from WO successfull!';
      ELSE
        IF LENGTH(NEK_CHECK_FOLDER_TYPE(FONAME_)) > 0 
        THEN
            FUNC_MESSAGE :=
               'Uporabnik '
            || USER_
            || ' ne more sprostiti osamitve na nivoju vodje del, ker ni poobla�cen za sprostitev ali ni sam prevzel te osamitve.';
         
            RESULT := FUNC_MESSAGE;
            --DBMS_OUTPUT.PUT_LINE('Res ' || RESULT);
            LOC_RESULT := FUNC_MESSAGE;
        ELSE 
            RESULT := FUNC_MESSAGE;
            LOC_RESULT := FUNC_MESSAGE;
        END IF;
      END IF;

      NEK_LOCAL_AUDIT ('WO',
                       WONAME_,
                       FONAME_,
                       TONAME_,
                       FIN_USER,
                       'OFF',
                       LOC_RESULT);
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_WO_SIGN_OFF_API');
         RESULT := SQLERRM;
   END NEK_WO_SIGN_OFF_API;

   PROCEDURE NEK_WO_SIGN (
      WONAME_      IN     ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_      IN     ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_      IN     ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_        IN     USERLOG.USER_ID%TYPE,
      REMOVE_SIG   IN     BOOLEAN,
      RESULT          OUT VARCHAR2,
      WORKINFO        OUT VARCHAR2)
   AS
   /********************************************************************************
   \ NEK_WO_SIGN - Sign Work Order / Remove signature wrapper function
   ********************************************************************************/
   BEGIN
      IF (REMOVE_SIG = FALSE)
      THEN
         NEK_WO_SIGN_ON_API (WONAME_,
                             FONAME_,
                             TONAME_,
                             USER_,
                             RESULT,
                             WORKINFO);
      ELSE
         -- DBMS_OUTPUT.PUT_LINE('SIGN OFF With ' || USER_ );
         NEK_WO_SIGN_OFF_API (WONAME_,
                              FONAME_,
                              TONAME_,
                              USER_,
                              RESULT);
         WORKINFO := '';
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_WO_SIGN');
         RESULT := SQLERRM;
   END NEK_WO_SIGN;

   PROCEDURE NEK_TO_SIGN (
      WORKORDER_NUM   IN     ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_         IN     ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_         IN     ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_           IN     USERLOG.USER_ID%TYPE,
      NOTES_          IN     VARCHAR2,
      REMOVE_SIG      IN     BOOLEAN,
      RESULT             OUT VARCHAR2,
      WORKINFO           OUT VARCHAR2)
   AS
   /********************************************************************************
   \ NEK_TO_SIGN - Sign Tagout / Remove signature wrapper function
   ********************************************************************************/
   BEGIN
      IF (REMOVE_SIG = FALSE)
      THEN
         NEK_TO_SIGN_ON_API (WORKORDER_NUM,
                             FONAME_,
                             TONAME_,
                             USER_,
                             NOTES_,
                             RESULT,
                             WORKINFO);
      ELSE
         NEK_TO_SIGN_OFF_API (WORKORDER_NUM,
                              FONAME_,
                              TONAME_,
                              USER_,
                              RESULT);
         WORKINFO := '';
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_TO_SIGN');
         RESULT := SQLERRM;
   END NEK_TO_SIGN;

   /* PRIVATE */
   PROCEDURE NEK_DELETE_WO_TIMESTAMP (
      MARKTIME   IN NEK_SIGN_TIMES.ESOMS_TIME%TYPE,
      FONAME_    IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_    IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      WONAME_    IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      USER_      IN USERLOG.USER_ID%TYPE)
   IS
   /********************************************************************************
   \ NEK_DELETE_WO_TIMESTAMP - Writes Timestamp for WO in TO signing (helper function with its table)
   ********************************************************************************/
   BEGIN
      DELETE FROM NEK_SIGN_TIMES
            WHERE     ESOMS_TIME = MARKTIME
                  AND TAG_OUT_ID = FONAME_
                  AND TAG_OUT_NUMB_ID = TONAME_
                  AND WO_NUMBER = WONAME_
                  AND ASSIGNED_USER = TRIM (USER_);
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_DELETE_WO_TIMESTAMP');
   END NEK_DELETE_WO_TIMESTAMP;

   FUNCTION NEK_GET_TIME_FROM_TOSIGN (
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      WONAME_   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      USER_     IN USERLOG.USER_ID%TYPE)
      RETURN TIMESTAMP
   IS
      /********************************************************************************
      \ NEK_GET_TIME_FROM_TOSING - Return Timestamp for WO in TO signing (helper function with its table)
      ********************************************************************************/
      CURSOR cGET_TIME
      IS
         SELECT ESOMS_TIME
           FROM NEK_SIGN_TIMES
          WHERE     TAG_OUT_ID = FONAME_
                AND TAG_OUT_NUMB_ID = TONAME_
                AND WO_NUMBER = WONAME_
                AND ASSIGNED_USER = TRIM (USER_);

      ESOMSTIME   NEK_SIGN_TIMES.ESOMS_TIME%TYPE;
   BEGIN
      OPEN cGET_TIME;

      FETCH cGET_TIME INTO ESOMSTIME;

      IF cGET_TIME%NOTFOUND
      THEN
         RETURN NULL;
      ELSE
         RETURN ESOMSTIME;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_GET_TIME_FROM_TOSING');
         RETURN NULL;
   END NEK_GET_TIME_FROM_TOSIGN;

   PROCEDURE NEK_WRITE_WO_TIMESTAMP (
      MARKTIME   IN TIMESTAMP,
      FONAME_    IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_    IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      WONAME_    IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      USER_      IN USERLOG.USER_ID%TYPE)
   IS
   /********************************************************************************
   \ NEK_WRITE_WO_TIMESTAMP - Writes Timestamp for WO in TO signing (helper function with its table)
   ********************************************************************************/
   BEGIN
      INSERT INTO NEK_SIGN_TIMES (ESOMS_TIME,
                                  TAG_OUT_ID,
                                  TAG_OUT_NUMB_ID,
                                  WO_NUMBER,
                                  ASSIGNED_USER)
           VALUES (MARKTIME,
                   FONAME_,
                   TONAME_,
                   WONAME_,
                   TRIM (USER_));
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_WRITE_WO_TIMESTAMP');
   END NEK_WRITE_WO_TIMESTAMP;

   FUNCTION NEK_WO_SIGNING_ON_ALLOWED_API (
      WONAME_   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_     IN USERLOG.USER_ID%TYPE)
      RETURN BOOLEAN
   IS
      /********************************************************************************
      \ NEK_WO_SIGNING_ALLOWED_API - checks if the required wo can be signed.
      ********************************************************************************/
      CURSOR cWO
      IS
         SELECT COUNT (*)
           FROM ACT_WORK_ORDER_HOLDERS
          WHERE     WORK_ORDER_NUMBER = WONAME_
                AND TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(FONAME_) --CHECKED_FOLDER_TYPE_
                AND ACT_TAGOUT_NUMBER = FONAME_
                AND ACT_TAGOUT_SECTION_NUMBER = TONAME_
                AND SIGN_ON_BY = USER_
                AND SIGN_OFF_BY IS NULL;

      WO_COUNT   NUMBER (9) := 0;
   BEGIN
      OPEN cWO;

      FETCH cWO INTO WO_COUNT;

      IF (LENGTH(NEK_CHECK_FOLDER_TYPE(FONAME_)) > 0)
      THEN
          IF TO_CHAR (WO_COUNT) = '0'
          THEN
             RETURN TRUE;
          ELSE
             --FUNC_MESSAGE := 'User ' || TRIM(USER_) || ' is already signed to workorder ' || TO_CHAR(WONAME_) || ' with tag out (' || FONAME_ || ') '|| TONAME_ || '.';
             FUNC_MESSAGE :=
                   'Vodja del '
                || TRIM (USER_)
                || ' je �e prevzel osamitev '
                || FONAME_
                || '/'
                || TONAME_
                || ' z operacijo '
                || TO_CHAR (WONAME_)
                || '!';
             RETURN FALSE;
          END IF;
      ELSE 
        RETURN FALSE;
      END IF;
        
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_WO_SIGNING_ALLOWED_API');
         RETURN FALSE;
   END NEK_WO_SIGNING_ON_ALLOWED_API;

   FUNCTION NEK_WO_SIGNING_OFF_ALLOWED_API (
      WONAME_   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      USER_     IN USERLOG.USER_ID%TYPE)
      RETURN VARCHAR2
   IS
      /********************************************************************************
      \ NEK_WO_SIGNING_OFF_ALLOWED_API - checks if the required wo can be signed.
      ********************************************************************************/
      CURSOR cWO
      IS
         SELECT COUNT (*)
           FROM ACT_WORK_ORDER_HOLDERS
          WHERE     WORK_ORDER_NUMBER = WONAME_
                AND TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(FONAME_) --CHECKED_FOLDER_TYPE_
                AND ACT_TAGOUT_NUMBER = FONAME_
                AND ACT_TAGOUT_SECTION_NUMBER = TONAME_
                AND SIGN_ON_BY = USER_
                AND SIGN_OFF_BY IS NULL;

      WO_COUNT         NUMBER (9) := 0;
      SIGNED           VARCHAR2 (50) := '';
      SELF_SIGNED      BOOLEAN := FALSE;
      ss               VARCHAR2 (10);
      SINGLE_MESSAGE   VARCHAR2 (4000);
   BEGIN
      OPEN cWO;

      FETCH cWO INTO WO_COUNT;

      -- Ali mu dovoli kdo drug?
      SIGNED :=
         VERIFY_USER_AUTH_ON_WO_ACT (WONAME_,
                                     FONAME_,
                                     TONAME_,
                                     USER_);

      -- DBMS_OUTPUT.PUT_LINE('SG '  || SIGNED);
      IF (SIGNED = USER_)
      THEN
         SELF_SIGNED := TRUE;
      END IF;

      IF SIGNED = 'FALSE'
      THEN
         SINGLE_MESSAGE := ADD_MSG (SINGLE_MESSAGE, FUNC_MESSAGE);
      END IF;

      IF (SELF_SIGNED = TRUE)
      THEN
         ss := 'T';
      ELSE
         ss := 'N';
      END IF;

      --DBMS_OUTPUT.PUT_LINE('SG '  || SIGNED  || 'WO '  ||  WO_COUNT  || 'SELF '  || ss  );

      IF (LENGTH(NEK_CHECK_FOLDER_TYPE(FONAME_)) > 0)
      THEN    
          IF TO_CHAR (WO_COUNT) = '0' AND (SELF_SIGNED OR SIGNED = 'FALSE')
          THEN
             SINGLE_MESSAGE :=
                ADD_MSG (
                   SINGLE_MESSAGE,
                      'Vodja del '
                   || TRIM (USER_)
                   || ' ni prevzel osamitev '
                   || FONAME_
                   || '/'
                   || TONAME_
                   || ' z operacijo '
                   || TO_CHAR (WONAME_)
                   || '!');
             FUNC_MESSAGE := SINGLE_MESSAGE;
             RETURN 'FALSE';
          ELSE
             --DBMS_OUTPUT.PUT_LINE('RET SG '  || SIGNED);
             RETURN SIGNED;
          END IF;
      ELSE 
        RETURN 'FALSE';
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_WO_SIGNING_ALLOWED_API');
         RETURN 'FALSE';
   END NEK_WO_SIGNING_OFF_ALLOWED_API;

   FUNCTION NEK_WO_EXISTS (
      WONAME_ IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE)
      RETURN BOOLEAN
   IS
      /********************************************************************************
      \ NEK_WO_EXISTS - Searches for the queried workorder
      ********************************************************************************/
      CURSOR cWO
      IS
         SELECT COUNT (*)
           FROM WORK_ORDERS
          WHERE WORK_ORDER_NUMBER = WONAME_;

      WO_COUNT   NUMBER (9) := 0;
   BEGIN
      OPEN cWO;

      FETCH cWO INTO WO_COUNT;

      IF TO_CHAR (WO_COUNT) = '0'
      THEN
         FUNC_MESSAGE := 'Operacija ' || WONAME_ || ' ne obstaja!';
         RETURN FALSE;
      ELSE
         RETURN TRUE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_WO_EXISTS');
         RETURN FALSE;
   END NEK_WO_EXISTS;

   FUNCTION NEK_FIND_USER_API (ID_ IN USERLOG.USER_ID%TYPE)
      RETURN BOOLEAN
   IS
      /********************************************************************************
      \ NEK_FIND_USER_API - Searches for a user with supplied ID.
      ********************************************************************************/
      CURSOR cUSER_ID
      IS
         SELECT USER_ID
           FROM USERLOG
          WHERE USERLOG.USER_ID = ID_;

      USER_ID_KEY   USERLOG.USER_ID%TYPE;

      HASIT         BOOLEAN;
   BEGIN
      OPEN cUSER_ID;

      FETCH cUSER_ID INTO USER_ID_KEY;

      IF cUSER_ID%ROWCOUNT = 0
      THEN
         HASIT := FALSE;
      ELSE
         HASIT := TRUE;
      END IF;

      CLOSE cUSER_ID;

      IF HASIT = FALSE
      THEN
         RETURN FALSE;
      ELSE
         RETURN TRUE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_FIND_USER_API');
         RETURN FALSE;
   END NEK_FIND_USER_API;

   FUNCTION IS_TAG_ON_TLIFT (
      FONAME_     IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TANUMBER_   IN VARCHAR2)
      RETURN VARCHAR2
   IS
      /********************************************************************************
      \ IS_TAG_ON_TLIFT - Checks if the queried tag is on the temporal lift
      ********************************************************************************/
      CURSOR cTLIFT
      IS
         SELECT COUNT (*)
           FROM TEMPORARY_LIFT_TAGS
          WHERE     TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(FONAME_) --CHECKED_FOLDER_TYPE_
                AND ACT_TAGOUT_NUMBER = FONAME_
                AND TAG_SERIAL_NUMBER = TANUMBER_;

      TLIFT   NUMBER (9) := 0;
   BEGIN
      OPEN cTLIFT;

      FETCH cTLIFT INTO TLIFT;

      IF (LENGTH(NEK_CHECK_FOLDER_TYPE(FONAME_)) > 0)
      THEN
          IF TO_CHAR (TLIFT) = '0'
          THEN
             RETURN 'FALSE';
          ELSE
             --FUNC_MESSAGE := '- Tag is on temporal lift.';
             FUNC_MESSAGE := 'Osamitvena kartica je pod zacasno odstranitvijo!';
             RETURN 'TRUE';
          END IF;
      ELSE 
        RETURN 'FALSE';
      END IF;
      
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'IS_TAG_ON_TLIFT');
         RETURN SQLERRM;
   END IS_TAG_ON_TLIFT;

   FUNCTION TAG_TLIFT_NUMBER (
      FONAME_     IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TANUMBER_   IN VARCHAR2)
      RETURN VARCHAR2
   IS
      /********************************************************************************
      \ TAG_TLIFT_NUMBER - Returns temporal lift number for the queried tag
      ********************************************************************************/
      CURSOR cTLIFT
      IS
         SELECT TEMP_LIFT_NUMBER
           FROM TEMPORARY_LIFT_TAGS
          WHERE     TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(FONAME_) --CHECKED_FOLDER_TYPE_
                AND ACT_TAGOUT_NUMBER = FONAME_
                AND TAG_SERIAL_NUMBER = TANUMBER_;

      TLIFT   TEMPORARY_LIFT_TAGS.TEMP_LIFT_NUMBER%TYPE;
   BEGIN
      OPEN cTLIFT;

      FETCH cTLIFT INTO TLIFT;

      RETURN TLIFT;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'TAG_TLIFT_NUMBER');
         RETURN SQLERRM;
   END TAG_TLIFT_NUMBER;

   FUNCTION IS_TAG_HUNG (
      FONAME_     IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_     IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      TANUMBER_   IN VARCHAR2)
      RETURN VARCHAR2
   IS
      /********************************************************************************
      \ IS_TAG_HUNG - Returns if a queried tag is hung or not
      ********************************************************************************/
      CURSOR cNUM_LEVEL_REACHED
      IS
         SELECT COUNT (*)
           FROM ACT_TAG_VERIF
          WHERE     ACT_TAGOUT_NUMBER = FONAME_
                AND TAG_SERIAL_NUMBER IN
                       (SELECT TAG_SERIAL_NUMBER
                          FROM ACT_TAGOUT_SECTION_TAGS
                         WHERE     TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(FONAME_) --CHECKED_FOLDER_TYPE_
                               AND ACT_TAGOUT_NUMBER = FONAME_
                               AND ACT_TAGOUT_SECTION_NUMBER = TONAME_
                               AND TAG_SERIAL_NUMBER = TANUMBER_)
                AND TAG_VERIF_LEVEL >= TAGS_VERIF_LEVEL_REQUIRED_;

      CURSOR cISREMOVED
      IS
         SELECT COUNT (*)
           FROM ACT_TAG_REST_VERIF
          WHERE     ACT_TAGOUT_NUMBER = FONAME_
                AND TAG_SERIAL_NUMBER IN
                       (SELECT TAG_SERIAL_NUMBER
                          FROM ACT_TAGOUT_SECTION_TAGS
                         WHERE     TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(FONAME_) --CHECKED_FOLDER_TYPE_
                               AND ACT_TAGOUT_NUMBER = FONAME_
                               AND ACT_TAGOUT_SECTION_NUMBER = TONAME_
                               AND TAG_SERIAL_NUMBER = TANUMBER_)
                AND TAG_VERIF_LEVEL >= TAGS_VERIF_LEVEL_REQUIRED_ * 2;

      NUM_LEVEL_REACHED       NUMBER (9) := 0;
      NUM_LEVEL_OUT_REACHED   NUMBER (9) := 0;
   BEGIN
      OPEN cNUM_LEVEL_REACHED;

      FETCH cNUM_LEVEL_REACHED INTO NUM_LEVEL_REACHED;

      CLOSE cNUM_LEVEL_REACHED;

      OPEN cISREMOVED;

      FETCH cISREMOVED INTO NUM_LEVEL_OUT_REACHED;

      CLOSE cISREMOVED;

      IF (NUM_LEVEL_REACHED > 0 AND NUM_LEVEL_OUT_REACHED > 0)
      THEN
         RETURN 'FALSE';
      ELSIF (NUM_LEVEL_REACHED = 0)
      THEN
         RETURN 'FALSE';
      ELSE
         RETURN 'TRUE';
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'IS_TAG_HUNG');
         RETURN SQLERRM;
   END IS_TAG_HUNG;

   FUNCTION USER_PASSWORD
      RETURN VARCHAR2
   IS
   /********************************************************************************
   \ USER_PASSWORD - Generate random user password
   ********************************************************************************/
   BEGIN
      RETURN '?';
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'USER_PASSWORD');
         RETURN SQLERRM;
   END USER_PASSWORD;

   PROCEDURE NEK_ADD_USER_API (ID_ IN USERLOG.USER_ID%TYPE)
   IS
      -- Changes done on 10.2.2012 --> eBS DB Link not working properly, semi-fix
      /********************************************************************************
      \ NEK_ADD_USER_API - Add user and puts it to the specified group.
      ********************************************************************************/
      --    EBS_RESULT XXEA_ESOMS_UTILITIES_PKG.t_person@xxesm_in;
      --    NAME_ VARCHAR2(30);
      --    EMAIL_ VARCHAR2(250);
      --
      --    NAMEU_ VARCHAR2(255);
      --    EMAILU_ VARCHAR2(255);
      --
      --    GRP_ VARCHAR(255);
      --BEGIN
      --
      --    -- If not the same...
      --    --GRP_ := DEFAULT_USER_DEPARTMENT_;
      --    GRP_ := USER_SEC_GROUP;
      --
      --    EBS_RESULT := XXEA_ESOMS_UTILITIES_PKG.get_person_data@xxesm_in(ID_);
      --
      --    IF USE_USER_ID = 1 THEN
      --        NAME_ := INITCAP(LOWER(EBS_RESULT.first_name)) || ' ' || INITCAP(LOWER(EBS_RESULT.last_name))  || ' ('  || TRIM(ID_) || ')';
      --    ELSE
      --        NAME_ := INITCAP(LOWER(EBS_RESULT.first_name)) || ' ' || INITCAP(LOWER(EBS_RESULT.last_name));
      --    END IF;
      --
      --    --DBMS_OUTPUT.PUT_LINE(TO_CHAR(USE_USER_ID));
      --
      --    EMAIL_ := EBS_RESULT.email;
      --
      --    -- User gets added
      --    IF (NAME_ IS NULL OR NAME_ = '') THEN
      --         NAMEU_ := 'Missing in eBS';
      --    ELSE
      --        NAMEU_ := NAME_;
      --    END IF;
      --    IF (EMAIL_ IS NULL OR EMAIL_ = '')  THEN
      --        EMAILU_ := 'esoms.support@nek.si';
      --    ELSE
      --        EMAILU_ := EMAIL_;
      --    END IF;
      --
      --    INSERT INTO USERLOG(
      --        USER_ID,
      --        LOGIN_ID,
      --        PASSWORD,
      --        USER_STATUS,
      --        NAME,
      --        DEPT,
      --        EMAIL)
      --    VALUES (
      --        ID_,
      --        ID_,
      --        USER_PASSWORD,
      --        1,
      --        NAMEU_,
      --        DEFAULT_USER_DEPARTMENT_,
      --        SUBSTR(EMAILU_, 1, 50)
      --    );
      --    NEK_AUDIT('USER', ID_, 'New User added by eBS-eSOMS interface.');
      --    NEK_AUDIT('PASS', ID_, 'Password assigned by eBS-eSOMS interface (EBS-ESM).');
      --    -- User gets added to the default group
      --
      --    INSERT INTO USER_GROUP_USERS(
      --        USER_GROUP_ID,
      --        USER_ID)
      --    VALUES (
      --        GRP_,
      --        ID_
      --    );
      --
      --    NEK_AUDIT('GROUP', ID_ , 'User ' || INITCAP(LOWER(EBS_RESULT.first_name)) || ' ' || INITCAP(LOWER(EBS_RESULT.last_name)) || ' (' || TRIM(ID_) || ') assigned to User Group ' || GRP_ ||  ' by eBS-eSOMS Interface.');
      --    NEK_AUDIT('GROUPLOG', GRP_ , INITCAP(LOWER(EBS_RESULT.first_name)) || ' ' || INITCAP(LOWER(EBS_RESULT.last_name)) || ' (' || TRIM(ID_) || ') assigned to User Group ' || GRP_ ||  ' by eBS-eSOMS Interface.');
      --
      --    -- copies all grants for the group to the user
      --    INSERT INTO USER_LEVELS(
      --        USER_ID,
      --        APPL,
      --        LNUM)
      --        (
      --        SELECT ID_,
      --                    APPL,
      --                    LNUM
      --        FROM USER_LEVELS
      --        WHERE  USER_ID = GRP_
      --        );
      --
      --EXCEPTION
      --    WHEN OTHERS THEN
      --        NEK_LOG(SQLERRM || ' (' || SQLCODE || ').', 'NEK_ADD_USER_API', 'USER ID: ' || ID_);

      NAME_     VARCHAR2 (30);
      EMAIL_    VARCHAR2 (250);
      NAMEU_    VARCHAR2 (255);
      EMAILU_   VARCHAR2 (255);
      GRP_      VARCHAR (255);
   BEGIN
      GRP_ := USER_SEC_GROUP;

      IF USE_USER_ID = 1
      THEN
         NAME_ :=
               INITCAP (LOWER ('First Name'))
            || ' '
            || INITCAP (LOWER ('Last Name'))
            || ' ('
            || TRIM (ID_)
            || ')';
      ELSE
         NAME_ :=
               INITCAP (LOWER ('First Name'))
            || ' '
            || INITCAP (LOWER ('Last Name'));
      END IF;

      -- User gets added
      IF (NAME_ IS NULL OR NAME_ = '')
      THEN
         NAMEU_ := 'Missing in eBS';
      ELSE
         NAMEU_ := NAME_;
      END IF;

      IF (EMAIL_ IS NULL OR EMAIL_ = '')
      THEN
         EMAILU_ := 'esoms.support@nek.si';
      ELSE
         EMAILU_ := EMAIL_;
      END IF;

      INSERT INTO USERLOG (USER_ID,
                           LOGIN_ID,
                           PASSWORD,
                           USER_STATUS,
                           NAME,
                           DEPT,
                           EMAIL)
           VALUES (ID_,
                   ID_,
                   USER_PASSWORD,
                   1,
                   NAMEU_,
                   DEFAULT_USER_DEPARTMENT_,
                   SUBSTR (EMAILU_, 1, 50));

      NEK_AUDIT ('USER', ID_, 'New User added by eBS-eSOMS interface.');
      NEK_AUDIT ('PASS',
                 ID_,
                 'Password assigned by eBS-eSOMS interface (EBS-ESM).');

      -- User gets added to the default group

      INSERT INTO USER_GROUP_USERS (USER_GROUP_ID, USER_ID)
           VALUES (GRP_, ID_);

      NEK_AUDIT (
         'GROUP',
         ID_,
            'User '
         || INITCAP (LOWER ('First Name'))
         || ' '
         || INITCAP (LOWER ('Last Name'))
         || ' ('
         || TRIM (ID_)
         || ') assigned to User Group '
         || GRP_
         || ' by eBS-eSOMS Interface.');
      NEK_AUDIT (
         'GROUPLOG',
         GRP_,
            INITCAP (LOWER ('First Name'))
         || ' '
         || INITCAP (LOWER ('Last Name'))
         || ' ('
         || TRIM (ID_)
         || ') assigned to User Group '
         || GRP_
         || ' by eBS-eSOMS Interface.');

      -- copies all grants for the group to the user
      INSERT INTO USER_LEVELS (USER_ID, APPL, LNUM)
         (SELECT ID_, APPL, LNUM
            FROM USER_LEVELS
           WHERE USER_ID = GRP_);
   --/* */ ****************
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_ADD_USER_API',
                  'USER ID: ' || ID_);
   END NEK_ADD_USER_API;

   FUNCTION NEK_TO_VERIFICATOR (
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE)
      RETURN VARCHAR2
   IS
      /********************************************************************************
      \ NEK_TO_VERIFICATOR - return verificator (user) on tagout.
      ********************************************************************************/
      CURSOR cVERIF
      IS
         SELECT USER_ID
           FROM ACT_TAGOUT_SECTION_VERIF
          WHERE     ACT_TAGOUT_NUMBER = FONAME_
                AND ACT_TAGOUT_SECTION_NUMBER = TONAME_
                AND SECTION_VERIF_LEVEL = 1;

      USER_NAME   USERLOG.USER_ID%TYPE;
      RETVALUE    USERLOG.USER_ID%TYPE;
   BEGIN
      OPEN cVERIF;

      FETCH cVERIF INTO USER_NAME;

      IF cVERIF%ROWCOUNT = 0
      THEN
         RETVALUE := DEFAULT_USER_;
      ELSE
         RETVALUE := USER_NAME;
      END IF;

      CLOSE cVERIF;

      RETURN RETVALUE;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_TO_VERIFICATOR');
         RETURN SQLERRM;
   END NEK_TO_VERIFICATOR;

   FUNCTION NEK_TAGOUT_SIGNING_ALLOWED_API (
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE)
      RETURN BOOLEAN
   IS
   /********************************************************************************
   \ NEK_TAGOUT_SIGNING_ALLOWED_API - checks if the required tag out can be (multi)signed.
   ********************************************************************************/
   BEGIN
      RETURN TRUE;                                                     -- test
   -- NEED RULES
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_TAGOUT_SIGNING_ALLOWED_API');
         RETURN FALSE;
   END NEK_TAGOUT_SIGNING_ALLOWED_API;

   FUNCTION NEK_NUM_TO_SIGNED_HOLDERS_API (
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE)
      RETURN NUMBER
   IS
      /********************************************************************************
      \ NEK_NUM_TO_SIGNED_HOLDERS_API - return number of holders on TO
      ********************************************************************************/
      CURSOR cTO_HASHOLDER
      IS
         SELECT COUNT (*)
           FROM ACT_TAGOUT_SECTION_HOLDERS
          WHERE     TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(FONAME_) --CHECKED_FOLDER_TYPE_
                AND ACT_TAGOUT_NUMBER = FONAME_
                AND ACT_TAGOUT_SECTION_NUMBER = TONAME_
                AND SIGNED_ON_BY IS NOT NULL
                AND (RELEASED_BY IS NULL AND WORK_COMPLETE IS NULL);

      cFIELD   NUMBER (9);
   BEGIN
      OPEN cTO_HASHOLDER;

      FETCH cTO_HASHOLDER INTO cFIELD;

      IF cFIELD = 1 AND LENGTH(NEK_CHECK_FOLDER_TYPE(FONAME_)) > 0
      THEN
         --FUNC_MESSAGE := '- Last signed holder on Tag out.';
         FUNC_MESSAGE := '- Zadnji nosilec osamitve (koordinator).';
      END IF;

      RETURN cFIELD;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_NUM_TO_SIGNED_HOLDERS_API');
         RETURN -1;
   END NEK_NUM_TO_SIGNED_HOLDERS_API;

   FUNCTION NEK_IS_TO_HOLDER_SIGNED_API (
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE)
      RETURN BOOLEAN
   IS
      /********************************************************************************
      \ NEK_IS_TO_HOLDER_SIGNED_API - checks if the selected Tag out has a holder or not
      ********************************************************************************/
      CURSOR cTO_HASHOLDER
      IS
         SELECT ACT_TAGOUT_SECTION_NUMBER
           FROM ACT_TAGOUT_SECTION_HOLDERS
          WHERE     TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(FONAME_) --CHECKED_FOLDER_TYPE_
                AND ACT_TAGOUT_NUMBER = FONAME_
                AND ACT_TAGOUT_SECTION_NUMBER = TONAME_
                AND SIGNED_ON_BY IS NOT NULL
                AND (RELEASED_BY IS NULL AND WORK_COMPLETE IS NULL);

      cFIELD   ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE;
   BEGIN
      OPEN cTO_HASHOLDER;

      FETCH cTO_HASHOLDER INTO cFIELD;

      IF cTO_HASHOLDER%ROWCOUNT = 0
      THEN
         RETURN FALSE;
      ELSE
         RETURN TRUE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_IS_TO_HOLDER_SIGNED_API');
         RETURN FALSE;
   END NEK_IS_TO_HOLDER_SIGNED_API;

   FUNCTION NEK_IS_WO_HOLDER_SIGNED_API (
      WONAME_ IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE)
      RETURN BOOLEAN
   IS
      /********************************************************************************
      \ NEK_IS_WO_HOLDER_SIGNED_API - Checks if there is a holder of the selected Work order
      ********************************************************************************/
      CURSOR cWO_HASHOLDER
      IS
         SELECT WORK_ORDER_NUMBER
           FROM ACT_WORK_ORDER_HOLDERS
          WHERE     WORK_ORDER_NUMBER = WONAME_
                AND SIGN_ON_DATE IS NOT NULL
                AND (SIGN_OFF_DATE IS NULL AND SIGN_OFF_BY IS NULL)
                AND (WORK_COMPLETE IS NULL OR WORK_COMPLETE != 1);

      cFIELD   ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE;
   BEGIN
      OPEN cWO_HASHOLDER;

      FETCH cWO_HASHOLDER INTO cFIELD;

      CLOSE cWO_HASHOLDER;

      IF cWO_HASHOLDER%ROWCOUNT = 0
      THEN
         RETURN FALSE;
      ELSE
         --FUNC_MESSAGE := '- There is a Work order holder.';
         FUNC_MESSAGE :=
            'Osamitev je prevzeta s strani enega ali vec vodji del!';
         RETURN TRUE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_IS_WO_HOLDER_SIGNED_API');
         RETURN FALSE;
   END NEK_IS_WO_HOLDER_SIGNED_API;

   FUNCTION NEK_IS_WO_HOLDER_SIGNED_API (
      WONAME_   IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE)
      RETURN BOOLEAN
   IS
      /********************************************************************************
      \ NEK_IS_WO_HOLDER_SIGNED_API - Checks if there is a holder of the selected Work order on the TO
      ********************************************************************************/
      CURSOR cWO_HASHOLDER
      IS
         SELECT WORK_ORDER_NUMBER
           FROM ACT_WORK_ORDER_HOLDERS
          WHERE     WORK_ORDER_NUMBER = WONAME_
                AND ACT_TAGOUT_NUMBER = FONAME_
                AND ACT_TAGOUT_SECTION_NUMBER = TONAME_
                AND SIGN_ON_DATE IS NOT NULL
                AND (SIGN_OFF_DATE IS NULL AND SIGN_OFF_BY IS NULL)
                AND (WORK_COMPLETE IS NULL OR WORK_COMPLETE != 1);

      cFIELD   ACT_WORK_ORDER_HOLDERS.WORK_ORDER_NUMBER%TYPE;
   BEGIN
      OPEN cWO_HASHOLDER;

      FETCH cWO_HASHOLDER INTO cFIELD;

      IF cWO_HASHOLDER%ROWCOUNT = 0
      THEN
         RETURN FALSE;
      ELSE
         --FUNC_MESSAGE := '- There is a Work order holder.';
         FUNC_MESSAGE :=
               'Osamitev '
            || FONAME_
            || '/'
            || TONAME_
            || ' je prevzeta s strani enega ali vec vodji del!';
         RETURN TRUE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_IS_WO_HOLDER_SIGNED_API');
         RETURN FALSE;
   END NEK_IS_WO_HOLDER_SIGNED_API;

   FUNCTION NEK_IS_WO_HOLDER_SIGNED_API (
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE)
      RETURN BOOLEAN
   IS
      /********************************************************************************
      \ NEK_IS_WO_HOLDER_SIGNED_API - Checks if there is a holder of the selected Work order on the TO
      ********************************************************************************/
      CURSOR cWO_HASHOLDER
      IS
         SELECT WORK_ORDER_NUMBER
           FROM ACT_WORK_ORDER_HOLDERS
          WHERE     ACT_TAGOUT_NUMBER = FONAME_
                AND ACT_TAGOUT_SECTION_NUMBER = TONAME_
                AND SIGN_ON_DATE IS NOT NULL
                AND (SIGN_OFF_DATE IS NULL AND SIGN_OFF_BY IS NULL)
                AND (WORK_COMPLETE IS NULL OR WORK_COMPLETE != 1);

      cFIELD   ACT_WORK_ORDER_HOLDERS.WORK_ORDER_NUMBER%TYPE;
   BEGIN
      OPEN cWO_HASHOLDER;

      FETCH cWO_HASHOLDER INTO cFIELD;

      IF cWO_HASHOLDER%ROWCOUNT = 0
      THEN
         RETURN FALSE;
      ELSE
         --FUNC_MESSAGE := '- There is a Work order holder.';
         FUNC_MESSAGE :=
               'Osamitev '
            || FONAME_
            || '/'
            || TONAME_
            || ' je prevzeta s strani enega ali vec vodji del!';
         RETURN TRUE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_IS_WO_HOLDER_SIGNED_API');
         RETURN FALSE;
   END NEK_IS_WO_HOLDER_SIGNED_API;

   FUNCTION NEK_VERIFY_TAGS_API (
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE)
      RETURN BOOLEAN
   IS
      /********************************************************************************
      \ NEK_VERIFY_TAGS_API - Function will verify if the tag placements on the tagout are all verified n-times by users
      ********************************************************************************/
      CURSOR cNUM_LEVEL_REACHED
      IS
         SELECT COUNT (*)
           FROM ACT_TAG_VERIF
          WHERE     ACT_TAGOUT_NUMBER = FONAME_
                AND TAG_SERIAL_NUMBER IN
                       (SELECT TAG_SERIAL_NUMBER
                          FROM ACT_TAGOUT_SECTION_TAGS
                         WHERE     TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(FONAME_) --CHECKED_FOLDER_TYPE_
                               AND ACT_TAGOUT_NUMBER = FONAME_
                               AND ACT_TAGOUT_SECTION_NUMBER = TONAME_)
                AND TAG_VERIF_LEVEL >= TAGS_VERIF_LEVEL_REQUIRED_;

      CURSOR cNUM_ALL_TAGS
      IS
         SELECT COUNT (*)
           FROM ACT_TAGOUT_SECTION_TAGS
          WHERE     TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(FONAME_) --CHECKED_FOLDER_TYPE_
                AND ACT_TAGOUT_NUMBER = FONAME_
                AND ACT_TAGOUT_SECTION_NUMBER = TONAME_;

      NUM_LEVEL_REACHED   NUMBER (6);
      NUM_ALL_TAGS        NUMBER (6);
   BEGIN
      OPEN cNUM_ALL_TAGS;

      FETCH cNUM_ALL_TAGS INTO NUM_ALL_TAGS;

      OPEN cNUM_LEVEL_REACHED;

      FETCH cNUM_LEVEL_REACHED INTO NUM_LEVEL_REACHED;

      IF NUM_ALL_TAGS = 0
      THEN
         IF NO_TAGS_OK_ = FALSE
         THEN
            FUNC_MESSAGE := '- There are no tags to verify.'; -- NO TAGS TO VERIFY
         END IF;

         RETURN NO_TAGS_OK_;
      END IF;

      IF NUM_ALL_TAGS > NUM_LEVEL_REACHED
      THEN
         --FUNC_MESSAGE := '- Tags are not hung and/or verified.'; -- TAGS ARE NOT HUNG AND/OR VERIFIED
         FUNC_MESSAGE :=
               'Operaciji z osamitvijo '
            || FONAME_
            || '/'
            || TONAME_
            || ' ni bilo podano dovoljenje za delo!';
         RETURN FALSE;
      ELSE
         RETURN TRUE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_VERIFY_TAGS_API');
         RETURN FALSE;
   END NEK_VERIFY_TAGS_API;

   FUNCTION NEK_TAGOUT_VERIF_INDEX_OK (
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      INDEX_    IN NUMBER)
      RETURN BOOLEAN
   IS
      /********************************************************************************
      \ NEK_TAGOUT_VERIF_INDEX_OK -
      ********************************************************************************/
      CURSOR cINDEX
        IS
            SELECT 
                SUM(SECTION_INTERNAL_LEVEL)
            FROM 
                ACT_TAGOUT_SECTION_VERIF ATSV
            INNER JOIN 
                SECTION_VERIF_LEVELS SCL ON
                ATSV.TAGOUT_TYPE_ID = SCL.TAGOUT_TYPE_ID AND
                ATSV.SECTION_VERIF_LEVEL = SCL.SECTION_VERIF_LEVEL
            WHERE
                ATSV.TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(FONAME_) AND
                ATSV.ACT_TAGOUT_NUMBER = FONAME_ AND 
                ATSV.ACT_TAGOUT_SECTION_NUMBER = TONAME_;

      cNUM   SECTION_VERIF_LEVELS.SECTION_INTERNAL_LEVEL%TYPE;
   BEGIN
      OPEN cINDEX;

      FETCH cINDEX INTO cNUM;

      CLOSE cINDEX;

      IF cNUM >= INDEX_
      THEN
         --FUNC_MESSAGE := '- Tag out''s verification index is equal or higher than required for function';
         FUNC_MESSAGE :=
               'Osamitev '
            || FONAME_
            || '/'
            || TONAME_
            || ' je v fazi odstranjevanja!';
         RETURN FALSE;
      ELSE
         RETURN TRUE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_TAGOUT_VERIF_INDEX_OK');
         RETURN FALSE;
   END NEK_TAGOUT_VERIF_INDEX_OK;

   FUNCTION NEK_TO_VERIF_EX_INDEX_OK (
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
      INDEX_    IN NUMBER)
      RETURN BOOLEAN
   IS
      /********************************************************************************
      \ NEK_TAGOUT_VERIF_EX_INDEX_OK - Exact!
      ********************************************************************************/
      CURSOR cINDEX
      IS
        SELECT 
            SUM(SECTION_INTERNAL_LEVEL)
        FROM 
            ACT_TAGOUT_SECTION_VERIF ATSV
        INNER JOIN 
            SECTION_VERIF_LEVELS SCL ON
            ATSV.TAGOUT_TYPE_ID = SCL.TAGOUT_TYPE_ID AND
            ATSV.SECTION_VERIF_LEVEL = SCL.SECTION_VERIF_LEVEL
        WHERE
            ATSV.TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(FONAME_) AND
            ATSV.ACT_TAGOUT_NUMBER = FONAME_ AND 
            ATSV.ACT_TAGOUT_SECTION_NUMBER = TONAME_;


      cNUM   SECTION_VERIF_LEVELS.SECTION_INTERNAL_LEVEL%TYPE;
   BEGIN
      OPEN cINDEX;

      FETCH cINDEX INTO cNUM;

      CLOSE cINDEX;

      IF TO_CHAR (cNUM) = TO_CHAR (INDEX_)
      THEN
         RETURN TRUE;
      ELSE
         RETURN FALSE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_TO_VERIF_EX_INDEX_OK');
         RETURN FALSE;
   END NEK_TO_VERIF_EX_INDEX_OK;

   FUNCTION NEK_IS_TAGOUT_LOCKED_API (
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE)
      RETURN BOOLEAN
   IS
      /********************************************************************************
      \ NEK_IS_TAGOUT_LOCKED_API - Checks if there is an operations hold on a tagout in a folder
      ********************************************************************************/
      CURSOR cTAG_LOCK
      IS
         SELECT ACT_TAGOUT_SECTION_NUMBER,
                SIGN_ON_LOCK_USER_ID,
                SIGN_ON_LOCK_REASON
           FROM ACT_TAGOUT_SECTIONS
          WHERE     TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(FONAME_) --CHECKED_FOLDER_TYPE_
                AND ACT_TAGOUT_NUMBER = FONAME_
                AND ACT_TAGOUT_SECTION_NUMBER = TONAME_
                AND (    SIGN_ON_LOCK_USER_ID IS NOT NULL
                     AND SIGN_ON_LOCK_DATE IS NOT NULL)
                AND ARCHIVED = 0;

      cFIELD    ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE;
      cUSER     ACT_TAGOUT_SECTIONS.SIGN_ON_LOCK_USER_ID%TYPE;
      cREASON   ACT_TAGOUT_SECTIONS.SIGN_ON_LOCK_REASON%TYPE;
   BEGIN
      OPEN cTAG_LOCK;

      FETCH cTAG_LOCK
      INTO cFIELD, cUSER, cREASON;

      IF cTAG_LOCK%ROWCOUNT = 0
      THEN
         RETURN FALSE;
      ELSE
         --FUNC_MESSAGE := '- Tag out is locked by user '  || NEK_GET_USER_FULLNAME(cUSER) ||  ' (' || TRIM(cUSER) || ') with reason: ' || cREASON ; -- TO Is locked
         FUNC_MESSAGE :=
               NEK_GET_USER_FULLNAME (cUSER)
            || ' ('
            || TRIM (cUSER)
            || ') je blokiral prevzemanje osamitve '
            || FONAME_
            || '/'
            || TONAME_
            || '! Razlog: '
            || cREASON;
         RETURN TRUE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_IS_TAGOUT_LOCKED_API');
         RETURN FALSE;
   END NEK_IS_TAGOUT_LOCKED_API;

   FUNCTION NEK_TAGOUT_LEVEL_REACHED_API (
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE)
      RETURN BOOLEAN
   IS
      /********************************************************************************
      \ NEK_TAGOUT_LEVEL_REACHED_API - Checks if selected tag out had reached required sign level
      ********************************************************************************/             
      CURSOR cTOLevel
      IS                
        SELECT 
            SUM(SECTION_INTERNAL_LEVEL)
        FROM 
            ACT_TAGOUT_SECTION_VERIF ATSV
        INNER JOIN 
            SECTION_VERIF_LEVELS SCL ON
            ATSV.TAGOUT_TYPE_ID = SCL.TAGOUT_TYPE_ID AND
            ATSV.SECTION_VERIF_LEVEL = SCL.SECTION_VERIF_LEVEL
        WHERE
            ATSV.TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(FONAME_) AND
            ATSV.ACT_TAGOUT_NUMBER = FONAME_ AND 
            ATSV.ACT_TAGOUT_SECTION_NUMBER = TONAME_;

      TAG_GOT_VERIF_LEVEL SECTION_VERIF_LEVELS.SECTION_INTERNAL_LEVEL%TYPE;
   BEGIN
      OPEN cTOLevel;

      FETCH cTOLevel INTO TAG_GOT_VERIF_LEVEL;

      IF (TAG_GOT_VERIF_LEVEL IS NULL AND LENGTH(NEK_CHECK_FOLDER_TYPE(FONAME_))>0)
      THEN
         --FUNC_MESSAGE := '- Verification has not begun yet.'; -- There are no VF levels yet.
         FUNC_MESSAGE :=
               'Osamitev '
            || FONAME_
            || '/'
            || TONAME_
            || ' je v procesu odobritve dovoljenja za delo!';
         RETURN FALSE;
      END IF;

      IF (TAG_GOT_VERIF_LEVEL < TAG_VERIF_BEGIN_VAL AND LENGTH(NEK_CHECK_FOLDER_TYPE(FONAME_))>0)
      THEN
         --FUNC_MESSAGE := '- Verification levels are not reached.'; -- There are VF Levels, but not reached.
         FUNC_MESSAGE :=
               'Osamitev '
            || FONAME_
            || '/'
            || TONAME_
            || ' je v procesu odobritve dovoljenja za delo!';
         RETURN FALSE;
      ELSE
         RETURN TRUE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_TAGOUT_LEVEL_REACHED_API');
         RETURN FALSE;
   END NEK_TAGOUT_LEVEL_REACHED_API;

   FUNCTION NEK_TAGOUT_FINISHED_WO_API (
      FONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE)
      RETURN BOOLEAN
   IS
      /********************************************************************************
      \ NEK_TAGOUT_FINISHED_WO_API - Checks if all workorders on a single TO are finished
      ********************************************************************************/
      CURSOR cALL_WOS_IN_TO
      IS
         SELECT COUNT (*)
           FROM ACT_TAGOUT_SECTION_WORK_ORDERS
          WHERE     ACT_TAGOUT_NUMBER = FONAME_
                AND ACT_TAGOUT_SECTION_NUMBER = TONAME_;

      CURSOR cFIN_WOS_IN_TO
      IS
         SELECT COUNT (*)
           FROM ACT_TAGOUT_SECTION_WORK_ORDERS
          WHERE     ACT_TAGOUT_NUMBER = FONAME_
                AND ACT_TAGOUT_SECTION_NUMBER = TONAME_
                AND (    WORK_COMPLETE_DATE IS NOT NULL
                     AND WORK_COMPLETE_USER_ID IS NOT NULL);

      ALL_WOS             NUMBER (6);
      ALL_WOS_COMPLETED   NUMBER (6);
   BEGIN
      OPEN cALL_WOS_IN_TO;

      FETCH cALL_WOS_IN_TO INTO ALL_WOS;

      OPEN cFIN_WOS_IN_TO;

      FETCH cFIN_WOS_IN_TO INTO ALL_WOS_COMPLETED;

      IF ALL_WOS > ALL_WOS_COMPLETED
      THEN
         RETURN FALSE;
      ELSE
         RETURN TRUE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_TAGOUT_FINISHED_WO_API');
         RETURN FALSE;
   END NEK_TAGOUT_FINISHED_WO_API;

   FUNCTION NEK_WORKORDER_FINISHED_TO_API (
      WONAME_ IN ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE)
      RETURN BOOLEAN
   IS
      /********************************************************************************
      \ NEK_WORKORDER_FINISHED_TO_API - Checks if all TOs on a single WO are finished
      ********************************************************************************/
      CURSOR cALL_WOS
      IS
         SELECT COUNT (*)
           FROM ACT_TAGOUT_SECTION_WORK_ORDERS
          WHERE WORK_ORDER_NUMBER = WONAME_;

      CURSOR cFIN_WOS
      IS
         SELECT COUNT (*)
           FROM ACT_TAGOUT_SECTION_WORK_ORDERS
          WHERE     WORK_ORDER_NUMBER = WONAME_
                AND (    WORK_COMPLETE_DATE IS NOT NULL
                     AND WORK_COMPLETE_USER_ID IS NOT NULL);

      ALL_WOS             NUMBER (6);
      ALL_WOS_COMPLETED   NUMBER (6);
   BEGIN
      OPEN cALL_WOS;

      FETCH cALL_WOS INTO ALL_WOS;

      OPEN cFIN_WOS;

      FETCH cFIN_WOS INTO ALL_WOS_COMPLETED;

      IF ALL_WOS > ALL_WOS_COMPLETED
      THEN
         RETURN FALSE;
      ELSE
         RETURN TRUE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_TAGOUT_FINISHED_WO_API');
         RETURN FALSE;
   END NEK_WORKORDER_FINISHED_TO_API;

   PROCEDURE NEK_AUDIT (TYPE_ IN VARCHAR2, KEY_ IN CHAR, VAL_ IN VARCHAR2)
   IS
   /********************************************************************************
   \ NEK_AUDIT
   ********************************************************************************/
   BEGIN
      IF TYPE_ = 'USER'
      THEN
         INSERT INTO USERLOG_CHANGES (USER_ID, CHANGE_DATE, CHANGE_TEXT)
              VALUES (KEY_, SYSTIMESTAMP, TRIM (SUBSTR (VAL_, 1, 4000)));
      ELSIF TYPE_ = 'GROUP'
      THEN -- mora biti loceno, saj ko se naredi ZUNAJ (ne smemo tu, ker potem ne dela preko dblink) dobi isti time tag, kar breakne key
         INSERT INTO USERLOG_CHANGES (USER_ID, CHANGE_DATE, CHANGE_TEXT)
              VALUES (
                        KEY_,
                        SYSTIMESTAMP + (1 / 86400),
                        TRIM (SUBSTR (VAL_, 1, 4000)));
      ELSIF TYPE_ = 'GROUPLOG'
      THEN
         INSERT
           INTO USER_GROUP_CHANGES (USER_GROUP_ID, CHANGE_DATE, CHANGE_TEXT)
         VALUES (
                   KEY_,
                   SYSTIMESTAMP + (2 / 86400),
                   TRIM (SUBSTR (VAL_, 1, 4000)));
      ELSIF TYPE_ = 'PASS'
      THEN
         INSERT INTO LOGINS (LOGIN_ID,
                             PASSWORD,
                             LOGIN_DT,
                             LOGIN_STATUS,
                             LOGIN_NOTES)
              VALUES (KEY_,
                      '?',
                      SYSTIMESTAMP + (3 / 86400),
                      1,
                      VAL_);
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_AUDIT');
   END NEK_AUDIT;

   PROCEDURE NEK_AUDIT (TYPE_     IN VARCHAR2,
                        TOTYPE_   IN VARCHAR2,
                        FONAME_   IN VARCHAR2,
                        TONAME_   IN VARCHAR2,
                        KEY_      IN CHAR,
                        VAL_      IN VARCHAR2)
   IS
   /********************************************************************************
   \ NEK_AUDIT
   ********************************************************************************/
   BEGIN
      LOOP_COUNT := LOOP_COUNT + 1;

      IF TYPE_ = 'SIGN'
      THEN
         INSERT INTO ACT_TAGOUT_SECTION_CHANGES (TAGOUT_TYPE_ID,
                                                 ACT_TAGOUT_NUMBER,
                                                 ACT_TAGOUT_SECTION_NUMBER,
                                                 USER_ID,
                                                 CHANGE_DATE,
                                                 CHANGE_TEXT)
              VALUES (TOTYPE_,
                      FONAME_,
                      TONAME_,
                      KEY_,
                      SYSTIMESTAMP + (LOOP_COUNT / 86400),
                      TRIM (SUBSTR (VAL_, 1, 4000)));
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_AUDIT');
   END NEK_AUDIT;

   PROCEDURE NEK_LOCAL_AUDIT (OPTYPE_     IN VARCHAR2,
                              WONAME_     IN VARCHAR2,
                              FONAME_     IN VARCHAR2,
                              TONAME_     IN VARCHAR2,
                              USER_       IN CHAR,
                              SIGONOFF_   IN VARCHAR2,
                              VAL_        IN VARCHAR2)
   IS
   /********************************************************************************
   \ LOCAL AUDIT
   ********************************************************************************/
   BEGIN
      INSERT INTO NEK_SIGN_LOG (OPERATION_TYPE,
                                WORK_ORDER_NUM,
                                TAGOUT_NUM,
                                TAGOUT_SEC_NUM,
                                USER_ID,
                                DATETIME,
                                SIGN_ON_OFF,
                                REASON)
           VALUES (OPTYPE_,
                   WONAME_,
                   FONAME_,
                   TONAME_,
                   USER_,
                   SYSTIMESTAMP,
                   SIGONOFF_,
                   TRIM (SUBSTR (VAL_, 1, 4000)));
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_LOCAL_AUDIT');
   END NEK_LOCAL_AUDIT;

   PROCEDURE NEK_LOG (VALUE_ IN VARCHAR2, LOC_ IN VARCHAR2, DES_ IN VARCHAR2)
   IS
      /********************************************************************************
      \ NEK_LOG - function logs inproper interface functionality. All exceptions are written through log function.
      ********************************************************************************/
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      INSERT INTO NEK_LOG_MAIN (LOG_DATETIME,
                                LOG_TYPE,
                                LOG_OWNER,
                                LOG_LOCATION,
                                LOG_VALUE,
                                LOG_DESCRIPTION)
           VALUES (SYSTIMESTAMP,
                   'ERR',
                   'ITF',
                   LOC_,
                   TRIM (SUBSTR (VALUE_, 1, 4000)),
                   TRIM (SUBSTR (DES_, 1, 1000)));

      NULL;
      COMMIT;                                         --AUTONOMOUS_TRANSACTION
   END NEK_LOG;

   PROCEDURE NEK_LOG (VALUE_ IN VARCHAR2, LOC_ IN VARCHAR2)
   IS
      /********************************************************************************
      \ NEK_LOG - function logs inproper interface functionality. All exceptions are written through log function.
      ********************************************************************************/
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      INSERT INTO NEK_LOG_MAIN (LOG_DATETIME,
                                LOG_TYPE,
                                LOG_OWNER,
                                LOG_LOCATION,
                                LOG_VALUE,
                                LOG_DESCRIPTION)
           VALUES (SYSTIMESTAMP,
                   'ERR',
                   'ITF',
                   LOC_,
                   TRIM (SUBSTR (VALUE_, 1, 4000)),
                   TRIM (SUBSTR (LOC_, 1, 1000)));

      NULL;
      COMMIT;                                         --AUTONOMOUS_TRANSACTION
   END NEK_LOG;

   FUNCTION NEK_GET_MESSAGES
      RETURN VARCHAR2
   IS
   /********************************************************************************
   \ NEK_GET_MESSAGES - just return global value
   ********************************************************************************/
   BEGIN
      RETURN FUNC_MESSAGE;
   END NEK_GET_MESSAGES;

   FUNCTION NEK_TRIG_IS_UPDATE_WO (WO_NUM IN VARCHAR2)
      RETURN BOOLEAN
   IS
      /********************************************************************************
      \ NEK_TRIG_IS_UPDATE_WO - is it insert or an update?
      ********************************************************************************/
      CURSOR cALL_WOS
      IS
         SELECT COUNT (*)
           FROM WORK_ORDERS
          WHERE WORK_ORDER_NUMBER = WO_NUM;

      ALL_WOS   NUMBER (6);
   BEGIN
      OPEN cALL_WOS;

      FETCH cALL_WOS INTO ALL_WOS;

      IF ALL_WOS > 0
      THEN
         RETURN TRUE;
      ELSE
         RETURN FALSE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_TRIG_IS_UPDATE_WO');
         RETURN FALSE;
   END NEK_TRIG_IS_UPDATE_WO;

   PROCEDURE NEK_TRIG_UPDATE_WO (WO_NUM      IN VARCHAR2,
                                 WO_STAT     IN VARCHAR2,
                                 WO_OPENED   IN DATE,
                                 WO_CLOSED   IN DATE,
                                 WO_DESC     IN VARCHAR2,
                                 EQUIP_ID    IN VARCHAR2,
                                 WOA1        IN VARCHAR2,
                                 WOA2        IN VARCHAR2,
                                 WOA3        IN VARCHAR2,
                                 WOA4        IN VARCHAR2,
                                 WOA5        IN VARCHAR2,
                                 WOA6        IN VARCHAR2,
                                 WOA7        IN VARCHAR2,
                                 WOA8        IN VARCHAR2,
                                 WOA9        IN VARCHAR2,
                                 WOA10       IN VARCHAR2)
   IS
   /********************************************************************************
   \ NEK_TRIG_UPDATE_WO - Update WO

    WO_OPENED spreminl v DATE
    WO_CLOSED spremenil v DATE
   ********************************************************************************/
   BEGIN
      UPDATE WORK_ORDERS
         SET WORK_ORDER_STATUS = WO_STAT,
             WORK_ORDER_OPENED = WO_OPENED,
             WORK_ORDER_CLOSED = WO_CLOSED,
             WORK_ORDER_DESCRIPTION = WO_DESC,
             EQUIP_OPERATOR_ID = EQUIP_ID,
             WORK_ORDER_ATTRIBUTE_01 = WOA1,
             WORK_ORDER_ATTRIBUTE_02 = WOA2,
             WORK_ORDER_ATTRIBUTE_03 = WOA3,
             WORK_ORDER_ATTRIBUTE_04 = WOA4,
             WORK_ORDER_ATTRIBUTE_05 = WOA5,
             WORK_ORDER_ATTRIBUTE_06 = WOA6,
             WORK_ORDER_ATTRIBUTE_07 = WOA7,
             WORK_ORDER_ATTRIBUTE_08 = WOA8,
             WORK_ORDER_ATTRIBUTE_09 = WOA9,
             WORK_ORDER_ATTRIBUTE_10 = WOA10
       WHERE WORK_ORDER_NUMBER = WO_NUM;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_TRIG_UPDATE_WO',
                  'WO: ' || WO_NUM);
   END NEK_TRIG_UPDATE_WO;

 PROCEDURE NEK_TRIG_UPDATE_WO_CLOSE (WO_NUM IN VARCHAR2)
   IS
   /********************************************************************************
    NEK_TRIG_UPDATE_WO_CLOSE - Update WO - close with the date
   ********************************************************************************/
   BEGIN
      UPDATE WORK_ORDERS
         SET 
             WORK_ORDER_CLOSED = SYSTIMESTAMP
       WHERE 
        WORK_ORDER_NUMBER = WO_NUM 
        AND WORK_ORDER_CLOSED IS NULL;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_TRIG_UPDATE_WO_CLOSE',
                  'WO: ' || WO_NUM);
   END NEK_TRIG_UPDATE_WO_CLOSE;

   PROCEDURE NEK_TRIG_INSERT_WO (WO_NUM      IN VARCHAR2,
                                 WO_STAT     IN VARCHAR2,
                                 WO_OPENED   IN DATE,
                                 WO_CLOSED   IN DATE,
                                 WO_DESC     IN VARCHAR2,
                                 EQUIP_ID    IN VARCHAR2,
                                 WOA1        IN VARCHAR2,
                                 WOA2        IN VARCHAR2,
                                 WOA3        IN VARCHAR2,
                                 WOA4        IN VARCHAR2,
                                 WOA5        IN VARCHAR2,
                                 WOA6        IN VARCHAR2,
                                 WOA7        IN VARCHAR2,
                                 WOA8        IN VARCHAR2,
                                 WOA9        IN VARCHAR2,
                                 WOA10       IN VARCHAR2)
   IS
   /********************************************************************************
   \ NEK_TRIG_INSERT_WO - Insert WO

    WO_OPENED spreminl v DATE
    WO_CLOSED spremenil v DATE
   ********************************************************************************/
   BEGIN
      INSERT INTO WORK_ORDERS (WORK_ORDER_NUMBER,
                               WORK_ORDER_STATUS,
                               WORK_ORDER_PERCENT_COMPLETE,
                               WORK_ORDER_OPENED,
                               WORK_ORDER_CLOSED,
                               WORK_ORDER_DESCRIPTION,
                               EQUIP_OPERATOR_ID,
                               WORK_ORDER_ATTRIBUTE_01,
                               WORK_ORDER_ATTRIBUTE_02,
                               WORK_ORDER_ATTRIBUTE_03,
                               WORK_ORDER_ATTRIBUTE_04,
                               WORK_ORDER_ATTRIBUTE_05,
                               WORK_ORDER_ATTRIBUTE_06,
                               WORK_ORDER_ATTRIBUTE_07,
                               WORK_ORDER_ATTRIBUTE_08,
                               WORK_ORDER_ATTRIBUTE_09,
                               WORK_ORDER_ATTRIBUTE_10)
           VALUES (WO_NUM,
                   WO_STAT,
                   0,
                   WO_OPENED,
                   WO_CLOSED,
                   WO_DESC,
                   EQUIP_ID,
                   WOA1,
                   WOA2,
                   WOA3,
                   WOA4,
                   WOA5,
                   WOA6,
                   WOA7,
                   WOA8,
                   WOA9,
                   WOA10);
   EXCEPTION
      WHEN OTHERS
      THEN
         --NEK_LOG(SQLERRM || ' (' || SQLCODE || ').', 'NEK_TRIG_INSERT_WO');
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_ADD_USER_API',
                  'WO: ' || WO_NUM);
   END NEK_TRIG_INSERT_WO;

   
   
   FUNCTION NEK_GET_USER_FULLNAME (ID_ IN CHAR)
      RETURN VARCHAR2
   IS
      /********************************************************************************
      \ NEK_GET_USER_FULLNAME - Return full user name without (ID_)
      ********************************************************************************/
      CURSOR cUSER
      IS
         SELECT NAME
           FROM USERLOG
          WHERE USER_ID = ID_;

      nUSER      USERLOG.NAME%TYPE;
      nINDEX     NUMBER (6);
      sFINUSER   USERLOG.NAME%TYPE;
   BEGIN
      OPEN cUSER;

      FETCH cUSER INTO nUSER;

      nINDEX := INSTR (nUSER, '(' || ID_ || ')');

      IF nINDEX > 0
      THEN
         sFINUSER := SUBSTR (nUSER, 1, nINDEX);
      ELSE
         sFINUSER := nUSER;
      END IF;

      RETURN sFINUSER;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_GET_USER_FULLNAME');
         RETURN 'ERR';
   END NEK_GET_USER_FULLNAME;
   FUNCTION NEK_WO_HAS_TO_SIGN (WONAME_   IN VARCHAR2,
                                FONAME_   IN VARCHAR2,
                                TONAME_   IN VARCHAR2)
      RETURN BOOLEAN
   IS
      /********************************************************************************
      \ NEK_WO_HAS_TO_SIGN - Checks if WO has any TO signed
      ********************************************************************************/
      CURSOR cSIGN_TIMES
      IS
         SELECT COUNT (*)
           FROM NEK_SIGN_TIMES
          WHERE     TAG_OUT_ID = FONAME_
                AND TAG_OUT_NUMB_ID = TONAME_
                AND WO_NUMBER = WONAME_;

      nHAS_SIGNED   NUMBER (6);
   BEGIN
      OPEN cSIGN_TIMES;

      FETCH cSIGN_TIMES INTO nHAS_SIGNED;

      CLOSE cSIGN_TIMES;

      IF nHAS_SIGNED > 0
      THEN
         RETURN TRUE;
      ELSE
         FUNC_MESSAGE :=
               'Koordinator �e ni prevzel osamitve '
            || FONAME_
            || '/'
            || TONAME_
            || ' z operacijo '
            || WONAME_
            || '.';
         --FUNC_MESSAGE := 'There is no Tagout holder on ' || FONAME_ || ' ' || TONAME_ || ' for Workorder ' || WONAME_ || '.';
         RETURN FALSE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_WO_HAS_TO_SIGN');
         RETURN FALSE;
   END NEK_WO_HAS_TO_SIGN;


   PROCEDURE NEK_TRIG_INSERT_REPORT_WO (VELJA_ZA_OPER  IN VARCHAR2, STEVILKA_DN IN VARCHAR2,OPIS_DN IN VARCHAR2, OPREMA IN VARCHAR2,
                                 TIP_DN IN VARCHAR2, PRED_DAT_ZACETKA_DN IN DATE, PRED_DAT_ZAKLJUCKA_DN IN DATE, ODDELEK IN VARCHAR2, PRIORITETA_DN IN VARCHAR2,
                                 TEHNOLOG_DN IN VARCHAR2, MATICNA_TEHNOLOG IN VARCHAR2, KOORDINATOR_ID IN VARCHAR2, KOORDINATOR_MATICNA IN VARCHAR2, P6_TASK_ID IN VARCHAR2,
                                 SW IN VARCHAR2, ZACETEK_SW IN DATE, ZAKLJUCEK_SW IN DATE, STATUS_DN IN VARCHAR2, SEKVENCA_OPER IN NUMBER, STATUS_OPER IN VARCHAR2,
                                 ODDELEK_OPER IN VARCHAR2, OPIS_OPER IN VARCHAR2, PRED_DAT_ZACETKA_OP IN DATE, PRED_DAT_ZAKLJUCKA_OP IN DATE, DEJAN_DAT_ZACETKA_OP IN DATE,
                                 DEJAN_DAT_ZAKLJUCKA_OP IN DATE, VODJA_DEL IN VARCHAR2, VODJA_DEL_ID IN VARCHAR2, STEVILKA_PERMITA IN VARCHAR2, OPIS_PERMITA IN VARCHAR2, 
                                 VELJAVEN_OD IN DATE, VELJAVEN_DO IN DATE, ZACASNI_ODMIK IN  VARCHAR2, DELNI_ODMIK IN VARCHAR2, KRATKOROCNI_ODMIK IN VARCHAR2, 
                                 KOMENTAR IN VARCHAR2, NEK_TAG_WO_ID IN NUMBER, WO_DESC IN VARCHAR2, WO_KEY IN VARCHAR2, STATUS IN VARCHAR2, OPERATION IN VARCHAR2)  IS
   /********************************************************************************
   \ NEK_TRIG_INSERT_REPORT_WO - Insert WO into report table
   ********************************************************************************/
    BEGIN

    INSERT INTO NEK_TAG_WO_REPORT (
        VELJA_ZA_OPERACIJO,
        STEVILKA_DN,
        OPIS_DN,
        OPREMA,
        TIP_DN,
        PREDVIDEN_DATUM_ZACETKA_DN,
        PREDVIDEN_DATUM_ZAKLJUCKA_DN,
        ODDELEK,
        PRIORITETA_DN,
        TEHNOLOG_DN,
        MATICNA_ST_TEHNOLOG,
        KOORDINATOR_DN_PERSON_ID,
        MATICNA_ST_KOORDINATOR_DN,
        P6_TASK_ID,
        SW,
        ZACETEK_SW,
        ZAKLJUCEK_SW,
        STATUS_DN,
        SEKVENCA_OPERACIJE,
        STATUS_OPERACIJE,
        ODDELEK_OPERACIJE,
        OPIS_OPERACIJE,
        PREDVIDENI_DAT_ZACETKA_OP,
        PREDVIDENI_DAT_ZAKLJUCKA_OP,
        DEJANSKI_DAT_ZACETKA_OP,
        DEJANSKI_DAT_ZAKLJUCKA_OP,
        VODJA_DEL,
        MATICNA_STEVILKA_VD,
        STEVILKA_PERMITA,
        OPIS_PERMITA,
        VELJAVEN_OD,
        VELJAVEN_DO,
        ZACASNI_ODMIK,
        DELNI_ODMIK,
        KRATKOROCNI_STIK,
        KOMENTAR,
        NEK_TAG_WO_ID,
        WO_DESC,
        WO_KEY,
        STATUS,
        OPERATION,
        COMMIT_DATE) 
   VALUES (
        VELJA_ZA_OPER,
        STEVILKA_DN,
        OPIS_DN,
        OPREMA,
        TIP_DN,
        PRED_DAT_ZACETKA_DN,
        PRED_DAT_ZAKLJUCKA_DN,
        ODDELEK,
        PRIORITETA_DN,
        TEHNOLOG_DN,
        MATICNA_TEHNOLOG, 
        KOORDINATOR_ID,
        KOORDINATOR_MATICNA, 
        P6_TASK_ID,
        SW,
        ZACETEK_SW,
        ZAKLJUCEK_SW,
        STATUS_DN,
        SEKVENCA_OPER, 
        STATUS_OPER,
        ODDELEK_OPER, 
        OPIS_OPER, 
        PRED_DAT_ZACETKA_OP,
        PRED_DAT_ZAKLJUCKA_OP, 
        DEJAN_DAT_ZACETKA_OP,
        DEJAN_DAT_ZAKLJUCKA_OP, 
        VODJA_DEL, 
        VODJA_DEL_ID, 
        STEVILKA_PERMITA, 
        OPIS_PERMITA, 
        VELJAVEN_OD, 
        VELJAVEN_DO, 
        ZACASNI_ODMIK, 
        DELNI_ODMIK, 
        KRATKOROCNI_ODMIK, 
        KOMENTAR, 
        NEK_TAG_WO_ID, 
        WO_DESC,
        WO_KEY, 
        STATUS, 
        OPERATION,
        SYSTIMESTAMP);
   
   
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_TRIG_INSERT_REPORT_WO',
                  'WO: ' || WO_KEY);
   END NEK_TRIG_INSERT_REPORT_WO;


   
   PROCEDURE NEK_TRIG_UPDATE_REPORT_WO (VELJA_ZA_OPER  IN VARCHAR2, STEVILKA_DN_IN IN VARCHAR2,OPIS_DN_IN IN VARCHAR2, OPREMA_IN IN VARCHAR2,
                                 TIP_DN_IN IN VARCHAR2, PRED_DAT_ZACETKA_DN IN DATE, PRED_DAT_ZAKLJUCKA_DN IN DATE, ODDELEK_IN IN VARCHAR2, PRIORITETA_DN_IN IN VARCHAR2,
                                 TEHNOLOG_DN_IN IN VARCHAR2, MATICNA_TEHNOLOG IN VARCHAR2, KOORDINATOR_ID IN VARCHAR2, KOORDINATOR_MATICNA IN VARCHAR2, P6_TASK_ID_IN IN VARCHAR2,
                                 SW_IN IN VARCHAR2, ZACETEK_SW_IN IN DATE, ZAKLJUCEK_SW_IN IN DATE, STATUS_DN_IN IN VARCHAR2, SEKVENCA_OPER IN NUMBER, STATUS_OPER IN VARCHAR2,
                                 ODDELEK_OPER IN VARCHAR2, OPIS_OPER IN VARCHAR2, PRED_DAT_ZACETKA_OP IN DATE, PRED_DAT_ZAKLJUCKA_OP IN DATE, DEJAN_DAT_ZACETKA_OP IN DATE,
                                 DEJAN_DAT_ZAKLJUCKA_OP IN DATE, VODJA_DEL_IN IN VARCHAR2, VODJA_DEL_ID IN VARCHAR2, STEVILKA_PERMITA_IN IN VARCHAR2, OPIS_PERMITA_IN IN VARCHAR2, 
                                 VELJAVEN_OD_IN IN DATE, VELJAVEN_DO_IN IN DATE, ZACASNI_ODMIK_IN IN  VARCHAR2, DELNI_ODMIK_IN IN VARCHAR2, KRATKOROCNI_ODMIK IN VARCHAR2, 
                                 KOMENTAR_IN IN VARCHAR2, NEK_TAG_WO_ID_IN IN NUMBER, WO_DESC_IN IN VARCHAR2, WO_KEY_IN IN VARCHAR2, STATUS_IN IN VARCHAR2, OPERATION_IN IN VARCHAR2)  IS
   /********************************************************************************
   \ NEK_TRIG_UPDATE_REPORT_WO - Insert WO into report table
   ********************************************************************************/
    BEGIN

    UPDATE NEK_TAG_WO_REPORT SET
        VELJA_ZA_OPERACIJO = VELJA_ZA_OPER,
        STEVILKA_DN = STEVILKA_DN_IN,
        OPIS_DN = OPIS_DN_IN,
        OPREMA = OPREMA_IN,
        TIP_DN = TIP_DN_IN,
        PREDVIDEN_DATUM_ZACETKA_DN = PRED_DAT_ZACETKA_DN,
        PREDVIDEN_DATUM_ZAKLJUCKA_DN = PRED_DAT_ZAKLJUCKA_DN,
        ODDELEK = ODDELEK_IN,
        PRIORITETA_DN = PRIORITETA_DN_IN,
        TEHNOLOG_DN = TEHNOLOG_DN_IN,
        MATICNA_ST_TEHNOLOG = MATICNA_TEHNOLOG,
        KOORDINATOR_DN_PERSON_ID = KOORDINATOR_ID,
        MATICNA_ST_KOORDINATOR_DN = KOORDINATOR_MATICNA,
        P6_TASK_ID = P6_TASK_ID_IN,
        SW = SW_IN,
        ZACETEK_SW = ZACETEK_SW_IN,
        ZAKLJUCEK_SW = ZAKLJUCEK_SW_IN,
        STATUS_DN = STATUS_DN_IN,
        SEKVENCA_OPERACIJE = SEKVENCA_OPER,
        STATUS_OPERACIJE = STATUS_OPER,
        ODDELEK_OPERACIJE = ODDELEK_OPER,
        OPIS_OPERACIJE = OPIS_OPER,
        PREDVIDENI_DAT_ZACETKA_OP = PRED_DAT_ZACETKA_OP,
        PREDVIDENI_DAT_ZAKLJUCKA_OP = PRED_DAT_ZAKLJUCKA_OP,
        DEJANSKI_DAT_ZACETKA_OP = DEJAN_DAT_ZACETKA_OP,
        DEJANSKI_DAT_ZAKLJUCKA_OP = DEJAN_DAT_ZAKLJUCKA_OP,
        VODJA_DEL = VODJA_DEL_IN,
        MATICNA_STEVILKA_VD = VODJA_DEL_ID,
        STEVILKA_PERMITA = STEVILKA_PERMITA_IN,
        OPIS_PERMITA = OPIS_PERMITA_IN,
        VELJAVEN_OD = VELJAVEN_OD_IN,
        VELJAVEN_DO = VELJAVEN_DO_IN,
        ZACASNI_ODMIK = ZACASNI_ODMIK_IN,
        DELNI_ODMIK = DELNI_ODMIK_IN,
        KRATKOROCNI_STIK = KRATKOROCNI_ODMIK,
        KOMENTAR = KOMENTAR_IN,
        NEK_TAG_WO_ID = NEK_TAG_WO_ID_IN,
        WO_DESC = WO_DESC_IN,
        STATUS = STATUS_IN,
        OPERATION = OPERATION_IN,
        COMMIT_DATE_UPDATED = SYSTIMESTAMP
   WHERE
       WO_KEY = WO_KEY_IN; 
   
   
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_TRIG_UPDATE_REPORT_WO',
                  'WO: ' || WO_KEY_IN);
   END NEK_TRIG_UPDATE_REPORT_WO;  
   
   PROCEDURE NEK_WOREP_SET_STR_VAL(WONAME IN VARCHAR2, STRVAL IN VARCHAR2) IS
      /********************************************************************************
      \ NEK_WOREP_SET_STR_VAL - set the context for the WO reports - name
      ********************************************************************************/
   BEGIN
        dbms_session.set_context(DEFAULT_PACKAGE_CONTEXT,WONAME, STRVAL);
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_WOREP_SET_STR_VAL');
   END NEK_WOREP_SET_STR_VAL;

   PROCEDURE NEK_WOREP_SET_DATE_VAL(WONAME IN VARCHAR2, DATEVAL IN DATE) IS
      /********************************************************************************
      \ NEK_WOREP_SET_DATE_VAL - set the context for the WO reports - dates
      ********************************************************************************/
   BEGIN
        dbms_session.set_context(DEFAULT_PACKAGE_CONTEXT, WONAME, TO_CHAR(DATEVAL, 'DD-MON-YYYY'));
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_WOREP_SET_DATE_VAL');
   END NEK_WOREP_SET_DATE_VAL;

   PROCEDURE NEK_WOREP_CLEAR_VAL IS
      /********************************************************************************
      \ NEK_WOREP_CLEAR_VAL - clears the context
      ********************************************************************************/
   BEGIN
        dbms_session.clear_context(DEFAULT_PACKAGE_CONTEXT);
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').',
                  'NEK_WOREP_CLEAR_VAL');
   END NEK_WOREP_CLEAR_VAL;
   
   FUNCTION NEK_WOREP_RETURN_WO_TOS(WONAME IN VARCHAR2) RETURN VARCHAR2
   IS
   /********************************************************************************
   /NEK_WOREP_RETURN_WO_TOS 
  / ********************************************************************************/
      CURSOR cACTS
      IS
        SELECT  ACT_TAGOUT_NUMBER, 
                ACT_TAGOUT_SECTION_NUMBER
        FROM    ACT_TAGOUT_SECTION_WORK_ORDERS
        WHERE
            WORK_ORDER_NUMBER = WONAME
            AND TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(ACT_TAGOUT_NUMBER); --CHECKED_FOLDER_TYPE_;

      AC_TN   ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_NUMBER%TYPE;
      AC_TSN  ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_SECTION_NUMBER%TYPE;
      
      USER_RES VARCHAR2(4000);
      CRLF CHAR(2) :=  CHR(13) || CHR(10);
   BEGIN
      OPEN cACTS;

      LOOP
      FETCH cACTS INTO AC_TN, AC_TSN;

         IF cACTS%NOTFOUND
         THEN
            EXIT;
         ELSE
            USER_RES := USER_RES || TRIM(AC_TSN) || CRLF;
         END IF;
      END LOOP;

      CLOSE cACTS;

     RETURN USER_RES;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_WOREP_RETURN_WO_TOS');
         RETURN 'ERR';
   END NEK_WOREP_RETURN_WO_TOS;
 
   FUNCTION NEK_WOREP_WO_TOS_STAT(WONAME IN VARCHAR2) RETURN VARCHAR2
   IS
      /********************************************************************************
      \ NEK_WOREP_WO_TOS_STAT 
      ********************************************************************************/
      AC_TN   ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_NUMBER%TYPE;
      AC_TSN  ACT_TAGOUT_SECTION_WORK_ORDERS.ACT_TAGOUT_SECTION_NUMBER%TYPE;

      CURSOR cACTS
      IS
        SELECT  ACT_TAGOUT_NUMBER, 
                ACT_TAGOUT_SECTION_NUMBER
        FROM    ACT_TAGOUT_SECTION_WORK_ORDERS
        WHERE
            WORK_ORDER_NUMBER = WONAME
            AND TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(ACT_TAGOUT_NUMBER); -- CHECKED_FOLDER_TYPE_;

      CURSOR cSTAT
      IS 
        SELECT SECTION_LEVEL_DESC
        FROM SECTION_VERIF_LEVELS lvl
        INNER JOIN ACT_TAGOUT_SECTION_VERIF mn ON
            lvl.TAGOUT_TYPE_ID = mn.TAGOUT_TYPE_ID 
            AND lvl.SECTION_VERIF_LEVEL = mn.SECTION_VERIF_LEVEL  
        WHERE mn.ACT_TAGOUT_NUMBER = AC_TN
        AND mn.ACT_TAGOUT_SECTION_NUMBER = AC_TSN
        AND mn.TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(AC_TN) -- CHECKED_FOLDER_TYPE_
        ORDER BY mn.SECTION_VERIF_LEVEL DESC;
      
      STATN SECTION_VERIF_LEVELS.SECTION_LEVEL_DESC%TYPE;
      USER_RES VARCHAR2(4000);
      CRLF CHAR(2) :=  CHR(13) || CHR(10);
   BEGIN
      OPEN cACTS;

      LOOP
      FETCH cACTS INTO AC_TN, AC_TSN;

         IF cACTS%NOTFOUND
         THEN
            EXIT;
         ELSE
         
            STATN := '';
         
            OPEN cSTAT;
            FETCH cSTAT INTO STATN;
            
            USER_RES := USER_RES || STATN  || CRLF;
            
            CLOSE cSTAT;
         END IF;
      END LOOP;

      CLOSE cACTS;

     RETURN USER_RES;
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_WOREP_WO_TOS_STAT');
         RETURN 'ERR';
   END NEK_WOREP_WO_TOS_STAT;


   FUNCTION NEK_CHECK_FOLDER_TYPE(FONAME_ IN VARCHAR2) RETURN VARCHAR2
   IS
   /********************************************************************************
   \ NEK_CHECK_FOLDER_TYPE - checks if the TO is in the Project that allows manipulation
    ********************************************************************************/         
    CURSOR getID IS 
        SELECT PROJ.TAGOUT_TYPE_ID
        FROM ACT_TAGOUT_UNITS PROJ
        INNER JOIN ACT_TAGOUT_ATTRIBUTES ATTR
        ON PROJ.ACT_TAGOUT_NUMBER = ATTR.ACT_TAGOUT_NUMBER
        WHERE TRIM(ATTRIBUTE_VALUE) = TRIM(PROJECT_ATTRIBUTE_REQUIRED_)
        AND ATTR.ATTRIBUTE_NUMBER = PROJECT_ATT_REQ_NUM_
        AND PROJ.ACT_TAGOUT_NUMBER = FONAME_;
    
    RES_STR ACT_TAGOUT_UNITS.TAGOUT_TYPE_ID%TYPE;
    
    BEGIN
        
    OPEN getID;
    FETCH getID INTO RES_STR;
    
    IF (LENGTH(RES_STR) <= 0) THEN
        NEK_LOG('Projekt '|| FONAME_ ||' nima iskanega atributa.', 'NEK_CHECK_FOLDER_TYPE');    
    END IF;

    CLOSE getID;
    
    RETURN RES_STR;

    EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_CHECK_FOLDER_TYPE');
         RETURN 'ERR';
    END NEK_CHECK_FOLDER_TYPE;

 FUNCTION NEK_CHECK_TEMP_LIFT_LEVEL(TVLV IN TEMP_LIFT_VERIF.TEMP_LIFT_VERIF_LEVEL%TYPE) RETURN BOOLEAN
   IS
   /********************************************************************************
   \ NEK_CHECK_TEMP_LIFT_LEVEL - checks if the TL reached the required level
    ********************************************************************************/     
    CURSOR chkTL IS 
     SELECT 
        COUNT(*)
     FROM 
        TEMP_LIFT_VERIF_LEVELS
     WHERE 
        TEMP_LIFT_VERIF_LEVEL = TVLV
        AND TEMP_LIFT_INTERNAL_LEVEL = 10;
 
    NUM_ NUMBER;
 BEGIN

    NUM_ := 0;
    
    OPEN chkTL;
    FETCH chkTL into NUM_;
    CLOSE chkTL;

    IF (NUM_ > 0) THEN
        RETURN TRUE;
    ELSE
        RETURN FALSE;
    END IF;
    
 EXCEPTION
    WHEN OTHERS
    THEN
        NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_CHECK_TEMP_LIFT_LEVEL');
        RETURN FALSE;
 END NEK_CHECK_TEMP_LIFT_LEVEL;

   PROCEDURE NEK_TO_SIGN_OFF_AUTO (
      WONAME_   IN     ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE,
      FONAME_   IN     ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE,
      TONAME_   IN     ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE, 
      TLN       IN     TEMP_LIFT_VERIF.TEMP_LIFT_NUMBER%TYPE)
   IS
      /********************************************************************************
      \ NEK_TO_SIGN_OFF_AUTO
      ********************************************************************************/
      MARKTIME     NEK_SIGN_TIMES.ESOMS_TIME%TYPE;
      FIN_USER     USERLOG.USER_ID%TYPE;
      NEW_NOTE     ACT_TAGOUT_SECTION_HOLDERS.NOTES%TYPE;
   BEGIN
 
      FIN_USER := TRIM (GET_SIGNED_USER_TO(WONAME_,
                                      FONAME_,
                                      TONAME_,
                                      ''));

        
      MARKTIME := NEK_GET_TIME_FROM_TOSIGN (FONAME_,
                                      TONAME_,
                                      WONAME_,
                                      FIN_USER);
        
      NEW_NOTE := ' User '
            || NEK_GET_USER_FULLNAME (FIN_USER)
            || ' ('
            || TRIM (FIN_USER)
            || ') with WO '
            || WONAME_ 
            || ' has been signed off by the eSOMS interface due to activated Temp. Lift '
            || TLN;

         UPDATE ACT_TAGOUT_SECTION_HOLDERS
            SET RELEASED_BY = FIN_USER,
                RELEASED_DT = SYSTIMESTAMP,
                WORK_COMPLETE = 1,
                NOTES = SUBSTR(NOTES || NEW_NOTE, 1, 500)
          WHERE 
                TAGOUT_TYPE_ID = NEK_CHECK_FOLDER_TYPE(FONAME_) 
                AND ACT_TAGOUT_NUMBER = FONAME_
                AND ACT_TAGOUT_SECTION_NUMBER = TONAME_
                AND SIGNED_ON_BY = FIN_USER
                AND ACCEPTED_DT = TO_DATE (TO_CHAR (MARKTIME, 'dd.mm.yy hh24:MI:SS'),
                                'dd.mm.yy hh24:MI:SS')
                AND (RELEASED_BY IS NULL AND WORK_COMPLETE IS NULL);

         NEK_DELETE_WO_TIMESTAMP (MARKTIME,
                                  FONAME_,
                                  TONAME_,
                                  WONAME_,
                                  FIN_USER);                              
   
         NEK_AUDIT ('SIGN', NEK_CHECK_FOLDER_TYPE(FONAME_), --CHECKED_FOLDER_TYPE_,
            FONAME_,
            TONAME_,
            FIN_USER,
            NEW_NOTE);


         NEK_LOCAL_AUDIT ('TO',
                       WONAME_,
                       FONAME_,
                       TONAME_,
                       FIN_USER,
                       'OFF',
                       'Auto sign off from TO due to activated TL succesful!');
   EXCEPTION
      WHEN OTHERS
      THEN
         NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_TO_SIGN_OFF_AUTO');
   END NEK_TO_SIGN_OFF_AUTO;

 PROCEDURE NEK_REMOVE_SIGNS_FOR_TL(FONAME_ IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_NUMBER%TYPE, TONAME_ IN ACT_TAGOUT_SECTIONS.ACT_TAGOUT_SECTION_NUMBER%TYPE,
     TLN IN TEMP_LIFT_VERIF.TEMP_LIFT_NUMBER%TYPE)
   IS
   /********************************************************************************
   \ NEK_REMOVE_SIGNS_FOR_TL - Removes TO signs where TL is made and no WO sign is present
    ********************************************************************************/            
    CURSOR GET_WO_withoutWOh IS
        SELECT 
            WORK_ORDER_NUMBER 
        FROM
            ACT_TAGOUT_SECTION_WORK_ORDERS
        WHERE
            ACT_TAGOUT_NUMBER = FONAME_
            AND ACT_TAGOUT_SECTION_NUMBER = TONAME_
            AND WORK_ORDER_NUMBER NOT IN (
                SELECT 
                    WORK_ORDER_NUMBER 
                FROM
                    ACT_WORK_ORDER_HOLDERS
                WHERE
                    ACT_TAGOUT_NUMBER = FONAME_
                    AND ACT_TAGOUT_SECTION_NUMBER = TONAME_
                    AND SIGN_OFF_DATE IS NULL 
                    AND (WORK_COMPLETE IS NULL OR WORK_COMPLETE = ''));  
   
    WONAME_ ACT_TAGOUT_SECTION_WORK_ORDERS.WORK_ORDER_NUMBER%TYPE;
 BEGIN
    
    OPEN GET_WO_withoutWOh;
    LOOP
        FETCH GET_WO_withoutWOh INTO WONAME_;
        EXIT WHEN GET_WO_withoutWOh%NOTFOUND;
            
        IF (NEK_TO_HAS_SIGNATURE(WONAME_, FONAME_, TONAME_) = FALSE) THEN -- false pomeni da je �e prevzeto
            NEK_TO_SIGN_OFF_AUTO(WONAME_, FONAME_, TONAME_, TLN);        
        END IF;
    END LOOP;
    CLOSE GET_WO_withoutWOh;
    
    
 EXCEPTION
    WHEN OTHERS
    THEN
        NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_REMOVE_SIGNS_FOR_TL');
 END NEK_REMOVE_SIGNS_FOR_TL;


END NEK_PACKAGE_TAGG_WOS;
/