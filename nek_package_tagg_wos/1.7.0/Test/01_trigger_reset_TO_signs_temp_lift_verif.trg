CREATE OR REPLACE TRIGGER NEK_ITF_RESET_TO_SIGNS
AFTER INSERT
ON TEMP_LIFT_VERIF
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DECLARE
    TLN     TEMP_LIFT_VERIF.TEMP_LIFT_NUMBER%TYPE;
    TLVL    TEMP_LIFT_VERIF.TEMP_LIFT_VERIF_LEVEL%TYPE;
    USERID  TEMP_LIFT_VERIF.USER_ID%TYPE;
    TLVD    TEMP_LIFT_VERIF.TEMP_LIFT_VERIF_DATE%TYPE;
    
    AFF_TO_ act_tagout_section_tags.act_tagout_number%TYPE;
    AFF_TON_ act_tagout_section_tags.act_tagout_section_number%TYPE;

    CURSOR GET_AFFECTED_TO IS
        SELECT DISTINCT
            act_tagout_section_tags.act_tagout_number,
            act_tagout_section_tags.act_tagout_section_number
        FROM 
            temporary_lift_tags,
            tag_types,
            tagout_types,
            act_tagout_section_tags,
            act_tagout_sections
        WHERE 
            archived = 0 AND
            act_tagout_sections.tagout_type_id = act_tagout_section_tags.tagout_type_id AND
            act_tagout_sections.act_tagout_number = act_tagout_section_tags.act_tagout_number AND
            act_tagout_sections.act_tagout_section_number = act_tagout_section_tags.act_tagout_section_number AND
            act_tagout_section_tags.tag_type_id = tag_types.tag_type_id AND
            act_tagout_section_tags.equip_operator_id = temporary_lift_tags.equip_operator_id AND
            act_tagout_section_tags.tag_serial_number = temporary_lift_tags.tag_serial_number AND
            act_tagout_section_tags.tag_type_id = temporary_lift_tags.tag_type_id AND
            tagout_types.tagout_type_id = act_tagout_section_tags.tagout_type_id AND
            temporary_lift_tags.temp_lift_number = TLN AND
            NOT EXISTS (
            SELECT 
                act_tagout_sections.tagout_type_id
            FROM 
                act_tagout_section_verif a,
                section_verif_levels s
            WHERE 
                a.tagout_type_id = act_tagout_sections.tagout_type_id AND
                a.act_tagout_number = act_tagout_sections.act_tagout_number AND
                a.act_tagout_section_number = act_tagout_sections.act_tagout_section_number AND
                a.tagout_type_id = s.tagout_type_id AND
                a.section_verif_level = s.section_verif_level AND
                s.section_internal_level >= 128);

BEGIN
    TLN     :=  :New.TEMP_LIFT_NUMBER;
    TLVL    :=  :New.TEMP_LIFT_VERIF_LEVEL;
    USERID  :=  :New.USER_ID;
    TLVD    :=  :New.TEMP_LIFT_VERIF_DATE;   
    
    IF (NEK_PACKAGE_TAGG_WOS.NEK_CHECK_TEMP_LIFT_LEVEL(TLVL) = TRUE) THEN
    -- Call the logical stuff to be done
        OPEN GET_AFFECTED_TO;
        LOOP
            FETCH GET_AFFECTED_TO INTO AFF_TO_, AFF_TON_;
            EXIT WHEN GET_AFFECTED_TO%NOTFOUND;
            
            NEK_PACKAGE_TAGG_WOS.NEK_REMOVE_SIGNS_FOR_TL(AFF_TO_, AFF_TON_, TLN);
        END LOOP;
        CLOSE GET_AFFECTED_TO;

    END IF;

    EXCEPTION
        WHEN OTHERS THEN
            NEK_PACKAGE_TAGG_WOS.NEK_LOG (SQLERRM || ' (' || SQLCODE || ').', 'NEK_ITF_RESET_TO_SIGNS');
        RAISE;
END NEK_ITF_RESET_TO_SIGNS;
/