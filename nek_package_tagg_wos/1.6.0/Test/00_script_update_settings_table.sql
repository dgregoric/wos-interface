INSERT INTO NEK_ITF_SETTINGS 
    (USER_ID, APP_ID, SECTION_ID, SETTING_ID, VALUE_ID, DESCRIPTION)
VALUES
    ('ADMIN', 'WOS_ITF', 'TAGOUT', 'TAG_VERIF_BEGIN_VAL', '10', 'Value for TAG minimum verification level when system allows operations on it.');
INSERT INTO NEK_ITF_SETTINGS 
    (USER_ID, APP_ID, SECTION_ID, SETTING_ID, VALUE_ID, DESCRIPTION)
VALUES
('ADMIN', 'WOS_ITF', 'TAGOUT', 'TAG_VERIF_END_VAL', '32', 'Value for TAG maximum verification level when system still allows operations on it.');

COMMIT;