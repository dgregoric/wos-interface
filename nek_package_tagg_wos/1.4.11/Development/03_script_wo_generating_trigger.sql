CREATE OR REPLACE TRIGGER ESM_DEV_ITF.NEK_CREATE_WO
AFTER INSERT
ON ESM_DEV_ITF.NEK_TAG_WO 
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DECLARE
    WO_KEY VARCHAR2(20);
    REPORT_OPERATION VARCHAR(10);
    CRLF CHAR(2) :=  CHR(13) || CHR(10);
    DATE_FORMAT VARCHAR(50) := 'DD.MM.YYYY HH24:MI:SS';
    Step_   Varchar(20) := '';
    Status_  WORK_ORDERS.WORK_ORDER_STATUS%TYPE;
    WO_DESC_ WORK_ORDERS.WORK_ORDER_DESCRIPTION%TYPE;
    ATT01_   WORK_ORDERS.WORK_ORDER_ATTRIBUTE_01%TYPE;
    ATT02_   WORK_ORDERS.WORK_ORDER_ATTRIBUTE_02%TYPE;
    ATT03_   WORK_ORDERS.WORK_ORDER_ATTRIBUTE_03%TYPE;
    ATT04_   WORK_ORDERS.WORK_ORDER_ATTRIBUTE_04%TYPE;
    ATT05_   WORK_ORDERS.WORK_ORDER_ATTRIBUTE_05%TYPE;
    ATT06_   WORK_ORDERS.WORK_ORDER_ATTRIBUTE_06%TYPE;
    ATT07_   WORK_ORDERS.WORK_ORDER_ATTRIBUTE_07%TYPE;
    ATT08_   WORK_ORDERS.WORK_ORDER_ATTRIBUTE_08%TYPE;
    ATT09_   WORK_ORDERS.WORK_ORDER_ATTRIBUTE_09%TYPE;
    ATT10_   WORK_ORDERS.WORK_ORDER_ATTRIBUTE_10%TYPE;
    
BEGIN
    Status_ := TRIM(SUBSTR('OP '  || TRIM(:NEW.STATUS_OPERACIJE), 1, 30));
    WO_KEY := TRIM(SUBSTR(TRIM(:NEW.STEVILKA_DN)  || '-' || TO_CHAR(:NEW.SEKVENCA_OPERACIJE), 1, 20));

    WO_DESC_ := TRIM(SUBSTR('Opis DN: '        || TRIM(:NEW.OPIS_DN)        || CRLF ||
                            'Opis operacije: ' || TRIM(:NEW.OPIS_OPERACIJE) || CRLF ||
                            'Tip DN: '         || TRIM(:NEW.TIP_DN)         || CRLF ||
                            'Prioriteta DN: '  || TRIM(:NEW.PRIORITETA_DN)  || CRLF , 1, 4000));
    ATT01_ := TRIM(SUBSTR('Tehnolog: '    || TRIM(:NEW.TEHNOLOG_DN) ||              ' (' || TRIM(:NEW.MATICNA_ST_TEHNOLOG)       || ')' || CRLF|| 
                          'Koordinator: ' || TRIM(:NEW.KOORDINATOR_DN_PERSON_ID) || ' (' || TRIM(:NEW.MATICNA_ST_KOORDINATOR_DN) || ')' || CRLF|| 
                          'Vodja del: '   || TRIM(:NEW.VODJA_DEL) ||                ' (' || TRIM(:NEW.MATICNA_STEVILKA_VD)       || ')', 1, 2000));
    ATT02_ := TRIM(SUBSTR('Dovolilnica �t: '   || TRIM(:NEW.STEVILKA_PERMITA) || CRLF || 
                          'Opis dovolilnice: ' || TRIM(:NEW.OPIS_PERMITA)     || CRLF, 1, 2000));
    ATT03_ := TRIM(SUBSTR('Za�asni odmik: ' || TRIM(:NEW.ZACASNI_ODMIK) || CRLF || 
                          'Delni odmik: '   || TRIM(:NEW.DELNI_ODMIK)   || CRLF ||
                          'Komentar: ' || TRIM(:NEW.KOMENTAR), 1, 2000));
    ATT04_ := TRIM(SUBSTR('Planiran za�etek delovnega naloga: ' || TO_CHAR(:NEW.PREDVIDEN_DATUM_ZAETKA_DN,    DATE_FORMAT) || CRLF ||
                          'Planiran konec delovnega naloga: '   || TO_CHAR(:NEW.PREDVIDEN_DATUM_ZAKLJUCKA_DN, DATE_FORMAT) || CRLF ||
                          '--'                                                                                             || CRLF ||
                          'Planiran za�etek operacije: '        || TO_CHAR(:NEW.PREDVIDENI_DAT_ZACETKA_OP,    DATE_FORMAT) || CRLF || 
                          'Planiran konec operacije: '          || TO_CHAR(:NEW.PREDVIDENI_DAT_ZAKLJUCKA_OP,  DATE_FORMAT) || CRLF ||
                          'Dejanski za�etek operacije: '        || TO_CHAR(:NEW.DEJANSKI_DAT_ZACETKA_OP,      DATE_FORMAT) || CRLF ||
                          'Dejanski konec operacije: '          || TO_CHAR(:NEW.DEJANSKI_DAT_ZAKLJUCKA_OP,    DATE_FORMAT) || CRLF, 1, 2000));
    ATT05_ := TRIM(SUBSTR('Sistemsko okno: ' || TRIM(:NEW.SW)                           || CRLF || 
                          'Za�etek: '        || TO_CHAR(:NEW.ZACETEK_SW, DATE_FORMAT)   || CRLF ||
                          'Zaklju�ek: '      || TO_CHAR(:NEW.ZAKLJUCEK_SW, DATE_FORMAT) || CRLF, 1, 2000));
    ATT06_ := '';
    ATT07_ := '';
    ATT08_ := '';
    ATT09_ := '';
    ATT10_ := 'Zadnja sprememba: ' || TO_CHAR(SYSDATE,DATE_FORMAT);
    
    Step_ := 'Pogoj';
    
    IF (NEK_PACKAGE_TAGG_WOS.NEK_TRIG_IS_UPDATE_WO(WO_KEY) = TRUE) THEN
        -- Updating existing WO in eSOMS WORKORDERS   
        
        -- Updating WO in eSOMS table
        Step_ := 'Update WO';
        NEK_PACKAGE_TAGG_WOS.NEK_TRIG_UPDATE_WO(
            WO_KEY,
            Status_,
            :NEW.PREDVIDENI_DAT_ZACETKA_OP,
            :NEW.DEJANSKI_DAT_ZAKLJUCKA_OP,
            WO_DESC_,
            :NEW.OPREMA, 
            ATT01_, ATT02_, ATT03_, ATT04_, ATT05_, ATT06_, ATT07_, ATT08_, ATT09_, ATT10_);
            
        
        REPORT_OPERATION := 'Updating';
        -- Updating WO in the reporting table
        Step_ := 'Inserting WO report.';
        NEK_PACKAGE_TAGG_WOS.NEK_TRIG_UPDATE_REPORT_WO(
        :NEW.VELJA_ZA_OPERACIJO,
        :NEW.STEVILKA_DN, 
        :NEW.OPIS_DN,
        :NEW.OPREMA,
        :NEW.TIP_DN,
        :NEW.PREDVIDEN_DATUM_ZAETKA_DN,
        :NEW.PREDVIDEN_DATUM_ZAKLJUCKA_DN,
        :NEW.ODDELEK, 
        :NEW.PRIORITETA_DN,
        :NEW.TEHNOLOG_DN, 
        :NEW.MATICNA_ST_TEHNOLOG,
        :NEW.KOORDINATOR_DN_PERSON_ID, 
        :NEW.MATICNA_ST_KOORDINATOR_DN, 
        :NEW.P6_TASK_ID, 
        :NEW.SW, 
        :NEW.ZACETEK_SW, 
        :NEW.ZAKLJUCEK_SW, 
        :NEW.STATUS_DN,
        :NEW.SEKVENCA_OPERACIJE, 
        :NEW.STATUS_OPERACIJE, 
        :NEW.ODDELEK_OPERACIJE, 
        :NEW.OPIS_OPERACIJE, 
        :NEW.PREDVIDENI_DAT_ZACETKA_OP,
        :NEW.PREDVIDENI_DAT_ZAKLJUCKA_OP, 
        :NEW.DEJANSKI_DAT_ZACETKA_OP, 
        :NEW.DEJANSKI_DAT_ZAKLJUCKA_OP, 
        :NEW.VODJA_DEL, 
        :NEW.MATICNA_STEVILKA_VD,
        :NEW.STEVILKA_PERMITA, 
        :NEW.OPIS_PERMITA, 
        :NEW.VELJAVEN_OD, 
        :NEW.VELJAVEN_DO, 
        :NEW.ZACASNI_ODMIK, 
        :NEW.DELNI_ODMIK, 
        :NEW.KRATKOROCNI_STIK,
        :NEW.KOMENTAR, 
        :NEW.NEK_TAG_WO_ID, 
        WO_DESC_,
        WO_KEY,
        Status_,
        REPORT_OPERATION);        
    -- Updating WO dates on ??
        Step_ := 'Update WO Dates';
        NEK_TO_SCHEDULE.Update_Wo_Dates (
            WO_KEY,                              -- Work_Order_Number_
            :NEW.PREDVIDEN_DATUM_ZAETKA_DN,      -- Predviden_Datum_Zacetka_Dn_
            :NEW.PREDVIDEN_DATUM_ZAKLJUCKA_DN,   -- Predviden_Datum_Zakljucka_Dn_
            :NEW.PREDVIDENI_DAT_ZACETKA_OP,      -- Predvideni_Dat_Zacetka_Op_ 
            :NEW.PREDVIDENI_DAT_ZAKLJUCKA_OP,    -- Predvideni_Dat_Zakljucka_Op_
            :NEW.DEJANSKI_DAT_ZACETKA_OP,        -- Dejanski_Dat_Zacetka_Op_ 
            :NEW.DEJANSKI_DAT_ZAKLJUCKA_OP,      -- Dejanski_Dat_Zakljucka_Op_
            :NEW.STEVILKA_PERMITA,               -- Stevilka_Permita_ 
            :NEW.VELJAVEN_OD,                    -- Veljaven_Od_ 
            :NEW.VELJAVEN_DO,                    -- Veljaven_Do_
            :NEW.ZACETEK_SW,                     -- Zacetek_Sw_
            :NEW.ZAKLJUCEK_SW,                   -- Zakljucek_Sw_
            :NEW.P6_TASK_ID,                     -- P6_Task_Id_
            :NEW.SW);                            -- Sw_                             
    ELSE
        -- Adding new WO in eSOMS WORKORDERS

        -- Adding WO to the eSOMS table
        Step_ := 'Insert WO';
        NEK_PACKAGE_TAGG_WOS.NEK_TRIG_INSERT_WO(
            WO_KEY,
            Status_,
            :NEW.PREDVIDENI_DAT_ZACETKA_OP,  
            :NEW.DEJANSKI_DAT_ZAKLJUCKA_OP, 
            WO_DESC_, 
            :NEW.OPREMA, 
            ATT01_, ATT02_, ATT03_, ATT04_, ATT05_, ATT06_, ATT07_, ATT08_, ATT09_, ATT10_); 
        
        REPORT_OPERATION := 'Inserting';
        -- Adding WO to the reporting table
        Step_ := 'Inserting WO report.';
        NEK_PACKAGE_TAGG_WOS.NEK_TRIG_INSERT_REPORT_WO(
        :NEW.VELJA_ZA_OPERACIJO,
        :NEW.STEVILKA_DN, 
        :NEW.OPIS_DN,
        :NEW.OPREMA,
        :NEW.TIP_DN,
        :NEW.PREDVIDEN_DATUM_ZAETKA_DN,
        :NEW.PREDVIDEN_DATUM_ZAKLJUCKA_DN,
        :NEW.ODDELEK, 
        :NEW.PRIORITETA_DN,
        :NEW.TEHNOLOG_DN, 
        :NEW.MATICNA_ST_TEHNOLOG,
        :NEW.KOORDINATOR_DN_PERSON_ID, 
        :NEW.MATICNA_ST_KOORDINATOR_DN, 
        :NEW.P6_TASK_ID, 
        :NEW.SW, 
        :NEW.ZACETEK_SW, 
        :NEW.ZAKLJUCEK_SW, 
        :NEW.STATUS_DN,
        :NEW.SEKVENCA_OPERACIJE, 
        :NEW.STATUS_OPERACIJE, 
        :NEW.ODDELEK_OPERACIJE, 
        :NEW.OPIS_OPERACIJE, 
        :NEW.PREDVIDENI_DAT_ZACETKA_OP,
        :NEW.PREDVIDENI_DAT_ZAKLJUCKA_OP, 
        :NEW.DEJANSKI_DAT_ZACETKA_OP, 
        :NEW.DEJANSKI_DAT_ZAKLJUCKA_OP, 
        :NEW.VODJA_DEL, 
        :NEW.MATICNA_STEVILKA_VD,
        :NEW.STEVILKA_PERMITA, 
        :NEW.OPIS_PERMITA, 
        :NEW.VELJAVEN_OD, 
        :NEW.VELJAVEN_DO, 
        :NEW.ZACASNI_ODMIK, 
        :NEW.DELNI_ODMIK, 
        :NEW.KRATKOROCNI_STIK,
        :NEW.KOMENTAR, 
        :NEW.NEK_TAG_WO_ID, 
        WO_DESC_,
        WO_KEY,
        Status_,
        REPORT_OPERATION);
        -- Adding WO dates to ??    
        Step_ := 'Insert WO Dates';
        NEK_TO_SCHEDULE.Insert_WO_Dates (
            WO_KEY,                              -- Work_Order_Number_
            :NEW.PREDVIDEN_DATUM_ZAETKA_DN,      -- Predviden_Datum_Zacetka_Dn_
            :NEW.PREDVIDEN_DATUM_ZAKLJUCKA_DN,   -- Predviden_Datum_Zakljucka_Dn_
            :NEW.PREDVIDENI_DAT_ZACETKA_OP,      -- Predvideni_Dat_Zacetka_Op_
            :NEW.PREDVIDENI_DAT_ZAKLJUCKA_OP,    -- Predvideni_Dat_Zakljucka_Op_
            :NEW.DEJANSKI_DAT_ZACETKA_OP,        -- Dejanski_Dat_Zacetka_Op_
            :NEW.DEJANSKI_DAT_ZAKLJUCKA_OP,      -- Dejanski_Dat_Zakljucka_Op_
            :NEW.STEVILKA_PERMITA,               -- Stevilka_Permita_ 
            :NEW.VELJAVEN_OD,                    -- Veljaven_Od_
            :NEW.VELJAVEN_DO,                    -- Veljaven_Do_
            :NEW.ZACETEK_SW,                     -- Zacetek_Sw_
            :NEW.ZAKLJUCEK_SW,                   -- Zakljucek_Sw_ 
            :NEW.P6_TASK_ID,                     -- P6_Task_Id_
            :NEW.SW);                            -- Sw_                             
    END IF;
 
 EXCEPTION
     WHEN OTHERS THEN
       NEK_PACKAGE_TAGG_WOS.NEK_LOG(SQLERRM || ' (' || SQLCODE || '). WO= ' || WO_KEY || ', Step=' || Step_ || '.',
                                    'NEK_TRIG_IS_UPDATE_WO');
       RAISE;
END;
/
