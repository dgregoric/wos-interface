CREATE OR REPLACE VIEW WO_HISTORY_NAME_TEST AS 
     SELECT 
        SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_name') NAME_FIELD, 
        SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_start_date') START_DATE,
        SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_end_date') END_DATE
     FROM DUAL;
     
CREATE OR REPLACE VIEW WO_HISTORY AS 
    SELECT 
        STEVIlKA_DN DN,
        eqtable.SYSTEM_ID System,
        OPREMA Oprema, 
        VELJA_ZA_OPERACIJO St_operacije, 
        OPIS_OPERACIJE Operation_description,
        TEHNOLOG_DN Planer,
        KOMENTAR Zahteva_za_osamitev,
        REPLACE(REPLACE(REPLACE(DELNI_ODMIK, 'N', 'NE'), 'D', 'DA'), 'Y', 'DA') Delna_odstranitev_osamitve, 
        REPLACE(REPLACE(REPLACE(ZACASNI_ODMIK, 'N', 'NE'), 'D', 'DA'), 'Y', 'DA') Zacasna_odstranitev_osamitve,
        SUBSTR(SW,1,INSTR(SW, '-')-1) Project,
        SUBSTR(SW,INSTR(SW, '-')+1, LENGTH(SW)) Sys_win,
        -- '??' Trajanje_operacije, <- trenutno izloceno
        STATUS_DN Status_dn, 
        PREDVIDENI_DAT_ZACETKA_OP Plan_start_operacije,
        PREDVIDENI_DAT_ZAKLJUCKA_OP Plan_finish_operacije,
        DEJANSKI_DAT_ZACETKA_OP Actual_start_operacije,
        DEJANSKI_DAT_ZAKLJUCKA_OP Actual_finish_operacije,
        ODDELEK Department_operacije,
        NEK_PACKAGE_TAGG_WOS.NEK_WOREP_RETURN_WO_TOS(STEVILKA_DN || '-' || VELJA_ZA_OPERACIJO) Stevilka_osamitve, 
        NEK_PACKAGE_TAGG_WOS.NEK_WOREP_WO_TOS_STAT(STEVILKA_DN || '-' || VELJA_ZA_OPERACIJO) Status_osamitve
    FROM ESM_DEV_ITF.NEK_TAG_WO_REPORT
    INNER JOIN EQUIP eqtable
        ON eqtable.equip_operator_id = OPREMA
    WHERE 
        -- WO id
        TRIM(STEVILKA_DN) LIKE NVL(TRIM(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_name')), '%')
        -- System id
        AND TRIM(eqtable.SYSTEM_ID) LIKE NVL(TRIM(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_system_name')), '%')
        -- Oprema id
        AND TRIM(OPREMA) LIKE NVL(TRIM(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_equipment')), '%')        
        -- Project
        AND TRIM(SUBSTR(SW,1,INSTR(SW, '-')-1)) LIKE NVL(TRIM(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_project')), '%')
        -- System window
        AND TRIM(SUBSTR(SW,INSTR(SW, '-')+1, LENGTH(SW))) LIKE NVL(TRIM(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_system_window')), '%')        
        -- User id
        AND TRIM(MATICNA_ST_TEHNOLOG) LIKE NVL(TRIM(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_tehnolog_id')), '%')        
        -- WO status
        AND TRIM(STATUS_DN) LIKE NVL(TRIM(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_status_dn')), '%')        
        -- Department 
        AND TRIM(ODDELEK) LIKE NVL(TRIM(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_department')), '%')        
        -- Commit envelope
        AND COMMIT_DATE >= NVL(TO_DATE(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_start_date_commit') ,'DD-MON-YYYY'), TO_DATE(1, 'J'))
        AND COMMIT_DATE <= NVL(TO_DATE(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_end_date_commit') ,'DD-MON-YYYY'), TO_DATE(9999, 'YYYY'))
        -- Operation date - predviden
        AND COMMIT_DATE >= NVL(TO_DATE(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_start_date_pr_op') ,'DD-MON-YYYY'), TO_DATE(1, 'J'))
        AND COMMIT_DATE <= NVL(TO_DATE(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_end_date_pr_op') ,'DD-MON-YYYY'), TO_DATE(9999, 'YYYY'))
        -- Operation date - dejanski
        AND COMMIT_DATE >= NVL(TO_DATE(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_start_date_de_op') ,'DD-MON-YYYY'), TO_DATE(1, 'J'))
        AND COMMIT_DATE <= NVL(TO_DATE(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_end_date_de_op') ,'DD-MON-YYYY'), TO_DATE(9999, 'YYYY'))

    ORDER BY ST_OPERACIJE ASC;   
   
CREATE OR REPLACE VIEW WO_HISTORY_DOUBLE AS 
    SELECT 
        STEVIlKA_DN DN,
        eqtable.SYSTEM_ID System,
        OPREMA Oprema, 
        VELJA_ZA_OPERACIJO St_operacije, 
        OPIS_OPERACIJE Operation_description,
        TEHNOLOG_DN Planer,
        KOMENTAR Zahteva_za_osamitev,
        REPLACE(REPLACE(REPLACE(DELNI_ODMIK, 'N', 'NE'), 'D', 'DA'), 'Y', 'DA') Delna_odstranitev_osamitve, 
        REPLACE(REPLACE(REPLACE(ZACASNI_ODMIK, 'N', 'NE'), 'D', 'DA'), 'Y', 'DA') Zacasna_odstranitev_osamitve,
        SUBSTR(SW,1,INSTR(SW, '-')-1) Project,
        SUBSTR(SW,INSTR(SW, '-')+1, LENGTH(SW)) Sys_win,
        -- '??' Trajanje_operacije, <- trenutno izloceno
        STATUS_DN Status_dn, 
        PREDVIDENI_DAT_ZACETKA_OP Plan_start_operacije,
        PREDVIDENI_DAT_ZAKLJUCKA_OP Plan_finish_operacije,
        DEJANSKI_DAT_ZACETKA_OP Actual_start_operacije,
        DEJANSKI_DAT_ZAKLJUCKA_OP Actual_finish_operacije,
        ODDELEK Department_operacije,
        worders.ACT_TAGOUT_SECTION_NUMBER Stevilka_osamitve,
        (
            SELECT SECTION_LEVEL_DESC 
            FROM SECTION_VERIF_LEVELS
            WHERE SECTION_VERIF_LEVEL = 
                (SELECT MAX(SECTION_VERIF_LEVEL)
                FROM  ACT_TAGOUT_SECTION_VERIF 
                WHERE ACT_TAGOUT_NUMBER = worders.ACT_TAGOUT_NUMBER
                AND ACT_TAGOUT_SECTION_NUMBER = worders.ACT_TAGOUT_SECTION_NUMBER
                AND TAGOUT_TYPE_ID = 'D1')
            AND TAGOUT_TYPE_ID = 'D1'
        ) Status_osamitve
    FROM ESM_DEV_ITF.NEK_TAG_WO_REPORT
    INNER JOIN EQUIP eqtable
        ON eqtable.equip_operator_id = OPREMA
    LEFT JOIN ACT_TAGOUT_SECTION_WORK_ORDERS worders
        ON WO_KEY = worders.WORK_ORDER_NUMBER AND
           'D1' = worders.TAGOUT_TYPE_ID
    WHERE 
        -- WO id
        TRIM(STEVILKA_DN) LIKE NVL(TRIM(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_name')), '%')
        -- System id
        AND TRIM(eqtable.SYSTEM_ID) LIKE NVL(TRIM(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_system_name')), '%')
        -- Oprema id
        AND TRIM(OPREMA) LIKE NVL(TRIM(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_equipment')), '%')        
        -- Project
        AND TRIM(SUBSTR(SW,1,INSTR(SW, '-')-1)) LIKE NVL(TRIM(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_project')), '%')
        -- System window
        AND TRIM(SUBSTR(SW,INSTR(SW, '-')+1, LENGTH(SW))) LIKE NVL(TRIM(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_system_window')), '%')        
        -- User id
        AND TRIM(MATICNA_ST_TEHNOLOG) LIKE NVL(TRIM(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_tehnolog_id')), '%')        
        -- WO status
        AND TRIM(STATUS_DN) LIKE NVL(TRIM(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_status_dn')), '%')        
        -- Department 
        AND TRIM(ODDELEK) LIKE NVL(TRIM(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_department')), '%')        
        -- Commit envelope
        AND COMMIT_DATE >= NVL(TO_DATE(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_start_date_commit') ,'DD-MON-YYYY'), TO_DATE(1, 'J'))
        AND COMMIT_DATE <= NVL(TO_DATE(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_end_date_commit') ,'DD-MON-YYYY'), TO_DATE(9999, 'YYYY'))
        -- Operation date - predviden
        AND COMMIT_DATE >= NVL(TO_DATE(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_start_date_pr_op') ,'DD-MON-YYYY'), TO_DATE(1, 'J'))
        AND COMMIT_DATE <= NVL(TO_DATE(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_end_date_pr_op') ,'DD-MON-YYYY'), TO_DATE(9999, 'YYYY'))
        -- Operation date - dejanski
        AND COMMIT_DATE >= NVL(TO_DATE(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_start_date_de_op') ,'DD-MON-YYYY'), TO_DATE(1, 'J'))
        AND COMMIT_DATE <= NVL(TO_DATE(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_end_date_de_op') ,'DD-MON-YYYY'), TO_DATE(9999, 'YYYY'))      
        -- TO number
        AND (worders.ACT_TAGOUT_SECTION_NUMBER LIKE NVL(TRIM(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_to_number')), '%') OR 
            NVL(TRIM(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_to_number')), '%') = '%')
        -- TO Status
        AND NVL(TRIM((
            SELECT SECTION_LEVEL_DESC 
            FROM SECTION_VERIF_LEVELS
            WHERE SECTION_VERIF_LEVEL = 
                (SELECT MAX(SECTION_VERIF_LEVEL)
                FROM  ACT_TAGOUT_SECTION_VERIF 
                WHERE ACT_TAGOUT_NUMBER = worders.ACT_TAGOUT_NUMBER
                AND ACT_TAGOUT_SECTION_NUMBER = worders.ACT_TAGOUT_SECTION_NUMBER
                AND TAGOUT_TYPE_ID = 'D1')
            AND TAGOUT_TYPE_ID = 'D1'
            )), '%') LIKE NVL(TRIM(SYS_CONTEXT('CTX_TAGG_WOS_DEV', 'wo_report_to_status')), '%')
 
 ORDER BY ST_OPERACIJE ASC;
   
COMMENT ON TABLE ESM_DEV_ITF.WO_HISTORY 
   IS 'Prikaze vse delovne naloge iz porocilne tabele, ki ustrezajo nastavljenim iskalnim parametrom. Osamitve prikazuje v eni celici z zdruzenimi informacijami.' ;

COMMENT ON TABLE ESM_DEV_ITF.WO_HISTORY_DOUBLE 
   IS 'Prikaze vse delovne naloge iz porocilne tabele, ki ustrezajo nastavljenim iskalnim parametrom. Ce ima delovni nalog vec osamitev jih prikazuje v vecih vrsticah pri cemer se informacije o DN ponavljajo.' ;
























