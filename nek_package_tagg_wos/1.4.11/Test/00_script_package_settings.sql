INSERT INTO 
    ESM_TEST_ITF.NEK_ITF_SETTINGS
   
    (USER_ID, 
    APP_ID, 
    SECTION_ID, 
    SETTING_ID, 
    VALUE_ID, 
    DESCRIPTION)
VALUES   
    ('ADMIN', 
    'WOS_ITF', 
    'REPORTS', 
    'DEF_CONTEXT_NAME', 
    'CTX_TAGG_WOS_DEV', 
    'Default context name for the environment/package used for reporting.');
    
COMMIT;

/*

Context names for different environments

DEV - CTX_TAGG_WOS_DEV
TEST - CTX_TAGG_WOS_TEST
PROD - CTX_TAGG_WOS_PROD

*/